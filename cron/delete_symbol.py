#!/usr/bin/env python3
# Import etoro symbols and download:
# - conids (symbols table)
# - contracts (contracts table)
# - secdefs (contracts table)
# Details:
# - Get etoro symbols from data/etoro_symbols.json
# - Filter out symbols listed in data/etoro_symbols_skip.json
# - Get conids for each of these
# - save to DB table symbols
# Note: takes about 4 minutes
import argparse
import os
import sys
from lib.ib import Ib
from lib.backend import Backend
from lib.api import Api


def main():

  parser = argparse.ArgumentParser(
    description='Get quote for a given symbol from cache/DB'
  )
  parser.add_argument(
    'symbol',
    metavar='SYMBOL',
    type=str,
    help='Symbol, e.g. XP'
  )
  args = parser.parse_args()
  symbol = args.symbol

  print(symbol)
  api = Api()
  api.delete_symbol(symbol)

if __name__ == '__main__':
  main()
