#!/usr/bin/env python3
import concurrent.futures
import json
import os
import urllib3

# Local imports
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.config import Config
from lib.util import get_etoro_symbols
from lib.util import log
import util
from lib.company import Company

# Config
pidfile = '/var/run/download-conids-quotes.pid'

# Settings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Vars
count_done = 0
count_perc = 0
count_total = 0
dir_quote = None
debug = True


def get_quote(symbol):
  global debug
  # Download conid and quote from IB
  # Also, update count percentage
  if debug:
    log('Start %s' % symbol)
  try:
    global count_done
    global count_perc
    global count_total
    global dir_quote
    ret = {}
    c = Company(symbol)
    quote = c.get_quote(period='3d', bar='1d')
    with open(dir_quote + '/' + symbol + '.json', 'w') as f:
      # Save quote to dir
      f.write(json.dumps(quote))
    if debug:
      log('End %s' % symbol)
    return {
      'symbol': symbol,
      'data': quote,
    }
  except Exception as e:
    # Failed to get conid, print the ticker
    raise Exception('symbol: %s: %s' %(symbol, e))


## MAIN
# Read config
cfg = Config()
dir_quote = '../data/quotes'
print('Will save quotes to', dir_quote)


# Get NASDAQ symbols from Etoro
log('Get quotes from already known conids')
symbols = get_etoro_symbols()
count_total = len(symbols)
print(f'Got {count_total} symbols')
quotes = []
for symbol in symbols:
  quote = get_quote(symbol)
  quotes.append(quote)

if len(quotes) == 0:
  log('Could not get quotes')
  exit(1)
print('Got %i quotes' % len(quotes))

log('Finish')
