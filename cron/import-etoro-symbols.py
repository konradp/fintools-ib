#!/usr/bin/env python3
# Download:
# - conids (symbols table)
# - contracts (contracts table)
# - secdefs (contracts table)
# Details:
# - Get etoro symbols from data/etoro_symbols.json
# - Filter out symbols listed in data/etoro_symbols_skip.json
# - Get conids for each of these
# - save to DB table symbols
# Note: takes about 4 minutes
import json
import os
import sys
from lib.api import Api
from lib.ib import Ib
from lib.backend import Backend


def main():
  debug = False
  backend = Backend()
  api = Api()
  symbols = backend.get_etoro_symbols()
  print(f"Got {len(symbols)} symbols")
  skip_symbols = backend.get_etoro_symbols_skip()
  print(f"Got {len(skip_symbols)} skip_symbols")
  if debug:
    symbols = symbols[:5]
  symbols = [
    {
      'symbol': symbol,
      'conid': None,
    }
    for symbol in symbols
    if symbol not in skip_symbols
  ]
  print(f"Reduced to {len(symbols)} symbols")

  # Conid
  print('Get conids')
  conids = api.fetch_conids()
  tmp = {}
  for i in conids:
    tmp[i['ticker']] = i['conid']
  conids = tmp

  for s in symbols:
    if s['symbol'] in tmp:
      s['conid'] = tmp[s['symbol']]
    else:
      print(f"ERROR: conid missing: {s['symbol']}")
      exit(1)
  print(f"Got conids for {len(symbols)} symbols")

  # Insert
  print('Save symbols')
  backend.delete_symbols()
  for s in symbols:
    backend.save_symbol(s['symbol'])
    backend.save_conid(s['symbol'], s['conid'])

  # Contracts
  print('Save contracts')
  symbols = [ s['symbol'] for s in symbols ]
  backend.delete_contracts()
  contracts = api.fetch_contracts(symbols)
  for c in contracts:
    backend.save_contract(
      c['symbol'],
      c['category'],
      c['industry'],
  )

  # Secdefs
  print('Save secdefs')
  symbols = api.get_symbols()
  secdefs = api.fetch_secdefs(symbols)
  for s in secdefs:
    backend.save_secdef(s)

if __name__ == '__main__':
  main()
