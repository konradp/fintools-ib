#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# TODO: Delete first all the snapshots
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api
from lib.backend import Backend


def main():
  api = Api()
  print('Get symbols')
  symbols = api.get_symbols()

  # Filter out skipped
  backend = Backend()
  skip_symbols = backend.get_etoro_symbols_skip()
  symbols = [ symbol for symbol in symbols if symbol not in skip_symbols ]

  print('Fetch snapshots')
  snapshots = api.fetch_snapshots(symbols)
  print('Save snapshots')
  for symbol, price_last in snapshots.items():
    #print(json.dumps(snapshots, indent=2))
    try:
      #print('save snapshot', snapshot)
      api.save_price_last(symbol, price_last)
    except Exception as e:
      print(f'Failed to save snapshot: {e}')
      print(symbol, price_last)

if __name__ == '__main__':
  main()
