#!/usr/bin/env python3
# Taken from https://en.wikipedia.org/wiki/Flask_(web_framework)
# This is the API, currently only has the methods:
# /lt/<price>
# /ltcontracts/<price>
# All of these rely on the data being in the database
import json
import logging
import os
import pandas as pd
import requests
import urllib3
from datetime import datetime
from flask import Flask, request
from flask import send_from_directory
from requests.exceptions import HTTPError
from fintools_ib.lib.api_fetch import ApiFetch
from fintools_ib.lib.api_get import ApiGet

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))


logging.basicConfig(level=logging.DEBUG)
app = Flask(__name__, static_folder='public', static_url_path='')
api = ApiFetch()
api_get = ApiGet()

dir_path = '/opt/fintools-ib/data/quotes'
url_health = 'https://gw:5000/v1/portal/iserver/auth/status'
header = {'Content-Type': 'application/json'}
user_agent_header = {
  "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) \
    AppleWebKit/605.1.15 (KHTML, like Gecko) \
    Chrome/100.0.4896.127 Safari/605.1.15 Firefox/100.0" 
}

@app.route("/")
def index():
  return send_from_directory(app.static_folder, 'index.html')

@app.route('/api/health')
def health():
  print('health')
  msg_err = { 'status': 'ERROR' }
  msg_ok = { 'status': 'SUCCESS' }
  try:
    response = requests.get(url_health, verify=False)
    response.raise_for_status()
  except HTTPError as http_err:
    msg_err['message'] = f'HTTPError: {str(http_err)}'
    msg_err['message2'] = 'This likely means you need to auth'
    return json.dumps(msg_err), 500, header
  except Exception as err:
    msg_err['message'] = str(err)
    return json.dumps(msg_err), 500, header
  else:
    # HTTP 200 OK
    res = json.loads(response.content)
    if res['authenticated'] is True and res['connected'] is True:
      return json.dumps(msg_ok), 200, {'Content-Type': 'application/json'}
    else:
      return json.dumps(msg_err), 500, header


@app.route('/api/fetch_conid/<symbol>')
def fetch_conid(symbol):
  conid = api.fetch_conid(symbol)
  ret = {
    'symbol': symbol,
    'conid': conid,
  }
  return json.dumps(ret), 200, header


@app.route('/api/fetch_scanner_params')
def fetch_scanner_params():
  symbol = 'AAPL'
  params = api.fetch_scanner_params()
  ret = params
  #ret = {
  #  'symbol': symbol,
  #  'conid': conid,
  #}
  return json.dumps(ret), 200, header


@app.route('/api/scanner', methods=['POST'])
def scanner():
  data = request.get_json()
  #return json.dumps(data['location']), 200, header
  try:
    ret = api.fetch_scanner(data['instrument'], data['type'], data['location'], data['filter'])
    return json.dumps(ret), 200, header
  except Exception as e:
    return json.dumps({'message': str(e)}), 500, header


@app.route('/api/fetch_nasdaq100')
def fetch_nasdaq100():
  try:
    #url = "https://api.nasdaq.com/api/quote/list-type/nasdaq100"
    #ret = requests.get(url=url, headers=user_agent_header)
    ret = api.fetch_nasdaq100()
    return json.dumps(ret), 200, header
  except Exception as e:
    return json.dumps({'message': str(e)}), 500, header

@app.route('/api/get_sectors_for_symbols', methods=['POST'])
def get_sectors_for_symbols():
  try:
    #url = "https://api.nasdaq.com/api/quote/list-type/nasdaq100"
    #ret = requests.get(url=url, headers=user_agent_header)
    print('GETTINGGGG')
    symbols = request.get_json()
    print('symbols', symbols)
    ret = api_get.get_sectors_for_symbols(symbols)
    print('ret', ret)
    # TODO: Clean the above and get directly from disk here
    return json.dumps(ret), 200, header
  except Exception as e:
    print(f"ERROR: {str(e)}")
    return json.dumps({'message': str(e)}), 500, header


@app.route('/api/fetch_yahoo/<symbol>/<datefrom>/<dateto>/<interval>')
def fetch_yahoo(symbol, datefrom, dateto, interval):
  # symbol: AAPL
  # datefrom/dateto: 123456 timestamp
  # interval: 1d, 5m
  datefrom = datetime.strptime(datefrom, '%Y-%m-%d')
  datefrom = int(datefrom.timestamp())
  dateto = datetime.strptime(dateto, '%Y-%m-%d')
  dateto = int(dateto.timestamp())
  print('dateFrom', datefrom);
  print('dateTo', dateto);
  url = f'https://query2.finance.yahoo.com/v8/finance/chart/{symbol}?period1={datefrom}&period2={dateto}&interval={interval}'
  print('Fetching', url)
  res = requests.get(url=url, headers=user_agent_header)
  if res.status_code != 200:
    return json.dumps(res.text), res.status_code, header
    print('res', res.text)
  #return res.json(), 200, header
  return json.dumps(res.json()), 200, header

  #conid = api.fetch_conid(symbol)
  #ret = {
  #  'symbol': symbol,
  #  'conid': conid,
  #}
  #return json.dumps(ret), 200, header


@app.route('/api/fetch_chart/<symbol>/<datefrom>/<interval>')
def fetch_chart(symbol, datefrom, interval):
  # symbol: AAPL
  # datefrom/dateto: 123456 timestamp
  # interval: 1d, 5m
  #datefrom = datetime.strptime(datefrom, '%Y-%m-%d')
  #datefrom = int(datefrom.timestamp())
  #dateto = datetime.strptime(dateto, '%Y-%m-%d')
  #dateto = int(dateto.timestamp())
  try:
    conid = api.fetch_conid(symbol)
    print('conid', conid)
    print('datefrom', datefrom)
    # TODO: the period should be calculated, but watch out, skip weekends
    res = api.fetch_history_for_conid(conid=conid, period='4d', bar=interval, start=datefrom+'-00:00:00')
    print(res)
    #return res.json(), 200, header
    return json.dumps(res), 200, header
  except Exception as e:
    print(f"ERROR: {str(e)}")
    return json.dumps({ "message": str(e) }), 500, header

@app.route('/api/fetch_filings/<cik>')
def fetch_filings(cik):
  user_agent_header = {
    "User-Agent": "Konrad Pisarczyk kpisarczyk@gmail.com",
    "Accept-Encoding": "gzip, deflate",
    #"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
    #"Accept-Language": "en-US,en;q=0.9",
  }
  cik = str(cik).zfill(10)
  url = f"https://data.sec.gov/submissions/CIK{cik}.json"
  print(url)
  res = requests.get(url=url, headers=user_agent_header)
  #print(res.text)
  #print(res.url)
  #print(res.status_code)
  ret = res.json()
  ret = ret['filings']['recent']
  df = pd.DataFrame(ret)
  filtered_df = df[(df['filingDate'].str.startswith('2024')) & (df['form'] == '10-Q')]
  filtered = filtered_df.to_dict(orient='list')
  print(filtered)
  #return res.json(), 200, header
  return json.dumps(filtered), 200, header


if __name__ == '__main__':
  app.run(port=80)
