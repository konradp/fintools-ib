window.addEventListener('load', RunMenu);
MENU = {
  home: '/',
  'chart:day': '/chart-day.html',
  'chart:day52w': '/chart-day52w.html',
  'chart:5min': '/chart-5min.html',
  'chart:earnings': '/chart-earnings.html',
  'chart:earnings-nq100': '/chart-earnings-nq100.html',
  'chart:day': '/chart-day.html',
  'ib:scanner': '/ib-scanner.html',
  'ndq100:sector': '/ndq100-sector.html',
  'ndq100:today': '/ndq100-today.html',
}

function RunMenu() {
  let nav = document.querySelector('nav');
  nav.innerHTML = '';
  let l = document.createElement('span');
  l.innerHTML = 'menu:';
  nav.appendChild(l);
  let s = document.createElement('select');
  s.addEventListener('change', goToUrl);
  nav.appendChild(s);
  for (let name in MENU) {
    let o = document.createElement('option');
    o.value = MENU[name];
    o.innerHTML = name;
    let title = document.querySelector('title').innerHTML;
    if (title == name) {
      o.selected = true;
    }
    s.appendChild(o);
  }
}

function goToUrl(e) {
  console.log('goto');
  let url = e.target.value;
  console.log(url);
  window.location.href = e.target.value;
}
