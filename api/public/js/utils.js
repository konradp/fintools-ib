function _get(id) {
  return document.getElementById(id)
}

function calculateMa(data, period) {
  maPoints = [];
  for (datapoint of data) {
    let index = data.findIndex(x => x.date === datapoint.date);
    let maData = data.slice(index - period + 1, index + 1);
    maData = maData.map(x => x.close);
    if (maData.length < period) {
      continue;
    }
    const averageFn = array => array.reduce((a, b) => a + b) / array.length;
    maPoints.push({
      'date': datapoint.date,
      'value': averageFn(maData)
    });
  }
  return maPoints;
}


function calculatePercentagePoints(data) {
  dataPoints = [];
  for (let i in data) {
    if (i == 0) continue;
    let pricePrev = data[i-1].close;
    let priceNow = data[i].close;
    let perc = 100*(priceNow - pricePrev)/pricePrev;
    dataPoints.push({
      "date": data[i].date,
      "value": perc,
    });
  }
  return dataPoints;
}
