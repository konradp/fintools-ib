let DATA = null;
let DATE_FROM = '2024-01-01';
let CIKS = null;
let PLOT = null;
let DATE_TO = new Date().toISOString().split('T')[0];
let settings = {
  show: {
    ma1: false,
    ma2: false,
  },
  vLines: [],
};

window.addEventListener('load', onLoad);

function onLoad() {
  _get('btn-chart').addEventListener('click', onChart);
  _get('ma1').addEventListener('input', drawPlot);
  _get('ma2').addEventListener('input', drawPlot);
  document.querySelectorAll('.ma-checkbox').forEach(e => {
    e.addEventListener('click', onMaClicked);
  });
  _get('lines-draw').addEventListener('click', onLinesDraw);
  fetchCik();
}

function fetchCik() {
  let url = '/nasdaq_to_cik.json';
  fetch(url)
    .then(res => res.json())
    .then(data => {
      // Sort by ticker
      data = Object.fromEntries(
        Object.entries(data).sort(([keyA], [keyB]) => keyA.localeCompare(keyB))
      );
      CIKS = data;
      // Populate symbol drop-down
      let dropDown = _get('symbol');
      for (let s in data) {
        let o = document.createElement('option');
        o.innerHTML = s;
        dropDown.appendChild(o);
      }
    });
}


function fetchFilings() {
  let symbol = _get('symbol').value;
  let cik = CIKS[symbol];
  let url = `/api/fetch_filings/${cik}`
  fetch(url)
    .then(res => res.json())
    .then(data => {
      let linesInput = _get('lines');
      linesInput.value = data.filingDate.join(',');
      onLinesDraw();
      //drawPlot();
  })

}


function onChart() {
  let symbol = _get('symbol').value;
  let dateFrom = DATE_FROM;
  let dateTo = DATE_TO;
  if (dateFrom == '' || dateTo == '') {
    alert('Missing dates');
    return;
  }
  let url = `/api/fetch_yahoo/${symbol}/${dateFrom}/${dateTo}/1d`
  fetch(url)
    .then(res => res.json())
    .then(data => {
      DATA = dataToOhlc(data.chart.result[0].timestamp, data.chart.result[0].indicators.quote[0]);
      let count = _get('count');
      count.innerHTML = DATA.length;
      initPlot();
      drawPlot();
    });
    fetchFilings();
}

function dataToOhlc(timestamps, data) {
  // timestamps: [..., ..., ...]
  // data: { close: [...], high: [...], ... }
  ret = [];
  for (let i in timestamps) {
    let date = new Date(timestamps[i]*1000);
    ret.push({
      date: date.toISOString().split('T')[0],
      open: data.open[i],
      high: data.high[i],
      low: data.low[i],
      close: data.close[i],
    });
  }
  return ret;
}


function initPlot() {
  PLOT = new Plot('chart');
}


function drawPlot() {
  let plot = PLOT;
  plot.canvas.width = 2*(window.innerWidth - 20);
  plot.canvas.height = 2*(window.innerHeight*(60/100));
  // display sizes
  plot.canvas.style.width = window.innerWidth - 20;
  plot.canvas.style.height = window.innerHeight*(60/100);
  plot.setDataOhlc(DATA);
  // Points data %
  dataPoints = calculatePercentagePoints(DATA);
  plot.setDataPoints( [ { "data": dataPoints } ]);
  // lines
  plot.setVlines(settings.vLines);
  // MA
  let maData = [];
  let period1 = _get('ma1').value;
  let period2 = _get('ma2').value;
  if (settings.show.ma1) {
    let ma1 = calculateMa(DATA, period1);
    maData.push({ "label": "MA1", "data": ma1, color: "purple" });
  }
  if (settings.show.ma2) {
    let ma2 = calculateMa(DATA, period2);
    maData.push({ "label": "MA2", "data": ma2, color: "gold" });
  }
  plot.setData(maData);
  //plot.setVlines([ { "date": "2024-08-29" }, ]);
  _get('ma1-val').innerHTML = period1;
  _get('ma2-val').innerHTML = period2;
  plot.draw();
}


function onMaClicked(e) {
  e = e.target;
  settings.show[e.value] = e.checked;
  drawPlot();
}


function onLinesDraw() {
  let e = _get('lines').value;
  if (e == "") return;
  let dates = e.replace(/\s+/g, '').split(',');
  dates = dates.map(date => ({date}));
  settings.vLines = dates;
  drawPlot();
}
