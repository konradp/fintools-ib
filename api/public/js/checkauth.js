checkAuth();
setInterval(checkAuth, 10000); // 10s


function checkAuth() {
  fetch('/api/health')
    .then(response => response.json())
    .then(data => {
      let span = document.getElementById('auth-status');
      span.innerHTML = JSON.stringify(data, null, 2);
    });
}
