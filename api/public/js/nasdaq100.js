DATA = {}
HEADERS = []
window.addEventListener('load', Run);


function Run() {
  fetch('/api/fetch_nasdaq100')
    .then(resp => resp.json())
    .then(data => {
      DATA = data;
      drawTable();
      let symbols = DATA['data']['data']['rows'].map(i => i.symbol);
      fetch('/api/get_sectors_for_symbols', {
        method: 'POST',
        headers: {
          "Content-Type": "application/json" // Inform the server that you're sending JSON
        },
        body: JSON.stringify(symbols),
      })
        .then(resp => resp.json())
        .then(data => {
          addSectors(data);
        })
  }); // fetch
}

function _get(id) {
  return document.getElementById(id);
}
function _new(id) {
  return document.createElement(id);
}
function mapHeader(header) {
  const mapping = {
    'Symbol': 'ticker',
    'Name': 'name',
    'Market Cap': 'mcap',
    'Last Sale': 'price',
    'Net Change': 'chg',
    'Percentage Change': 'perc',
  }
  if (mapping.hasOwnProperty(header)) {
    return mapping[header];
  }
  return header;
}
function cleanValue(value) {
  // Remove a dollar sign, and remove % sign
  // Remove 'Inc. Common Stock'
  value = value.replace(/%$/, "");
  value = value.replace(/^\$/, "");
  value = value.replace(/, Inc\. Common Stock$/, "");
  value = value.replace(/ Common Stock$/, "");
  value = value.replace(/ Capital Stock$/, "");
  value = value.replace(/ Inc\.$/, "");
  value = value.replace(/, Inc\./, "");
  value = value.replace(/ Common Stock New/, "");
  value = value.replace(/ Common Stock/, "");
  value = value.replace(/ Class A/, "");
  value = value.replace(/ Class C/, "");
  value = value.replace(/ Ordinary Shares$/, "");
  value = value.replace(/ Depositary Shares$/, "");
  value = value.replace(/ Registry Shares$/, "");
  value = value.replace(/ Series A/, "");
  value = value.replace(/ Incorporated$/, "");
  value = value.replace(/ Inc\.$/, "");
  value = value.replace(/ Corporation$/, "");
  value = value.replace(/ Corporation $/, "");
  value = value.replace(/ Inc\. $/, "");
  value = value.replace(/ inc\.$/, "");
  value = value.replace(/ Company$/, "");
  value = value.replace(/ plc$/, "");
  return value;
}
function addSectors(sectors) {
  // Append the sectors to the table
  for (let symbol in sectors) {
    let td = _get(`td-sector-${symbol}`);
    td.innerHTML = sectors[symbol];
  }
}


function drawTable() {
  let data = DATA.data.data;
  // headers
  let divHeaders = _get('tbl-head');
  HEADERS = Object.keys(data.headers);
  for (let header of Object.keys(data.headers)) {
    let hName = data.headers[header];
    let th = _new('th');
    if (! ["symbol", "companyName"].includes(header) ) {
      th.className = 'td-right';
    }
    th.innerHTML = mapHeader(hName);
    divHeaders.appendChild(th);
  }
  // Sector header
  thSector = _new('th');
  thSector.innerHTML = 'sector';
  divHeaders.appendChild(thSector);

  let divBody = _get('tbl-body');
  for (let rowData of data.rows) {
    let tr = _new('tr');
    for (let field of HEADERS) {
      let td = _new('td');
      td.innerHTML = cleanValue(rowData[field]);
      if (! ["symbol", "companyName"].includes(field) ) {
        td.className = 'td-right';
      }
      tr.appendChild(td);
    }
    // Sector
    let tdSector = _new('td');
    tdSector.innerHTML = '';
    tdSector.id = `td-sector-${rowData['symbol']}`;
    tr.appendChild(tdSector);
    divBody.appendChild(tr);
  }

  let tbl = _get('tbl-results');
  sorttable.makeSortable(tbl);
}
