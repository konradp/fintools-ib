let DATA_RAW_TICKERS = {};
let DATA_RAW_SECTORS = [];
let DATA = {};
let DATA_CHARTS = {};
window.addEventListener('load', Run);
// TODO: This should show a dashboard for each sector, i.e. how did the sector do overall
// maybe a table
// maybe also show price for each ticker in the sector once it has been selected from the drop-down


function Run() {
  fetch('/api/fetch_nasdaq100')
    .then(resp => resp.json())
    .then(data => {
      data = cleanData(data);
      DATA = data;
      DATA_RAW_TICKERS = data;
      let symbols = DATA['data']['data']['rows'].map(i => i.symbol);
      fetch('/api/get_sectors_for_symbols', {
        method: 'POST',
        headers: {
          "Content-Type": "application/json" // Inform the server that you're sending JSON
        },
        body: JSON.stringify(symbols),
      })
        .then(resp => resp.json())
        .then(data => {
          DATA_RAW_SECTORS = data;
          combineData();
          console.log('data', DATA);
          drawPage();
        })
  }); // fetch
  // Events
  _get('draw').addEventListener('click', onDrawPressed);
}

function _get(id) {
  return document.getElementById(id);
}
function _new(id, innerHTML = null) {
  let el = document.createElement(id);
  if (innerHTML != null) {
    el.innerHTML = innerHTML;
  }
  //return document.createElement(id);
  return el;
}


function cleanData(data) {
  data['data']['data']['rows'] = data['data']['data']['rows'].map(item => 
    Object.fromEntries(
      Object.entries(item).map(([key, val]) => [key, cleanValue(key, val)])
    )
  );
  return data;
}
function cleanValue(key, value) {
  // Remove a dollar sign, and remove % sign
  // Remove 'Inc. Common Stock'
  value = value.replace(/%$/, "");
  value = value.replace(/^\$/, "");
  value = value.replace(/, Inc\. Common Stock$/, "");
  value = value.replace(/ Common Stock$/, "");
  value = value.replace(/ Capital Stock$/, "");
  value = value.replace(/ Inc\.$/, "");
  value = value.replace(/, Inc\./, "");
  value = value.replace(/ Common Stock New/, "");
  value = value.replace(/ Common Stock/, "");
  value = value.replace(/ Class A/, "");
  value = value.replace(/ Class C/, "");
  value = value.replace(/ Ordinary Shares$/, "");
  value = value.replace(/ Depositary Shares$/, "");
  value = value.replace(/ Registry Shares$/, "");
  value = value.replace(/ Series A/, "");
  value = value.replace(/ Incorporated$/, "");
  value = value.replace(/ Inc\.$/, "");
  value = value.replace(/ Corporation$/, "");
  value = value.replace(/ Corporation $/, "");
  value = value.replace(/ Inc\. $/, "");
  value = value.replace(/ inc\.$/, "");
  value = value.replace(/ Company$/, "");
  value = value.replace(/ plc$/, "");
  if (key == 'percentageChange' && value == 'UNCH') {
    console.log('Changing UNCH to 0');
    value = '0';
  }
  return value;
}
function cleanPlus(value) {
  // Remove a plus sign, e.g. +1 to 1
  value = value.replace(/^\+/, "");
}
function combineData() {
  let tickers = DATA_RAW_TICKERS;
  let sectors = DATA_RAW_SECTORS;
  let combined = tickers['data']['data']['rows'].map(item => ({
    ...item,
    sector: sectors[item.symbol]
  }));
  DATA = DATA_RAW_TICKERS;
  DATA['data']['data']['rows'] = combined;
}
function getMedian(arr) {
  if (arr.length === 0) throw new Error("Array is empty");
  arr.sort((a, b) => a - b);
  const n = arr.length;
  if (n % 2 === 1) {
    return arr[Math.floor(n / 2)];
  } else {
    const mid1 = arr[n / 2 - 1];
    const mid2 = arr[n / 2];
    return (mid1 + mid2) / 2;
  }
}
function getAverage(arr) {
  if (arr.length === 0) throw new Error("Array is empty");
  return arr.reduce((sum, num) => sum + num, 0) / arr.length;
}
function dataToOhlc(timestamps, data) {
  // timestamps: [..., ..., ...]
  // data: { close: [...], high: [...], ... }
  ret = [];
  for (let i in timestamps) {
    let date = new Date(timestamps[i]*1000);
    ret.push({
      date: date.toISOString().split('T')[0],
      open: data.open[i],
      high: data.high[i],
      low: data.low[i],
      close: data.close[i],
      volume: data.volume[i],
    });
  }
  return ret;
}


function drawPage() {
  // Draw drop-down
  let select = _get('select-sectors');
  let sectors = [...new Set(Object.values(DATA_RAW_SECTORS))].sort();
  for (let sector of sectors) {
    let opt = _new('option');
    opt.innerHTML = sector;
    select.appendChild(opt);
  }

  // Overall aggregates
  // Perc change: median
  let data = DATA['data']['data']['rows'];
  let percentages = data.map(i => parseFloat(i.percentageChange));
  let median = getMedian(percentages);
  _get('perc-median').innerHTML = median;
  // Perc change: average
  let avg = getAverage(percentages);
  _get('perc-average').innerHTML = avg.toFixed(2);

  // Per sector aggregates
  let perSector = data.reduce((acc, curr) => {
    if (!acc[curr.sector]) {
        acc[curr.sector] = [];
    }
    acc[curr.sector].push(parseFloat(curr.percentageChange));
    return acc;
  }, {});
  let tbody = _get('per-sector');
  for (let sector in perSector) {
    let median = getMedian(perSector[sector]).toFixed(2);
    let avg = getAverage(perSector[sector]).toFixed(2);
    let tdMed = _new('td', median);
    let tdAvg = _new('td', avg);
    let tr = _new('tr');
    tr.appendChild(_new('td', sector));
    tr.appendChild(_new('td', median));
    tr.appendChild(_new('td', avg));
    tbody.appendChild(tr);
  }
}


function onDrawPressed() {
  // Draw a chart for each ticker in a given sector
  let sector = _get('select-sectors').value;
  let data = DATA['data']['data']['rows'];
  let perSector = data.reduce((acc, curr) => {
    if (!acc[curr.sector]) {
        acc[curr.sector] = [];
    }
    acc[curr.sector].push(curr.symbol);
    return acc;
  }, {});
  let tickers = perSector[sector].sort();
  let div = _get('charts');
  div.innerHTML = '';
  dateFrom = new Date(); // three months ago
  dateFrom.setMonth(dateFrom.getMonth() - 3);
  dateFrom = dateFrom.toISOString().split('T')[0];
  let dateTo = new Date(); // today
  dateTo.setDate(dateTo.getDate() + 1);
  dateTo = dateTo.toISOString().split('T')[0];
  for (ticker of tickers) {
    let title = _new('span', ticker);
    div.appendChild(title);
    div.appendChild(_new('br'));
    let c = _new('canvas');
    c.id = ticker;
    div.appendChild(c);
    div.appendChild(_new('br'));

    // Fetch chart
    let url = `/api/fetch_yahoo/${ticker}/${dateFrom}/${dateTo}/1d`
    fetch(url)
      .then(res => res.json())
      .then(d => {
        let symbol = d['chart']['result'][0]['meta']['symbol'];
        DATA_CHARTS[symbol] = dataToOhlc(d.chart.result[0].timestamp, d.chart.result[0].indicators.quote[0]);
        let p = new Plot(symbol, {
          settings: {
            showCrosshair: true,
            hoverLabels: true,
            showVolume: true,
          }
        });
        p.setDataOhlc(DATA_CHARTS[symbol]);
        p.draw();
      });
  } // for tickers
} // onDrawPressed


