let DATA = null;
let DATE_FROM = '2024-01-01';
let DATE_TO = '2024-12-31';
let CIKS = null;
let LINES = [];
let PLOT = null;
let SYMBOL = '^NDX';
let filings = {};

window.addEventListener('load', onLoad);

function onLoad() {
  fetchCik();
  fetchChart();
}

function fetchCik() {
  let url = '/nasdaq_to_cik.json';
  fetch(url)
    .then(res => res.json())
    .then(data => {
      // Sort by ticker
      data = Object.fromEntries(
        Object.entries(data).sort(([keyA], [keyB]) => keyA.localeCompare(keyB))
      );
      CIKS = data;
      fetchFilings();
    });
}


function fetchFilings() {
  for (let symbol in CIKS) {
    let cik = CIKS[symbol];
    let url = `/api/fetch_filings/${cik}`
    fetch(url)
      .then(res => res.json())
      .then(data => {
        filings[symbol] = data.filingDate;
        let dates = data.filingDate;
        dates = dates.map(date => ({date}));
        LINES = LINES.concat(dates);
        initPlot();
        drawPlot();
    })
  }
  return;
}


function fetchChart() {
  let url = `/api/fetch_yahoo/${SYMBOL}/${DATE_FROM}/${DATE_TO}/1d`
  fetch(url)
    .then(res => res.json())
    .then(data => {
      DATA = dataToOhlc(data.chart.result[0].timestamp, data.chart.result[0].indicators.quote[0]);
      drawPlot();
    });
}

function dataToOhlc(timestamps, data) {
  // timestamps: [..., ..., ...]
  // data: { close: [...], high: [...], ... }
  ret = [];
  for (let i in timestamps) {
    let date = new Date(timestamps[i]*1000);
    ret.push({
      date: date.toISOString().split('T')[0],
      open: data.open[i],
      high: data.high[i],
      low: data.low[i],
      close: data.close[i],
    });
  }
  return ret;
}


function initPlot() {
  PLOT = new Plot('chart-nasdaq100');
}


function drawPlot() {
  let plot = PLOT;
  plot.canvas.width = 2*(window.innerWidth - 20);
  plot.canvas.height = 2*(window.innerHeight*(50/100));
  // display sizes
  plot.canvas.style.width = window.innerWidth - 20;
  plot.canvas.style.height = window.innerHeight*(50/100);
  plot.setDataOhlc(DATA);
  // lines
  plot.setVlines(LINES);
  plot.draw();
}

