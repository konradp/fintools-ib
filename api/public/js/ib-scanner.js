DATA = {};
SELECTED = {
  instrument: { name: 'US Stocks', code: 'STK' },
  location: { name: 'US Stocks', code: 'STK.US.MAJOR' },
  scan: {
    code: 'SCAN_lipperMktCapAvgLatest_DESC',
    name: 'Market Capitalisation - Avg. Latest Desc',
  },
  filters: [
    { name: 'Market Capitalisation - Avg. Latest Above', code: 'lipperMktCapAvgLatestAbove' }
  ],
};
FILTERS = [{
  name: 'Market Capitalisation - Avg. Latest Above',
  code: 'lipperMktCapAvgLatestAbove',
  value: 1*100*100*100*100*100*100,
}]
RESULT_FIELDS = [
  'symbol',
  'company_name',
  //'con_id',
  //'conidex',
  //'contract_description_1',
  'listing_exchange',
  //'sec_type',
  //'server_id',
];
window.addEventListener('load', Run);


function Run() {
  fetch('/api/fetch_scanner_params')
    .then(resp => resp.json())
    .then(data => {
      DATA = data;
      populateSettings();
      displaySelected();
      drawFiltersAdded();
  }); // fetch
  // Button events
  _get('btn-filter-add').addEventListener('click', onAddFilter);
  _get('btn-scan').addEventListener('click', onScanRun);
  _get('scan-search').addEventListener('input', onScanSearchInput);
  _get('filter-search').addEventListener('input', onFilterSearchInput);
}

function _get(id) {
  return document.getElementById(id);
}


function getSelectedRadioValue(option) {
  // Using querySelector to get the selected radio button
  const selectedRadio = document.querySelector(`input[name="${option}"]:checked`);
  if (selectedRadio) {
    return selectedRadio.value; // Return the value of the selected radio button
  } else {
    return null; // No radio button selected
  }
}

function populateSettings() {
  // instrument: instrument_list
  let div = _get('instrument');
  div.innerHTML = '';
  for (let itemType of DATA['instrument_list']) {
    let displayName = itemType['display_name'];
    let code = itemType['type'];
    let label = document.createElement('label');
    label.className = 'row-selection';
    let input = document.createElement('input');
    input.setAttribute('type', 'radio');
    input.setAttribute('name', 'instrument');
    input.setAttribute('value', code);
    input.setAttribute('displayname', displayName);
    label.appendChild(input);
    let span = document.createElement('span');
    span.innerHTML = displayName;
    label.appendChild(span);
    div.appendChild(label);
    // Default
    if (code == 'STK') {
      input.checked = true;
      SELECTED['instrument'] = {
        name: displayName,
        code: code,
      };
    }
    // event
    input.addEventListener('click', (e) => {
      let code = e.target.value;
      let name = e.target.getAttribute('displayname');
      SELECTED['instrument'] = { code: code, name: name };
      displaySelected();
    });
  }

  // location: location_tree
  div = _get('location');
  div.innerHTML = '';
  for (let itemType of DATA['location_tree']) {
    let displayName = itemType['display_name'];
    for (let loc of itemType['locations']) {
      let displayNameFine = loc['display_name'];
      let disp = displayName + ': ' + displayNameFine;
      let code = loc['type'];
      let label = document.createElement('label');
      label.className = 'row-selection';
      let input = document.createElement('input');
      input.setAttribute('displayname', disp);
      input.setAttribute('type', 'radio');
      input.setAttribute('name', 'location');
      input.setAttribute('value', code);
      label.appendChild(input);
      let span = document.createElement('span');
      span.innerHTML = disp;
      label.appendChild(span);
      div.appendChild(label);
      // Default
      if (code == 'STK.US.MAJOR') {
        input.checked = true;
        SELECTED['location'] = {
          'name': disp,
          'code': code,
        };
      }
      // event
      input.addEventListener('click', (e) => {
        let code = e.target.value;
        let name = e.target.getAttribute('displayname');
        SELECTED['location'] = { code: code, name: name };
        displaySelected();
      });
    } // for location
  }

  // type: scan_type_list
  div = _get('type');
  div.innerHTML = '';
  for (let itemType of DATA['scan_type_list']) {
    let displayName = itemType['display_name'];
    let code = itemType['code'];
    let label = document.createElement('label');
    label.className = 'row-selection scan-type-item';
    let input = document.createElement('input');
    input.setAttribute('type', 'radio');
    input.setAttribute('name', 'type');
    input.setAttribute('value', code);
    input.setAttribute('displayname', displayName);
    label.appendChild(input);
    let span = document.createElement('span');
    span.innerHTML = displayName;
    label.appendChild(span);
    div.appendChild(label);
    // Default
    if (code == 'SCAN_lipperMktCapAvgLatest_DESC') {
      input.checked = true;
      SELECTED['scan'] = {
        'name': displayName,
        'code': code,
      };
    }
    // event
    input.addEventListener('click', (e) => {
      let code = e.target.value;
      let name = e.target.getAttribute('displayname');
      SELECTED['scan'] = { code: code, name: name };
      displaySelected();
    });
  }

  // filter: filter_list
  div = _get('filter');
  div.innerHTML = '';
  for (let itemType of DATA['filter_list']) {
    let displayName = itemType['display_name'];
    let code = itemType['code'];
    let label = document.createElement('label');
    label.className = 'row-selection filter-item';
    let input = document.createElement('input');
    input.setAttribute('type', 'radio');
    input.setAttribute('name', 'filter');
    input.setAttribute('value', code);
    input.setAttribute('displayname', displayName);
    label.appendChild(input);
    let span = document.createElement('span');
    span.innerHTML = displayName;
    label.appendChild(span);
    div.appendChild(label);
    // Default
    if (code == 'afterHoursChangeAbove') {
      input.checked = true;
      SELECTED['filter'] = { name: displayName, code: code };
    }
    // event
    input.addEventListener('click', (e) => {
      let code = e.target.value;
      let name = e.target.getAttribute('displayname');
      SELECTED['filter'] = { code: code, name: name };
      displaySelected();
    });
  }
}


function displaySelected() {
  let items = [ 'instrument', 'location', 'scan', 'filter' ];
  for (let item of items) {
    let span = _get(`${item}-selected`);
    let selected = SELECTED[item].name;
    span.innerHTML = selected;
  }
}


function onAddFilter() {
  let selected = SELECTED['filter'];
  let name = selected['name'];
  let code = selected['code'];
  let value = _get('filter-value').value;
  // Add filter to list of filters
  // The code of the filter must be unique
  if (FILTERS.map(i => i['code']).includes(code)) {
    return;
  }
  // TODO: Check if already exists
  // if map FILTERS -> code contains code, return
  FILTERS.push({ name: name, code: code, value: value });
  drawFiltersAdded();
}

function drawFiltersAdded() {
  let div = _get('filters-added');
  div.innerHTML = '';
  for (let filter of FILTERS) {
    let name = filter['name'];
    let code = filter['code'];
    let value = filter['value'];
    let c = document.createElement('div');
    c.className = 'filter-row';
    let divName = document.createElement('span');
    divName.innerHTML = name + ': ' + value;
    c.appendChild(divName);
    let divBtn = document.createElement('button');
    divBtn.className = 'filter-btn';
    divBtn.innerHTML = '-';
    divBtn.setAttribute('code', code);
    c.appendChild(divBtn);
    div.appendChild(c);
    divBtn.addEventListener('click', deleteFilter);
  }
}


function deleteFilter(e) {
  let code = e.target.getAttribute('code');
  FILTERS = FILTERS.filter(i => i.code !== code);
  drawFiltersAdded();
}


function onScanRun() {
  let payload = {
    instrument: SELECTED.instrument.code,
    type: SELECTED.scan.code,
    location: SELECTED.location.code,
    filter: FILTERS.map(i => ({ code: i.code, value: i.value })),
  };
  fetch("/api/scanner", {
    method: "post",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    //make sure to serialize your JSON body
    body: JSON.stringify(payload)
  })
    .then(resp => resp.json())
    .then(data => {
      console.log('response', data);
      drawResultsTable(data);
  });
}

function onScanSearchInput(e) {
  //console.log('input', e);
  let search = e.target.value.toLowerCase();
  let items = document.querySelectorAll('.scan-type-item');
  for (let row of items) {
    let name = row.firstElementChild.getAttribute('displayname');
    if (name.toLowerCase().includes(search)) {
      row.style.display = 'block';
    } else {
      row.style.display = 'none';
    }
  }
}


function onFilterSearchInput(e) {
  //console.log('input', e);
  let search = e.target.value.toLowerCase();
  let items = document.querySelectorAll('.filter-item');
  for (let row of items) {
    let name = row.firstElementChild.getAttribute('displayname');
    if (name.toLowerCase().includes(search)) {
      row.style.display = 'block';
    } else {
      row.style.display = 'none';
    }
  }
}

function drawResultsTable(data) {
  let thead = _get('result-header');
  thead.innerHTML = '';
  for (let name of RESULT_FIELDS) {
    let th = document.createElement('th');
    th.innerHTML = name;
    thead.appendChild(th);
  }
  let contracts = data['contracts'];
  _get('result-count').innerHTML = contracts.length;
  //let div = _get('result');
  //div.innerHTML = '';
  //div.innerHTML = JSON.stringify(data, null, 2);
  let tbl = _get('result-table');
  for (let contract of contracts) {
    let tr = document.createElement('tr');
    for (let name of RESULT_FIELDS) {
      let td = document.createElement('td');
      td.innerHTML = contract[name];
      tr.appendChild(td);
    }
    tbl.appendChild(tr);
  }
  //sorttable.makeSortable(tbl);
}
