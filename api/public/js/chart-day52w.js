let DATA = null;
let DATA_FULL = null;
let DATA_MIN_MAX = null;
let PLOT = null;
let settings = {
  show: {
    ma1: false,
    ma2: false,
  },
  vLines: [],
};

window.addEventListener('load', onLoad);

function onLoad() {
  _get('btn-chart').addEventListener('click', onChart);
  // Dates
  let today = new Date();
  today.setDate(today.getDate() + 1);
  _get('date-to').value = today.toISOString().split('T')[0];
  _get('date-from').value = minusYear(today);
  // Events
  _get('ma1').addEventListener('input', drawPlot);
  _get('ma2').addEventListener('input', drawPlot);
  document.querySelectorAll('.ma-checkbox').forEach(e => {
    e.addEventListener('click', onMaClicked);
  });
  _get('lines-draw').addEventListener('click', onLinesDraw);
}


function onChart() {
  let symbol = _get('symbol').value;
  let dateFrom = _get('date-from').value;
  dateFrom = minusYear(dateFrom);
  let dateTo = _get('date-to').value;
  if (dateFrom == '' || dateTo == '') {
    alert('Missing dates');
    return;
  }
  let url = `/api/fetch_yahoo/${symbol}/${dateFrom}/${dateTo}/1d`
  fetch(url)
    .then(res => res.json())
    .then(data => {
      DATA_FULL = dataToOhlc(
        data.chart.result[0].timestamp,
        data.chart.result[0].indicators.quote[0]
      );
      let yearAgo = _get('date-from').value;
      DATA = DATA_FULL.filter(i => i.date >= yearAgo);
      let dataSpan = _get('data');
      dataSpan.innerHTML = JSON.stringify(DATA, null, 2);
      let count = _get('count');
      count.innerHTML = DATA.length;
      initPlot();
      drawPlot();
    });
}


function dataToOhlc(timestamps, data) {
  // timestamps: [..., ..., ...]
  // data: { close: [...], high: [...], ... }
  ret = [];
  console.log(data);
  for (let i in timestamps) {
    let date = new Date(timestamps[i]*1000);
    ret.push({
      date: date.toISOString().split('T')[0],
      open: data.open[i],
      high: data.high[i],
      low: data.low[i],
      close: data.close[i],
      volume: data.volume[i],
    });
  }
  return ret;
}

function initPlot() {
  PLOT = new Plot('chart', {
    settings: {
      showCrosshair: true,
      hoverLabels: true,
    }
  });
  //PLOT.setOnMouseMove(onMouseMove);
  calculate52Wk();
}


function onMouseMove(e) {
  // Draw 52w range slider
  let c = this.c;
  let h = 30;
  let w = 400; // slider width
  let i = this.mouseToIndex(e.clientX);
  //let dateLastYear = minusYear(DATA[i].date);
  //let points = DATA_FULL.filter(p =>
  //  p.date >= dateLastYear &&
  //  p.date <= DATA[i].date);
  //let min = points.map(p => p.low);
  //let max = points.map(p => p.high);
  //min = Math.min(...min).toFixed(2);
  //max = Math.max(...max).toFixed(2);
  //console.log(DATA_MIN_MAX);
  let min = DATA_MIN_MAX[0].data[i].value.toFixed(2);
  let max = DATA_MIN_MAX[1].data[i].value.toFixed(2);
  // Horiz line
  c.save();
  c.lineWidth = 2;
  c.translate(
    this.canvas.width - w - 20,
    this.canvas.height - h*4,
  );
  c.moveTo(0, 0);
  c.lineTo(w, 0);
  c.stroke();
  // Vert line
  let n = transform(
    DATA[i].close, parseFloat(min), parseFloat(max),
    0, w,
  );
  c.moveTo(n, -h/4);
  c.lineTo(n, h/4);
  c.stroke();
  // Text
  c.fillText([
    min, DATA[i].close.toFixed(2), max
    ].join(' '),
    0, +2*h,
  );
  c.restore();
}

function calculate52Wk() {
  const averageFn = array => array.reduce((a, b) => a + b) / array.length;
  console.log('calculate52Wk');
  let mins = { label: "MINS", data: [], color: 'red' };
  let maxs = { label: "MAXS", data: [], color: 'blue' };
  let minsAvg = { label: "MINS_AVG", data: [], color: 'orange' };
  let maxsAvg = { label: "MAXS_AVG", data: [], color: 'silver' };
  for (let i of DATA) {
    let dateLastYear = minusYear(i.date);
    let points = DATA_FULL.filter(p =>
      p.date >= dateLastYear &&
      p.date <= i.date);
    let minSegment = points.map(p => p.low);
    let maxSegment = points.map(p => p.high);
    mins.data.push({ date: i.date, value: Math.min(...minSegment) });
    maxs.data.push({ date: i.date, value: Math.max(...maxSegment) });
    minsAvg.data.push({ date: i.date, value: averageFn(minSegment) });
    maxsAvg.data.push({ date: i.date, value: averageFn(maxSegment) });
  }
  DATA_MIN_MAX = [ mins, maxs, minsAvg, maxsAvg ];
}

function drawPlot() {
  let plot = PLOT;
  plot.canvas.width = 2*(window.innerWidth - 20);
  plot.canvas.height = 2*(window.innerHeight*(60/100));
  // display sizes
  plot.canvas.style.width = window.innerWidth - 20;
  plot.canvas.style.height = window.innerHeight*(60/100);
  plot.setDataOhlc(DATA);
  // Points data %
  dataPoints = calculatePercentagePoints(DATA);
  plot.setDataPoints( [ { "data": dataPoints } ]);
  // lines
  plot.setVlines(settings.vLines);
  // MA
  let maData = [];
  let period1 = _get('ma1').value;
  let period2 = _get('ma2').value;
  if (settings.show.ma1) {
    let ma1 = calculateMa(DATA, period1);
    maData.push({ "label": "MA1", "data": ma1, color: "gold" });
  }
  if (settings.show.ma2) {
    let ma2 = calculateMa(DATA, period2);
    maData.push({ "label": "MA2", "data": ma2, color: "purple" });
  }
  maData.push(DATA_MIN_MAX[0]);
  maData.push(DATA_MIN_MAX[1]);
  maData.push(DATA_MIN_MAX[2]);
  maData.push(DATA_MIN_MAX[3]);
  plot.setData(maData);
  //plot.setVlines([ { "date": "2024-08-29" }, ]);
  _get('ma1-val').innerHTML = period1;
  _get('ma2-val').innerHTML = period2;
  plot.draw();
}


function onMaClicked(e) {
  e = e.target;
  settings.show[e.value] = e.checked;
  drawPlot();
}


function onLinesDraw(e) {
  e = _get('lines').value;
  if (e == "") return;
  let dates = e.replace(/\s+/g, '').split(',');
  dates = dates.map(date => ({date}));
  settings.vLines = dates;
  drawPlot();
}

function minusYear(date) {
  date = new Date(date);
  date.setFullYear(date.getFullYear() - 1);
  return date.toISOString().split("T")[0];
}

function transform(x, a, b, c, d) {
  return c + ((d-c)/(b-a))*(x-a);
}
