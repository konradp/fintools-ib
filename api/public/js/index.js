window.addEventListener('load', RunIndex);

function RunIndex() {
  console.log('test');
  _get('btn-conid').addEventListener('click', onFetchConid);
}

function onFetchConid() {
  let symbol = _get('symbol').value;
  let url = `/api/fetch_conid/${symbol}`;
  fetch(url)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      let conidSpan = _get('conid-res');
      conidSpan.innerHTML = data.conid;
    });
}
