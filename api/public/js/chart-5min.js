let DATA = null;
let PLOT = null;

window.addEventListener('load', RunIndex);


function RunIndex() {
  _get('btn-chart').addEventListener('click', onChart);
  // Default dates
  // Date to
  let dateTo = _get('date-to');
  let today = new Date();
  //dateTo.value = '2024-10-14';
  dateTo.value = today.toISOString().split('T')[0];
  // Date from
  today.setDate(today.getDate() - 5);
  let dateFrom = _get('date-from');
  //dateFrom.value = '2024-10-11';
  dateFrom.value = today.toISOString().split('T')[0];
  _get('ma1').addEventListener('input', drawPlot);
  _get('ma2').addEventListener('input', drawPlot);
}


function onChart() {
  let symbol = _get('symbol').value;
  let dateFrom = _get('date-from').value;
  let dateTo = _get('date-to').value;
  if (dateFrom == '' || dateTo == '') {
    alert('Missing dates');
    return;
  }
  let period = _get('period').value;
  let source = _get('source').value;
  let url = null;
  if (source == 'yahoo') {
    url = `/api/fetch_yahoo/${symbol}/${dateFrom}/${dateTo}/${periodToYahoo(period)}`;
  } else if (source == 'ib') {
    dateFrom = dateFrom.replace(/-/g, '');
    url = `/api/fetch_chart/${symbol}/${dateFrom}/${period}`;
  } else {
    alert('unsupported source', source);
    return;
  }
  fetch(url)
    .then(res => {
      if (!res.ok) {
        // Extract JSON and handle the error
        return res.json()
          .then(errorJson => {
            console.error(`ERROR: ${res.status} : ${res.statusText} : ${JSON.stringify(errorJson)}`);
            alert(`ERROR: ${res.status} : ${res.statusText} : ${JSON.stringify(errorJson)}`);
            //throw new Error(`HTTP Error: ${res.status} - ${res.statusText}`); // Prevent further execution
          })
          .catch(() => {
            console.error(`ERROR: ${res.status} : ${res.statusText} : Unable to parse JSON`);
            throw new Error(`HTTP Error: ${res.status} - ${res.statusText}`);
          });
      }
      return res; // Pass the response to the next .then() if ok
    })
    .then(res => res.json())
    .then(data => {
      let source = _get('source').value;
      if (source == 'yahoo') {
        DATA = dataToOhlc(
          data.chart.result[0].timestamp,
          data.chart.result[0].indicators.quote[0]
        );
      } else if (source == 'ib') {
        DATA = dataToOhlc(null, data.data);
      } else {
        alert('error');
      }
      //DATA = dataToOhlc(data.chart.result[0].timestamp, data.chart.result[0].indicators.quote[0]);
      let count = _get('count');
      count.innerHTML = DATA.length;
      initPlot();
      drawPlot();
    });
}


function dataToOhlc(timestamps, data) {
  // timestamps: [..., ..., ...]
  // data: { close: [...], high: [...], ... }
  ret = [];
  let source = _get('source').value;
  if (source == 'yahoo') {
    for (let i in timestamps) {
      //let date = new Date(timestamps[i]*1000);
      ret.push({
        //date: date.toISOString().split('T')[0],
        date: timestamps[i],
        open: data.open[i],
        high: data.high[i],
        low: data.low[i],
        close: data.close[i],
      });
    }
    return ret;
  } else if (source == 'ib') {
    // convert 'o' to 'open'
    for (let i in data) {
      //let date = new Date(timestamps[i]*1000);
      ret.push({
        //date: date.toISOString().split('T')[0],
        date: data[i].t,
        open: data[i].o,
        high: data[i].h,
        low: data[i].l,
        close: data[i].c,
      });
    }
    return ret;
  }
}


function initPlot() {
  PLOT = new Plot('chart', {
    settings: {
      showCrosshair: true,
      hoverLabels: true,
    },
  });
}


function drawPlot() {
  let plot = PLOT;
  plot.canvas.width = 2*(window.innerWidth - 20);
  plot.canvas.height = 2*(window.innerHeight*(60/100));
  // display sizes
  plot.canvas.style.width = window.innerWidth - 20;
  plot.canvas.style.height = window.innerHeight*(60/100);
  //plot.c.scale(2, 1);
  plot.setDataOhlc(DATA);
  // MA
  let period1 = _get('ma1').value;
  let period2 = _get('ma2').value;
  let ma1 = calculateMa(DATA, period1);
  let ma2 = calculateMa(DATA, period2);
  plot.setVlines(getDayLines(DATA));
  plot.setData([
    { "label": "MA1", "data": ma1, color: "purple" },
    { "label": "MA2", "data": ma2, color: "gold" },
  ]);
  _get('ma1-val').innerHTML = period1;
  _get('ma2-val').innerHTML = period2;
  plot.draw();
}


function getDayLines(data) {
  // get timestamps which highlight a new day
  // so that there is a vertical line on each
  // new day
  lines = [];
  let lastDay = 0;
  let source = _get('source').value;
  for (let i in data) {
    let date = new Date(data[i].date*1000);
    if (source == 'ib') {
      date = new Date(data[i].date);
    }
    let d = date.getDate();
    if (i == 0) {
      lastDay = d;
    }
    if (d != lastDay) {
      lastDay = d;
      lines.push({'date': data[i].date});
    }
  }
  return lines;
}


function periodToYahoo(period) {
  if (period == "1min") {
    return "1m";
  }
  if (period == "5min") {
    return "5m";
  }
}
