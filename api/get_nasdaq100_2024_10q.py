#!/usr/bin/env python3
import json
import requests


with open('../data/nasdaq_to_cik.json', 'r') as f:
  data = json.load(f)
  print(data)

# Get filings
data = {
  'AAPL': 320193,
  'NVDA': 1045810,
}
for symbol, cik in data.items():
  print(symbol, cik)
  url = f"http://localhost:8000/api/fetch_filings/{cik}"
  res = requests.get(url=url)
  print(res.json())
