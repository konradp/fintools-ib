#!/usr/bin/env python3
# KEEP THIS
# 101 ndq100 stocks take 67MB for data from 1990-01-01 to 2024-12-31
import argparse
from datetime import datetime
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from fintools_ib.lib.api_fetch import ApiFetch
from backtest.lib.util import _read_file

FILE_NDQ100 = "../data/nasdaq100.json"
FILE_DATES = "./data/dates.json"
INTERVAL = "1d"
DATE_FROM = "1990-01-01"
DATE_TO = "2024-12-31"


def get_chart(symbol):
  datefrom = datetime.strptime(DATE_FROM, '%Y-%m-%d')
  datefrom = int(datefrom.timestamp())
  dateto = datetime.strptime(DATE_TO, '%Y-%m-%d')
  dateto = int(dateto.timestamp())
  interval="1d"
  try:
    api = ApiFetch()
    res = api.fetch_yh_chart(symbol, datefrom, dateto, interval=interval)
    return res
  except Exception as e:
    print(f"Could not get conid: {e}")
    exit(1)


def main():
  with open(FILE_NDQ100, 'r') as f:
    data = json.load(f)['data']['data']['rows']
  symbols = sorted([ i['symbol'] for i in data ])
  print('symbols', len(symbols), symbols)
  #for symbol in symbols:
  #  ## Download chart for each symbol, and save
  #  #print('symbol', symbol)
  #  #chart = get_chart(symbol)
  #  #with open(f"./data/{symbol}.json", 'w') as f:
  #  #  f.write(json.dumps(chart))
  # Get dates
  #dates = []
  #for symbol in symbols:
  #  print('symbol', symbol)
  #  data = _read_file(symbol)
  #  data = data['timestamps']
  #  for date in data:
  #    if date not in dates:
  #      dates.append(date)
  #with open(FILE_DATES, 'w') as f:
  #  f.write(json.dumps(dates))
  ###for symbol in symbols:
  ###  # timestamps = { 2024-05-01: 0, 2024-05-02: 1, ... }
  ###  data = _read_file(symbol)
  ###  timestamps= {timestamp: i for i, timestamp in enumerate(timestamps)}
  ###  with open(f"./data/{symbol}_timestamps.json", 'w') as f:
  ###    f.write(json.dumps(timestamps))


if __name__ == '__main__':
  main()
