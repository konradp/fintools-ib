#!/usr/bin/env python3
import csv
import json
import math
import numpy as np
import pandas as pd
from decimal import Decimal, getcontext

import lib.util as util
from lib.runner import Runner
import lib.indicator.tbp as tbp

#getcontext().prec = 2
pd.options.display.float_format = '{:.2f}'.format


#prices = [ 49.25, 49.75, 50.25, 50.75, 51.10,
#  50.75, 51.00, 49.75, 49.25, 49.50 ]
#res = tbp.calculate_new(prices)
#with open('test/test_tbp1.json', 'r') as f:
#  expect = json.load(f)
#print('WAS')
#print(res)
##df_res = pd.DataFrame(res)
##print(df_res)
##if json.dumps(expect, indent=2) != json.dumps(res, indent=2):
##  print('ERROR')
##  print('EXPECT')
##  df_expect = pd.DataFrame(expect)
##  print(df_expect)
##else:
##  print('PASS')
#exit(0)

#with open('test/test_tbp2_input.csv', newline='') as file:
#  reader = csv.DictReader(file)
#  prices = [ float(i['close']) for i in reader ]
#print(prices)
#res = tbp.calculate_new(prices)
#df_res = pd.DataFrame(res)
df = pd.read_csv('test/test_tbp2.tsv', sep=r'\s+', engine='python')
df_in = df[['open', 'high', 'low', 'close']]
df_res = tbp.calculate(df_in)

#df_res = df_res.drop(columns=['day', 'position', 'price'])
#df_res = df_res.drop(columns=['day', 'price'])
df_res = df_res.drop(columns=['day'])
df = df.drop(columns=['day', 'entry', 'exit', 'note'])
#df = df.sort_index(axis=1)
#df_res = df_res.sort_index(axis=1)
df_res = df_res.drop(df_res.index[-1])
print('CHECKS')
print('---- CALCULATED ----')
print(df_res)
print('---- EXPECT ----')
print(df)

print('----- DIFF ------')
print('self: expect, other: calculated')
# Ignore if EXPECT has a NaN, only compare the actual values
nan_mask = df.isna()
df_res[nan_mask] = float('nan')
diff = df.compare(df_res)
print(diff)


exit(0)
print(len(df_expect['mf']))
print(len(df_res['mf'][:-1]))
print(df_expect['mf'].values)
print(df_res['mf'].values)
expect = df_expect['mf'].values
was = df_res['mf'].values
for i, _ in enumerate(expect):
  if math.isnan(expect[i]) and math.isnan(was[i]):
    continue
  if expect[i] != round(was[i], 2):
    print('ERROR', i, 'was', type(was[i]), was[i], 'expect', type(expect[i]), expect[i])


# DONE: mf, tbp
# position, price
# TODO: TR, X, lstop(X-TR), sstop(X+TR), ltgt(2X-L), stgt(2X-H), ENTRY, EXIT
#with open('test/test_tbp2_input.csv', newline='') as file:
#  reader = csv.DictReader(file)
#  prices = [ {
#    'o': Decimal(i['open']),
#    'h': Decimal(i['high']),
#    'l': Decimal(i['low']),
#    'c': Decimal(i['close']),
#  } for i in reader ]
#print(prices)
#res = tbp.calculate_stops(prices)
