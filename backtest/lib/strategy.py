# All strategies should inherit from this
import lib.util as util

class Strategy:
  symbols = []
  DATA = {}
  def init(self):
    print("Initialise strategy")
    self.symbols = util.get_symbols_ndq100()
    # DEBUG
    #self.symbols = self.symbols[:5]
    print("Load data")
    for s in self.symbols:
      self.DATA[s] = util._read_file(s)


  def run_day(self, date):
    stock_candidates = self.get_candidates(date)


  def get_candidates(self, date):
    # Get symbol candidates (the ones which satisfy the entry rules)
    candidates = [ s for s in self.symbols if self.is_buy(s, date) ]
    print(f"Candidates: {candidates}")


  def run(self):
    # Override
    print('running strategy parent')


  def is_buy(self, symbol):
    # Override
    pass


  ##### FOR USER #####
  def _shift_date(self, symbol, date, shift):
    # For (2024-01-02, -1), return 2024-01-01
    # Get index of the given date
    data = self.DATA[symbol]
    idx = data['timestamps'].index(date)
    return data['timestamps'][idx+shift]


  def _get_ma_for_date(self, symbol, date, period):
    data = util.data_for_date_index(symbol, date, -period, self.DATA)
    prices = [ i['close'] for i in data ]
    ma = sum(prices) / len(prices)
    return ma

  def _is_ma_cross(self, symbol, date, period_short, period_long):
    # return:
    #   - True: there is a cross, positive direction
    #   - False: there is a cross, negative direction
    #   - None: there is no cross
    # Note: If direction is positive, then at x=0, ma_short is below ma_long
    #   and at t=1, ma_short is above ma_long, i.e. ma_short crossed above ma_long
    ma_short = [
      self._get_ma_for_date(symbol,
        date=self._shift_date(symbol, date, -1),
        period=period_short),
      self._get_ma_for_date(symbol,
        date=date,
        period=period_short)
    ]
    ma_long = [
      self._get_ma_for_date(symbol,
        date=self._shift_date(symbol, date, -1),
        period=period_long),
      self._get_ma_for_date(symbol,
        date=date,
        period=period_long)
    ]
    # Does short MA cross above long MA?
    if ma_short[0] < ma_long[0] and ma_short[1] > ma_long[1]:
      return True
    if ma_short[0] > ma_long[0] and ma_short[1] < ma_long[1]:
      return False
    return None
