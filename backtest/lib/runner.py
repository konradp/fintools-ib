import importlib
from lib.util import get_dates

class Runner:
  # TODO: What about weekends
  strategy = None
  date_init = "2024-02-01"
  day_init = 0
  day_cur = 0
  #day_end = 10
  day_end = 2
  balance = 100
  def __init__(self):
    print('Init runner')


  def run(self):
    print(f"Running strategy {self.strategy_name}")
    self.strategy.init()
    print("Running days")
    for i in range(self.day_init, self.day_end+1):
      date = self.get_day_at_index(i)
      print(f"Running day {i}: {date}")
      self.strategy.run_day(date)


  def load_strategy(self, name):
    print(f"Loading strategy {name}")
    mod = importlib.import_module(f"strategies.{name}")
    strategy_class = getattr(mod, name.capitalize())
    strategy = strategy_class()
    self.strategy = strategy
    self.strategy_name = name


  def get_day_at_index(self, index):
    dates = get_dates()
    idx_date_init = dates.index(self.date_init)
    return dates[idx_date_init+index]
