import json
from datetime import datetime

DIR_DATA = 'data/'
FILE_NDQ100 = '../data/nasdaq100.json'
FILE_DATES = './data/dates.json'


def get_symbols_ndq100():
  with open(FILE_NDQ100, 'r') as f:
    data = json.load(f)['data']['data']['rows']
  symbols = sorted([ i['symbol'] for i in data ])
  return symbols


def data_for_date(symbol, date):
  data = _read_file(symbol)
  timestamps = data['timestamps']
  index = timestamps.index(date)
  ret = {
    't': timestamps[index],
    'open': data['open'][index],
    'high': data['high'][index],
    'low': data['low'][index],
    'close': data['close'][index],
    'volume': data['volume'][index],
  }
  return ret


def data_for_dates(symbol, date_from, date_to):
  data = _read_file(symbol)
  timestamps = data['timestamps']
  i_from = timestamps.index(date_from)
  i_to = timestamps.index(date_to)
  ret = [{
    't': timestamps[i],
    'open': data['open'][i],
    'high': data['high'][i],
    'low': data['low'][i],
    'close': data['close'][i],
    'volume': data['volume'][i],
  } for i in range(i_from, i_to+1)]
  return ret


def data_for_date_index(symbol, date, index, data=None):
  # Index can be positive or negative
  # e.g. 2024-01-02, -2 gives 2024-01-01 and 2024-01-02
  # e.g. 2024-01-02, 2 gives 2024-01-02 and 2024-01-03
  if data:
    # Use cache
    data = data[symbol]
  else:
    data = _read_file(symbol)
  timestamps = data['timestamps']
  try:
    i_date = timestamps.index(date)
  except Exception as e:
    print(f"ERROR: No data for {symbol} on {date}")
  r = range(i_date, i_date + index, -1 if index < 0 else 1)
  ret = [{
    't': timestamps[i],
    'open': data['open'][i],
    'high': data['high'][i],
    'low': data['low'][i],
    'close': data['close'][i],
    'volume': data['volume'][i],
  } for i in r]
  ret = sorted(ret, key=lambda x: x['t'])
  return ret


def get_dates():
  with open(FILE_DATES, 'r') as f:
    dates = json.load(f)
  return dates


def _read_file(symbol):
  # Returns: { 'timestamps': [], 'open': [], high, low, close, volume }
  # timestamps are in YYYY-MM-DD format
  with open(f"{DIR_DATA}/{symbol}.json", "r") as f:
    data = json.load(f)
  timestamps = data['chart']['result'][0]['timestamp']
  data = data['chart']['result'][0]['indicators']['quote'][0]
  timestamps = [ datetime.fromtimestamp(i).strftime('%Y-%m-%d') for i in timestamps ]
  data['timestamps'] = timestamps
  return data
