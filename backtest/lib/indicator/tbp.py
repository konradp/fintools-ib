# Trend Balance Point System
import pandas as pd
from decimal import Decimal, getcontext
import numpy as np

def calculate(df):
    # Input: pd dataframe with columns: open, high, low, close
    # Output: pd dataframe with columns: day, close, mf, tbp, position, price
    # definitions:
    # - mf: diff(close[-1], close[-2])
    # - tbp:
    #   - if long: close[-1] + min(mf[-1],mf[-2])
    #   - if short: close[-1] + max(mf[-1],mf[-2])
    # - TR: max( dist(h,l), dist(h,c[-1]), dist(l,c[-1])
    # - x: (h+l+c)/3  <-- AVG
    # - lstop: X-TR, sstop: X+TR
    # - ltgt: 2X-L, stgt: 2X-H
    df = df.copy()
    # Empty row at the end, as TBP can be calculated for tomorrow
    df.loc[len(df)] = [None] * len(df.columns)
    # MF
    df['day'] = df.index + 1
    df['mf'] = round(df['close'] - df['close'].shift(2), 2)
    df['mf_1'] = df['mf'].shift(1)
    df['mf_2'] = df['mf'].shift(2)
    # TBP. Note: We need min(mf_1,mf_2) for TBP calculation
    df['mf_lo'] = np.where(pd.notna(df['mf_1']) & pd.notna(df['mf_2']),
      df[['mf_1', 'mf_2']].min(axis=1), np.nan)
    df['mf_hi'] = np.where(pd.notna(df['mf_1']) & pd.notna(df['mf_2']),
      df[['mf_1', 'mf_2']].max(axis=1), np.nan)
    df['tbpL'] = round(df['close'].shift(2) + df['mf_lo'], 2)
    df['tbpS'] = round(df['close'].shift(2) + df['mf_hi'], 2)
    df = df.drop(columns=['mf_lo', 'mf_hi'])
    # Signals
    #df['signalL'] = (df['mf'] > df['mf_1']) & (df['mf'] > df['mf_2'])
    #df['signalS'] = (df['mf'] < df['mf_1']) & (df['mf'] < df['mf_2'])
    df['signalL'] = df['mf'] > df[['mf_1', 'mf_2']].min(axis=1)
    df['signalS'] = df['mf'] < df[['mf_1', 'mf_2']].min(axis=1)
    # Stops and targets
    df['x'] = round((df['high'] + df['low'] + df['close']) / 3, 2)
    df['tr'] = df.apply(lambda row: max(
      abs(row['high'] - row['low']),
      abs(row['high'] - df.loc[row.name - 1, 'close'] if row.name > 0 else 0),
      abs(row['low'] - df.loc[row.name - 1, 'close'] if row.name > 0 else 0)
    ) if row.name > 0 else None, axis=1)
    df['tr'] = round(df['tr'], 2)
    df['stopL'] = df['x'] - df['tr']
    df['stopL'] = df['stopL'].shift(1)
    df['stopS'] = df['x'] + df['tr']
    #df['tgtL'] = 2 * df['x'] - df['low']
    #df['tgtS'] = 2 * df['x'] - df['high']
    df['tgtL'] = round(2 * df['x'].shift(1) - df['low'].shift(1), 2)
    df['tgtS'] = round(2 * df['x'].shift(1) - df['high'].shift(1), 2)

    # TODO: Strategy
    position = 0 # -1=short, 0=no position, 1=long
    for index, r in df.iterrows():
      print(f"Index: {index} O {r['open']} H {r['high']} L {r['low']} C {r['close']} TGT_S {r['tgtS']} TGT_L {r['tgtL']}")
      if index <= 3:
        # Need at least two MFs to enter
        continue
      isL = position > 0
      isS = position < 0
      entryL = (not isL) and r['signalL']
      entryS = (not isS) and r['signalS']
      signalL = r['signalL']
      signalS = r['signalS']

      if isL and r['stopL'] >= r['low']:
        print('STOP L')
        position = 0
      elif isS and r['stopS'] <= r['high']:
        print('STOP S')
        position = 0
      elif isL and r['tgtL'] <= r['high']:
        print('TGT L')
        position = 0
      elif isS and r['tgtS'] >= r['low']:
        print('TGT S')
        position = 0

      print("isL", isL, "isS", isS, "entryL", entryL, "entryS", entryS)

      if entryL and not isS:
        print("ENTER L")
        position = 1
      if entryS and not isL:
        print("ENTER S")
        position = -1

      if not isL and not isS:
        if signalS:
          print('No position, ENTER S at', r['close'])
        if signalL:
          print('No position, ENTER L at', r['close'])



      ## Check for error
      #if (isL and r['stopL'] >= r['low'] and r['tgtL'] <= r['high']) or \
      #  (isS and r['stopS'] <= r['high'] and r['tgtS'] >= r['low']):
      #  # If both stop loss and target are triggered
      #  # Warn: On short, the target is the low price, and the stop is the high
      #  print(f'WARN: isL {isL} isS {isS}: both TGT and STOP')


      ## Strategy
      #if entryL and not isS:
      #  # ENTER L
      #  print('ENTER L')
      #  position = 1
      #if isL:
      #  print('isL')
      #  # target/stop
      #  if r['stopL'] >= r['low']:
      #    print('STOP L')
      #  if r['tgtL'] <= r['high']:
      #    print('TGT L')
      #if entryS and not isL:
      #  # ENTER S
      #  print("ENTER S at close", r['close'])
      #  position = -1
      #if isS:
      #  print('isS')
      #  # If short, did we hit target or stop
      #  if r['stopS'] <= r['high']:
      #    print('STOP S')
      #  elif r['tgtS'] >= r['low']:
      #    print('TGT S at target', r['tgtS'])


      #if not isL and not isS:
      #  # No position, do we enter short or long
      #  if entryL:
      #    print("ENTER L at close", r['close'])
      #    position = 1
      #  elif entryS:
      #    print("ENTER S at close", r['close'])
      #    position = -1

        



      ## Long: entry and exits
      #if entryL and not isS:
      #  print('ENTER LONG')
      #  position = 1
      ## Short
      #if entryS and not isL:
      #  print('ENTER SHORT')
      #  position = -1
      ## Stops and targets
      #if isL and r['stopL'] >= r['low']:
      #  print('STOP L')
      #elif isL and r['tgtL'] <= r['high']:
      #  print('TGT L')
      #elif isS and r['stopS'] <= r['high']:
      #    print('STOP S')
      #elif isS and r['tgtS'] >= r['low']:
      #    print('TGT S')
      #else:
      #  # Neither stop or target, consider reversal
      #  if isL:
      #    print('REVERSE to L')
      #  if isS and r['signalL']:
      #    print(f'REVERSE to L, isS {isS} entryS {entryS}')
      ## TODO: exit S
      



      #if index + 1 < len(df):
      #  # Last item
      #  next_row = df.iloc[index + 1]
      #  # Set
      #  #df.loc[1, 'A'] = 10

    print(df)
    exit(0)


    # Reorder
    df = df[['day', 'open', 'high', 'low', 'close', 'mf', 'tr', 'x', 'tbpL',
      'tbpS', 'stopL', 'stopS', 'tgtL', 'tgtS', 'signalL', 'signalS']]
    return df

def calculate_old(prices):
  # The last item is tomorrow
  res = []
  pos_cur = None
  for i, price in enumerate(prices):
    today = {
      'day': i+1,
      'close': price,
      'mf': None,
      'tbp': None,
      'position': None,
      'price': None,
    }
    res.append(today)
    if i < 2:
      # Can't calculate MF for the first two days
      continue
    # Calculate MF
    today['mf'] = price - prices[i-2]
    if i < 4:
      # Need four days to be able enter (long/short)
      continue
    lo = min(res[i-1]['mf'], res[i-2]['mf'])
    hi = max(res[i-1]['mf'], res[i-2]['mf'])
    pos = 'long' if today['mf'] > lo else 'short'
    if pos != pos_cur:
      # Position change
      pos_cur = pos
      today['position'] = pos
      today['price'] = price
    # Calculate TBP
    today['tbp'] = hi + res[i-2]['close']
    # On last date, calculate tomorrow's TBP
    if i+1 == len(prices):
      if pos_cur == 'long':
        tomorrow_tbp = min(today['mf'], res[i-1]['mf']) + res[i-1]['close']
      else:
        tomorrow_tbp = max(today['mf'], res[i-1]['mf']) + res[i-1]['close']
      res.append({'day': i+2, 'tbp': tomorrow_tbp})
  return res

def calculate_stops_old(data):
  # data = [ { o:123, h:123, l:123, c:123 }, ... ]
  # Returns:
  # X: (h+l+c)/3
  # TR: max( dist(h,l), dist(h, c_yesterday), dist(l, c_yesterday) )
  # lstop: X-TR, sstop: X+TR
  # ltgt: 2X-L, stgt: 2X-H
  # ENTRY: ?
  # EXIT: ?
  res = []
  tomorrow = { 'lstop': None, 'sstop': None, 'ltgt': None, 'stgt': None }
  for i, d in enumerate(data):
    print('d', d)
    t = {
      'day': i+1,
      'o': d['o'],
      'h': d['h'],
      'l': d['l'],
      'c': d['c'],
      'x': None,
      'tr': None,
      'lstop': None,
      'sstop': None,
      'ltgt': None,
      'stgt': None,
    }
    res.append(t)
    #x = Decimal((d['h']+d['l']+d['c'])/3)
    # CRUCIAL ROUNDING HERE !!!!
    x = round((d['h']+d['l']+d['c'])/3, 2)
    t['x'] = x
    if i == 0:
      # Can't calculate TR on day 0
      continue
    y = res[i-1]
    tr = Decimal( max( abs(d['h']-d['l']), abs(d['h']-y['c']), abs(d['l']-y['c']) ) )
    t['tr'] = tr
    # TODO: these depend on long or short
    t['lstop'] = tomorrow['lstop']
    t['sstop'] = tomorrow['sstop']
    tomorrow['lstop'] = x-tr
    tomorrow['sstop'] = x+tr

    t['ltgt'] = tomorrow['ltgt']
    tomorrow['ltgt'] = 2*x - d['l']
    print('LTGT', i, 'x', x, '2x', 2*x, 'rounded', round(2*x, 2), 'l', d['l'], '2x-l', 2*x-d['l'], 'rounded', round(2*x-d['l'], 2))
    t['stgt'] = tomorrow['stgt']
    tomorrow['stgt'] = 2*x - d['h']

  pd.options.display.float_format = '{:.2f}'.format
  df = pd.DataFrame(res)
  df['x'] = df['x'].apply(lambda x: round(x, 2) if isinstance(x,Decimal) else x)
  df['lstop'] = df['lstop'].apply(lambda x: round(x, 2) if isinstance(x,Decimal) else x)
  df['sstop'] = df['sstop'].apply(lambda x: round(x, 2) if isinstance(x,Decimal) else x)
  df['ltgt'] = df['ltgt'].apply(lambda x: round(x, 2) if isinstance(x,Decimal) else x)
  df['stgt'] = df['stgt'].apply(lambda x: round(x, 2) if isinstance(x,Decimal) else x)

  #df = df.quantize('0.01')
  #
  print(df)
