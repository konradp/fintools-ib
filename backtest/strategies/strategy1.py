# Example strategy
# 5 day MA crosses above 10 day MA
from lib.strategy import Strategy
from lib.indicator.moving_average import moving_average
from lib.util import data_for_date_index

class Strategy1(Strategy):
  def __init__(self):
    super().__init__()
    print('Imported strategy1')


  def is_buy(self, symbol, date):
    ma_len_short = 5
    ma_len_long = 10
    cross_direction = self._is_ma_cross(symbol, self._shift_date(symbol, date, -1), ma_len_short, ma_len_long)
    if cross_direction is not None and cross_direction:
      return True
    return False
