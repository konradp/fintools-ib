// Read a CSV file and return a list of symbols

#include <iostream>

#include <Nsdq.hpp>

int main() {
    Nsdq nasdaq;
    auto v = nasdaq.List();
    for(auto i: v) {
        std::cout << i.GetSymbol() << std::endl;
    }
}
