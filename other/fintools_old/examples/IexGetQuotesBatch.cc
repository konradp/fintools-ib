// Get Iex quotes

#include <iomanip> // setprecision
#include <iostream>
#include <string>

#include <Iex.hpp>
#include <Nsdq.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::vector;

void die(string msg) {
    cerr << msg << endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    try {
        Nsdq nsdq;
        vector<Symbol> symbols = nsdq.GetSymbList();
        Iex iex;
        std::map<string, Quote> quotes = iex.GetQuotesBatch(symbols);
        for(auto i: quotes) {
            cout << std::fixed << std::setprecision(2)
                 << i.first << " " << i.second.changePercent << endl;
        }
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
