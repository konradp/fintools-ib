// Return a list of market caps as read from the CSV file
// Not useful

#include <iomanip> // std::setprecision
#include <iostream>

#include <Nsdq.hpp>

using std::cout;
using std::endl;
using std::setprecision;
using std::vector;

int main() {
    cout << std::fixed << setprecision(2);

    Nsdq nsdq;
    vector<Symbol> symbols = nsdq.GetSymbList();
    for(auto s: symbols) {
        std::cout << s.Ticker() << " "
                  << s.MarketCap() << std::endl;
    }
}
