// Return a list of symbols and their
// day percent change
// Sort this with | sort -k2g

#include <iomanip> // setprecision
#include <iostream>
#include <string>

#include <Nsdq.hpp>
#include <Symbol.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::vector;
char const *pName;

void die(string msg) {
    cerr << msg << endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    try {
        Nsdq nsdq;
        vector<Symbol> symbols = nsdq.GetSymbList();
        std::map<string, float> changes;
        for(auto s: symbols) {
            cout << std::fixed << std::setprecision(2)
                 << s.GetSymbol() << " " << s.ChangePercent() << endl;
        }
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
