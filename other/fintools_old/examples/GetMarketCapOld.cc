// Return a market cap of a company
// given its symbol/ticker
// Example:
// in:  AAPL
// out: 942870000000.00

#include <iomanip> // std::setprecision
#include <iostream>

#include <NsdqCsvFile.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::setprecision;
char const *pName;

void usage() {
    cout << "Usage: " << pName << " SYMBOL" << endl
         << "Example: " << pName << " AAPL" << endl;
}

void die(string msg) {
    cerr << msg << endl;
    usage();
    exit(1);
}

int main(int argc, char *argv[]) {
    pName = argv[0];
    // Check params
    if(argc < 2) die("Not enough parameters.");
    cout << std::fixed << setprecision(2);

    NsdqCsvFile s;
    try {
        cout << s.GetMarketCap(argv[1]) << endl;
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
