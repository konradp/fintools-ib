// Return the sector of a company
// given its symbol/ticker
//
// Example:
// in:  AAPL
// out: Technology

#include <iomanip> // setprecision
#include <iostream>
#include <string>

#include <Nsdq.hpp>
#include <Symbol.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
char const *pName;
string type;

void usage() {
    cout << "Usage: " << pName << " TYPE" << endl
         << "Example: " << pName << " smallprice" << endl
         << "Example: " << pName << " all" << endl;
}

void die(string msg) {
    cerr << msg << endl;
    usage();
    exit(1);
}

int main(int argc, char *argv[]) {
    pName = argv[0];
    // Check params
    if(argc < 2) die("Not enough parameters.");
    type = string(argv[1]);
    cout << std::fixed << std::setprecision(2);

    try {
        Nsdq nsdq;
        vector<Symbol> symbols = nsdq.GetSymbList();
        for(auto s: symbols) {
            if( (type == "smallprice") && (s.LatestPrice() >= 10) ) {
                continue;
            }
            cout << s.ChangePercent() << ",";
        }
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
