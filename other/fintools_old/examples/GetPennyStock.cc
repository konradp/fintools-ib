// Return the sector of a company
// given its symbol/ticker
//
// Example:
// in:  AAPL
// out: Technology

#include <iomanip> // setprecision
#include <iostream>
#include <string>

#include <Nsdq.hpp>
#include <Symbol.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
char const *pName;
string type;

void die(string msg) {
    cerr << msg << endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    // Check params
    cout << std::fixed << std::setprecision(2);

    try {
        Nsdq nsdq;
        vector<Symbol> symbols = nsdq.GetSymbList();
        for(auto s: symbols) {
            if( s.LatestPrice() > 1 ) {
                continue;
            }
            cout << s.Name() << " "
                 << s.Price() << " "
                 << s.ChangePercent() << endl;
        }
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
