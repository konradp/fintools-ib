// Return a quote of a company
// given its symbol/ticker
//
// Example:
// in:  AAPL

#include <iostream>
#include <string>

#include <Iex.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
char const *pName;

void usage() {
    cout << "Usage: " << pName << " SYMBOL" << endl
         << "Example: " << pName << " AAPL" << endl;
}

void die(string msg) {
    cerr << msg << endl;
    usage();
    exit(1);
}

int main(int argc, char *argv[]) {
    pName = argv[0];
    // Check params
    if(argc < 2) die("Not enough parameters.");

    try {
        Iex i;
        Quote q = i.GetQuote(argv[1]);
        cout << q.changePercent << endl;
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
