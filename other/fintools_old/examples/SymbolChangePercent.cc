// Return a day percent change of a company
// given its symbol/ticker
//
// Example:
// in:  AAPL
// out: 5 (i.e. +5%)

#include <iomanip> // setprecision
#include <iostream> // cout, fixed
#include <Symbol.hpp>

using std::cerr;
using std::cout;
using std::endl;
char const *pName;

void usage() {
    cout << "Usage: " << pName << " SYMBOL" << endl
         << "Example: " << pName << " AAPL" << endl;
}

void die(string msg) {
    cerr << msg << endl;
    usage();
    exit(1);
}

int main(int argc, char *argv[]) {
    pName = argv[0];
    // Check params
    if(argc < 2) die("Not enough parameters.");

    try {
        Symbol s(argv[1]);
        cout << std::fixed << std::setprecision(2)
             << s.ChangePercent() << endl;
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
