// Return the sector of a company
// given its symbol/ticker
//
// Example:
// in:  AAPL
// out: Technology

#include <iomanip> // setprecision
#include <iostream>
#include <string>

#include <Nsdq.hpp>
#include <RSymbol.hpp>
#include <Symbol.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
char const *pName;
string date;

void usage() {
    cout << "Usage: " << pName << " TYPE" << endl
         << "Example: " << pName << " smallprice" << endl
         << "Example: " << pName << " all" << endl;
}

void die(string msg) {
    cerr << msg << endl;
    usage();
    exit(1);
}

string fmtNumber(string v) {
    if( (v == "") || (v == "null") ) {
        return "NaN";
    } else {
        double d = std::stod(v);
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << d;
        return ss.str();
    }
}

// * 100
string fmtPerc(string v) {
    if( (v == "") || (v == "null") ) {
        return "NaN";
    } else {
        double d = std::stod(v);
        d = d*100;
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << d;
        return ss.str();
    }
}

string fmtText(string v) {
    if( (v == "") || (v == "null") ) {
        return "NA";
    } else {
        return v;
    }
}

int main(int argc, char *argv[]) {
    pName = argv[0];
    // Check params
    if(argc < 2) die("Not enough parameters.");
    date = string(argv[1]);
    //cout << std::fixed << std::setprecision(2);

    try {
        cout << "symbol,changePerc,price,sector,open,high,low,close" << endl;
        Nsdq nsdq;
        vector<Symbol> symbols = nsdq.GetSymbList();
        for(auto s: symbols) {
            RSymbol t = RSymbol(s.GetSymbol());
            cout
                << t.GetSymbol() << ","
                << fmtPerc(t.ChangePercent()) << ","
                << fmtNumber(t.LatestPrice()) << ","
                << fmtText(t.Sector()) << ","
                << fmtNumber(t.Open()) << ","
                << fmtNumber(t.High()) << ","
                << fmtNumber(t.Low()) << ","
                << fmtNumber(t.Close()) << endl;
        }
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
}
