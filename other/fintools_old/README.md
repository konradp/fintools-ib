# DEPRECATED
This is no longer used, since IEX API data feed is no longer free.  
See instead https://gitlab.com/konradp/fintools-ib

# Quickstart
https://finapi.tk/pennystock/winners

# Overview
The project consists of:
- `ansible`: The Ansible playbook triggered by `.gitlab-ci.yml` to deploy the application
- `examples/`: Examples which use the library
- `src/` the application source code

# Data downloader
Run the downloader.
```
/opt/fintools/download.sh
```
The above will:
- get Nasdaq symbols
- clean data

# Library/API reference
TODO: See README-lib.md

# Server
TODO: See README-server.md
