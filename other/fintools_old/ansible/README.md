# Ansible deployment

Run the playbook
```
ansible-playbook deploy-test.yml
```

## Prerequisite on nodes (likely not needed)
/* NOT TRUE
On Debian.
```
apt-get install -y python-minimal
```
On CentOS it's more involved.
```
sudo yum -y install https://centos7.iuscommunity.org/ius-release.rpm
sudo yum -y install python36u
```
NOT TRUE */
