#!/bin/bash
sudo cp service.systemd.j2 /etc/systemd/system/fintools.service
sudo systemctl daemon-reload
sudo systemctl enable fintools.service
sudo systemctl restart fintools.service
sudo systemctl status fintools.service
