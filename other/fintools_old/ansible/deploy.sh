#!/bin/bash
ansible-playbook deploy.yml --extra-vars "APP_NAME=fintools" \
  --vault-password-file=./.vaultpass
