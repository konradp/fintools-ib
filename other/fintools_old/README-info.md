# Exchange opening times
LSE: Europe/London time
NASDAQ: America/New_York time

LOCAL TIME
exchange open  close
LSE      08:00  16:30
NASDAQ   09:30  14:30

AEDT time
exchange open  close
LSE      18:00 02:30
NASDAQ   00:30 05:30

# Other reference
## NASDAQ exchange
NASDAQ is a stock exchange, much like:
- NYSE (NewYorkStockExchange)
- LSE (LondonStockExchange)

NASDAQ lists around 3323 companies.


It lists more than 3,300 companies, according to Google.

## Quotes
To obtain company quotes, use the IEX trading API
https://iextrading.com/developer/docs/

Example:
https://api.iextrading.com/1.0/ref-data/symbols  
gives 5445 symbols
