#pragma once
#ifndef IEX_JSON_PARSER_HPP
#define IEX_JSON_PARSER_HPP

#include <iostream>
#include "IexStructs.h"
#include "jsoncpp/json/json.h"
#include "tinynet/tinynet.hpp"

using std::string;
using std::stringstream;

class IexJsonParser {
public:
IexJsonParser() {};
Quote ParseQuote(Json::Value v) {
  // TODO: This is unnecessary repetition
  // The structure is already defined in the header
  // We should be able to infer the asString/asDouble etc
  // Can't do this because c++ has no reflection
  return {
    v["avgTotalVolume"].asDouble(),
    v["calculationPrice"].asString(),
    v["change"].asDouble(),
    v["changePercent"].asDouble(),
    v["close"].asDouble(),
    v["closeTime"].asDouble(),
    v["companyName"].asString(),
    v["delayedPrice"].asDouble(),
    v["delayedPriceTime"].asDouble(),
    v["extendedChange"].asDouble(),
    v["extendedChangePercent"].asDouble(),
    v["extendedPrice"].asDouble(),
    v["extendedPriceTime"].asDouble(),
    v["high"].asDouble(),
    v["iexAskPrice"].asDouble(),
    v["iexAskSize"].asDouble(),
    v["iexBidPrice"].asDouble(),
    v["iexBidSize"].asDouble(),
    v["iexLastUpdated"].asDouble(),
    v["iexMarketPercent"].asDouble(),
    v["iexRealtimePrice"].asDouble(),
    v["iexRealtimeSize"].asDouble(),
    v["iexVolume"].asDouble(),
    v["latestPrice"].asDouble(),
    v["latestSource"].asString(),
    v["latestTime"].asString(),
    v["latestUpdate"].asDouble(),
    v["latestVolume"].asDouble(),
    v["low"].asDouble(),
    v["marketCap"].asDouble(),
    v["open"].asDouble(),
    v["openTime"].asDouble(),
    v["peRatio"].asDouble(),
    v["previousClose"].asDouble(),
    v["symbol"].asString(),
    v["week52High"].asDouble(),
    (v["week52Low"].asString() == "")? 0 : v["week52Low"].asDouble(),
    v["ytdChange"].asDouble()
  };
};
Company ParseCompany(Json::Value v) {
  // TODO: This is unnecessary repetition
  // The structure is already defined in the header
  // We should be able to infer the asString/asDouble etc
  // Use templating?
  return {
    v["companyName"].asString(),
    v["description"].asString(),
    v["exchange"].asString(),
    v["industry"].asString(),
    v["issueType"].asString(),
    v["sector"].asString(),
    v["symbol"].asString(),
    v["website"].asString()
  };
};

}; // class IexJsonParser
#endif //IEX_JSON_PARSER_HPP
