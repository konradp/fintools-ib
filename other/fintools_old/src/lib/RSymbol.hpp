#pragma once
#ifndef R_SYMBOL_HPP
#define R_SYMBOL_HPP
#include <iostream>
#include <stdlib.h> // for system()
#include "FileSystem.hpp"

// This class represents a NASDAQ symbol

class RSymbol {
public:
RSymbol(string v)
: _SymbolName(v) {};

// Methods
// Get symbol name
string GetSymbol()     { return _SymbolName; };
// Day change (%)
string  ChangePercent() { return GetQuote().changePercent; };
// CPI -> Capita Plc.
string  LatestPrice()   { return GetQuote().latestPrice; };
// Get sector, e.g. Finance, Technology etc.
string Sector() { return GetQuote().sector; };
// OHLC
string Open()          { return GetQuote().open; };
string High()          { return GetQuote().high; };
string Low()           { return GetQuote().low; };
string Close()         { return GetQuote().close; };

RQuote GetQuote() {
  FileSystem fs;
  return fs.RReadQuote(_SymbolName);
};

private:
  string _SymbolName;

}; // class RSymbol
#endif //R_SYMBOL_HPP
