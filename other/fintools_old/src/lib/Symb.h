#pragma once
#ifndef SYMB_H
#define SYMB_H

#include <string>

using std::string;

struct Symb {
  // Ticker
  string ticker;
  // Info
  double change_perc;
  string company_name;
  string desc;
  string exchange;
  string industry;
  double latest_price;
  double market_cap;
  double price;
  string sector;
  string website;
  // OHLC
  double open;
  double high;
  double low;
  double close;
};

#endif
