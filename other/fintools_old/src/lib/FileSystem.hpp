#pragma once
#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP

#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include "IexJsonParser.hpp"
#include "IexStructs.h"
#include "RJsonParser.hpp"
#include "RStructs.h"
#include "Symb.h"
#include "jsoncpp/json/json.h"

// A filesystem interface
// It does:
// - Create data dirs
// - Read list of symbols (disk->JSON)
// - Write list of symbols to disk (JSON->disk)
// - Read company data (disk->JSON)
// - Write company data (JSON->disk)
// - Read quote (disk->JSON)
// - Write quote (JSON->disk)

using std::string;

class FileSystem {
public:
  FileSystem(string data_dir)
  : m_DataDir(data_dir)
  {  SetDate(_GetDate()); };

  FileSystem(string data_dir, string date)
  : m_DataDir(data_dir)
  { SetDate(date); };

  void SetDate(string date) {
    _companyDir = m_DataDir + "/company/";
    _quoteDir = m_DataDir + "/quote/" + date + "/";
    m_SymbolsFile = m_DataDir + "/symbols";
    if(!_Isdir(m_DataDir)) _Mkdir(m_DataDir);
    if(!_Isdir(_companyDir)) _Mkdir(_companyDir);
    if(!_Isdir(_quoteDir)) _Mkdir(_quoteDir);
  };

  // Symbol
  Symb GetSymb(string sym) {
    Company c = GetCompany(sym);
    Quote q = GetQuote(sym);
    return {
      /*ticker*/      sym,
      // INFO
      /*change_perc*/  q.changePercent*100,
      /*company_name*/ q.companyName,
      /*desc*/         c.description,
      /*exchange*/     c.exchange,
      /*industry*/     c.industry,
      /*latest_price*/ q.latestPrice,
      /*market_cap*/   q.marketCap,
      /*price*/        q.latestPrice,
      /*sector*/       c.sector,
      /*website*/      c.website
    };
  };

  // Symbols: Get from symbols file
  Json::Value
  ReadSymbols() {
    try {
      return _FileToJson(m_SymbolsFile);
    } catch(std::exception &e) {
      throw std::runtime_error("Could not convert to json " + m_SymbolsFile);
    }
  };
  void
  WriteSymbols(Json::Value v) {
    _JsonToFile(m_SymbolsFile, v);
  };

  // Company
  Company
  GetCompany(string symbol) {
    Json::Value q = _FileToJson(_companyDir + symbol);
    IexJsonParser parser;
    return parser.ParseCompany(q);
  };
  void
  WriteCompany(Json::Value v) {
    _JsonToFile(_companyDir + v["symbol"].asString(), v);
  };

  // Quote
  void
  WriteQuote(Json::Value v) {
    _JsonToFile(_quoteDir + v["symbol"].asString(), v);
  };
  Quote
  GetQuote(string symbol) {
    Json::Value q = _FileToJson(_quoteDir + symbol);
    IexJsonParser parser;
    return parser.ParseQuote(q);
  };
  RQuote
  RReadQuote(string symbol) {
    Json::Value q = _FileToJson(_quoteDir + symbol);
    RJsonParser parser;
    return parser.ParseQuote(q);
  };

private:
  string m_DataDir;
  string m_SymbolsFile;
  string _quoteDir;
  string _companyDir;

  // Converters
  Json::Value
  _FileToJson(string path) {
    std::ifstream ifs;
    ifs.open(path);
    if(ifs.is_open()) {
      Json::Value r;
      ifs >> r;
      return r;
    } else {
      throw std::runtime_error("Could not open file " + path);
    }
  };
  void
  _JsonToFile(string fPath, Json::Value v) {
    std::ofstream ofs;
    ofs.open(fPath);
    if(ofs.is_open())
      ofs << v;
    else
      throw std::runtime_error("Could not open file to write to: " + fPath);
  };
  string
  _GetDate() {
    time_t t = time(0);
    struct tm *tm_now = localtime(&t);
    return std::to_string(1900 + tm_now->tm_year)
           + zeros(std::to_string(tm_now->tm_mon + 1))
           + zeros(std::to_string(tm_now->tm_mday));
  };

  // Filesystem helpers
  bool
  _Isdir(string d) {
    struct stat info;
    if(stat(d.c_str(), &info) && S_ISDIR(info.st_mode))
      return true;
    else
      return false;
  };
  void
  _Mkdir(string d) {
    mkdir(
      d.c_str(),
      S_IRWXU
      | S_IRGRP | S_IXGRP
      | S_IROTH | S_IXOTH
    );
  };
  string
  zeros(string a) {
    return (stoi(a) < 10)? "0" + a : a;
  };
}; // class FileSystem
#endif // FILESYSTEM_HPP
