#pragma once
#ifndef R_JSON_PARSER_HPP
#define R_JSON_PARSER_HPP

#include <iostream>
#include "jsoncpp/json/json.h"
#include "tinynet/tinynet.hpp"
#include "RStructs.h"

using std::string;

class RJsonParser {
public:
RJsonParser() {};
RQuote ParseQuote(Json::Value v) {
  // TODO: This is unnecessary repetition
  // The structure is already defined in the header
  // We should be able to infer the asString/asString etc
  // Use templating?
  return {
    v["changePercent"].asString(),
    v["close"].asString(),
    v["high"].asString(),
    v["latestPrice"].asString(),
    v["low"].asString(),
    v["open"].asString(),
    v["sector"].asString(),
    v["symbol"].asString(),
  };
};

}; // class RJsonParser
#endif //R_JSON_PARSER_HPP

