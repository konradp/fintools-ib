#pragma once
#ifndef IEX_HPP
#define IEX_HPP

#include <iostream>
#include "IexStructs.h"
#include "IexJsonParser.hpp"
#include "Symb.h"
#include "jsoncpp/json/json.h"
#include "tinynet/tinynet.hpp"

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::stringstream;
using std::vector;

class Iex {
// Iex class operates on vectors of strings (ticker names)

public:
Iex(string token)
: m_ApiBaseUrl("https://cloud.iexapis.com/stable/"),
  m_Token(token)
{};

vector<string> GetSymbStrList() {
  // Get IEX symbols which are CS and enabled
  // TODO: Handle if nothing received
  // SINGLE CALL
  vector<string> symbols;
  string s = _GetUrl("ref-data/symbols");
  Json::Value q; stringstream ss; ss << s; ss >> q;
  cout << "Got Iex tickers: " << q.size() << endl;
  for(auto symbol : q) {
    if(symbol["type"].asString() == "cs"
      && symbol["isEnabled"].asString() == "true"
    ) {
      symbols.push_back(symbol["symbol"].asString());
    }
  }
  cout << "Filtered to tickers: " << symbols.size() << endl;
  return symbols;
};

Quote
GetQuote(string symbol) {
  string s = _GetUrl("stock/" + symbol + "/quote");
  Json::Value q;
  stringstream ss;
  ss << s; ss >> q;
  IexJsonParser p;
  return p.ParseQuote(q);
};

Company GetCompany(string v) {
  string s = _GetUrl("stock/" + v + "/company");
  Json::Value q; stringstream ss; ss << s; ss >> q;
  IexJsonParser p;
  return p.ParseCompany(q);
};

// BATCH REQUESTS
map<string, Json::Value>
GetCompanyBatchJson(vector<string> s) {
  return _GetCompanyBatchJson(s);
}
map<string, Json::Value>
GetQuotesBatchJson(vector<string> s) {
  return _GetQuotesBatchJson(s);
};

/* PRIVATE */
private:
string m_ApiBaseUrl;
string m_Token;
bool m_Debug = false;

map<string, float>
_GetBatchFloat(vector<Symb> symbols, string v) {
  map<string, float> ret;
  string urlBase = "stock/market/batch?symbols=";
  string sList = string("");
  string urlFull;
  string out;
  int i = 1;

  for(auto symbol: symbols) {
    sList += symbol.ticker + ",";
    if(i == 100) {
      urlFull = urlBase + sList + "&types=quote";
      // Parse: out -> q
      out = _GetUrl(urlFull);
      Json::Value q; stringstream ss; ss << out; ss >> q;
      for(auto x: q) {
        ret[x["quote"]["symbol"].asString()]
          = x["quote"][v].asFloat()*100;
      }
      i = 1;
      sList = string("");
    } else {
      i++;
    }
  } //for

  // TODO: Hack
  urlFull = urlBase + sList + "&types=quote";
  // Parse: out -> q
  out = _GetUrl(urlFull);
  Json::Value q; stringstream ss; ss << out; ss >> q;
  for(auto x: q) {
    ret[x["quote"]["symbol"].asString()]
      = x["quote"][v].asFloat()*100;
  }
  return ret;
};

map<string, Json::Value>
_GetCompanyBatchJson(vector<string> symbols) {
  cout << "Get batch symbols" << endl;
  map<string, Json::Value> ret;
  string call = "stock/market/batch";
  string params = string("&types=company&symbols=");
  string urlFull;
  string out;
  int i = 1;

  for(auto symbol: symbols) {
    params += symbol + ",";
    if(i == 100) {
      // Parse: out -> q
      // TODO: Do some checking here if data valid
      out = _GetUrl(call, params);
      Json::Value q; stringstream ss; ss << out; ss >> q;
      for(auto x: q) {
        ret[x["company"]["symbol"].asString()]
          = x["company"];
      }
      i = 1;
      params = string("&types=company&symbols=");
    } else {
      i++;
    }
  } //for

  // TODO: Hack: This is for the last batch < 100
  //urlFull = urlBase + sList + "&types=company";
  // Parse: out -> q
  // TODO: No error checking here, what if 404?
  out = _GetUrl(call, params);
  Json::Value q; stringstream ss; ss << out; ss >> q;
  for(auto x: q) {
    ret[x["company"]["symbol"].asString()]
      = x["company"];
  }
  return ret;
};

map<string, Json::Value>
_GetQuotesBatchJson(vector<string> symbols) {
  // Process list of quotes in batches of 100
  map<string, Json::Value> ret;
  string call = "stock/market/batch";
  string params = "&types=quote&symbols=";
  string out;
  string first, last;
  int i = 1;

  for(auto symbol: symbols) {
    params += symbol + ",";
    if(i == 1) first = symbol;
    if(i == 100) {
      last = symbol;
      cout << "Getting tickers " << first << " to " << last << endl;
      out = _GetUrl(call, params);
      Json::Value q; stringstream ss; ss << out; ss >> q;
      for(auto x: q) {
        ret[x["quote"]["symbol"].asString()]
          = x["quote"];
      }
      i = 1;
      params = string("&types=quote&symbols=");
    } else {
      i++;
    }
  } //for

  // TODO: Hack
  // Parse: out -> q
  out = _GetUrl(call, params);
  Json::Value q; stringstream ss; ss << out; ss >> q;
  for(auto x: q) {
    ret[x["quote"]["symbol"].asString()] = x["quote"];
  }
  return ret;
};

// Example
// in: /stock/aapl/quote
// out: string
string _GetUrl(string call, string params = "") {
  // Construct url and download it
  string divider = (params == "")? string("?") : string("&");
  Net t;
  if(m_Debug) {
    cout << "GET " << m_ApiBaseUrl << call << "?token=TOKEN" << params
      << endl;
  }
  return t.Get(m_ApiBaseUrl + call + "?token=" + m_Token + params);
};

}; // class Iex
#endif //IEX_HPP
