#pragma once
#ifndef IEXSTRUCTS_H
#define IEXSTRUCTS_H

using std::string;

// Structs representing the Iex API endpoints

struct Company {
    string companyName;
    string description;
    string exchange;
    string industry;
    string issueType;
    string sector;
    string symbol;
    string website;
};

struct Quote {
    double avgTotalVolume;
    string calculationPrice;
    double change;
    double changePercent;
    double close;
    double closeTime;
    string companyName;
    double delayedPrice;
    double delayedPriceTime;
    double extendedChange;
    double extendedChangePercent;
    double extendedPrice;
    double extendedPriceTime;
    double high;
    double iexAskPrice;
    double iexAskSize;
    double iexBidPrice;
    double iexBidSize;
    double iexLastUpdated;
    double iexMarketPercent;
    double iexRealtimePrice;
    double iexRealtimeSize;
    double iexVolume;
    double latestPrice;
    string latestSource;
    string latestTime;
    double latestUpdate;
    double latestVolume;
    double low;
    double marketCap;
    double open;
    double openTime;
    double peRatio;
    double previousClose;
    string symbol;
    double week52High;
    double week52Low;
    double ytdChange;
};

#endif //IEXSTRUCTS_H
