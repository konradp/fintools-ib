#pragma once
#ifndef NSDQ_HPP
#define NSDQ_HPP
#include <iostream>
#include <vector>

#include "Symb.h"
#include <FileSystem.hpp>

// This class represents the NASDAQ exchange
using std::pair;
using std::string;
using std::vector;

class Nsdq {
public:
Nsdq(string dir_data)
: m_DirData(dir_data),
  m_FileSystem(dir_data)
{};

Symb GetSymb(string ticker) {
  return m_FileSystem.GetSymb(ticker);
}

// List
// Read list from the 'symbols' file
// Return a list of Symbols (vector of symbols)
vector<Symb> GetSymbList() {
  vector<string> names = GetSymbListStr();
  std::cout << "Processing " << names.size() << " symbols" << std::endl;
  vector<Symb> r;
  for(auto name: names) {
    r.push_back(GetSymb(name));
  }
  return r;
};

vector<string>
GetSymbListStr() {
  // Get list of symbol names from file
  vector<string> r;
  Json::Value symbols = m_FileSystem.ReadSymbols();
  for(auto s: symbols) {
    r.push_back(s["symbol"].asString());
  }
  return r;
};
void
WriteList(vector<pair<string, string>> symbols) {
  // In: symbol, CompanyName
  Json::Value root, item;
  FileSystem fs(m_DirData);
  for(auto s: symbols) {
    item["symbol"] = s.first;
    item["name"] = s.second;
    root.append(item);
  }
  fs.WriteSymbols(root);
};

// PRIVATE
private:
  FileSystem m_FileSystem;
  std::string m_DirData;
  // Methods

}; // class Nsqd
#endif //NSDQ_HPP
