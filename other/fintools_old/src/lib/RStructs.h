#pragma once
#ifndef RSTRUCTS_H
#define RSTRUCTS_H

using std::string;

// Structs representing the Iex API endpoints

struct RQuote {
    string changePercent;
    string close;
    string high;
    string latestPrice;
    string low;
    string open;
    string sector;
    string symbol;
};

#endif //RSTRUCTS_H
