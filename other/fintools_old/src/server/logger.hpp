#include <cstdio>
#include <cstdarg>
#include <fstream>
#include <restbed>
#include <memory> // make_shared, shared_ptr
#include <iostream> // cout

using namespace restbed;
using std::ofstream;
using std::shared_ptr;
using restbed::Logger;

class CustomLogger : public Logger {
public:
  CustomLogger(std::string logfile) {
    logFile = logfile;
  }

  void stop(void) {}

  void start(const shared_ptr<const Settings>&) {
    return;
  }

  void log(const Level, const char* format, ...) {
    va_list argptr;
    // Write to stderr
    va_start(argptr, format);
    vfprintf(stderr, format, argptr);
    fprintf(stderr, "\n");

    /* Write to file */
    // https://stackoverflow.com/questions/19009094/c-variable-arguments-with-stdstring-only
    // initialize use of the variable argument array
    va_start(argptr, format);

    // Get size
    va_list vaCopy;
    va_copy(vaCopy, argptr);
    const int iLen = std::vsnprintf(NULL, 0, format, vaCopy);
    va_end(vaCopy);

    // Convert to string
    std::vector<char> zc(iLen + 1);
    std::vsnprintf(zc.data(), zc.size(), format, argptr);
    va_end(argptr);
    std::string s = std::string(zc.data(), zc.size());
    // Write to file
    logStream.open(logFile, ofstream::out | ofstream::app);
    logStream << s << std::endl;
    logStream.close();
  }
        
void log_if(bool expression, const Level level, const char* format, ...) {
  if (expression) {
    va_list arguments;
    va_start(arguments, format);
    log(level, format, arguments);
    va_end(arguments);
  }
}

private:
  std::string logFile;
  ofstream logStream;

}; //class logger
