# C++ app
Compile and run.
```
make
./main
```
Test: http://localhost:5000/health

## Examples
http://localhost:5000/health 
http://localhost:5000/marketcap/AAPL 

Lists:
http://localhost:5000/listchangeperc
http://localhost:5000/listtengain


## API

### /detail/{detail: [a-zA-Z]*}/{symbols: [a-zA-Z,]*}
### /filings/{symbol: [a-zA-Z]*}

### /health
Return 'OK'

### /list/{type: (winners|losers)}
### /list/{type: [a-z]*}/{info: (full|list)}
### /listchangeperc
### /marketcap/{symbol: [a-zA-Z]*}
### /p
### /p/{info: list}
### /pennystock/losers
### /pennystock/losers/{date: [0-9]*}
### /pennystock/winners
### /pennystock/winners/{perc: [0-9]*}
### /pennystock/winners/{perc: [0-9]*}/{date: [0-9]*}
