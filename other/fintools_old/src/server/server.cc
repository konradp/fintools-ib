#include <algorithm>
#include <cstdlib>
#include <iomanip> // std::setprecision
#include <iostream>
#include <ifstream>
#include <memory> // make_shared, shared_ptr
#include <restbed>
#include <vector>

#include <tinynet/tinynet.hpp>
#include <tinyxml2.h>

#include <Nsdq.hpp>
#include <Symb.h>
#include "cfgreadcpp.hpp"
#include "logger.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::make_shared;
using std::map;
using std::pair;
using std::shared_ptr;
using std::string;
using std::stringstream;
using std::vector;
using tinyxml2::XMLDocument;
using tinyxml2::XMLElement;
using namespace restbed;

// Config
// this is a hack for CORS
string AllowedOrigin = "*";
string kCfgFile = "/opt/fintools/config/config";
Nsdq* nsdq;
char const *pName;
bool isHttps = false;

/* HELPERS */
void usage() {
  cout << "Usage: " << pName << " PORT [https]" << endl
       << "Example: " << pName << " 80" << endl
       << "Example: " << pName << " 80 https" << endl
       << "Note: If 'https' is enabled, the following"
       << " two files are required:" << endl
       << "CFG_DIR/cert/server.key" << endl
       << "CFG_DIR/cert/server.crt" << endl
       << "where CFG_DIR is defined as cfg_dir in /opt/fintools/config/config"
       << endl;
}

void die(string msg) {
  cerr << msg << endl;
  usage();
  exit(1);
}

vector<string>
StringSplit(string delimiter, string text) {
  // Split string on delimiter
  vector<string> v;
  size_t m = text.find(delimiter);
  while(m != string::npos) {
    v.push_back(text.substr(0, m)); // text until delimiter
    text = text.substr(m + delimiter.size(), string::npos); // rest
    m = text.find(delimiter); // update position
  }
  if(text.size() > 0) v.push_back(text);
  return v;
};

bool Compare(
  const pair<string, float>& a,
  const pair<string, float>& b
) {
  return a.second > b.second;
};

bool CompareLosers(
  const pair<string, float>& a,
  const pair<string, float>& b
) {
  return a.second < b.second;
};

string getElementStr(XMLElement* e) {
  // getElement string from XML element
  if(e != NULL) return e->GetText();
  else return "";
};

string _JsonToStr(Json::Value v) {
  string r;
  stringstream ss;
  ss << v;
  return ss.str();
};
/* HELPERS END */


/* ENDPOINTS */
// health
void
_Health_handler(const shared_ptr<Session> session) {
  session->close(OK, "OK", { { "Content-Length", "2" } });
  return;
};

// marketcap
void
_Marketcap_handler(const shared_ptr<Session> session) {
  const auto request = session->get_request();
  string name = request->get_path_parameter("symbol");
  Symb symbol = nsdq->GetSymb(name);

  cout << std::fixed << std::setprecision(0);
  double mCap = symbol.market_cap;
  std::ostringstream mCapStream;
  mCapStream << std::fixed << std::setprecision(0) << mCap;
  int len = mCapStream.str().length();

  session->close(
    OK,
    mCapStream.str(),
    {
      { "Content-Length", std::to_string(len) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
    }
  );
  return;
};

// Lists
void
_ListChangePerc_handler(const shared_ptr<Session> session) {
  vector<Symb> symbols = nsdq->GetSymbList();
  map<string, float> changes;
  Json::Value resJson, i;
  for(auto symbol: symbols) {
    try {
      i["name"] = symbol.ticker;
      stringstream ss;
      ss << std::fixed << std::setprecision(2) << symbol.change_perc;
      i["value"] = ss.str();
      resJson.append(i);
    } catch(std::exception &e) {
      cerr << e.what() << endl;
    }
  }

  string res = _JsonToStr(resJson);
  session->close(OK, res,
    { { "Content-Length", std::to_string(res.length()) } }
  );
  return;
};

// Gainers greater than 10%
void
_ListTenGainPerc_handler(const shared_ptr<Session> session) {
  const auto request = session->get_request();
  string type = request->get_path_parameter("type");
  string info = request->get_path_parameter("info");
  vector<Symb> symbols = nsdq->GetSymbList();
  vector<pair<string, float>> v = {};

  Json::Value resJson, i;
  for(auto symbol: symbols) {
    try {
      if(type == "losers") {
        // Type: Losers
        if(symbol.change_perc <= -10
          && symbol.latest_price <= 1
          && symbol.company_name != ""
        ) {
          v.push_back({symbol.ticker, symbol.change_perc});
        }
      } else {
        // Type: Gainers (default)
        if(symbol.change_perc >= 10
          && symbol.latest_price <= 1
          && symbol.industry != ""
          && symbol.sector != ""
        ) {
          v.push_back({symbol.ticker, symbol.change_perc});
        }
      }
    } catch(std::exception &e) {
      cerr << e.what() << endl;
    }
  }
  // Sort
  if(type == "losers") {
    std::sort(v.begin(), v.end(), CompareLosers);
  } else {
    std::sort(v.begin(), v.end(), Compare);
  }

  for(auto l: v) {
    i["symbol"] = l.first;
    stringstream tmp;
    tmp << std::fixed << std::setprecision(2) << l.second;
    i["changePerc"] = tmp.str();
    Symb symbol = nsdq->GetSymb(l.first);
    tmp << symbol.latest_price;
    i["latestPrice"] = tmp.str();
    i["sector"] = symbol.sector;
    if(info == "full") {
      // Full info
      i["companyName"] = symbol.company_name;
      i["description"] = symbol.desc;
      i["industry"] = symbol.industry;
      i["marketCap"] = symbol.market_cap;
      i["sector"] = symbol.sector;
      i["website"] = symbol.website;
    }
    resJson.append(i);
  }
  // Return
  stringstream ss;
  string res;
  if(info == "list") {
    // Symbol list
    for(auto i : resJson) {
      ss << i["symbol"].asString() << ",";
    }
  } else {
    // Json table
    ss << resJson;
  }
  res = ss.str();
  session->close(OK, res, {
      { "Content-Length", std::to_string(res.length()) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
  });
  return;
};

// Penny stock
void
hPenny(const shared_ptr<Session> session) {
  const auto request = session->get_request();
  string info = request->get_path_parameter("info");

  vector<Symb> symbols = nsdq->GetSymbList();
  Json::Value resJson, i;
  vector<pair<string, float>> v = {};
  stringstream tmp;
  tmp << std::fixed << std::setprecision(2);

  for(auto symbol: symbols) {
    try {
      if(symbol.price <= 1
        && symbol.industry != ""
        && symbol.sector != ""
      ) {
        i["symbol"] = symbol.ticker;
        tmp.str("");
        tmp<< symbol.latest_price;
        i["price"] = tmp.str();
        tmp.str("");
        tmp<< symbol.change_perc;
        i["changePerc"] = tmp.str();
        resJson.append(i);
      }
    } catch(std::exception &e) {
      cerr << e.what() << endl;
    }
  } //for

  // Return
  stringstream ss;
  string res;

  // List
  if(info == "list") {
    // Symbol list
    for(auto i : resJson) {
      ss << i["symbol"].asString() << ",";
    }
    res = ss.str();
  } else {
    res = _JsonToStr(resJson);
  }

  session->close(OK, res,
    {
      { "Content-Length", std::to_string(res.length()) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
    }
  );
  return;
};

// Penny stock losers
void
hPennyLoss(const shared_ptr<Session> session) {
  vector<Symb> symbols = nsdq->GetSymbList();
  Json::Value resJson, i;
  vector<pair<string, float>> v = {};
  stringstream tmp;
  tmp << std::fixed << std::setprecision(2);

  const auto request = session->get_request();
  string date = request->get_path_parameter("date");

  for(auto symbol : symbols) {
    try {
      //if(date != "") { symbol.SetDate(date); }
      if(symbol.price <= 1
          && symbol.industry != ""
          && symbol.sector != ""
          && symbol.change_perc < 0
      ) {
        i["symbol"] = symbol.ticker;
        tmp.str("");
        tmp<< symbol.latest_price;
        i["price"] = tmp.str();
        tmp.str("");
        tmp<< symbol.change_perc;
        i["changePerc"] = tmp.str();
        resJson.append(i);
      }
    } catch(std::exception &e) {
      cerr << e.what() << endl;
    }
  } //for

  string res = _JsonToStr(resJson);
  session->close(OK, res,
    {
      { "Content-Length", std::to_string(res.length()) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
    }
  );
  return;
};

// Penny stock winners
void
hPennyWin(const shared_ptr<Session> session) {
  vector<Symb> symbols = nsdq->GetSymbList();
  Json::Value resJson, i;
  vector<pair<string, float>> v = {};
  stringstream tmp;
  tmp << std::fixed << std::setprecision(2);

  // Read date from file
  string date;

  const auto request = session->get_request();
  string date = request->get_path_parameter("date");
  string percStr = request->get_path_parameter("perc");
  int perc;
  if(percStr != "") {
    perc = std::stoi(percStr);
  } else {
    perc = 0;
  }

  for(auto s: symbols) {
    try {
      if(s.price <= 1 && s.industry != "" && s.sector != ""
          && s.change_perc > perc
      ) {
        i["symbol"] = s.ticker;
        tmp.str("");
        tmp << s.latest_price;
        i["price"] = tmp.str();
        tmp.str("");
        tmp << s.change_perc;
        i["changePerc"] = tmp.str();
        i["sector"] = s.sector;
        resJson.append(i);
      }
    } catch(std::exception &e) {
      cerr << e.what() << endl;
    }
  } //for

  string res = _JsonToStr(resJson);
  session->close(OK, res,
    {
      { "Content-Length", std::to_string(res.length()) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
    }
  );
  return;
};

// Batch request: /detail/symbol1,symbol2,symbol3
void
_Detail_handler(const shared_ptr<Session> session) {
  const auto request = session->get_request();
  string detail = request->get_path_parameter("detail");
  string symbols = request->get_path_parameter("symbols");

  vector<string> symbolList = StringSplit(",", symbols);
  Json::Value resJson, i;
  stringstream ns;
  ns << std::fixed << std::setprecision(2);

  for(auto symbol: symbolList) {
    Symb s = nsdq->GetSymb(symbol);
    ns.str("");

    i["symbol"] = symbol;
    if(detail == "changePercent") {
      i["detail"] = s.change_perc;
    } else if(detail == "companyName") {
      i["detail"] = s.company_name;
    } else if(detail == "description") {
      i["detail"] = s.desc;
    } else if(detail == "industry") {
      i["detail"] = s.industry;
    } else if(detail == "latestPrice") {
      ns << s.latest_price;
      i["detail"] = ns.str();
    } else if(detail == "marketCap") {
      ns << s.market_cap;
      i["detail"] = ns.str();
    } else if(detail == "sector") {
      i["detail"] = s.sector;
    } else if(detail == "website") {
      i["detail"] = s.website;
    } else {
      // TODO: Handle error
      cout << "Other" << endl;
    }
    resJson.append(i);
  }

  string res = _JsonToStr(resJson);
  session->close(OK,res,
    {
      { "Content-Length", std::to_string(res.length()) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
    }
  );
  return;
};

// SEC filings
void
_Filings_handler(const shared_ptr<Session> session) {
  const auto request = session->get_request();
  string symbol = request->get_path_parameter("symbol");
  Json::Value resJson, i;
  Net n;
  string page;
  string res;
  string url = string("https://www.sec.gov/cgi-bin/browse-edgar")
             + "?action=getcompany"
             + "&CIK=" + symbol
             + "&type="
             + "&dateb="
             + "&owner=exclude"
             + "&count=40"
             + "&output=atom";
  try {
    page = n.Get(url);
  } catch(std::exception &e) {
    cerr << "Could not get file: " << e.what() << endl;
  }
  // Parse
  XMLDocument doc;
  int err = doc.Parse(page.c_str());
  if(err != tinyxml2::XML_SUCCESS) {
    cerr << "Could not parse XML: " << url << endl;
    res = string("{ Could not parse XML: ") + url;
    session->close(INTERNAL_SERVER_ERROR, res,
      {
        { "Content-Length", std::to_string(res.length()) },
        { "Access-Control-Allow-Origin", AllowedOrigin }
      }
    );
    return;
  }
  // Analyse
  XMLElement* r = doc.FirstChildElement("feed");
  if(r == NULL) die("Error parsing");
  XMLElement* entry = r->FirstChildElement("entry");
  XMLElement* c;
  while(entry != NULL) {
    c = entry->FirstChildElement("content");
    i["date"] = getElementStr(c->FirstChildElement("filing-date"));
    i["type"] = getElementStr(c->FirstChildElement("filing-type"));
    i["name"] = getElementStr(c->FirstChildElement("form-name"));
    resJson.append(i);
    entry = entry->NextSiblingElement("entry");
  }
  res = _JsonToStr(resJson);
  session->close(OK, res,
    {
      { "Content-Length", std::to_string(res.length()) },
      { "Access-Control-Allow-Origin", AllowedOrigin }
    }
  );
  return;
};


// MAIN
int main(int argc, char *argv[]) {
  // Check params
  pName = argv[0];
  if(argc != 1) die("Invalid number of parameters");
  int port;
  Service service;

  // Config
  string cfg_dirCert,
         cfg_dirData,
         cfg_dirLog,
         cfg_sslEnabled,
         cfg_port;
  try {
    CfgRead cfg;
    cfg.SetPath(kCfgFile);
    cfg_dirCert = cfg.GetValue("dir_cert");
    cfg_dirData = cfg.GetValue("dir_data");
    cfg_dirLog = cfg.GetValue("dir_log");
    cfg_sslEnabled = cfg.GetValue("ssl_enabled");
    cfg_port = cfg.GetValue("port");
    port = std::stoi(cfg_port);
    if(cfg_sslEnabled == "True")
      isHttps = true;
    else
      isHttps = false;
  } catch(const std::exception& e) {
    std::cerr << "Failed reading config: " << std::endl
    << e.what() << std::endl;
  }
  nsdq = new Nsdq(cfg_dirData);

  // ENDPOINTS
  auto aHealth = make_shared<Resource>();
  aHealth->set_path("/health");
  aHealth->set_method_handler("GET", _Health_handler);
  service.publish(aHealth);

  // Penny stock
  auto aPenny = make_shared<Resource>();
  aPenny->set_paths({
    "/p",
    "/p/{info: list}"
  });
  aPenny->set_method_handler("GET", hPenny);
  service.publish(aPenny);

  // Penny stock losers
  auto aPennyLoss = make_shared<Resource>();
  aPennyLoss->set_paths({
    "/pennystock/losers",
    "/pennystock/losers/{date: [0-9]*}"
  });
  aPennyLoss->set_method_handler("GET", hPennyLoss);
  service.publish(aPennyLoss);

  // Penny stock winners
  auto aPennyWin = make_shared<Resource>();
  aPennyWin->set_paths({
    "/pennystock/winners",
    "/pennystock/winners/{perc: [0-9]*}",
    "/pennystock/winners/{perc: [0-9]*}/{date: [0-9]*}"
  });
  aPennyWin->set_method_handler("GET", hPennyWin);
  service.publish(aPennyWin);

  auto aMarketcap = make_shared<Resource>();
  aMarketcap->set_path("/marketcap/{symbol: [a-zA-Z]*}");
  aMarketcap->set_method_handler("GET", _Marketcap_handler);
  service.publish(aMarketcap);

  // Lists
  auto aListChangePerc = make_shared<Resource>();
  aListChangePerc->set_path("/listchangeperc");
  aListChangePerc->set_method_handler("GET", _ListChangePerc_handler);
  service.publish(aListChangePerc);

  // Winners/losers
  auto aListTenGainPerc = make_shared<Resource>();
  aListTenGainPerc->set_paths({
    "/list/{type: (winners|losers)}",
    "/list/{type: [a-z]*}/{info: (full|list)}"
  });
  aListTenGainPerc->set_method_handler("GET", _ListTenGainPerc_handler);
  service.publish(aListTenGainPerc);

  // Batch requests
  auto aDetail = make_shared<Resource>();
  aDetail->set_path("/detail/{detail: [a-zA-Z]*}/{symbols: [a-zA-Z,]*}");
  aDetail->set_method_handler("GET", _Detail_handler);
  service.publish(aDetail);

  // SEC filings
  auto aFilings = make_shared<Resource>();
  aFilings->set_path("/filings/{symbol: [a-zA-Z]*}");
  aFilings->set_method_handler("GET", _Filings_handler);
  service.publish(aFilings);

  // Settings
  auto settings = make_shared<Settings>();
  settings->set_port(port);
  settings->set_default_header("Connection", "close");
  if(isHttps) {
    auto ssl = make_shared<SSLSettings>();
    ssl->set_http_disabled(true);
    ssl->set_private_key(Uri("file://" + cfg_dirCert + "/server.key"));
    ssl->set_certificate(Uri("file://" + cfg_dirCert + "/server.crt"));
    settings->set_ssl_settings(ssl);
  }
  // Logger
  service.set_logger(make_shared<CustomLogger>(cfg_dirLog));
  service.start(settings);
  return EXIT_SUCCESS;
}
