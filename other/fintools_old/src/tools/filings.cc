// This downloads the RSS feed of publications from SEC
// for a given company, and prints out the data
// in tab-separated table

#include <iostream>
#include <tinynet/tinynet.hpp>
#include <tinyxml2.h>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using tinyxml2::XMLDocument;
using tinyxml2::XMLElement;
char const *pName;

void
die(string msg) {
  cerr << msg << endl;
  exit(1);
}

void
usage() {
  cout << "Usage: " << pName << " SYMBOL" << endl
       << "Example: " << pName << " AAPL" << endl;
  exit(1);
}

// Utility method
string
getElementStr(XMLElement* e) {
  if(e != NULL) return e->GetText();
  else return "";
};

int
main(int argc, char *argv[])
{
  // Check params
  bool isDebug = false;
  pName = argv[0];
  if(argc < 2 || argc > 3) usage();
  // Debug
  if(argc == 3) {
    if(strcmp(argv[2], "-d") == 0) isDebug = true;
    else usage();
  }

  // Download
  Net n;
  string page;
  string url = string("https://www.sec.gov/cgi-bin/browse-edgar")
             + "?action=getcompany"
             + "&CIK=" + argv[1]
             + "&type="
             + "&dateb="
             + "&owner=exclude"
             + "&count=40"
             + "&output=atom";

  try {
    page = n.Get(url);
    // Debug mode: print and exit
    if(isDebug) {
      cout << page << endl;
      exit(0);
    }
  } catch(std::exception &e) {
    cerr << "Could not get file: " << e.what() << endl;
  }

  // Parse
  XMLDocument doc;
  int err = doc.Parse(page.c_str());
  if(err != tinyxml2::XML_SUCCESS) die("Could not load file");

  // Analyse
  XMLElement* r = doc.FirstChildElement("feed");
  if(r == NULL) die("Error parsing");

  XMLElement* entry = r->FirstChildElement("entry");
  XMLElement* c;
  while(entry != NULL) {
    c = entry->FirstChildElement("content");
    cout
      << getElementStr(c->FirstChildElement("filing-date"))
      << '\t'
      << getElementStr(c->FirstChildElement("filing-type"))
      << '\t'
      << getElementStr(c->FirstChildElement("form-name"))
      << endl;
    // TODO: create a helper method for GetText, checking null
    entry = entry->NextSiblingElement("entry");
  }
}
