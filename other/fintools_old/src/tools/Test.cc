// From the list of all IEX companies, select only
// these which trade on Nasdaq exchange

#include <iostream>
#include <string>
#include <vector>
#include "FileSystem.hpp"
#include "Iex.hpp"
#include "Nsdq.hpp"
#include "Symb.h"
#include "jsoncpp/json/json.h"

using std::cerr;
using std::cout;
using std::endl;
using std::pair;
using std::string;
using std::vector;
char const *pName;

void die(string msg) {
    cerr << msg << endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    try {
        cout << "Create Nsdq obj" << endl;
        Nsdq nsdq("/opt/fintools/data");
        cout << "Get list" << endl;
        Symb s = nsdq.GetSymb("NAME1");
        cout << "Ticker: " << s.ticker << endl;
        cout << "Sector: " << s.sector << endl;
        vector<Symb> symbols = nsdq.GetSymbList();
        for(auto symbol: symbols) {
          cout << symbol.ticker << endl;
        }
    } catch(std::exception &e) {
        die(e.what());
    }
}
