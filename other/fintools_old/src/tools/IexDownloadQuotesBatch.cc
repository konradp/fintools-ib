// Download quotes to filesystem

#include <iostream>
#include <string>
#include "FileSystem.hpp"
#include "Iex.hpp"
#include "cfgreadcpp.hpp"
#include "Nsdq.hpp"
#include "jsoncpp/json/json.h"

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::vector;
char const *pName;
string kCfgFile = "/opt/fintools/config/config";
string kCfgToken = "";

void die(string msg) {
    cerr << msg << endl;
    exit(1);
}

int main(int argc, char *argv[]) {
    try {
        // Read cfg, get token
        CfgRead cfg;
        cfg.SetPath(kCfgFile);
        kCfgToken = cfg.GetValue("iex_api_token");

        // Get quotes
        Nsdq nsdq("/opt/fintools/data");
        vector<string> symbols = nsdq.GetSymbListStr();
        FileSystem f("/opt/fintools/data");
        cout << "Getting quotes" << endl;
        Iex iex(kCfgToken);
        std::map<string, Json::Value>
          quotes = iex.GetQuotesBatchJson(symbols);
        cout << "Writing quotes" << endl;
        for(auto i: quotes) {
            f.WriteQuote(i.second);
        }
        cout << endl;
    } catch(std::exception &e) {
        die(e.what());
    }
}
