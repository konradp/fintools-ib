// From the list of all IEX companies, select only
// these which trade on Nasdaq exchange

#include <iostream>
#include <string>
#include <vector>
#include "FileSystem.hpp"
#include "Iex.hpp"
#include "Nsdq.hpp"
#include "cfgreadcpp.hpp"
#include "jsoncpp/json/json.h"

using std::cerr;
using std::cout;
using std::endl;
using std::pair;
using std::string;
using std::vector;
char const *pName;
string kCfgFile = "/opt/fintools/config/config";
string kCfgToken = "";

void die(string msg) {
    cerr << msg << endl;
    exit(1);
}

// This takes an Iex list (of all exchanges) and picks out
// only the Iex symbols
int main(int argc, char *argv[]) {
    try {
        // Read cfg, get token
        CfgRead cfg;
        cfg.SetPath(kCfgFile);
        kCfgToken = cfg.GetValue("iex_api_token");

        // Get list
        cout << "Get NASDAQ list" << endl;
        FileSystem fs("/opt/fintools/data");
        Iex iex(kCfgToken);
        Nsdq nsdq("/opt/fintools/data");
        cout << "Get Iex symbol list" << endl;
        vector<string> symbols = iex.GetSymbStrList();
        cout << "Get company info" << endl;
        std::map<string, Json::Value>
          companies = iex.GetCompanyBatchJson(symbols);

        cout << "Get companies exchange" << endl;
        string tmp;
        vector<pair<string, string>> newSymbols;
        for(auto c: companies) {
          // Select symbol where exchange=NASDAQ
          tmp = c.second["exchange"].asString().substr(0, 6);
          if(tmp == "Nasdaq" || tmp == "NASDAQ") {
            fs.WriteCompany(c.second);
            newSymbols.push_back({
              c.second["symbol"].asString(),
              c.second["companyName"].asString()
            });
          }
        }
        nsdq.WriteList(newSymbols);
    } catch(std::exception &e) {
        die(e.what());
    }
}
