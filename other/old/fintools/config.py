# Use me as:
#   from config import Config
#   cfg['section']['field']
from configparser import ConfigParser

FILE_CFG = '/etc/fintools.ini'

class Config(ConfigParser):
  def __init__(self):
    ConfigParser.__init__(self)
    self.read(FILE_CFG)
