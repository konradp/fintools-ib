from dataclasses import dataclass
from datetime import date

@dataclass
class Stock:
  ticker: str
  name: str = None
  industry: str = None
  market_cap: int = None
  shares_outstanding: int = None

@dataclass
class Quote:
  ticker: str
  quote_date: date
  price_open: float
  price_high: float
  price_low: float
  price_close: float
  volume: int
  def __post_init__(self):
    if type(self.quote_date) is str:
      self.quote_date = date.fromisoformat(self.quote_date)
