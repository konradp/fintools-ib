# Get and filter stocks from finnhub.io
# - total stocks: 5737
# - 'type=Common Stock': 3510 stocks
import finnhub
import sys
sys.path.append('..')
from config import Config

class Finnhub():
  client = None
  def __init__(self) -> None:
    cfg = Config()
    api_key = cfg['finnhub']['api_key']
    self.client = finnhub.Client(api_key=api_key)

  # PUBLIC
  def fetch_nasdaq_symbols(self) -> list:
    # Example symbol:
    # {
    #   "currency": "USD",
    #   "description": "APPLE INC",
    #   "displaySymbol": "AAPL",
    #   "figi": "BBG000B9XRY4",
    #   "isin": null,
    #   "mic": "XNAS",
    #   "shareClassFIGI": "BBG001S5N8V8",
    #   "symbol": "AAPL",
    #   "symbol2": "",
    #   "type": "Common Stock"
    # },
    symbols = self.client.stock_symbols(
      exchange='US',
      mic='XNAS',
      security_type='Common Stock',
    )
    return symbols

  def fetch_stock(self, symbol: str) -> dict:
    # {
    #   "country": "US",
    #   "currency": "USD",
    #   "exchange": "NASDAQ NMS - GLOBAL MARKET",
    #   "finnhubIndustry": "Technology",
    #   "ipo": "1980-12-12",
    #   "logo": "https://static2.finnhub.io/file/publicdatany/finnhubimage/stock_logo/AAPL.svg",
    #   "marketCapitalization": 2219182.483887,
    #   "name": "Apple Inc",
    #   "phone": "14089961010.0",
    #   "shareOutstanding": 15908.1,
    #   "ticker": "AAPL",
    #   "weburl": "https://www.apple.com/"
    # }
    symbol = self.client.company_profile2(
      symbol=symbol,
    )
    if symbol == {}:
      return symbol
    symbol['marketCapitalization'] = int(symbol['marketCapitalization'])
    symbol['shareOutstanding'] = int(symbol['shareOutstanding'])
    return symbol
