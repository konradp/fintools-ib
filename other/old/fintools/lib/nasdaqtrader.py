# Get and filter stocks from:
# - ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt
# ref: https://www.nasdaqtrader.com/trader.aspx?id=symboldirdefs
# raw data:
# Symbol|Security Name|Market Category|Test Issue|Financial Status|Round Lot Size|ETF|NextShares
# AACG|ATA Creativity Global - American Depositary Shares, each representing two common shares|G|N|N|100|N|N
# AACI|Armada Acquisition Corp. I - Common Stock|G|N|N|100|N|N
# AACIU|Armada Acquisition Corp. I - Unit|G|N|N|100|N|NSymbol|Security Name|Market Category|Test Issue|Financial Status|Round Lot Size|ETF|NextShares
# AACG|ATA Creativity Global - American Depositary Shares, each representing two common shares|G|N|N|100|N|N
# AACI|Armada Acquisition Corp. I - Common Stock|G|N|N|100|N|N
# AACIU|Armada Acquisition Corp. I - Unit|G|N|N|100|N|N
# [...]
# giving:
# - 5692 stocks total
# - 5223 stocks for 'Financial Status = N'
# - 3481 stocks for 'type = common stock'

import sys
import urllib.request
from lib.types import Stock
from lib.excluded_stock import excluded_stock
import typing

class NasdaqTrader():
  url = 'ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt'
  def __init__(self) -> None:
    pass

  # PUBLIC
  def fetch_stocks(self, stock_type: str='common stock') -> typing.List[Stock]:
    # type = string, options:
    # - 'American depositary shares'
    # - 'common stock'
    # - 'ETF'
    # - 'fund'
    # - 'UNKNOWN'
    # - None
    # Example: type='common stock'
    # Example: type=['common stock', 'ETF'
    #with open('nasdaqlisted.txt') as f: # DEBUG
    with urllib.request.urlopen(self.url) as response:
      #debug_text = [line.rstrip('\n') for line in f][1:-1] # DEBUG
      symbols = []
      #for line in debug_text: # DEBUG
      for line in response.read().decode('utf-8').splitlines()[1:-1]:
        fields = line.split('|')
        #symbol = {
        #  'symbol': fields[0],
        #  'name': fields[1],
        #}
        symbol = Stock(
          ticker=fields[0],
          name=fields[1],
        )
        if not self._is_symbol_valid(symbol):
          continue
        if fields[3] != 'N':
          # Skip test stocks ('Test Issue' field)
          continue
        symbol = self._guess_type(symbol)
        if stock_type is not None:
          if symbol.type != stock_type:
            continue
        #symbols.append(symbol)
        symbols.append({
          'ticker': symbol.ticker,
          'name': symbol.name,
        })
      return symbols


  def _is_symbol_valid(self, stock: Stock) -> bool:
    # Exclude various/invalid companies, e.g. those whose names end with
    # '- Warrants'
    # Return true if symbol is valid, false if not
    if stock.name.endswith((
      '- Rights',
      '- Subunit',
      '- Subunits',
      '- Trust Preferred Securities',
      '- Unit',
      '- Units',
      '- Warrant',
      '- Warrants',
      )) \
      or '.' in stock.ticker:
      return False
    if stock.ticker in excluded_stock:
      return False
    return True

  def _guess_type(self, stock: Stock):
    name = stock.name
    stock.type = 'UNKNOWN'
    if name.endswith(' ETF'):
      # ETF
      stock.type = 'ETF'
    elif name.lower().endswith(' - common shares') \
      or name.lower().endswith(' - common stock') \
      or name.lower().endswith(' - ordinary share') \
      or name.lower().endswith(' - ordinary shares')  \
      or name.lower().endswith(' - class a common stock') \
      or name.lower().endswith(' - class a common shares') \
      or name.lower().endswith(' - class a common share') \
      or name.lower().endswith(' - class a ordinary shares') \
      or name.lower().endswith(' - class a ordinary share') \
      or name.lower().endswith(' - class a ordinary shre') \
      or name.lower().endswith(' - class a shares') \
      or name.lower().endswith(' -  ordinary shares, class a common stock'):
      # common stock (includes class A)
      stock.type = 'common stock'
      stock.name = name.split(' - ')[0]
    elif name.endswith(' - American Depositary Shares') \
      or name.endswith(' - American Depository Shares'):
      # American depositary shares
      stock.type = 'American depositary shares'
      stock.name = name.split(' - ')[0]
    elif name.endswith(' Fund'):
      stock.type = 'fund'
    return stock

