# Database connection
# TODO: Cache the industry ids
import sys
import mysql.connector
from typing import List

sys.path.append('..')
from config import Config
from lib.types import Quote
from lib.types import Stock

class Db():
  cnx = None
  cursor = None
  def __init__(self) -> None:
    cfg = Config()
    self.cnx = mysql.connector.connect(
      host=cfg['db']['hostname'],
      database=cfg['db']['database'],
      user=cfg['db']['user'],
      password=cfg['db']['password'],
    )
    self.cursor = self.cnx.cursor(dictionary=True)


  # GETTERS
  def get_industry_id(self, name: str) -> int:
    c = self.cursor
    c.execute("""
      SELECT id from industries
      WHERE name = %s
      LIMIT 1
    """, (name,))
    industry = c.fetchone()
    return None if industry is None else industry['id']


  def get_last_quote_date(self) -> str:
    c = self.cursor
    c.execute("""
      SELECT quote_date from quotes
      ORDER BY quote_date desc
      LIMIT 1
    """)
    quote = c.fetchone()
    if quote is None:
      return quote
    quote_date = quote['quote_date']
    return quote_date


  def get_quote(self, ticker: str, date: str) -> Quote:
    c = self.cursor
    c.execute("""
      SELECT *
      FROM quotes
      WHERE ticker = %s
      AND quote_date = %s
      LIMIT 1
    """, (ticker, date,))
    quote = c.fetchone()
    if quote is None:
      return quote
    return Quote(
      quote['ticker'],
      quote['quote_date'],
      float(quote['open']),
      float(quote['high']),
      float(quote['low']),
      float(quote['close']),
      quote['volume'],
    )


  def get_quote_dates(self) -> str:
    c = self.cursor
    c.execute("""
      SELECT
        DISTINCT quote_date
      from quotes
      ORDER BY quote_date desc
    """)
    dates = c.fetchall()
    return [ str(q['quote_date']).replace('-','') for q in dates ]


  def get_stock(self, stock: Stock) -> Stock:
    c = self.cursor
    c.execute("""
      SELECT
        s.*,
        i.name i_name
      FROM stocks s
      LEFT JOIN industries i
        ON s.industry_id = i.id
      WHERE ticker = %s
      LIMIT 1
    """, (stock.ticker,))
    stock = c.fetchone()
    if stock is None:
      return stock
    stock = Stock(
      stock['ticker'],
      stock['name'],
      stock['i_name'],
    )
    return stock


  def get_stock_by_ticker(self, ticker: str) -> Stock:
    c = self.cursor
    c.execute("""
      SELECT
        s.*,
        i.name i_name
      FROM stocks s
      LEFT JOIN industries i
        ON s.industry_id = i.id
      WHERE ticker = %s
      LIMIT 1
    """, (ticker,))
    stock = c.fetchone()
    if stock is None:
      return stock
    return Stock(
      stock['ticker'],
      stock['name'],
      stock['i_name'],
    )


  def get_tables(self) -> list:
    c = self.cnx.cursor(dictionary=False)
    c.execute('show tables;')
    tables = [ table[0] for table in c.fetchall() ]
    return tables


  def get_table(self, table) -> dict:
    # Unsafe method due to the 'table' var in the query
    # There should be a list of 'safe' tables
    c = self.cursor
    c.execute(f"SELECT * FROM {table};")
    return c.fetchall()


  def get_stocks_to_fh_update(self) -> List[Stock]:
    c = self.cursor
    c.execute("""
      SELECT
        s.*,
        i.name industry
      FROM stocks s
      LEFT JOIN industries i
        ON s.industry_id = i.id
      WHERE industry_id IS NULL
      OR market_cap IS NULL
      OR shares_outstanding IS NULL
    ;""")
    stocks = [
      Stock(
        ticker=stock['ticker'],
        name=stock['name'],
        industry=stock['industry'],
        market_cap=stock['market_cap'],
        shares_outstanding=stock['shares_outstanding'],
      )
      for stock in c.fetchall()
    ]
    return stocks


  # SETTERS/ADDERS
  def add_quote(self, quote: Quote) -> None:
    # If there is already a quote in the DB and it's different from
    # the new one, then delete the old one and insert the new one
    # (update)
    cur_quote = self.get_quote(quote.ticker, quote.quote_date)
    if cur_quote is not None:
      # This quote already exists - update if necessary
      if quote == cur_quote:
        # The quote values are the same, so do nothing
        return
      # The quotes are different, delete old quote
      self.del_quote(quote.ticker, quote.quote_date)
    # Add new quote
    c = self.cursor
    c.execute("""
      INSERT INTO quotes (
        ticker,
        quote_date,
        open,
        high,
        low,
        close,
        volume
      )
      VALUES (%s, %s, %s, %s, %s, %s, %s);
    """, (
      quote.ticker,
      quote.quote_date,
      quote.price_open,
      quote.price_high,
      quote.price_low,
      quote.price_close,
      quote.volume,
    ))
    self.cnx.commit()

  def add_stock(self, stock: Stock) -> None:
    c = self.cursor
    c.execute("""
      INSERT INTO stocks (
        ticker,
        name
      )
      VALUES (%s, %s);
    """, (stock.ticker, stock.name))
    self.cnx.commit()


  def add_industry(self, name: str) -> None:
    c = self.cursor
    c.execute("""
      INSERT INTO industries (name)
      VALUES (%s);
    """, (name,))
    self.cnx.commit()


  def update_stock(self, stock: Stock) -> None:
    c = self.cursor
    # Get industry_id (create if doesn't exist)
    if self.get_industry_id(stock.industry) is None:
      self.add_industry(stock.industry)
    industry_id = self.get_industry_id(stock.industry)

    c.execute("""
      UPDATE stocks
      SET
        industry_id = %s,
        market_cap = %s,
        shares_outstanding = %s
      WHERE ticker = %s
    """, (
      industry_id,
      stock.market_cap,
      stock.shares_outstanding,
      stock.ticker))
    self.cnx.commit()


  # DELETERS
  def del_industry(self, name: str) -> None:
    c = self.cursor
    qry = """
      DELETE FROM industries WHERE name = %s
    """
    c.execute(qry, (name,))
    self.cnx.commit()


  def del_quote(self, ticker: str, date: str) -> None:
    c = self.cursor
    qry = """
      DELETE FROM quotes
      WHERE ticker = %s
      AND quote_date = %s
    """
    c.execute(qry, (ticker, date))
    self.cnx.commit()


  def del_stock(self, stock: Stock) -> None:
    c = self.cursor
    qry = """
      DELETE FROM stocks WHERE ticker = %s
    """
    c.execute(qry, (stock.ticker,))
    self.cnx.commit()
