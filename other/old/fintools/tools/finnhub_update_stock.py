import json
import sys

sys.path.append('..')
from lib.db import Db
from lib.finnhub import Finnhub
from lib.nasdaqtrader import NasdaqTrader
from lib.types import Stock

def main():
  fh = Finnhub()
  db = Db()
  ticker = 'AAPL'
  stock = db.get_stock_by_ticker(ticker)
  stock_fh = fh.fetch_stock(ticker)
  stock.industry = stock_fh['finnhubIndustry']
  stock.market_cap = stock_fh['marketCapitalization']
  stock.shares_outstanding = stock_fh['shareOutstanding']
  db.update_stock(stock)
  return 0

if __name__ == '__main__':
  sys.exit(main())
