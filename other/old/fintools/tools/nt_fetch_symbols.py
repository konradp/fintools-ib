import json
import sys
sys.path.append('..')
from lib.nasdaqtrader import NasdaqTrader

def main():
  nasdaqtrader = NasdaqTrader()
  stocks = nasdaqtrader.fetch_stocks()
  #stocks = nasdaqtrader.fetch_stocks(stock_type='ETF')
  print(json.dumps(stocks, indent=2))
  return 0

if __name__ == '__main__':
  sys.exit(main())
