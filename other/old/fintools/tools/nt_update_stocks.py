import json
import sys

sys.path.append('..')
from lib.db import Db
from lib.nasdaqtrader import NasdaqTrader
from lib.types import Stock

def main():
  db = Db()
  nt = NasdaqTrader()
  stocks = nt.fetch_stocks()
  for stock in stocks:
    db.add_stock(stock)
  return 0

if __name__ == '__main__':
  sys.exit(main())
