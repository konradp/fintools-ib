import json
import sys
sys.path.append('..')
from lib.finnhub import Finnhub

def main():
  finnhub = Finnhub()
  stock = finnhub.fetch_stock('AAPL')
  print(json.dumps(stock, indent=2))
  return 0

if __name__ == '__main__':
  sys.exit(main())
