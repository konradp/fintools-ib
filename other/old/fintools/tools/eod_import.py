# Import last day quote data from eoddata.com
# Go to https://www.eoddata.com/download.aspx and press download
# https://www.eoddata.com/data/filedownload.aspx?e=NASDAQ&sd=20221116&ed=20221116&d=4&k=6ngr6vwpgm&o=d&ea=1&p=0
# duration: 1min
import csv
import sys
from glob import glob
from os import path
sys.path.append('..')
from lib.db import Db
from lib.types import Quote

months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
  'Sep', 'Oct', 'Nov', 'Dec' ]

def tidy_date(date: str) -> str:
  # From 11-Nov-2022 to 2022-11-11
  date = date.split('-')
  return '-'.join([date[2], str(months.index(date[1])+1), date[0]])


def main():
  filepath = sorted(glob(f'{path.dirname(path.realpath(__file__))}/data/*.csv'))[-1]
  # For manual imports
  #filepath = 'data/NASDAQ_20221110.csv'
  db = Db()
  dates_db = db.get_quote_dates()
  print(f'Dates in DB: {dates_db}')
  date_file = filepath.split('_')[-1].split('.')[0]
  if date_file in dates_db:
    print(f'There already are quotes for date {date_file} in the DB')
    exit(1)
  print(f'Date to import:   {date_file}')
  yes = input('Proceed (y/n)? ')
  if yes != 'y':
    print('Exitting')
    exit(0)
  print('Importing quotes')
  db_stocks = db.get_table('stocks')
  db_stocks = [ s['ticker'] for s in db_stocks ]
  print('Found', len(db_stocks), 'stocks in DB')
  with open(filepath, newline='') as csvfile:
      reader = csv.DictReader(csvfile)
      updated = 0
      for row in reader:
        if row['Symbol'] not in db_stocks:
          # We don't have this ticker, skip
          continue
        db.add_quote(Quote(
          ticker=row['Symbol'],
          quote_date=tidy_date(row['Date']),
          price_open=row['Open'],
          price_high=row['High'],
          price_low=row['Low'],
          price_close=row['Close'],
          volume=row['Volume'],
        ))
        updated = updated + 1
      print('Updated:', updated)


if __name__ == '__main__':
  sys.exit(main())
