import json
import sys

sys.path.append('..')
from lib.db import Db
from lib.types import Quote
from lib.types import Stock

db = Db()
stocks = [
  Stock(
    ticker='AAAAA',
    name='A company',
  ),
  Stock('BBBBB', 'B company'),
  Stock('CCCCC', 'C company'),
  Stock('DDDDD', 'D company'),
]

def pprint(msg):
  print(json.dumps(msg, indent=2))


def die(msg):
  print('FAIL:', msg)
  industry_name = 'Test industry'
  stock = Stock(ticker='EEEEE', name='E company')
  db.del_stock(stock)
  db.del_industry(industry_name)
  exit(1)


def main():
  print('TEST: get_tables()')
  #print('tables:', db.get_tables())
  tables = db.get_tables()
  if tables != ['industries', 'quotes', 'stocks']:
    die(tables)
  print('Pass')

  print('TEST: add_stock()')
  before = len(db.get_table('stocks'))
  for stock in stocks:
    db.add_stock(stock)
  after = len(db.get_table('stocks'))
  if len(stocks) != after-before:
    die('Adding stocks did not change the number of stocks')
  print('Pass')

  print('TEST: del_stock()')
  for stock in stocks:
    db.del_stock(stock)
  after = len(db.get_table('stocks'))
  if before != after:
    die('Removing stocks did not delete stocks')
  print('Pass')

  industry_name = 'Test industry'
  print('TEST: get_industry_id() for non-existing')
  industry_id = db.get_industry_id(name=industry_name)
  if industry_id is not None:
    die(f'Should have returned "None": {industry_id}')
  print('Pass')

  print('TEST: add_industry()')
  before = len(db.get_table('industries'))
  db.add_industry(industry_name)
  after = len(db.get_table('industries'))
  if after != before+1:
    die('Adding industry should increment count of industries')
  print('Pass')

  print('TEST: get_industry_id() for existing')
  industry = db.get_industry_id(name=industry_name)
  if industry is None:
    die('Industry should have been added')
  print('Pass')

  print('TEST: update_stock()')
  stock = Stock(ticker='EEEEE', name='E company')
  db.add_stock(stock)
  stock.industry = industry_name
  db.update_stock(stock)
  if db.get_stock(stock).industry != industry_name:
    die("Updating stock didn't update the industry")
  db.del_stock(stock)
  print('Pass')

  print('TEST: del_industry()')
  before = len(db.get_table('industries'))
  db.del_industry(industry_name)
  after = len(db.get_table('industries'))
  if after != before-1:
    die('Deleting industry should decrement count')
  print('Pass')

  print('TEST: update_stock() with non-existing industry')
  if db.get_industry_id(name=industry_name) is not None:
    die('Something went wrong, there should be no industry')
  stock = Stock(ticker='EEEEE', name='E company')
  db.add_stock(stock)
  stock.industry = industry_name
  before = len(db.get_table('industries'))
  db.update_stock(stock)
  after = len(db.get_table('industries'))
  if db.get_stock(stock).industry != industry_name:
    die("Updating stock didn't update the industry")
  if after != before+1:
    die("Updating stock didn't create the industry")
  db.del_stock(stock)
  print('Pass')

  #print(len(db.get_stocks_to_fh_update()))
  #if len(db.get_stocks_to_fh_update()) != 0:
  #  die('There should be no stocks to update')
  #print('Pass')
  print('TEST: get_stocks_to_fh_update(): none to update')
  stocks_to_update = db.get_stocks_to_fh_update()
  stock = None
  for s in stocks_to_update:
    if s.ticker == 'EEEEE':
      stock = s
      break
  if stock is not None:
    die("The stock 'EEEEE' should be missing")
  print('Pass')

  print('TEST: get_stocks_to_fh_update(): one or more to update')
  stock = Stock(ticker='EEEEE', name='E company')
  db.add_stock(stock)
  stocks_to_update = db.get_stocks_to_fh_update()
  found = False
  for s in stocks_to_update:
    if s.ticker == 'EEEEE':
      found = True
      stock = s
      break
  if not found:
    die("The stock 'EEEEE' should be present")
  db.del_stock(stock)
  print('Pass')

  print('TEST: get_stocks_to_fh_update(): stock with industry but no market cap')
  stock = Stock(ticker='EEEEE', name='E company')
  db.add_stock(stock)
  stock.industry = industry_name
  db.update_stock(stock)
  stocks_to_update = db.get_stocks_to_fh_update()
  found = False
  for s in stocks_to_update:
    if s.ticker == 'EEEEE':
      found = True
      stock = s
      break
  if stock.industry is None:
    print(stock)
    die("The stock 'EEEEE' should have industry populated")
  db.del_stock(stock)
  print('Pass')


  print('TEST: get_quote(), non-existing')
  quote = db.get_quote(ticker='EEEEE', date='2022-11-14')
  if quote != None:
    print(quote)
    die('This quote should not exist (None expected). But above exists')
  print('Pass')

  print('TEST: add_quote()')
  before = len(db.get_table('quotes'))
  test_quote = Quote(
    ticker='EEEEE',
    quote_date='2022-11-14',
    price_open=10.01,
    price_high=10.0002,
    price_low=10.03,
    price_close=10.04,
    volume=20,
  )
  db.add_quote(test_quote)
  after = len(db.get_table('quotes'))
  if after != before+1:
    die('Adding quote should increase number of quotes')
  print('Pass')


  print('TEST: get_quote(), existing')
  quote = db.get_quote(ticker='EEEEE', date='2022-11-14')
  if quote != test_quote:
    print(test_quote)
    print(quote)
    die('The quote we added is different from the one received')
  print('Pass')


  print('TEST: del_quote()')
  before = len(db.get_table('quotes'))
  db.del_quote(ticker='EEEEE', date='2022-11-14')
  after = len(db.get_table('quotes'))
  if after != before-1:
    die('Deleting quote should decrement number of quotes')
  print('Pass')

  print('TEST: add_quote(): add the same quote twice does nothing')
  test_quote = Quote(
    ticker='EEEEE',
    quote_date='2022-11-14',
    price_open=10.01,
    price_high=10.0002,
    price_low=10.03,
    price_close=10.04,
    volume=20,
  )
  db.add_quote(test_quote)
  before = len(db.get_table('quotes'))
  db.add_quote(test_quote)
  after = len(db.get_table('quotes'))
  if before != after:
    die('Adding the same quote again should not increase count')
  print('Pass')


  print('TEST: add_quote() to update')
  test_quote = Quote(
    ticker='EEEEE',
    quote_date='2022-11-14',
    price_open=10.01,
    price_high=10.0002,
    price_low=10.03,
    price_close=10.04,
    volume=20,
  )
  db.add_quote(test_quote)
  test_quote.price_open = 20.01
  before = len(db.get_table('quotes'))
  db.add_quote(test_quote)
  after = len(db.get_table('quotes'))
  new_quote = db.get_quote(test_quote.ticker, test_quote.quote_date)
  if before != after:
    die('Updating the quote should not affect quote count')
  if new_quote.price_open != 20.01:
    print(new_quote.price_open)
    die('Updating the quote did not work')
  print('Pass')


  # Cleanup
  db.del_stock(stock)
  db.del_industry(industry_name)
  #db.del_quote(test_quote)
  return 0

if __name__ == '__main__':
  sys.exit(main())
