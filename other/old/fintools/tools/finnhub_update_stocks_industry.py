# Updates:
# - industry
# - market_cap
# - shares_outstanding
import json
import sys

sys.path.append('..')
from lib.db import Db
from lib.finnhub import Finnhub
from lib.nasdaqtrader import NasdaqTrader
from lib.types import Stock

def compare_stocks(old: Stock, new: Stock) -> None:
  if old.industry != new.industry:
    print(f"UPDATE: {old.ticker} industry: {old.industry} to {new.industry}")
  if old.market_cap != new.market_cap:
    print(f"UPDATE: {old.ticker} market_cap: {old.market_cap} to {new.market_cap}")
  if old.shares_outstanding != new.shares_outstanding:
    print(f"UPDATE: {old.ticker} shares_outstanding: {old.shares_outstanding} to {new.shares_outstanding}")

def main():
  db = Db()
  fh = Finnhub()
  stocks = db.get_stocks_to_fh_update()
  print(len(stocks))
  for stock in stocks:
    stock_fh = fh.fetch_stock(stock.ticker)
    if stock_fh == {}:
      print(f'ERROR: No FH stock data for {stock.ticker}: {stock_fh}')
      continue
    # Detailed messages
    new_stock = Stock(
      ticker=stock.ticker,
      industry=stock_fh['finnhubIndustry'],
      market_cap=stock_fh['marketCapitalization'],
      shares_outstanding=stock_fh['shareOutstanding'],
    )
    compare_stocks(stock, new_stock)
    db.update_stock(new_stock)
  return 0

if __name__ == '__main__':
  sys.exit(main())
