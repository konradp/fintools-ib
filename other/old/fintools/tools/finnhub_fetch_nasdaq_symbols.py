import json
import sys
sys.path.append('..')
from lib.finnhub import Finnhub

def main():
  finnhub = Finnhub()
  symbols = finnhub.fetch_nasdaq_symbols()
  print(json.dumps(symbols, indent=2))
  return 0

if __name__ == '__main__':
  sys.exit(main())
