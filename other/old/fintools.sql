/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `industries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=330 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `industries_perc_increase` AS SELECT
 1 AS `industry`,
  1 AS `perc`,
  1 AS `cnt` */;
SET character_set_client = @saved_cs_client;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes` (
  `ticker` varchar(100) NOT NULL,
  `quote_date` date DEFAULT NULL,
  `open` decimal(8,4) DEFAULT NULL,
  `high` decimal(8,4) DEFAULT NULL,
  `low` decimal(8,4) DEFAULT NULL,
  `close` decimal(8,4) DEFAULT NULL,
  `volume` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `ticker` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `industry_id` int(11) DEFAULT NULL,
  `market_cap` int(11) DEFAULT NULL,
  `shares_outstanding` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `stocks_perc_increase` AS SELECT
 1 AS `ticker`,
  1 AS `industry`,
  1 AS `perc` */;
SET character_set_client = @saved_cs_client;
/*!50001 DROP VIEW IF EXISTS `industries_perc_increase`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `industries_perc_increase` AS select `stocks_perc_increase`.`industry` AS `industry`,round(avg(`stocks_perc_increase`.`perc`),2) AS `perc`,count(0) AS `cnt` from `stocks_perc_increase` group by `stocks_perc_increase`.`industry` order by round(avg(`stocks_perc_increase`.`perc`),2) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP VIEW IF EXISTS `stocks_perc_increase`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `stocks_perc_increase` AS select `s`.`ticker` AS `ticker`,`i`.`name` AS `industry`,round(100 * (`q`.`close` - `q`.`open`) / `q`.`open`,1) AS `perc` from ((`stocks` `s` join `industries` `i` on(`s`.`industry_id` = `i`.`id`)) join `quotes` `q` on(`s`.`ticker` = `q`.`ticker`)) where `q`.`quote_date` = (select max(`quotes`.`quote_date`) from `quotes`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
