# fintools


# Get NASDAQ symbols
- nasdaqtrader.com: 3481
- finnhub: 3512

# Workflow
- Go to https://www.eoddata.com/download.aspx and download
- ```
  mv ~/Downloads/NASDAQ_202*.csv ~/code/konradp/fintools/fintools/tools/data/
  cd ~/code/konradp/fintools/fintools/tools
  python3 eod_import.py
```
- Go to dashboard: http://localhost:3000

# Setup

# DB
Install and create database
```
sudo apt install mariadb-server

sudo mysql
create database fintools;
CREATE USER 'fintools'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON fintools.* TO 'fintools'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;


cd scripts
./recreate_db.sh
```


## config
/etc/fintools.ini

# Dashboard

- market red/green: overall/average percent
- industries red/green
- which stocks in market are green/red?
- what's more likely to be red:
  - smallcap or largecap?
  - shares outstanding?

