// Load API token
var debug = false;
var token = 'asd';
var base = location.href.substring(0, location.href.lastIndexOf("/")+1);
var kUrlChart = base + '/test_data/';
var kUrlFilings = 'https://finapi.tk/filings/';
var kUrlWinners = base + '/test_data/winners';
var kPlotNames = [];

// CORS Request
function createRequest(method, url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = () => resolve(JSON.parse(xhr.responseText));
    xhr.onerror = () => reject(xhr.statusText);
    xhr.send();
  });
};


// Submit
var checkForm = (e) => {
  e.preventDefault();
  var symbol = document.getElementById('inputSymbol').value;
  console.log('symbol: ' + symbol);
  var chart = new Chart(token);
  chart.setSymbol(symbol);
  chart.getChart('3m')
    .then((data) => {
      chart.drawChart(data);
    })
    .catch((err) => {
      console.log(err);
    });
}


/* GETTERS */
function getChart(ticker, time) {
  if(time == '')
    throw 'Time is not set';
  var url = kUrlChart + ticker + '/chart/' + time + '?token=' + token;
  return new Promise((resolve, reject) => {
    createRequest('GET', url)
      .then((res) => { resolve({ "ticker": ticker, "data": res }); })
      .catch((err) => { reject(err); });
  });
}


function getFilings(symbol) {
  // Output: JSON { symbol, data }
  if(symbol == '') throw 'Symbol is not set';
  return new Promise((resolve, reject) => {
    createRequest('GET', kUrlFilings + symbol)
      .then((r) => {
        // Repad data
        d = [];
        for(var i = 0; i < r.length; i++) {
          d[i] = {
            date: Date.parse(r[i].date),
            value: r[i].type
          };
        }
        if(debug) console.log({symbol: symbol, data: d});
        resolve({ symbol: symbol, data: d });
      }).catch((err) => { reject(err); });
  });
}


function getWinners() {
  console.log("Getting symbols");
  return new Promise((resolve, reject) => {
    createRequest('GET', kUrlWinners)
      .then((res) => { resolve(res); })
      .catch((err) => { reject(err); });
  })
};

var kPlots = [];
function drawPlots(timeRange) {
  kPlotNames.forEach((ticker) => {
    getChart(ticker, timeRange)
      .then((d) => {
        var c = document.getElementById(d.ticker);
        if (c.getContext) {
          var p = new Plot(c.getContext('2d'));
          p.setData(d.data);
          kPlots[d.ticker] = p;
          getFilings(d.ticker).then((filings) => {
            var plot = kPlots[filings.symbol];
            var d = [];
            for (var i in filings.data) {
              d.push({
                "date": new Date(filings.data[i].date)
                .toISOString().slice(0, 10),
                "label": filings.data[i].value,
              });
            }
            plot.setLines(d);
            plot.draw();
          });
          p.draw();
        }
      }); //getChart
  }); //forEach
}


// MAIN
var plots = document.getElementById('plots');
getWinners()
  .then((winners) => {
    for (var i in winners) {
      // List
      var ticker = winners[i].symbol;
      kPlotNames.push(ticker);
      // Title
      var title = document.createElement('div');
      title.innerHTML = ticker;
      plots.appendChild(title);
      // Canvas
      var c = document.createElement('canvas');
      c.id = ticker
      c.style = 'width:100%; height:50%; display: block;';
      c.width = 800;
      c.height = 400;
      plots.appendChild(c);
      plots.appendChild(document.createElement('br'));
    }
    // Initial draw
    drawPlots('3m');
  }); // getWinners
