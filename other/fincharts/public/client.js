// Load API token
var debug = false;
var base = location.href.substring(0, location.href.lastIndexOf("/"));
var kUrlChart = 'https://cloud.iexapis.com/stable/stock/';
var kUrlBase = 'https://cloud.iexapis.com/stable/';
var kUrlWinners = 'https://finapi.tk/pennystock/winners/10';
var kUrlFilings = 'https://finapi.tk/filings/';
var kPlotNames = [];
var kToken = '';

// CORS Request
function createRequest(method, url) {
  var req = new XMLHttpRequest();
  return new Promise((resolve, reject) => {
    req.onreadystatechange = function() {
      //authReceived = true;
      if (req.readyState == 4) {
      } else {
        return;
      }
      if (req.status >= 200 && req.status < 300) {
        resolve(JSON.parse(req.responseText));
      } else {
        reject({
          status: req.status,
          statusText: req.statusText
        });
      }
    };
    req.open(method, url);
    req.send();
  });
};

function auth(token) {
  return new Promise((resolve, reject) => {
    console.log('Authenticating');
    kToken = token;
    createRequest('GET', kUrlBase + 'account/usage' + '?token=' + kToken)
      .then(() => { resolve(); })
      .catch((e) => { reject('Could not authenticate'); })
  });
}


/* GETTERS */
function getChart(ticker, time) {
  if(time == '')
    throw 'Time is not set';
  var url = kUrlChart + ticker + '/chart/' + time + '?token=' + kToken;
  return new Promise((resolve, reject) => {
    createRequest('GET', url)
      .then((res) => { resolve({ "ticker": ticker, "data": res }); })
      .catch((err) => { reject(err); });
  });
}


function getFilings(symbol) {
  // Output: JSON { symbol, data }
  if(symbol == '') throw 'Symbol is not set';
  return new Promise((resolve, reject) => {
    createRequest('GET', kUrlFilings + symbol)
      .then((r) => {
        // Repad data
        d = [];
        for(var i = 0; i < r.length; i++) {
          d[i] = {
            date: Date.parse(r[i].date),
            value: r[i].type
          };
        }
        if(debug) console.log({symbol: symbol, data: d});
        resolve({ symbol: symbol, data: d });
      }).catch((err) => { reject(err); });
  });
}


function getWinners() {
  console.log("Get winners");
  return new Promise((resolve, reject) => {
    createRequest('GET', kUrlWinners)
      .then((res) => { resolve(res); })
      .catch((err) => { reject(err); });
  })
};

var kPlots = [];
function drawPlots(timeRange) {
  kPlotNames.forEach((ticker) => {
    getChart(ticker, timeRange)
      .then((d) => {
        var c = document.getElementById(d.ticker);
        if (c.getContext) {
          var p = new Plot(c.getContext('2d'));
          p.setData(d.data);
          kPlots[d.ticker] = p;
          //getFilings(d.ticker).then((filings) => {
          //  var plot = kPlots[filings.symbol];
          //  var d = [];
          //  for (var i in filings.data) {
          //    d.push({
          //      "date": new Date(filings.data[i].date)
          //      .toISOString().slice(0, 10),
          //      "label": filings.data[i].value,
          //    });
          //  }
          //  plot.setLines(d);
          //  plot.draw();
          //});
          p.draw();
        }
      }); //getChart
  }); //forEach
}


// MAIN
var tokenField = document.getElementById('token');
tokenField.focus();
tokenField.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) { // Enter
    event.preventDefault();
    go();
  }
});


function go() {
  var plots = document.getElementById('plots');
  var kToken = tokenField.value;
  auth(kToken).catch(() => { alert('Could not authenticate') });
  getWinners()
    .then((winners) => {
      console.log('Got winners');
      for (var i in winners) {
        // List
        var ticker = winners[i].symbol;
        kPlotNames.push(ticker);
        // Title
        var title = document.createElement('div');
        title.innerHTML = ticker;
        plots.appendChild(title);
        // Canvas
        var c = document.createElement('canvas');
        c.id = ticker
        c.style = 'width:100%; height:50%; display: block;';
        c.width = 800;
        c.height = 400;
        plots.appendChild(c);
        plots.appendChild(document.createElement('br'));
      }
      // Initial draw
      drawPlots('3m');
    })
    .catch(() => { alert('TIMED OUT'); });
}
