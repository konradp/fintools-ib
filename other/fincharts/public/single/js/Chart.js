class Chart {

constructor(token) {
  this.symbol = '';
  this.chart = '';
  this.token = token;
}

// Getters
getChart(type) {
  if(this.symbol == '')
    throw 'Symbol is not set';
  if(type == '')
    throw 'Type is not set';

  var url = 'https://cloud.iexapis.com/stable/stock/'
    + this.symbol + '/chart/' + type + '?token=' + this.token;
  return new Promise((resolve, reject) => {
    createRequest('GET', url)
      .then((res) => { resolve(res); })
      .catch((err) => { reject(err); });
  });
}

// Setters
setSymbol(v) { this.symbol = v; }

// PRIVATE
get _url() {
};

drawChart(data) {
  // create the chart
  this.chart = Highcharts.stockChart('container', {
    rangeSelector: {
      allButtonsEnabled: true,
      buttons: [
        {
          type: 'day', count: 1, text: '1d',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'month', count: 1, text: '1m',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'month', count: 3, text: '3m',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'month', count: 6, text: '6m',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'ytd', text: 'YTD',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'year', count: 1, text: '1y',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'year', count: 2, text: '2y',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'year', count: 5, text: '5y',
          events: { click: function(e) { updateRange(this.text) } },
        }, {
          type: 'all', text: 'All',
          events: { click: function(e) { updateRange(this.text) } },
        }
      ],
      selected: 2
    },

    title: {
      text: this.symbol + ' stock price'
    },

    series: [{
      type: 'candlestick',
      name: this.symbol + ' stock price',
      data: data,
      dataGrouping: {
        units: [
          [ 'week', [1] ],
          [ 'month', [1, 2, 3, 4, 6] ]
        ]
      }
    }] //series
  }); //stockChart

  var updateRange = function(type) {
    console.log(this.chart);
    console.log(document);
    this.chart.getChart(type)
      .then((data) => {
        chart.series[0].setData(0);
      })
      .catch((err) => {
        console.log(err);
      });
  };
}; //drawChart



}; //Chart
