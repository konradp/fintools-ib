// Load API token
var token = prompt("Enter IEX API token",);

// FUNCTIONS

// CORS Request
function createRequest(method, url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);

    xhr.onload = () => resolve(JSON.parse(xhr.responseText));
    xhr.onerror = () => reject(xhr.statusText);
    xhr.send();
  });
};

// Submit
var checkForm = (e) => {
  e.preventDefault();
  var symbol = document.getElementById('inputSymbol').value;
  console.log('symbol: ' + symbol);
  var chart = new Chart(token);
  chart.setSymbol(symbol);
  chart.getChart("3m")
    .then((data) => {
      chart.drawChart(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

// Symbol input
var onInputKeydown = (e) => {
  if(e.keyCode == 13) checkForm(e);
}
