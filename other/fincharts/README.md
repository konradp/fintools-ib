# fincharts
Pretty NASDAQ stock price and SEC filing charts with javascript.

# Demo
https://konradp.gitlab.io/fincharts  
Display stock price chart for a given NASDAQ ticker.


https://konradp.gitlab.io/fincharts/pennystock  
Display stock price charts for today's 'penny stock winners' and SEC filing dates.  
*Penny stock winners*: Stocks at $1 per share or less which saw 10% or more day price increase.

# Run on local machine

Run
```
npm start
```
Run in debug mode
```
npm run debug
```
Explore the app in a web browser.  
http://localhost:8080/  
http://localhost:8080/pennystock/

# What it uses
Chart drawing is done using Highcharts.  
https://en.wikipedia.org/wiki/Highcharts

Historical stock data is provided by IEX API.  
https://cloud.iexapis.com/developer/docs  
Example:  
https://cloud.iexapis.com/stable/stock/aapl/chart/3m

List of today's penny stock winners.  
https://finapi.tk/pennystock/winners/10

SEC filings dates.  
https://finapi.tk/filings/aapl  
which itself obtains data from SEC Edgar system.  
https://www.sec.gov/edgar  
Example:
https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=aapl&type=&dateb=&owner=exclude&count=40&output=atom  
