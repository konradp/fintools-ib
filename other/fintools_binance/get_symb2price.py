#!/usr/bin/env python3
import json
import sys
from lib.binance import Binance

symbol = sys.argv[1]

b = Binance()
print(json.dumps(b.getPrice(symbol)))
