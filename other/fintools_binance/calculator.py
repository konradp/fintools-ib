#!/usr/bin/env python3

start = 100.00
days = 200
current = start
fee_perc = 0.1/100
start_withdraw = 80
withdraw_perc = 10/100
withdraw_freq = 20

days = range(days+1)
for day in days:
  current = 1.04*current
  fee = current*fee_perc
  current = current - fee
  print('%i %.2f' % (day, current))
  if day >= start_withdraw \
    and (day - start_withdraw) % withdraw_freq == 0:
    withdraw = current*withdraw_perc
    print('WITHDRAW %.2f (%.2f)' % (withdraw, withdraw_perc))
    current = current - withdraw
    print('%i %.2f' % (day, current))
