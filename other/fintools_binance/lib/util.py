# Utility functions
import json
import requests
from requests.exceptions import HTTPError

msg_err = { 'status': 'ERROR' }
msg_ok = { 'status': 'SUCCESS' }
header = {'Content-Type': 'application/json'}


def get(url=None, params=None, auth=None):
  try:
    response = requests.get(url, verify=True, params=params, headers=auth)
    response.raise_for_status()
  except HTTPError as http_err:
    print(http_err)
    return json.dumps(msg_err), 500, header
  except Exception as err:
    print(err)
    return json.dumps(msg_err), 500, header
  else:
    # HTTP 200 OK
    res = json.loads(response.content)
    return res
