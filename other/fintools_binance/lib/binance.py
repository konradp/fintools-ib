from lib.config import Config
import lib.util as UF

class Binance:
  # Properties
  url = None
  key = None
  auth = None

  # Constructor
  def __init__(self):
    cfg = Config()
    self.url = cfg['api']['url']
    self.key = cfg['api']['key']
    self.auth = {
      'X-MBX-APIKEY': self.key
    }

  def getHealth(self):
    print('Health')
    res = UF.get(self.url + '/wapi/v3/systemStatus.html')
    print(res)

  def getPing(self):
    print('Ping')
    res = UF.get(self.url + '/api/v3/ping')
    print(res)

  def getTime(self):
    print('Time')
    res = UF.get(self.url + '/api/v3/time')
    print(res)

  def getExchangeInfo(self):
    print('Exchange info')
    res = UF.get(self.url + '/api/v3/exchageInfo')
    print(res)

  def getExchangeInfo(self):
    print('Exchange info')
    res = UF.get(self.url + '/api/v3/exchangeInfo', auth=self.auth)
    print(res)

  def getDepth(self, symbol, limit):
    print('Depth')
    res = UF.get(
      url=self.url + '/api/v3/depth',
      params={
        'symbol': symbol,
        'limit': limit,
      },
      auth=self.auth
    )
    return res

  def getPrice(self, symbol):
    res = UF.get(
      url=self.url + '/api/v3/ticker/bookTicker',
      params={
        'symbol': symbol,
      },
      auth=self.auth
    )
    return res

#  try:
#    response = requests.get(url_health, verify=True)
#    response.raise_for_status()
#  except HTTPError as http_err:
#    return json.dumps(msg_err), 500, header
#  except Exception as err:
#    return json.dumps(msg_err), 500, header
#  else:
#    # HTTP 200 OK
#    res = json.loads(response.content)
#    if res['authenticated'] is True and res['connected'] is True:
#      return json.dumps(msg_ok), 200, {'Content-Type': 'application/json'}
#    else:
#      return json.dumps(msg_err), 500, header

