# FTSE 100 constituents
Download the FTSE100 constituents from its Wikipedia page.

# Compile and run
Compile and run the program

    $ make
    $ ./ftse100
    
## Web service version
Run the application as a HTTP server.

    $ ./ftse100 -p 8080

View the table on http://localhost:8080/

## Tips
Show the table without headers

    $ ./ftse100 | tail -n +2

Set a separator different from the default "|"

    $ ./ftse100 -d ":"

Get names of FTSE 100 constituents which are banks

    $ ./ftse 100 | grep Bank | cut -d"|" -f1

See `example.txt` for an example output.
