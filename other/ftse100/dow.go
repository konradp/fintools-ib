package main

import (
    "bufio"
    "bytes"
    "encoding/json"
    "flag"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "net/url"
    "strconv"
    "strings"
    "time"
)

/* Get the wikitext for our section
   Input:
   - url: a WikiActionAPI url, filtered to specific section
     e.g. https://en.wikipedia.org/w/api.php
          ?action=query&prop=revisions&rvprop=content
          &rvsection=4&format=json&titles=FTSE_100_Index
*/
func GetSection(u string) (ret string) {
    // Check url valid
    _, err := url.Parse(u)
    if err != nil {
        log.Fatal(err)
    }

    // Get page
    var client = &http.Client {
        Timeout: 10 * time.Second,
    }
    res, err := client.Get(u)
    if err != nil {
        log.Fatal(err)
    }

    // Read body
    defer res.Body.Close()
    body, err := ioutil.ReadAll(res.Body)
    if err != nil {
        log.Fatal(err)
    }

    // Parse JSON
    // JSON structs
    type Revision struct {
        ContentFormat string             `json:"contentformat"`
        ContentModel  string             `json:"contentmodel"`
        All           string             `json:"*"`
    }
    type RevisionList struct {
        Revision     Revision
    }
    type Page struct {
        PageId        int32              `json:"pageid"`
        Ns            int                `json:"ns"`
        Title         string             `json:"title"`
        Revisions     []Revision         `json:"revisions"`
    }
    type Pages struct {
        Page          []Page             `json:"*"`
    }
    type Normalized struct {
        From          string             `json:"from"`
        To            string             `json:"to"`
    }
    type Query struct {
        Normalized    []Normalized       `json:"normalized"`
        Pages         map[int32]Page     `json:"pages"`
    }
    type Response struct {
        BatchComplete string             `json:"batchcomplete"`
        Query         Query              `json:"query"`
    }

    // Parse
    var r Response
    err = json.Unmarshal(body, &r)
    if err != nil {
        log.Fatal(err)
    }

    // Get the last page: Unclear what this is
    // as there should only be one page anyway
    var page int32 = 0;
    for k, _ := range r.Query.Pages {
        if k > page {
            page = k
        }
    }
    //fmt.Printf("%s\n", (r.Query.Pages)[page].Revisions)

    // Get the last revision: There will be one anyway
    var rev int = 0;
    for k, _ := range r.Query.Pages[page].Revisions {
        if k > rev {
            rev = k
        }
    }
    //fmt.Printf("%s\n", r.Query.Pages[page].Revisions[rev].All)

    return r.Query.Pages[page].Revisions[rev].All
}

// Extract the string between delimiting strings 'a' and 'b'
// Warning: No nesting. Assumed the string contains both delimiters
func GetStringBetween(s string, a string, b string) (string) {
    return s[:strings.Index(s,a)] +
           s[strings.LastIndex(s,b) + len(b):]
}

// Given a wikitext section, extract a table
// Returns an array of string arrays
// Warning: This is very hardcoded
func ExtractTable(s string) (ret [][]string) {
    //var x string = "{| class=\"wikitable sortable\" id=\"constituents\""
    var x string = "|-"
    var y string = "|}"
    var p string = "<ref>"
    var q string = "</ref>"
    var sep string = "|"

    var b bytes.Buffer
    var got_start bool = false

    scanner := bufio.NewScanner(strings.NewReader(s))

    // For each line
    for scanner.Scan() {
        if scanner.Text() == x {
            if !got_start {
                got_start = true // Mark table start
            }
            continue
        }

        if scanner.Text() == y {
            break // Mark table end, break
        }

        // Inside table
        if got_start {
            var tmp_str string
            if strings.Contains(scanner.Text(), p) &&
                    strings.Contains(scanner.Text(), q) {
                // Header:
                // - remove the 'ref' section if found
                // - trim leading '!' and '|'
                tmp_str = GetStringBetween(scanner.Text(), p, q)
                tmp_str = strings.Trim(strings.Trim(tmp_str, "!"), "|")
            } else {
                // Trim leading '|' and append row
                tmp_str = strings.Trim(scanner.Text(), "|")
            }

            // Split row on '|' chars
            // Trim '[' and ']' chars, trim spaces, rejoin
            f := func(c rune) bool { return c == '|' }
            var fields []string
            for _, v := range strings.FieldsFunc(tmp_str, f) {
                var t = strings.TrimSpace(v)
                t = strings.TrimLeft(t, "[")
                t = strings.TrimRight(t, "]")
                t = strings.TrimSpace(t)
                t = strings.TrimRight(t, ".")
                fields = append(fields, t)
            }
            ret = append(ret, fields)
            b.WriteString(strings.Join(fields, sep) + "\n")
        }
    }
    //ret = b.String()
    return
}

func TableArrayToStr(a [][]string, sep string) (r string) {
    var row string
    var k bytes.Buffer
    for _, i := range a {
        row = strings.Join(i, sep)
        k.WriteString(row + "\n")
    }
    r = k.String()
    return
}

func Handler(w http.ResponseWriter, r *http.Request, s string) {
    section := GetSection(u)
    table := ExtractTable(section)

    tableStr := TableArrayToStr(table, s)
    fmt.Fprintf(w, "%s", tableStr)
}

// Set url
var u = "https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&rvsection=1&format=json&titles=Dow_Jones_Industrial_Average"

// Main
func main() {
    portPtr := flag.Int("p", 0, "Port to run the server on")
    sepPtr := flag.String("d", "|", "Field divider (separator)")
    flag.Parse()

    if *portPtr == 0 {
        // Command line mode
        section := GetSection(u)
        table := ExtractTable(section)
        tableStr := TableArrayToStr(table, *sepPtr)
        fmt.Printf("%s", tableStr)
    } else {
        // Server mode
        fmt.Printf("Listening on port %v\n", *portPtr)
        fmt.Printf("Will query %v\n", u)
        http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
            Handler(w, r, *sepPtr)
        })
        log.Fatal(http.ListenAndServe(":" + strconv.Itoa(*portPtr), nil))
    }
}

