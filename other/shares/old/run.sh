#!/bin/bash
cd src
echo "Compile resources"
glib-compile-resources --target resources.c --generate-source menu.gresource.xml
echo "Compile the app"
g++ main.cc mainwindow.cc runframe.cc resources.c `pkg-config --cflags --libs gtkmm-3.0` -o ../a.out
$(pwd)/../a.out
