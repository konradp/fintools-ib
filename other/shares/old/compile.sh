#!/bin/bash
cd src
echo "Compile resources"
cd  gui
glib-compile-resources --target resources.c --generate-source menu.gresource.xml
echo "Compile the app"
cd ..
g++ main.cc \
    gui/mainwindow.cc \
    gui/runframe.cc
    gui/resources.c \
    `pkg-config --cflags --libs gtkmm-3.0` -o ../a.out

