# Shares
This app won't be useful to anyone at the moment as it relies on a specific dataset format.  
It's only public because I want to get the source code easily.  
Only useful if: You're writing a `gtkmm` app or want to badly parse RSS news feeds.

## Compile and run
In top of this directory, next to the `src` directory:
```bash
tpm
mkdir build
cd build
cmake ..
make
```

Note: To build the app with debug flags (to alow debugging with gdb, run the below instead of `cmake ..`). This corresponds to the `-g` flag in `gcc`.
```bash
cmake -DCMAKE_BUILD_TYPE=Debug ..
```

## Get test data
Visit, for example, the following three addresses.
```
https://uk.finance.yahoo.com/quote/ANTO.L/history?p=ANTO.L
https://uk.finance.yahoo.com/quote/GLEN.L/history?p=GLEN.L
https://uk.finance.yahoo.com/quote/ULVR.L/history?p=ULVR.L
```
On each page, click "Download data" and save the .csv files in `data/uk/FTSE/current/`.

## Run the application
```bash
./src/shares
```

Note: If the app was built with debug flags, it can be debugged with either of the below.
```bash
gdb -tui ./src/shares
gdb ./src/shares
```

