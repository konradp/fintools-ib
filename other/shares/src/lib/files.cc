#include "files.h"
#include <dirent.h>

Files::Files()
{}

// This filters files, excluding dirs
static int
fileSelector(const struct dirent *fileList)
{
    return (fileList->d_name[0]=='.')? 0 : 1;
}

std::vector<std::string>
Files::getDir(std::string dir)
{
    struct dirent **eps;
    int n;

    std::vector<std::string> elements;
    n = scandir(dir.c_str(), &eps, fileSelector, alphasort);
    for(int i=0; i<n; ++i) {
	elements.push_back(eps[i]->d_name);
    }

    return elements;
}

Files::~Files()
{
}
