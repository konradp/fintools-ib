#ifndef SUPPORT_HPP
#define SUPPORT_HPP

#include "lib/tinydate.hpp"
#include <string>
#include <iostream> // DEBUG only

//using namespace std;
using namespace Tinydate;

// Data types and constants
enum {
    G_OPEN = 1,
    G_HIGH = 2,
    G_LOW = 3,
    G_CLOSE = 4,
    G_ADJ_CLOSE = 6,
    G_VOLUME = 5
};

struct xy_pair {
    date x;
    float y;
};

struct gain_pair {
    xy_pair in;
    xy_pair out;
    float gain;
};

struct tPeriod {
    std::string ticker;
    date range_in;
    date range_out;
    gain_pair gain;
};

// Helper methods
namespace Support {
bool ExtractString(std::string &v, std::string &r, std::string a, std::string b);
std::string ReplaceHtmlChars(std::string s);
std::string ReplaceStr(std::string s, std::string _old, std::string _new);
};

#endif /* SUPPORT_HPP */
