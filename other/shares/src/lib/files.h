#ifndef FILES_H
#define FILES_H

#include <string>
#include <vector>

class Files
{
public:
    Files();
    ~Files();

    std::vector<std::string> getDir(std::string dir);
};

#endif // FILES_H
