#ifndef SUPPORT_CPP
#define SUPPORT_CPP

#include "support.h"
#include "lib/tinydate.hpp"
#include <string>

using namespace std;
using namespace Tinydate;

namespace Support {
// v = between 1st match 2nd match: value in between
// r = remainder of the string, following the 2nd match
bool
ExtractString(string &v, string &r, string a, string b)
{
    size_t a_pos = r.find(a);
    size_t b_pos = r.find(b);

    if( (a_pos != string::npos) && (b_pos != string::npos) ) {
	// get inner value, cut out the rest
	v = r.substr(a_pos+a.size(), b_pos-a_pos-a.size());
	r = r.substr(b_pos+b.size(), string::npos-b_pos-b.size());
	return true;
    } else return false;
}

string
ReplaceHtmlChars(string s)
{
    s = ReplaceStr(s, "&amp;#39;", "'");
    s = ReplaceStr(s, "&amp;amp;", "&");
    return s;
}

string
ReplaceStr(string s, string _old, string _new) {
    std::size_t pos;
    while((pos = s.find(_old)) && (pos != string::npos)) {
	s = s.substr(0, pos) + _new + s.substr(pos+_old.length());
    }
    return s;
    // ALTERNATIVE VERSION
    //size_t i = 0;
    //while(true) {
	//i = s.find(_old); // find occurrence
	//if(i == string::npos) break; // reached the end
	//s = s.replace(i, _old.length(), _new);
	//i += _old.length(); // advance index (chop)
    //}
};


};


#endif /* SUPPORT_CPP */
