#ifndef NET_HPP
#define NET_HPP

#include <curl/curl.h>
#include <fstream> // for ostream
#include <iostream> // debug only
#include <sstream> // for ostringstream

class Net {
public:
    Net() {};
    virtual ~Net() {};
    // return page (or .csv) as string
    std::string get(std::string url) {
	std::cout << "Downloading " << url << std::endl;
	CURL* curl;
	
	// only need curl for this function, so we init here
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	std::ostringstream oss;

	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlCallback);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, (long) 30);
	curl_easy_setopt(curl, CURLOPT_FILE, &oss);
	curl_easy_perform(curl);
	curl_easy_cleanup(curl);
	curl_global_cleanup();
	return oss.str();
    };

private:
    static size_t CurlCallback(void* buf, size_t size, size_t nmemb, void* userdata) {
	if(userdata) {
	    std::ostream& os = *static_cast<std::ostream*>(userdata);
	    std::streamsize len = size*nmemb;
	    if(os.write(static_cast<char*>(buf), len))
		return len;
	}
	return 0; // fail
    };


}; // class Net
#endif /* NET_HPP */
