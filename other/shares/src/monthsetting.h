#ifndef MONTH_SETTING_H
#define MONTH_SETTING_H

#include "datechooser.h"
#include "pricechooser.h"
#include "lib/tinydate.hpp"
#include <gtkmm/box.h>
#include <gtkmm/separator.h>
#include <string>


class MonthSetting : public Gtk::Box
{
public:
    MonthSetting();
    virtual ~MonthSetting();
    void init();

    // Signal emit
    sigc::signal<void, Tinydate::date, std::string> signal_run_clicked;

private:
    // Signal handlers
    void run_clicked();

    // Widgets
    Gtk::Label lbl_, lbl_price_;
    DateChooser date_field_;
    PriceChooser price_field_;
    Gtk::Separator separator_;
    Gtk::Button btn_run_;
}; // class MonthSetting
#endif // MONTH_SETTING_H
