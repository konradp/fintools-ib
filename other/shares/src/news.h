#ifndef NEWS_H
#define NEWS_H

#include "lib/tinydate.hpp"
#include <vector>

class News
{
public:
    News();
    ~News();

    // data types
    struct NewsItem {
	std::string title;
	std::string url;
	Tinydate::date pub_date;
	std::string desc;
    };

    // set/get
    std::vector<NewsItem> get();
    void set_date_range(Tinydate::date from, Tinydate::date to);
    void set_feed(std::string feed) { news_feed_ = feed; };
    void set_ticker(std::string name) { ticker_name_ = name; };


private:
    std::string news_feed_;
    Tinydate::date date_from_;
    Tinydate::date date_to_;
    std::string ticker_name_;
};
#endif // NEWS_H
