#include "monthsummary.h"
#include <string>
#include <iostream>
#include <gtkmm/cellrenderer.h>

MonthSummary::MonthSummary(std::vector<tPeriod> month_list, int price_id)
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  m_switch_btn("Show graphs"),
  price(price_id)
{
    // Signals
    m_switch_btn.signal_clicked().connect(sigc::mem_fun(*this,
	&MonthSummary::graph_toggle_clicked));

    // Table
    m_refTreeModel = Gtk::ListStore::create(m_Columns);
    m_TreeView.set_model(m_refTreeModel);
    //m_TreeView.set_headers_clickable(true);

    
    // Table body
    Gtk::TreeModel::Row row;
    for(auto v : month_list) {
	//std::ostringstream g_str;
	//g_str.precision(2);
	//g_str << std::fixed << v.gain.gain;
	Ticker tic(v.ticker);
		
	// Table data
	row = *(m_refTreeModel->append());
	row[m_Columns.m_col_t] = v.ticker;
	row[m_Columns.m_col_g] = v.gain.gain;
	row[m_Columns.m_col_d1] = v.gain.in.x.day();
	row[m_Columns.m_col_d2] = v.gain.out.x.day();
	row[m_Columns.m_col_s] = tic.get_sector();
    }

    // Table headers
    m_TreeView.append_column("Ticker", m_Columns.m_col_t);
    m_TreeView.append_column_numeric("Gain", m_Columns.m_col_g, "%.2f");
    m_TreeView.append_column("DayIn", m_Columns.m_col_d1);
    m_TreeView.append_column("DayOut", m_Columns.m_col_d2);
    m_TreeView.append_column("Sector", m_Columns.m_col_s);

    // Column alignment
    Gtk::CellRendererText* pRenderer;
    Gtk::TreeViewColumn* pCol;
    // Right
    pCol = m_TreeView.get_column(1);
    pCol->set_alignment(0.5);
    pCol->set_sort_column(m_Columns.m_col_g);
    pRenderer = static_cast<Gtk::CellRendererText*>(pCol->get_first_cell());
    pRenderer->set_alignment(1, 0.5);
    // Mid
    for(int i : { 2, 3 }) {
	pCol = m_TreeView.get_column(i);
	pCol->set_alignment(0.5);
	pRenderer = static_cast<Gtk::CellRendererText*>(pCol->get_first_cell());
	pRenderer->set_alignment(0.5, 0.5);
    }
    // Make sortable. TODO: Remove repetitions
    pCol = m_TreeView.get_column(0);
    pCol->set_sort_column(m_Columns.m_col_t);
    pCol = m_TreeView.get_column(1);
    pCol->set_sort_column(m_Columns.m_col_g);
    pCol = m_TreeView.get_column(2);
    pCol->set_sort_column(m_Columns.m_col_d1);
    pCol = m_TreeView.get_column(3);
    pCol->set_sort_column(m_Columns.m_col_d2);
    pCol = m_TreeView.get_column(4);
    pCol->set_sort_column(m_Columns.m_col_s);

    // Add widgets
    m_Scrolled_Window.add(m_TreeView);
    pack_start(m_Scrolled_Window, Gtk::PACK_EXPAND_WIDGET);
    pack_start(m_switch_btn, Gtk::PACK_SHRINK);
    show_all_children();
}

MonthSummary::~MonthSummary()
{
}

// Button 'show graphs' clicked, pass the call to parent view
void
MonthSummary::graph_toggle_clicked()
{
    signal_graph_toggle_clicked.emit(2);
}
