#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include "graphsettings.h"
#include "periodsetting.h"
#include "periodview.h"
#include "periodsummary.h"
#include "monthsetting.h"
#include "monthsummary.h"
#include "simsetting.h"
#include "simview.h"
//#include "testsetting.h"
//#include "testview.h"
#include "backend.h"
#include <gtkmm.h>

class MainWindow : public Gtk::Window
{
public:
    MainWindow();
    virtual ~MainWindow();

private:
    void clear_view();

    // menu
    void on_action_file_period();
    void on_action_file_topmonth();
    void on_action_file_sim();
//    void on_action_file_testwindow();
    void on_action_file_quit();
    void on_action_toggleview(int i);
    void on_action_graphsettings();

    // actions
    void run_period_clicked(Tinydate::date a, Tinydate::date b, std::string price);
    void run_month_clicked(Tinydate::date a, std::string price);
    void run_sim_clicked(Tinydate::date a, std::string price);
//    void run_testwindow_clicked();

    // Widgets
    Gtk::Box m_Box;

    Glib::RefPtr<Gtk::Builder> m_refBuilder;
    Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;
    Glib::RefPtr<Gio::SimpleAction> m_refActionSetViewOverall;
    PeriodSetting m_periodsetting;
    MonthSetting _month_setting;
    SimSetting _sim_setting;
//    TestSetting _testwindow_setting;
    int price_id_;

    // Backend: functionality
    Backend m_Backend;
    bool months_ready_;
    std::vector<tPeriod> months_list;
};

#endif //GTKMM_EXAMPLEWINDOW_H
