#include "backend.h"
#include "ticker.h"
#include <algorithm>
#include <iostream>

Backend::Backend()
{
}

Backend::~Backend()
{}

std::vector<tPeriod>
Backend::run(Tinydate::date date_from, Tinydate::date date_to, int price)
{
    return investRun(date_from, date_to, price, true);
}


std::vector<tPeriod>
Backend::run_month(Tinydate::date date_from, int price)
{
    return investRunMonth(date_from, price, true);
}


std::vector<std::string>
Backend::GetTickers()
{
    // Get files from dir, store names (tickers)
    std::string folder_path = dataDir + std::string("/uk/FTSE/current/");
    Files folderHandle;
    std::vector<std::string> v = folderHandle.getDir(folder_path);
    if(v.size() < 1) throw std::runtime_error("No files found in the dir " + folder_path + ".");

    return v;
}


/* PRIVATE */

std::vector<tPeriod>
Backend::investRun(Tinydate::date begin_date, Tinydate::date end_date, int price, bool skipZeroVol)
{
    std::cout << "DEBUG: Backend::investRun() start" << std::endl;
    using namespace Tinydate;
    std::vector<tPeriod> month_list;
    std::string ticker_name;
    gain_pair tmp_gain, month_max;
    
    // Get files from dir, store names (tickers)
    std::string folder_path = dataDir + std::string("/uk/FTSE/current/");
    Files folderHandle;
    std::vector<std::string> files = folderHandle.getDir(folder_path);
    if(files.size() < 1) throw std::runtime_error("No files found in the dir " + folder_path + ".");
   
    for(date cur_date = begin_date; cur_date <= end_date; cur_date.next_month()) {
	std::cout << "DEBUG: iteration" << std::endl;
	// Iterate over given month range
	month_max.gain = -1000;
	for(auto cur_ticker : files) {
	    Ticker ticker(cur_ticker);
	    tmp_gain = ticker.get_best_gain(cur_date, cur_date.end_of_month(), price);
	    
	    if(tmp_gain.gain > month_max.gain) {
		month_max = tmp_gain;
		ticker_name = cur_ticker;
	    }
	} // for
	
	month_list.push_back(
	    tPeriod {
		ticker_name,
		cur_date.get_date(),
		cur_date.end_of_month(),
		month_max
	    }
	);
	std::cout.precision(2);
	std::cout << cur_date.year() << "-" << cur_date.month() << " "
	    << ticker_name << " " << std::fixed
	    << month_max.gain
	    << " " << month_max.in.x.day()
	    << " " << month_max.out.x.day() << std::endl;

    }
    return month_list;
}

std::vector<tPeriod>
Backend::investRunMonth(Tinydate::date date_in, int price, bool skipZeroVol)
{
    std::cout << "DEBUG: Backend::investRunMonth()" << std::endl;
    using namespace Tinydate;
    std::vector<tPeriod> all_gains;
    gain_pair tmp_gain;
    
    // Get files from dir, store names (tickers)
    std::string folder_path = dataDir + std::string("/uk/FTSE/current/");
    Files folderHandle;
    std::vector<std::string> files = folderHandle.getDir(folder_path);
    if(files.size() < 1) throw std::runtime_error("No files found in the dir " + folder_path + ".");
   
    // Get all tickers gains
    gain_pair gain;
    for(std::string cur_ticker : files) {
	Ticker ticker(cur_ticker);
	gain = ticker.get_best_gain(date_in, date_in.end_of_month(), price);
	all_gains.push_back(
	    tPeriod {
		cur_ticker,
		date_in,
		date_in.end_of_month(),
		gain
	    }
	);
    } // for files
	

    // Order by vector.period.gain.gain
    std::sort(all_gains.begin(), all_gains.end(), compare_by_gain);
    std::reverse(all_gains.begin(), all_gains.end());
    // Get first best gains
    //for(int i = 0; i < 10; i++) { best_gains.push_back(all_gains.at(i)); }
    std::cout << "DEBUG: return investRunMonth()" << std::endl;
    // TODO
    return all_gains;
}

bool
Backend::compare_by_gain(const tPeriod& a, const tPeriod& b) {
    return a.gain.gain < b.gain.gain;
}
