#ifndef PERIOD_SETTING_H
#define PERIOD_SETTING_H

#include "datechooser.h"
#include "pricechooser.h"
#include "lib/tinydate.hpp"
#include <gtkmm/box.h>
#include <gtkmm/separator.h>
#include <string>


class PeriodSetting : public Gtk::Box
{
public:
    PeriodSetting();
    virtual ~PeriodSetting();
    void init();

    // Signal emit
    sigc::signal<void, Tinydate::date, Tinydate::date, std::string> signal_run_clicked;

private:
    // Signal handlers
    void run_clicked();

    // Widgets
    Gtk::Label lbl_dates_, lbl_from_, lbl_to_, lbl_price_;
    DateChooser date_chooser1_, date_chooser2_;
    PriceChooser price_chooser_;
    Gtk::Separator separator_;
    Gtk::Button btn_run_;
}; // class PeriodSetting
#endif // PERIOD_SETTING_H
