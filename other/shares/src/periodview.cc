#include "periodview.h"
#include "news.h"
#include "ticker.h"
#include <string>
#include <iostream>
#include <gtkmm/linkbutton.h>
#include <gtkmm/scrolledwindow.h>

PeriodView::PeriodView(std::vector<tPeriod> m_list, int price_id)
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  month_list(m_list),
  cur_month(1),
  cur_news_month(0),
  plot_candlestick(true),
  price(price_id),
  zoom_factor(0),
  m_Btn_Box(Gtk::ORIENTATION_HORIZONTAL), // TODO: Center
  m_Sum_Box(Gtk::ORIENTATION_VERTICAL, 10),
  m_Graph_Box(Gtk::ORIENTATION_VERTICAL, 10),
  m_News_Box(Gtk::ORIENTATION_VERTICAL, 10),
  m_Data_Box(Gtk::ORIENTATION_VERTICAL, 10),
  m_prev_btn("<"),
  m_next_btn(">"),
  m_Label_Month_Id(std::to_string(cur_month)),
  m_Label_Ticker(m_list[0].range_in.year() + "-" + m_list[0].range_in.month()
    + ": " + m_list[0].ticker
    + " (" + std::to_string((int)m_list[0].gain.gain) + "%)"),
  plot_()
{
    // Arrows
    m_Btn_Box.pack_start(m_prev_btn, Gtk::PACK_SHRINK);
    m_Btn_Box.pack_start(m_Label_Month_Id, Gtk::PACK_SHRINK);
    m_Btn_Box.pack_start(m_next_btn, Gtk::PACK_SHRINK);
    m_Btn_Box.pack_start(m_Label_Ticker, Gtk::PACK_SHRINK);
    pack_start(m_Btn_Box, Gtk::PACK_SHRINK);
 
    // Tabs
    pack_start(m_tabs);
    m_tabs.append_page(m_Sum_Box, "Overview");
    m_tabs.append_page(m_Graph_Box, "Graph");
    m_tabs.append_page(m_News_Box, "News");
    m_tabs.append_page(m_Data_Box, "Data");

    plot_.set_show_vol(true);
    plot_.set_mouse_on(true);
    m_Graph_Box.pack_start(plot_, Gtk::PACK_EXPAND_WIDGET);

    // Signals
    m_prev_btn.signal_clicked().connect(sigc::mem_fun(*this,
	&PeriodView::on_prev_clicked));
    m_next_btn.signal_clicked().connect(sigc::mem_fun(*this,
	&PeriodView::on_next_clicked));
    m_tabs.signal_switch_page().connect(sigc::mem_fun(*this,
	&PeriodView::on_tabs_switch_page));
    
    show_all_children();
}

PeriodView::~PeriodView()
{
}

void
PeriodView::init_tab()
{
    // Set label
    m_Label_Month_Id.set_label(std::to_string(cur_month));
    m_Label_Ticker.set_label(month_list[cur_month-1].range_in.year_month()
	+ ": " + month_list[cur_month-1].ticker
	+ " (" + std::to_string((int)month_list[cur_month-1].gain.gain) + "%)");
    cur_news_month = 0;

    if(m_tabs.get_current_page() == 0) draw_summary();
    else if(m_tabs.get_current_page() == 1) draw_graph();
    else if(m_tabs.get_current_page() == 2) draw_news();
    else if(m_tabs.get_current_page() == 3) draw_data();
}

void
PeriodView::draw_data()
{
    if(Gtk::Box* b = dynamic_cast<Gtk::Box*>(m_tabs.get_nth_page(3))) {
	// Clear old child
	for(auto child : b->get_children()) b->remove(*child);

	// Get data
	tPeriod p = month_list[cur_month-1];
	Ticker t(p.ticker);

    Tinydate::date from, to;
    from = p.range_in.get_prev_day(30*zoom_factor);
    to = p.range_out.get_next_day(30*zoom_factor);

    std::cout << from.get_date() << " : " << to.get_date() << std::endl;
	auto data = t.get_all(from, to, true);
	
	// Display data in a grid
	Gtk::Grid* g = new Gtk::Grid(); g->set_column_spacing(10);
	Gtk::ScrolledWindow* sw = new Gtk::ScrolledWindow();
	Gtk::Box *label_bg;
	Gtk::Label *l;

	// Loop over horiz/vert cells
	int i = 0;
	bool is_in_trade_period = false;
	for(auto line : data) {
	    if(i != 0) {
		// Highlight in/out trade period
		is_in_trade_period = ((p.gain.in.x <= Tinydate::date(line[0]))
		    && (Tinydate::date(line[0]) <= p.gain.out.x))? true : false;
	    }
	    int j = 0;
	    for(auto field : line) {
		l = new Gtk::Label(field); l->set_halign(Gtk::ALIGN_START);

		if((price == j) || is_in_trade_period) {
		    // Add colour behind the label
		    label_bg = new Gtk::Box();
		    if(price == j) {		    
			// Highlight the price column: OHLC
			label_bg->override_background_color(Gdk::RGBA("#bfbfbf")); // grey
		    } else {
			// Highlight trading period
			label_bg->override_background_color(Gdk::RGBA("#e6e6e6")); // light grey
		    }

		    label_bg->add(*l);
		    g->attach(*label_bg, j,i,1,1);
		} else {
		    // Plain label, no colour
		    g->attach(*l, j,i,1,1);
		}
		j++;
	    } // field
	    i++;
	} // line
	
	sw->add(*g);
	b->pack_start(*sw, Gtk::PACK_EXPAND_WIDGET);
	show_all_children();
    } //if box exists
}


void
PeriodView::draw_graph()
{
    // Plot
    tPeriod p = month_list[cur_month-1];
    Ticker t(p.ticker);
    //plot_.clear_data();

    Tinydate::date from, to;
    from = p.range_in.get_prev_day(30*zoom_factor);
    to = p.range_out.get_next_day(30*zoom_factor);

    // Bollinger bands
    auto bol = t.get_bollinger_bands(from, to, price, false, 20);

    if(plot_candlestick) {
        plot_.set_type("candlestick");
        auto data = t.get_all_data(from, to, true);
        if( (data.size() > 0)
                && (bol[0].size() > 0)
                && (bol[1].size() > 0)
                && (bol[2].size() > 0) ) {
            plot_.add_data(p.ticker, data);
            plot_.add_data("avg", bol[0]);
            plot_.add_data("lo_band", bol[1]);
            plot_.add_data("hi_band", bol[2]);
        }
    } else {
        //auto data = t.get(
        //p.range_in.get_prev_month(zoom_factor),
        //p.range_out.get_next_month(zoom_factor),
        //price, true);
        //plot_.add_data(p.ticker, data);
    }

    auto volume = t.get(from, to, G_VOLUME, true);
    plot_.add_data_vol(volume);
    plot_.add_vline(p.gain.in.x);
    plot_.add_vline(p.gain.out.x);

    // Set range
    // TODO: There is a bug in add_data in tinyplotlib
    // TODO: We shouldn't have to set the range just before queue_draw()
    plot_.range_from = from;
    plot_.range_to = to;
    plot_.queue_draw();
}


void
PeriodView::draw_news()
{
    if(Gtk::Box* tab = dynamic_cast<Gtk::Box*>(m_tabs.get_nth_page(2))) {
	// Clear old data
	for(auto child : tab->get_children()) tab->remove(*child);

	// Get data
	tPeriod p = month_list[cur_month-1];
	News n;
	// date range
	Tinydate::date news_date_in, news_date_out;
	if(cur_news_month == 0 ) {
	    news_date_in = p.range_in;
	    news_date_out = p.range_out;
	} else if(cur_news_month < 0) {
	    news_date_in = p.range_in.get_prev_month(-cur_news_month);
	    news_date_out = p.range_out.get_prev_month(-cur_news_month);
	} else if(cur_news_month > 0) {
	    news_date_in = p.range_in.get_next_month(cur_news_month);
	    news_date_out = p.range_out.get_next_month(cur_news_month);
	}
	std::cout << "date: " << news_date_in.get_date() << std::endl;
	n.set_date_range(news_date_in, news_date_out);
	n.set_ticker(p.ticker); // ticker
	std::vector<News::NewsItem> v = n.get(); // get news

	// Display data in a grid
	using namespace Gtk;
	Grid* g = new Grid(); g->set_column_spacing(10); g->set_row_homogeneous(false);
	ScrolledWindow* sw = new ScrolledWindow();

	// Arrow buttons
	Box* b = new Box(ORIENTATION_HORIZONTAL, 10);
	Button* btn_prev = new Button("<");
	Button* btn_next = new Button(">");
	Label* l_month = new Label(news_date_in.year_month());
	b->pack_start(*btn_prev, PACK_SHRINK);
	b->pack_start(*l_month, PACK_SHRINK);
	b->pack_start(*btn_next, PACK_SHRINK);
    
	// Arrow button signals
	btn_prev->signal_clicked().connect(sigc::mem_fun(*this,
	    &PeriodView::on_news_prev_clicked));
	btn_next->signal_clicked().connect(sigc::mem_fun(*this,
	    &PeriodView::on_news_next_clicked));
	
	// Headers
	Label *l1 = new Label("Date");
	Label *l2 = new Label("Title");
	// align left
	for(auto label : { l1, l2 }) { label->set_halign(ALIGN_START); }
	g->attach(*l1, 0,0,1,1);
	g->attach(*l2, 1,0,1,1);

	// Data fields
	Label* pub_date;
	LinkButton* title;
	int i = 1;
	for(auto item : v) {
	    pub_date = new Label(item.pub_date.get_date()); pub_date->set_halign(ALIGN_START);
	    title = new LinkButton(item.url, item.title); title->set_halign(ALIGN_START);
	    g->attach(*pub_date, 0,i,1,1);
	    g->attach(*title, 1,i,1,1);
	    i++;
	}

	sw->add(*g);
	tab->pack_start(*b, PACK_SHRINK);
	tab->pack_start(*sw, PACK_EXPAND_WIDGET);
	show_all_children();
    } // if box
}


void
PeriodView::draw_summary()
{
    if(Gtk::Box* tab = dynamic_cast<Gtk::Box*>(m_tabs.get_nth_page(0))) {
	// Clear old data
	for(auto child : tab->get_children()) tab->remove(*child);
	
	// Get data
	tPeriod p = month_list[cur_month-1];
	Ticker t(p.ticker);

	// Display data in a grid
	using namespace Gtk;
	Gtk::Grid* g = new Grid(); g->set_column_spacing(10);
	// Labels
	Label *l1 = new Label("Ticker:");
	Label *l2 = new Label("Company:");
	Label *l3 = new Label("Sector:");
	Label *l4 = new Label("In:");
	Label *l5 = new Label("Out:");
	Label *l6 = new Label("In price:");
	Label *l7 = new Label("Out price:");
	Label *l8 = new Label("Gain:");
	Label *l9 = new Label("Gain %:");
	// Data
	Label *d1 = new Label(p.ticker);
	Label *d2 = new Label(t.get_name());
	Label *d3 = new Label(t.get_sector());
	Label *d4 = new Label(p.gain.in.x.get_date());
	Label *d5 = new Label(p.gain.out.x.get_date());
	Label *d6 = new Label(std::to_string(p.gain.in.y));
	Label *d7 = new Label(std::to_string(p.gain.out.y));
	Label *d8 = new Label(std::to_string(p.gain.out.y-p.gain.in.y));
	Label *d9 = new Label(std::to_string(100*(p.gain.out.y-p.gain.in.y)/p.gain.in.y));

	// Align labels to the left
	for(auto label : {
	    l1,l2,l3,l4,l5,l6,l7,l8,l9,
	    d1,d2,d3,d4,d5,d6,d7,d8,d9
	}) { label->set_halign(ALIGN_START); }

	// Attach labels    
	g->attach(*l1, 0,0,1,1); g->attach(*d1, 1,0,1,1);
	g->attach(*l2, 0,1,1,1); g->attach(*d2, 1,1,1,1);
	g->attach(*l3, 0,2,1,1); g->attach(*d3, 1,2,1,1);
	g->attach(*l4, 0,3,1,1); g->attach(*d4, 1,3,1,1);
	g->attach(*l5, 0,4,1,1); g->attach(*d5, 1,4,1,1);
	g->attach(*l6, 0,5,1,1); g->attach(*d6, 1,5,1,1);
	g->attach(*l7, 0,6,1,1); g->attach(*d7, 1,6,1,1);
	g->attach(*l8, 0,7,1,1); g->attach(*d8, 1,7,1,1);
	g->attach(*l9, 0,8,1,1); g->attach(*d9, 1,8,1,1);
	tab->pack_start(*g, Gtk::PACK_EXPAND_WIDGET);
	show_all_children();
    } // if box
}


///// Signals
void
PeriodView::on_tabs_switch_page(Gtk::Widget* tab, guint page_num)
{
    init_tab();
}


void
PeriodView::on_prev_clicked()
{
    if(cur_month > 1) {
	cur_month--;
	init_tab();
    }
}
void
PeriodView::on_next_clicked()
{
    if(cur_month < month_list.size()) {
	cur_month++;
	init_tab();
    }
}

void
PeriodView::go_to_first()
{
    cur_month = 1;
    init_tab();
}

void
PeriodView::go_to_last()
{
    cur_month = month_list.size();
    init_tab();
}

bool
PeriodView::on_key_release_event(GdkEventKey* key_event)
{
    // Key constants in /usr/include/gtk-3.0/gdk/gdkkeysyms.h
    std::cout << "Pressed: " << std::hex << key_event->keyval << std::dec << std::endl;
    switch(key_event->keyval) {
	case GDK_KEY_bracketright: // next graph
	    on_next_clicked();
	    break;
	case GDK_KEY_bracketleft: // prev graph
	    on_prev_clicked();
	    break;
	case GDK_KEY_1: // first graph
	    go_to_first();
	    break;
	case GDK_KEY_2: // last graph
	    go_to_last();
	    break;
	case GDK_KEY_minus: // zoom out
	    zoom_factor++;
	    init_tab();
	    break;
	case GDK_KEY_equal: // zoom in
	    if(zoom_factor > 0) {
		zoom_factor--;
		init_tab();
	    }
	    break;
	case GDK_KEY_0: // zoom reset
	    zoom_factor = 0;
	    init_tab();
	    break;
	default:
	    return Gtk::Box::on_key_release_event(key_event);
	    break;

    }
    return true;
    // call base class function
    //return Gtk::Box::on_key_release_event(key_event);
}


void
PeriodView::on_news_prev_clicked()
{
    cur_news_month--;
    draw_news();
}
void
PeriodView::on_news_next_clicked()
{
    cur_news_month++;
    draw_news();
}
