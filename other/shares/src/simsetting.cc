#include "simsetting.h"
#include <string>
#include <iostream>

SimSetting::SimSetting()
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  _lbl("Choose month to begin simulation at"),
  _date_field(true),
  _lbl_price("Price type:"),
  _price_field(),
  _btn_run("Run")
{
    _lbl.set_halign(Gtk::ALIGN_START);
    _lbl_price.set_halign(Gtk::ALIGN_START);

    // Signals
    _btn_run.signal_clicked().connect(sigc::mem_fun(*this,
	&SimSetting::run_clicked));

    // Add widgets
    pack_start(_lbl);
    pack_start(_date_field, Gtk::PACK_SHRINK);
    pack_start(_lbl_price, Gtk::PACK_SHRINK);
    pack_start(_price_field, Gtk::PACK_SHRINK);
    pack_start(_separator);
    pack_start(_btn_run, Gtk::PACK_SHRINK);
}

SimSetting::~SimSetting()
{
}

void
SimSetting::init()
{
    _date_field.init(); // Reset date chooser
}

void
SimSetting::run_clicked()
{
    signal_run_clicked.emit(_date_field.get_date(),
	_price_field.get_price());
}
