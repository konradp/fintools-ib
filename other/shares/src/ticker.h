#ifndef TICKER_H
#define TICKER_H

#include <string>
#include <vector>
#include "lib/tinydate.hpp"
#include "lib/support.h"

class Ticker {
public:
    Ticker(std::string name);
    virtual ~Ticker();
    
    // methods
    std::string get_ticker();
    std::string get_name();
    std::string get_sector();

    // get data
    std::vector<std::pair<Tinydate::date, float>>
    get(
        Tinydate::date date_from,
        Tinydate::date date_to,
        int property,
        bool chrono=false,
        int extra_front=0
    );

    // get_all
    std::vector<std::vector<std::string>>
    get_all(
        Tinydate::date date_from,
        Tinydate::date date_to,
        bool chrono=false
    );

    // get_all_data
    std::vector<std::pair<Tinydate::date, std::vector<float>>>
    get_all_data(
        Tinydate::date date_from,
        Tinydate::date date_to,
        bool chrono=false
    );

    /* STATISTICAL */
    // bollinger bands
    std::vector< std::vector<std::pair<Tinydate::date, float>> >
    get_bollinger_bands(
        Tinydate::date date_from,
        Tinydate::date date_to,
        int property,
        bool chrono=false,
        int extra_front=0
    );

    // moving average
    std::vector<std::pair<Tinydate::date, float>>
    get_moving_avg(
        std::vector<std::pair<Tinydate::date, float>> data,
        int period,
        bool chrono=false
    );

    // standard deviation
    std::vector<std::pair<Tinydate::date, float>>
    get_std_deviation(
        std::vector<std::pair<Tinydate::date, float>> data,
        int period,
        bool chrono=false
    );

    // Best gain
    gain_pair get_best_gain(
        Tinydate::date date_from,
        Tinydate::date date_to,
        int property
    );

    // constants
    gain_pair t_gain_pair;

private:
    std::string t_name;
    std::string t_data_dir;
    bool skip_zero_volume;
}; // class Ticker
#endif // TICKER_H
