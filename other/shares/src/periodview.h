#ifndef PERIOD_VIEW_H
#define PERIOD_VIEW_H

#include "lib/support.h"
#include "lib/tinyplot.hpp"
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/grid.h>
#include <gtkmm/notebook.h>
#include <string>
#include <vector>

class PeriodView : public Gtk::Box
{
public:
    PeriodView(std::vector<tPeriod> month_list, int price_id);
    virtual ~PeriodView();

private:
    void init_tab();
    void draw_data();
    void draw_graph();
    void draw_news();
    void draw_summary();

    // Signal handlers
    void on_tabs_switch_page(Gtk::Widget* page, guint page_num);
    void on_prev_clicked();
    void on_next_clicked();
    void go_to_first();
    void go_to_last();
    bool on_key_release_event(GdkEventKey* event) override;
    // News
    void on_news_prev_clicked();
    void on_news_next_clicked();

    std::vector<tPeriod>    month_list;
    size_t                  cur_month;
    int                     cur_news_month;
    bool                    plot_candlestick;
    int                     price;
    int                     zoom_factor;

    // Widgets
    Gtk::Box m_Btn_Box;
    Gtk::Box m_Sum_Box, m_Graph_Box, m_News_Box, m_Data_Box;
    //Gtk::Grid m_Sum_Grid, m_Data_Grid;
    Gtk::Button m_prev_btn, m_next_btn;
    Gtk::Label m_Label_Month_Id, m_Label_Ticker;
    Gtk::Notebook m_tabs;
    Tinyplot::Plot plot_;
};

#endif // PERIOD_VIEW_H
