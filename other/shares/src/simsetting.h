#ifndef SIM_SETTING_H
#define SIM_SETTING_H

#include "datechooser.h"
#include "pricechooser.h"
#include "lib/tinydate.hpp"
#include <gtkmm/box.h>
#include <gtkmm/separator.h>
#include <string>


class SimSetting : public Gtk::Box
{
public:
    SimSetting();
    virtual ~SimSetting();
    void init();

    // Signal emit
    sigc::signal<void, Tinydate::date, std::string> signal_run_clicked;

private:
    // Signal handlers
    void run_clicked();

    // Widgets
    Gtk::Label _lbl;
    DateChooser _date_field;
    Gtk::Label _lbl_price;
    PriceChooser _price_field;
    Gtk::Separator _separator;
    Gtk::Button _btn_run;
}; // class SimSetting
#endif // SIM_SETTING_H
