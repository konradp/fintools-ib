#include "datechooser.h"

DateChooser::DateChooser(bool include_day)
: Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 0),
  yrFirst(1970),
  yrLast(2017)
{
    // Fill dropdowns with numbers
    for(int i=1; i<=31; i++) { m_Day.append(std::to_string(i)); }
    for(int i=1; i<=12; i++) {m_Month.append(std::to_string(i)); }
    for(int i=yrFirst; i<= yrLast; i++) {m_Year.append(std::to_string(i)); }

    if(include_day) pack_start(m_Day);
    pack_start(m_Month);
    pack_start(m_Year);
    
    init();
}

void
DateChooser::init()
{
    // Default values. TODO: Today's date
    m_Day.set_active(0);
    m_Month.set_active(0);
    m_Year.set_active(yrLast-yrFirst);
}

DateChooser::~DateChooser()
{
}

Tinydate::date
DateChooser::get_date()
{
    Tinydate::date d(
	std::stoi(m_Year.get_active_text()),
	std::stoi(m_Month.get_active_text()),
	std::stoi(m_Day.get_active_text()));
    // TODO: Validate and throw exception if incorrect date
    return d.get_date();
}
