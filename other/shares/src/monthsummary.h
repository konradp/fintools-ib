#ifndef MONTH_SUMMARY_H
#define MONTH_SUMMARY_H

#include "datechooser.h"
#include "ticker.h"
#include "lib/support.h"
#include "lib/tinyplot.hpp"
#include <gtkmm/box.h>
#include <gtkmm/grid.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
//#include <gtkmm/treemodelcolumn.h>
#include <string>
#include <vector>


class MonthSummary : public Gtk::Box
{
public:
    MonthSummary(std::vector<tPeriod> month_list, int price_id);
    virtual ~MonthSummary();
    
    // Signal emit
    sigc::signal<void, int> signal_graph_toggle_clicked;

private:
    // Signal handlers
    void graph_toggle_clicked();

    // Table (tree model columns)
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
	ModelColumns()
	{ add(m_col_id); add(m_col_t); add(m_col_g);
	    add(m_col_d1); add(m_col_d2); add(m_col_s); }

	Gtk::TreeModelColumn<unsigned int> m_col_id;
	Gtk::TreeModelColumn<float> m_col_g;
	Gtk::TreeModelColumn<std::string> m_col_t, m_col_s, m_col_d1, m_col_d2;
    };

    // Child widgets
    Gtk::ScrolledWindow m_Scrolled_Window;
    Gtk::Button m_switch_btn;
    int price;
    // Table
    ModelColumns m_Columns;
    Gtk::TreeView m_TreeView;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;

};

#endif // MONTH_SUMMARY_H
