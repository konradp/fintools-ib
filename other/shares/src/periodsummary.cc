#include "periodsummary.h"
#include <string>
#include <iostream>

PeriodSummary::PeriodSummary(std::vector<tPeriod> month_list, int price_id)
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  m_switch_btn("Show graphs"),
  m_m_label("Month"),
  m_t_label("Ticker"),
  m_g_label("Gain"),
  m_d1_label("Buy day"),
  m_d2_label("Sell day"),
  price(price_id)
{
    // Signals
    m_switch_btn.signal_clicked().connect(sigc::mem_fun(*this,
	&PeriodSummary::graph_toggle_clicked));

    // Widgets
    m_scroll.add(m_grid);
    m_grid.set_column_spacing(10);

    // Table headers
    m_grid.attach(m_m_label, 0,0,1,1); 
    m_grid.attach(m_t_label, 1,0,1,1);
    m_grid.attach(m_g_label, 2,0,1,1);
    m_grid.attach(m_d1_label, 3,0,1,1);
    m_grid.attach(m_d2_label, 4,0,1,1);    

    // Table body
    int i = 1;
    for(auto v : month_list) {
	std::ostringstream g_str;
	g_str.precision(2);
	g_str << std::fixed << v.gain.gain;
		
	Gtk::Label *m = new Gtk::Label(v.gain.in.x.year() + "-" + v.gain.in.x.month());
	Gtk::Label *t = new Gtk::Label(v.ticker); t->set_halign(Gtk::ALIGN_START);
	Gtk::Label *g = new Gtk::Label(g_str.str()); g->set_halign(Gtk::ALIGN_END);
	Gtk::Label *d_in = new Gtk::Label(v.gain.in.x.day());
	Gtk::Label *d_out = new Gtk::Label(v.gain.out.x.day());
	for(auto i : {m, t, g, d_in, d_out}) std::cout << i->get_text() << " " << std::endl;

	m_grid.attach(*m, 0,i,1,1);
	m_grid.attach(*t, 1,i,1,1);
	m_grid.attach(*g, 2,i,1,1);
	m_grid.attach(*d_in, 3,i,1,1);
	m_grid.attach(*d_out, 4,i,1,1);
	i++;
    }
    
    // Add widgets
    pack_start(m_scroll, Gtk::PACK_EXPAND_WIDGET);
    //pack_start(m_plot);
    pack_start(m_switch_btn, Gtk::PACK_SHRINK);
}

PeriodSummary::~PeriodSummary()
{
}

// Button 'show graphs' clicked, pass the call to parent view
void
PeriodSummary::graph_toggle_clicked()
{
    signal_graph_toggle_clicked.emit(2);
}
