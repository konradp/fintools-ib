#include "pricechooser.h"

PriceChooser::PriceChooser()
: Gtk::ComboBoxText()
{
    // Fill dropdowns with numbers
    for(std::string i : { "Open",
	    "High",
	    "Low",
	    "Close",
	    "Adjusted close" }
    ) { append(i); }

    set_active(3);
}

PriceChooser::~PriceChooser()
{}

std::string
PriceChooser::get_price()
{
    return get_active_text();
}
