#ifndef PERIOD_SUMMARY_H
#define PERIOD_SUMMARY_H

#include "datechooser.h"
#include "lib/support.h"
#include "lib/tinyplot.hpp"
#include <gtkmm/box.h>
#include <gtkmm/grid.h>
#include <gtkmm/scrolledwindow.h>
#include <string>
#include <vector>


class PeriodSummary : public Gtk::Box
{
public:
    PeriodSummary(std::vector<tPeriod> month_list, int price_id);
    virtual ~PeriodSummary();
    
    // Signal emit
    sigc::signal<void, int> signal_graph_toggle_clicked;

private:
    // Signal handlers
    void graph_toggle_clicked();
    // Widgets
    Gtk::ScrolledWindow m_scroll;
    Gtk::Button m_switch_btn;
    Gtk::Grid m_grid;
    Gtk::Label m_m_label;
    Gtk::Label m_t_label;
    Gtk::Label m_g_label;
    Gtk::Label m_d1_label;
    Gtk::Label m_d2_label;
    //Tinyplot::Plot m_plot;
    int price;
};

#endif // PERIOD_SUMMARY_H
