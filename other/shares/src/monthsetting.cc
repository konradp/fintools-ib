#include "monthsetting.h"
#include <string>
#include <iostream>

MonthSetting::MonthSetting()
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  lbl_("Choose month to analyse"),
  lbl_price_("Price type:"),
  date_field_(false),
  price_field_(),
  btn_run_("Run")
{
    lbl_.set_halign(Gtk::ALIGN_START);
    lbl_price_.set_halign(Gtk::ALIGN_START);

    // Signals
    btn_run_.signal_clicked().connect(sigc::mem_fun(*this,
	&MonthSetting::run_clicked));

    // Add widgets
    pack_start(lbl_);
    pack_start(date_field_, Gtk::PACK_SHRINK);
    pack_start(lbl_price_, Gtk::PACK_SHRINK);
    pack_start(price_field_, Gtk::PACK_SHRINK);
    pack_start(separator_);
    pack_start(btn_run_, Gtk::PACK_SHRINK);
}

MonthSetting::~MonthSetting()
{
}

void
MonthSetting::init()
{
    date_field_.init(); // Reset date chooser
}

void
MonthSetting::run_clicked()
{
    signal_run_clicked.emit(date_field_.get_date(),
	price_field_.get_price());
}
