#include "mainwindow.h"
#include <iostream>

MainWindow::MainWindow()
: m_Box(Gtk::ORIENTATION_VERTICAL, 10),
  price_id_(2),
  months_ready_(false)
{
    add(m_Box);

    // Actions: File menu
    m_refActionGroup = Gio::SimpleActionGroup::create();
    m_refActionGroup->add_action("period",
	sigc::mem_fun(*this, &MainWindow::on_action_file_period) );
    m_refActionGroup->add_action("topmonth",
	sigc::mem_fun(*this, &MainWindow::on_action_file_topmonth) );
    m_refActionGroup->add_action("sim",
	sigc::mem_fun(*this, &MainWindow::on_action_file_sim) );
//    m_refActionGroup->add_action("testwindow",
//	sigc::mem_fun(*this, &MainWindow::on_action_file_testwindow) );
    m_refActionGroup->add_action("quit",
	sigc::mem_fun(*this, &MainWindow::on_action_file_quit) );
    // Actions: View menu
    m_refActionSetViewOverall = m_refActionGroup->add_action_radio_integer("toggleview",
	sigc::mem_fun(*this, &MainWindow::on_action_toggleview), 1);
    m_refActionGroup->add_action("graphsettings",
	sigc::mem_fun(*this, &MainWindow::on_action_graphsettings) );
    insert_action_group("example", m_refActionGroup);

    // Signals
    m_periodsetting.signal_run_clicked.connect(sigc::mem_fun(*this,
	&MainWindow::run_period_clicked));
    _month_setting.signal_run_clicked.connect(sigc::mem_fun(*this,
	&MainWindow::run_month_clicked));
    _sim_setting.signal_run_clicked.connect(sigc::mem_fun(*this,
	&MainWindow::run_sim_clicked));
//    _testwindow_setting.signal_run_clicked.connect(sigc::mem_fun(*this,
//	&MainWindow::run_testwindow_clicked));

    // Menu
    m_refBuilder = Gtk::Builder::create();

    try {
	m_refBuilder->add_from_resource("/menu/menu.ui");
	auto object = m_refBuilder->get_object("menubar");
	auto gmenu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);

        if(!gmenu) g_warning("GMenu not found");
	else {
	    auto pMenuBar = Gtk::manage(new Gtk::MenuBar(gmenu));
	    // Disable view menu options: TODO
	    //for(auto i : pMenuBar->get_children()) {
		//i->set_sensitive(false);
	    //}
	    m_Box.pack_start(*pMenuBar, Gtk::PACK_SHRINK);
	}
    } catch(const Glib::Error& ex) {
	std::cerr << "Building menus failed: " << ex.what();
    }

    m_Box.pack_start(_sim_setting);
    show_all_children();
}

MainWindow::~MainWindow()
{
}

// Clear main window
void
MainWindow::clear_view()
{
    // Get all children, remove all except menu
    std::vector<Widget*> v = m_Box.get_children();
    int i = 0;
    for(auto j : v) {
	if(i!= 0) { m_Box.remove(*j); }
	i++;
    }
}

void
MainWindow::on_action_file_period()
{
    months_ready_ = false;
    clear_view();
    m_periodsetting.init();
    m_Box.pack_start(m_periodsetting);
    show_all_children();
}

void
MainWindow::on_action_file_topmonth()
{
    months_ready_ = false;
    clear_view();
    _month_setting.init();
    m_Box.pack_start(_month_setting);
    show_all_children();
}

void
MainWindow::on_action_file_sim()
{
    months_ready_ = false;
    clear_view();
    _sim_setting.init();
    m_Box.pack_start(_sim_setting);
    show_all_children();
}


/*void
MainWindow::on_action_file_testwindow()
{
    std::cout << "clicked" << std::endl;
    months_ready_ = false;
    clear_view();
    _testwindow_setting.init();
    m_Box.pack_start(_testwindow_setting);
    show_all_children();
}*/


void MainWindow::on_action_file_quit() { hide(); }

void
MainWindow::on_action_toggleview(int i)
{
    if(months_ready_) {
	std::cout << "toggled between summary/graphs:" << i << std::endl;
	clear_view();

	if(i == 1) {
	    MonthSummary* m_monthsummary = new MonthSummary(months_list, price_id_);
	    m_Box.pack_start(*m_monthsummary);
	} else if(i == 2) {
	    PeriodView* m_graphview = new PeriodView(months_list, price_id_);
	    m_Box.pack_start(*m_graphview);
	}
	show_all_children();
    }
}

void
MainWindow::on_action_graphsettings()
{
    std::cout << "Graph settings " << std::endl;
    GraphSettings dialog(*this, "Graph settings");
    dialog.set_secondary_text("bla");
    dialog.run();
    return;
}

void
MainWindow::run_period_clicked(Tinydate::date a, Tinydate::date b, std::string price)
{
    std::vector<tPeriod> v;
    // TODO: This is ugly, fix
    // ADJ_CLOSE is 4th on the list, but is 5th in the CSV file
    if(price == "Open") price_id_ = G_OPEN;
    else if(price == "High") price_id_ = G_HIGH;
    else if(price == "Low") price_id_ = G_LOW;
    else if(price == "Close") price_id_ = G_CLOSE;
    else if(price == "Adjusted close") price_id_ = G_ADJ_CLOSE;

    try {
	if (!a.is_valid() || !b.is_valid()) throw std::runtime_error(
		std::string("You have entered an invalid date.\nExample: Feb 31st "
		+ std::string("is invalid because Feb does not have 31 days.")));
	months_list = m_Backend.run(a, b, price_id_);
	// TODO: Progress bar here
    } catch(std::exception& e) {
	// Exceptions
	std::cout << "Exception: " << e.what() << std::endl;
	Gtk::MessageDialog dialog(*this, "Exception");
	dialog.set_secondary_text(e.what());
	dialog.run();
	return;
    }
	
    // Remove runview, replace with summaryview
    clear_view();
    //m_Box.remove(m_periodsetting);
    PeriodSummary* periodsummary = new PeriodSummary(months_list, price_id_);
    m_Box.pack_start(*periodsummary);
    // Signals
    periodsummary->signal_graph_toggle_clicked.connect(sigc::mem_fun(*this,
	&MainWindow::on_action_toggleview));
    
    months_ready_ = true;
    show_all_children();
}


void
MainWindow::run_month_clicked(Tinydate::date a, std::string price)
{
    std::vector<tPeriod> v;
    // TODO: This is ugly, fix. ADJ_CLOSE is 4th on the list, but is 5th in the CSV file
    if(price == "Open") price_id_ = G_OPEN;
    else if(price == "High") price_id_ = G_HIGH;
    else if(price == "Low") price_id_ = G_LOW;
    else if(price == "Close") price_id_ = G_CLOSE;
    else if(price == "Adjusted close") price_id_ = G_ADJ_CLOSE;

    try {
	if (!a.is_valid()) throw std::runtime_error(
		std::string("You have entered an invalid date.\nExample: Feb 31st "
		+ std::string("is invalid because Feb does not have 31 days.")));
	std::cout << "DEBUG: m_Backend.run_month()" << std::endl;
	months_list = m_Backend.run_month(a, price_id_);
	// TODO: Progress bar here
    } catch(std::exception& e) {
	// Exceptions
	Gtk::MessageDialog dialog(*this, "Exception");
	dialog.set_secondary_text(e.what());
	dialog.run();
	return;
    }
	
    // Remove setting view, replace with summary view
    //m_Box.remove(_month_setting);
    clear_view();
    MonthSummary* monthsummary = new MonthSummary(months_list, price_id_);
    m_Box.pack_start(*monthsummary);

    // Signals
    monthsummary->signal_graph_toggle_clicked.connect(sigc::mem_fun(*this,
	&MainWindow::on_action_toggleview));
    
    months_ready_ = true;
    show_all_children();
}


void
MainWindow::run_sim_clicked(Tinydate::date a, std::string price)
{
    std::vector<tPeriod> v;
    // TODO: This is ugly, fix. ADJ_CLOSE is 4th on the list, but is 5th in the CSV file
    if(price == "Open") price_id_ = G_OPEN;
    else if(price == "High") price_id_ = G_HIGH;
    else if(price == "Low") price_id_ = G_LOW;
    else if(price == "Close") price_id_ = G_CLOSE;
    else if(price == "Adjusted close") price_id_ = G_ADJ_CLOSE;

    try {
	if (!a.is_valid()) throw std::runtime_error(
		std::string("You have entered an invalid date.\nExample: Feb 31st "
		+ std::string("is invalid because Feb does not have 31 days.")));
	// TODO: Progress bar here
    } catch(std::exception& e) {
	// Exceptions
	Gtk::MessageDialog dialog(*this, "Exception");
	dialog.set_secondary_text(e.what());
	dialog.run();
	return;
    }
	
    // Remove setting view, replace with summary view
    clear_view();
    SimView* simview = new SimView(a, price_id_);
    m_Box.pack_start(*simview);

    show_all_children();
}


/*void
MainWindow::run_testwindow_clicked()
{
    // Remove setting view, replace with summary view
    clear_view();
    TestView* testview = new TestView();
    m_Box.pack_start(*testview);

    show_all_children();
}*/
