#include "simview.h"
#include "news.h"
#include "ticker.h"
#include <string>
#include <iostream>
#include <gtkmm/linkbutton.h>
#include <gtkmm/scrolledwindow.h>

SimView::SimView(Tinydate::date d, int price_id)
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  cur_news_month(0),
  _now(d),
  plot_candlestick(true),
  price(price_id),
  _ticker_id(0),
  zoom_factor(0),
  _box_btn(Gtk::ORIENTATION_HORIZONTAL),
  _box_sum(Gtk::ORIENTATION_VERTICAL, 10),
  _box_plot(Gtk::ORIENTATION_VERTICAL, 10),
  _box_news(Gtk::ORIENTATION_VERTICAL, 10),
  _box_data(Gtk::ORIENTATION_VERTICAL, 10),
  _l_now(_now.get_date()),
  plot_()
{
    std::cout << "initialising" << std::endl;

    // Init tickers
    _tickers = _backend.GetTickers();
    _l_ticker.set_label(_tickers[_ticker_id]);

    // Arrows
    _box_btn.pack_start(_btn_ticker_prev, Gtk::PACK_SHRINK);
    _box_btn.pack_start(_l_ticker, Gtk::PACK_SHRINK);
    _box_btn.pack_start(_btn_ticker_next, Gtk::PACK_SHRINK);
    _box_btn.pack_start(_separator, Gtk::PACK_EXPAND_WIDGET);
    _box_btn.pack_start(_btn_prev, Gtk::PACK_SHRINK);
    _box_btn.pack_start(_l_now, Gtk::PACK_SHRINK);
    _box_btn.pack_start(_btn_next, Gtk::PACK_SHRINK);
    pack_start(_box_btn, Gtk::PACK_SHRINK);
 
    // Tabs
    pack_start(_tabs);
    _tabs.append_page(_box_sum, "Overview");
    _tabs.append_page(_box_plot, "Graph");
    _tabs.append_page(_box_news, "News");
    _tabs.append_page(_box_data, "Data");

    plot_.set_show_vol(true);
    plot_.set_mouse_on(true);
    _box_plot.pack_start(plot_, Gtk::PACK_EXPAND_WIDGET);

    // Signals
    _btn_prev.signal_clicked().connect(sigc::mem_fun(*this,
	&SimView::on_prev_clicked));
    _btn_next.signal_clicked().connect(sigc::mem_fun(*this,
	&SimView::on_next_clicked));
    _btn_ticker_prev.signal_clicked().connect(sigc::mem_fun(*this,
	&SimView::on_ticker_prev_clicked));
    _btn_ticker_next.signal_clicked().connect(sigc::mem_fun(*this,
	&SimView::on_ticker_next_clicked));
    _tabs.signal_switch_page().connect(sigc::mem_fun(*this,
	&SimView::on_tabs_switch_page));
    // TODO: This already triggers init_tab on page change
    // There is one too many init_tab call somewhere in the code
    show_all_children();
}

SimView::~SimView()
{
}

void
SimView::init_tab()
{
    // Set label
    _l_now.set_label(_now.get_date());
    _l_ticker.set_label(_tickers[_ticker_id]);
    cur_news_month = 0;

    if(_tabs.get_current_page() == 0) draw_summary();
    else if(_tabs.get_current_page() == 1) draw_graph();
    else if(_tabs.get_current_page() == 2) draw_news();
    else if(_tabs.get_current_page() == 3) draw_data();
}

void
SimView::draw_data()
{
    if(Gtk::Box* b = dynamic_cast<Gtk::Box*>(_tabs.get_nth_page(3))) {
	// Clear old child
	for(auto child : b->get_children()) b->remove(*child);
	
	// Get data
	Ticker t(_tickers[_ticker_id]);
	auto data = t.get_all(_now.get_prev_month(zoom_factor+1), _now, false);

	// Display data in a grid
	Gtk::Grid* g = new Gtk::Grid(); g->set_column_spacing(10);
	Gtk::ScrolledWindow* sw = new Gtk::ScrolledWindow();
	Gtk::Box *label_bg;
	Gtk::Label *l;

	// Loop over horiz/vert cells
	int i = 0;
	bool is_in_trade_period = false;
	for(auto line : data) {
	    int j = 0;
	    is_in_trade_period = (i == 1)? true : false; // highlight today
	    for(auto field : line) {
		l = new Gtk::Label(field); l->set_halign(Gtk::ALIGN_START);

		if((price == j) || is_in_trade_period) {
		    // Add colour behind the label
		    label_bg = new Gtk::Box();
		    if(price == j) {		    
			// Highlight the price column: OHLC
			label_bg->override_background_color(Gdk::RGBA("#bfbfbf")); // grey
		    } else {
			// Highlight trading period
			label_bg->override_background_color(Gdk::RGBA("#e6e6e6")); // light grey
		    }

		    label_bg->add(*l);
		    g->attach(*label_bg, j,i,1,1);
		} else {
		    // Plain label, no colour
		    g->attach(*l, j,i,1,1);
		}
		j++;
	    } // field
	    i++;
	} // line
	
	sw->add(*g);
	b->pack_start(*sw, Gtk::PACK_EXPAND_WIDGET);
	show_all_children();
    } //if box exists
}


void
SimView::draw_graph()
{
    // Plot
    Ticker t(_tickers[_ticker_id]);
    plot_.clear_data();
    Tinydate::date from, to;
    from = _now.get_prev_day(30*(zoom_factor+1));

    // Bollinger bands
    auto bol = t.get_bollinger_bands(from, _now, price, false, 20);

    if(plot_candlestick) {
	// Candlestick chart
	plot_.set_type("candlestick");
	auto data = t.get_all_data(from, _now, true);
	if( (data.size() > 0)
		&& (bol[0].size() > 0)
		&& (bol[1].size() > 0) 
		&& (bol[2].size() > 0) ) {
	    plot_.add_data(t.get_ticker(), data);
	    plot_.add_data("avg", bol[0]);
	    plot_.add_data("lo_band", bol[1]);
	    plot_.add_data("hi_band", bol[2]);
	}
    } else {
	// Line chart
	auto data = t.get(from, _now, price, true);
	if(data.size() > 0) plot_.add_data(t.get_ticker(), data);
    }

    auto volume = t.get(from, _now, G_VOLUME, true);
    plot_.add_data_vol(volume);

    // Set range
    // TODO: There is a bug in tinyplotlib::add_data which sets the from/to range wrong
    // TODO: We shouldn't have to set the range just before the render function
    plot_.range_from = from;
    plot_.range_to = _now;
    plot_.queue_draw();
}


void
SimView::draw_news()
{
/*
    if(Gtk::Box* tab = dynamic_cast<Gtk::Box*>(_tabs.get_nth_page(2))) {
	// Clear old data
	for(auto child : tab->get_children()) tab->remove(*child);

	// Get data
	tPeriod p = month_list[cur_month-1];
	News n;
	// date range
	Tinydate::date news_date_in, news_date_out;
	if(cur_news_month == 0 ) {
	    news_date_in = p.range_in;
	    news_date_out = p.range_out;
	} else if(cur_news_month < 0) {
	    news_date_in = p.range_in.get_prev_month(-cur_news_month);
	    news_date_out = p.range_out.get_prev_month(-cur_news_month);
	} else if(cur_news_month > 0) {
	    news_date_in = p.range_in.get_next_month(cur_news_month);
	    news_date_out = p.range_out.get_next_month(cur_news_month);
	}
	std::cout << "date: " << news_date_in.get_date() << std::endl;
	n.set_date_range(news_date_in, news_date_out);
	n.set_ticker(p.ticker); // ticker
	std::vector<News::NewsItem> v = n.get(); // get news

	// Display data in a grid
	using namespace Gtk;
	Grid* g = new Grid(); g->set_column_spacing(10); g->set_row_homogeneous(false);
	ScrolledWindow* sw = new ScrolledWindow();

	// Arrow buttons
	Box* b = new Box(ORIENTATION_HORIZONTAL, 10);
	Button* btn_prev = new Button("<");
	Button* btn_next = new Button(">");
	Label* l_month = new Label(news_date_in.year_month());
	b->pack_start(*btn_prev, PACK_SHRINK);
	b->pack_start(*l_month, PACK_SHRINK);
	b->pack_start(*btn_next, PACK_SHRINK);
    
	// Arrow button signals
	btn_prev->signal_clicked().connect(sigc::mem_fun(*this,
	    &SimView::on_news_prev_clicked));
	btn_next->signal_clicked().connect(sigc::mem_fun(*this,
	    &SimView::on_news_next_clicked));
	
	// Headers
	Label *l1 = new Label("Date");
	Label *l2 = new Label("Title");
	// align left
	for(auto label : { l1, l2 }) { label->set_halign(ALIGN_START); }
	g->attach(*l1, 0,0,1,1);
	g->attach(*l2, 1,0,1,1);

	// Data fields
	Label* pub_date;
	LinkButton* title;
	int i = 1;
	for(auto item : v) {
	    pub_date = new Label(item.pub_date.get_date()); pub_date->set_halign(ALIGN_START);
	    title = new LinkButton(item.url, item.title); title->set_halign(ALIGN_START);
	    g->attach(*pub_date, 0,i,1,1);
	    g->attach(*title, 1,i,1,1);
	    i++;
	}

	sw->add(*g);
	tab->pack_start(*b, PACK_SHRINK);
	tab->pack_start(*sw, PACK_EXPAND_WIDGET);
	show_all_children();
    } // if box
*/
}


void
SimView::draw_summary()
{
    std::cout << "Drawing summary" << std::endl;
    if(Gtk::Box* tab = dynamic_cast<Gtk::Box*>(_tabs.get_nth_page(0))) {
	// Clear old data
	for(auto child : tab->get_children()) tab->remove(*child);
	
	// Get data
	Ticker t(_tickers[_ticker_id]);

	// Display data in a grid
	using namespace Gtk;
	Gtk::Grid* g = new Grid(); g->set_column_spacing(10);
	// Labels
	Label *l1 = new Label("Ticker:");
	Label *l2 = new Label("Company:");
	//Label *l3 = new Label("In:");
	//Label *l4 = new Label("Out:");
	//Label *l5 = new Label("In price:");
	//Label *l6 = new Label("Out price:");
	//Label *l7 = new Label("Gain:");
	//Label *l8 = new Label("Gain %:");
	// Data
	Label *d1 = new Label(t.get_ticker());
	Label *d2 = new Label(t.get_name());
	//Label *d3 = new Label(p.gain.in.x.get_date());
	//Label *d4 = new Label(p.gain.out.x.get_date());
	//Label *d5 = new Label(std::to_string(p.gain.in.y));
	//Label *d6 = new Label(std::to_string(p.gain.out.y));
	//Label *d7 = new Label(std::to_string(p.gain.out.y-p.gain.in.y));
	//Label *d8 = new Label(std::to_string(100*(p.gain.out.y-p.gain.in.y)/p.gain.in.y));

	// Align labels to the left
	for(auto label : {
	    //l1,l2,l3,l4,l5,l6,l7,l8,
	    //d1,d2,d3,d4,d5,d6,d7,d8
	    l1,l2,
	    d1,d2
	}) { label->set_halign(ALIGN_START); }

	// Attach labels    
	g->attach(*l1, 0,0,1,1); g->attach(*d1, 1,0,1,1);
	g->attach(*l2, 0,1,1,1); g->attach(*d2, 1,1,1,1);
	//g->attach(*l3, 0,2,1,1); g->attach(*d3, 1,2,1,1);
	//g->attach(*l4, 0,3,1,1); g->attach(*d4, 1,3,1,1);
	//g->attach(*l5, 0,4,1,1); g->attach(*d5, 1,4,1,1);
	//g->attach(*l6, 0,5,1,1); g->attach(*d6, 1,5,1,1);
	//g->attach(*l7, 0,6,1,1); g->attach(*d7, 1,6,1,1);
	//g->attach(*l8, 0,7,1,1); g->attach(*d8, 1,7,1,1);
	tab->pack_start(*g, Gtk::PACK_EXPAND_WIDGET);
	show_all_children();
    } // if box
}


///// Signals
void
SimView::on_tabs_switch_page(Gtk::Widget* tab, guint page_num)
{
    init_tab();
}

// Days navigation
void
SimView::on_prev_clicked()
{
    _now.prev_day();
    init_tab();
}
void
SimView::on_next_clicked()
{
    _now.next_day();
    init_tab();
}

// Ticker navigation
void
SimView::on_ticker_prev_clicked()
{
    if(_ticker_id > 0) {
	_ticker_id--;
	init_tab();
    }
}
void
SimView::on_ticker_next_clicked()
{
    if(_ticker_id + 1 < _tickers.size()) {
	_ticker_id++;
	init_tab();
    }
}
void
SimView::go_to_first()
{
    _ticker_id = 0;
    init_tab();
}
void
SimView::go_to_last()
{
    _ticker_id = _tickers.size()-1;
    init_tab();
}

// News navigation
void
SimView::on_news_prev_clicked()
{
    cur_news_month--;
    draw_news();
}

void
SimView::on_news_next_clicked()
{
    cur_news_month++;
    draw_news();
}

bool
SimView::on_key_release_event(GdkEventKey* key_event)
{
    switch(key_event->keyval) {
	case GDK_KEY_bracketleft: // prev ticker
	    on_ticker_prev_clicked();
	    break;
	case GDK_KEY_bracketright: // next ticker
	    on_ticker_next_clicked();
	    break;
	case GDK_KEY_1: // first ticker
	    go_to_first();
	    break;
	case GDK_KEY_2: // last ticker
	    go_to_last();
	    break;
	case GDK_KEY_comma: // prev day
	    on_prev_clicked();
	    break;
	case GDK_KEY_period: // next day
	    on_next_clicked();
	    break;
	case GDK_KEY_minus: // zoom out
	    zoom_factor++;
	    init_tab();
	    break;
	case GDK_KEY_equal: // zoom in
	    if(zoom_factor > 0) {
		zoom_factor--;
		init_tab();
	    }
	    break;
	case GDK_KEY_0: // zoom reset
	    zoom_factor = 0;
	    init_tab();
	    break;
	default:
	    return Gtk::Box::on_key_release_event(key_event);
	    break;
    }
    return true;
}


