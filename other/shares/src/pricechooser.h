#ifndef PRICE_CHOOSER_H
#define PRICE_CHOOSER_H

#include "lib/tinydate.hpp"
#include <gtkmm/box.h>
#include <gtkmm/comboboxtext.h>
#include <string>

class PriceChooser : public Gtk::ComboBoxText
{
public:
    PriceChooser();
    virtual ~PriceChooser();
    std::string get_price();
};

#endif /* PRICE_CHOOSER_H */
