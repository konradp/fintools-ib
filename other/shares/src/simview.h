#ifndef SIM_VIEW_H
#define SIM_VIEW_H

#include "backend.h"
#include "lib/support.h"
#include "lib/tinyplot.hpp"
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/grid.h>
#include <gtkmm/notebook.h>
#include <gtkmm/separator.h>
#include <string>
#include <vector>

class SimView : public Gtk::Box
{
public:
    SimView(Tinydate::date d, int price_id);
    virtual ~SimView();

private:
    void init_tab();
    void draw_data();
    void draw_graph();
    void draw_news();
    void draw_summary();

    // Signal handlers
    void on_tabs_switch_page(Gtk::Widget* page, guint page_num);
    // Days navigation
    void on_prev_clicked();
    void on_next_clicked();
    // Ticker navigation
    void on_ticker_prev_clicked();
    void on_ticker_next_clicked();
    void go_to_first();
    void go_to_last();
    // News
    void on_news_prev_clicked();
    void on_news_next_clicked();

    bool on_key_release_event(GdkEventKey* event) override;

    Backend                     _backend;
    int                         cur_news_month;
    Tinydate::date              _now;
    bool                        plot_candlestick;
    int                         price;
    size_t                      _ticker_id;
    int                         zoom_factor;
    std::vector<std::string>    _tickers;

    // Widgets
    Gtk::Box _box_btn;
    Gtk::Box _box_sum, _box_plot, _box_news, _box_data;
    Gtk::Button _btn_prev = Gtk::Button("<");
    Gtk::Button _btn_next = Gtk::Button(">");
    Gtk::Button _btn_ticker_prev = Gtk::Button("<");
    Gtk::Button _btn_ticker_next = Gtk::Button(">");
    Gtk::Separator _separator;
    Gtk::Label _l_now, _l_ticker;
    Gtk::Notebook _tabs;
    Tinyplot::Plot plot_;
};

#endif // SIM_VIEW_H
