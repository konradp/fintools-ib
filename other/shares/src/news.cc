#include "news.h"
#include "news/google.hpp"
#include "news/yahoo.hpp"
#include <iostream> // DEBUG only

// This is an interface
// It calls the YahooNews, or GoogleNews classes

//PUBLIC
News::News()
:
news_feed_("google"),
date_from_(),
date_to_()  
{
}

News::~News()
{
}

// TODO: This should return some data
std::vector<News::NewsItem>
News::get()
{
    if(news_feed_ == "yahoo") {
	// Yahoo plugin
	YahooNews y;
	std::cout << y.get() << std::endl;
    } else if(news_feed_ == "google") {
	// Google plugin
	GoogleNews g;
	g.set_ticker(ticker_name_);
	g.set_date_from(date_from_);
	g.set_date_to(date_to_);
	return g.get();
    } else {
	std::cout << "The news feed specified has not been implemented yet." << std::endl;
    }
    std::vector<News::NewsItem> v = {
        NewsItem {
            "title",
            "url",
            Tinydate::date("2010-02-02"),
            "desc"
        }
    };
    return v;
}

// get/set methods
void
News::set_date_range(Tinydate::date from, Tinydate::date to)
{
    date_from_ = from;
    date_to_ = to;
}
