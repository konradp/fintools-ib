#include "ticker.h"
#include "lib/split.hpp"
#include <algorithm> // for std::reverse
#include <fstream> // for opening files
#include <iostream> // TEMPORARY
#include <math.h> // for sqrt()
using namespace std; // TEMPORARY

Ticker::Ticker(std::string name)
: t_name(name),
  t_data_dir("../data"),
  skip_zero_volume(true)
{
}

Ticker::~Ticker()
{
}

std::string
Ticker::get_ticker()
{
    return t_name;
}

std::string
Ticker::get_name()
{
    std::string f_path = t_data_dir + "/uk/FTSE/tickers.txt";
    std::string ticker_name = "";
    ifstream f(f_path);
    // Open file
    if(f.is_open()) {
	std::string line;
	std::vector<std::string> v;
	while(getline(f, line)) {
	    // Get each line, compare
	    v = split(",", line);
	    if(v[0] == t_name) {
		ticker_name = v[1];
		break;
	    }
	}
	f.close();
    }
    return ticker_name;
}

std::string
Ticker::get_sector()
{
    std::string sector;
    std::string f_path = t_data_dir + "/uk/FTSE/tickers.txt";
    ifstream f(f_path);
    // Open file
    if(f.is_open()) {
	std::string line;
	std::vector<std::string> v;
	while(getline(f, line)) {
	    // Get each line, compare
	    v = split(",", line);
	    if(v[0] == t_name) {
		if(v.size() == 3) sector = v[2];
		else sector = "?";
		break;
	    }
	}
	f.close();
    }
    return sector;
}


// Get v(date,value)
std::vector<std::pair<Tinydate::date, float>>
Ticker::get(Tinydate::date date_from, Tinydate::date date_to, int field, bool chrono, int extra_front)
{
    std::vector<std::pair<Tinydate::date, float>> v;

    // Open file
    std::string file_path = t_data_dir + std::string("/uk/FTSE/current/");
    file_path += t_name;

    ifstream file(file_path);
    if(file.is_open()) {
	// Lose first line
	std::string line;
	getline(file, line);

	std::vector<std::string> line_split;
	int i = 0;
	while(getline(file,line)) {
	    line_split = split(",", line);
	    if(skip_zero_volume && (stof(line_split[field]) == 0)) continue;

	    // If date within our range
	    if(Tinydate::date(line_split[0]) <= date_to) {
		if(Tinydate::date(line_split[0]) < date_from) {
		    if(i == extra_front)
			break;
		    else i++;
		}
		
		v.push_back(
		    make_pair(
			Tinydate::date(line_split[0]),
			stof(line_split[field])
		    )
		);
	    }
	} //while
    } //if
    file.close();
    if(v.size() > 0) if(chrono) std::reverse(v.begin(), v.end());
    return v;
}


// Get_all v(date,value)
std::vector<vector<std::string>>
Ticker::get_all(Tinydate::date date_from, Tinydate::date date_to, bool chrono)
{
    std::vector<std::vector<std::string>> v;
    std::vector<std::string> line_labels;

    // Open file
    std::string file_path = t_data_dir + std::string("/uk/FTSE/current/");
    file_path += t_name;

    ifstream file(file_path);
    if(file.is_open()) {
	// The first line is always the labels
	std::string line;
	getline(file, line);
	std::vector<std::string> line_v;
	line_labels = split(",", line);
	if(!chrono) v.push_back(line_labels);

	while(getline(file,line)) {
	    // Process the rest
	    line_v = split(",", line);
	    // If date within our range : This is incorrect? TODO TODO DEBUG
	    if( (Tinydate::date(line_v[0]) <= date_to) && (date_from < Tinydate::date(line_v[0])) )
            v.push_back(line_v);
            //break;
		//else
        //    break;
            //v.push_back(line_v);
	} //while
    } //if
    file.close();
    
    if(chrono) {
	v.push_back(line_labels);
	std::reverse(v.begin(), v.end());
    }
    return v;
}


// get_all_data v(date,value)
std::vector<std::pair<Tinydate::date, std::vector<float>>>
Ticker::get_all_data(Tinydate::date date_from, Tinydate::date date_to, bool chrono)
{
    std::vector<std::pair<Tinydate::date, std::vector<float>>> v;
    std::vector<std::string> line_labels;

    // Open file
    std::string file_path = t_data_dir + std::string("/uk/FTSE/current/");
    file_path += t_name;

    ifstream file(file_path);
        if(file.is_open()) {
        // The first line is always the labels, lose it
        std::string line;
        getline(file, line);
        std::vector<std::string> line_v;
        std::vector<float> line_f;

        while(getline(file,line)) {
            // Process the rest
            line_v = split(",", line);
            // If date within our range
            if( (Tinydate::date(line_v[0]) <= date_to)
                    && (Tinydate::date(line_v[0]) < date_from) ) {
                 break;
            } else {
                line_f = {
                    stof(line_v[1]),
                    stof(line_v[2]),
                    stof(line_v[3]),
                    stof(line_v[4])
                };
                v.push_back(make_pair(
                Tinydate::date(line_v[0]),
                line_f));
            }
        } //while
    } //if
    file.close();
    
    if(chrono) { std::reverse(v.begin(), v.end()); }
    return v;
}


/* STATISTICAL METHODS */

// get bollinger bands
// output: 0: avg, 1: lower, 2: upper
std::vector < std::vector<std::pair<Tinydate::date, float>> >
Ticker::get_bollinger_bands(Tinydate::date date_from, Tinydate::date date_to,
    int field, bool chrono, int extra_front)
{
    // Init
    int k = 2;

    // Main vector to hold moving avg, lower band, upper band
    std::vector < std::vector<std::pair<Tinydate::date, float>> > v;
    std::vector<std::pair<Tinydate::date, float>> data;
    std::vector<std::pair<Tinydate::date, float>> moving_avg;
    std::vector<std::pair<Tinydate::date, float>> deviation;
    std::vector<std::pair<Tinydate::date, float>> lo_band;
    std::vector<std::pair<Tinydate::date, float>> hi_band;

    // Get raw data + extra days
    data = get(date_from, date_to, field, chrono, extra_front);
    
    // Get moving average
    moving_avg = get_moving_avg(data, extra_front, true);
    v.push_back(moving_avg);

    // Upper/lower bands
    deviation = get_std_deviation(data, extra_front, true);

    for(size_t i = 0; i < deviation.size(); i++) {
	// Lower band
	lo_band.push_back(make_pair(
	    deviation[i].first,
	    moving_avg[i].second - k*deviation[i].second));
	// Upper band
	hi_band.push_back(make_pair(
	    deviation[i].first,
	    moving_avg[i].second + k*deviation[i].second));
    }
    
    v.push_back(lo_band);
    v.push_back(hi_band);
    return v;
}


// get_moving_avg v(date,value)
std::vector<std::pair<Tinydate::date, float>>
Ticker::get_moving_avg(std::vector<std::pair<Tinydate::date, float>> v, int period, bool chrono)
{
    std::vector<std::pair<Tinydate::date, float>> q;

    int n = v.size();
    int out_n = n - period; // this is how many data points we will get

    for(int i = 0; i < out_n; i++) {
	// for each average point
	float mean = 0.0;

	// mean
	for(int j = 0; j < period; j++) { mean += v[i+j].second; }
	mean = mean/(float) period;

	q.push_back(make_pair(v[i].first, mean));
    }
    
    if(chrono) { std::reverse(q.begin(), q.end()); }
    return q;
}


// get_std_deviation v(date,value)
std::vector<std::pair<Tinydate::date, float>>
Ticker::get_std_deviation(std::vector<std::pair<Tinydate::date, float>> v, int period, bool chrono)
{
    std::vector<std::pair<Tinydate::date, float>> q;

    int n = v.size();
    int out_n = n - period; // this is how many data points we will get

    for(int i = 0; i < out_n; i++) {
	// for each average point
	float mean = 0.0;
	float dev = 0.0;

	// mean
	for(int j = 0; j < period; j++) { mean += v[i+j].second; }
	mean = mean/(float) period;

	// sum of squared deviations from the mean
	for(int j = 0; j < period; j++) { dev += (v[i+j].second-mean)*(v[i+j].second-mean); }

	dev = dev/((float) period-1);
	dev = sqrt(dev);

	q.push_back(make_pair(v[i].first, dev));
    }
    
    if(chrono) { std::reverse(q.begin(), q.end()); }
    return q;
}


// Best gain
gain_pair
Ticker::get_best_gain(Tinydate::date date_from, Tinydate::date date_to, int property)
{
    // Say date_from is 2016-01-01 and that volume on that day is 0
    // get() will not include it
    xy_pair zero_xy = { Tinydate::date(), 0 };
    gain_pair zero_pair = { zero_xy, zero_xy, -10000};
    std::vector<std::pair<Tinydate::date, float>> v;
    v = get(date_from, date_to, property, false, 0);
    if(v.size() == 0)
	return zero_pair; // no data for given period

    // Make the first line the best gain so far
    t_gain_pair.in.x = t_gain_pair.out.x = v[0].first;
    t_gain_pair.in.y = t_gain_pair.out.y = v[0].second;
    t_gain_pair.gain = 0;
    xy_pair tmp_pair = { t_gain_pair.in.x, t_gain_pair.in.y };

    // Note, starting at the end of the month
    for(auto cur_pair : v) {
	if(cur_pair.second > tmp_pair.y) {
	    // Found higher out, take note
	    tmp_pair = { cur_pair.first, cur_pair.second };
	}
	if(tmp_pair.y - cur_pair.second > t_gain_pair.gain) {
	    // Make this our current best pair
	    t_gain_pair.in = { cur_pair.first, cur_pair.second };
	    t_gain_pair.out = { tmp_pair.x, t_gain_pair.out.y = tmp_pair.y };
	    t_gain_pair.gain = t_gain_pair.out.y - t_gain_pair.in.y;
	}
    }
    t_gain_pair.gain = (t_gain_pair.out.y - t_gain_pair.in.y) / t_gain_pair.in.y * 100;
    return t_gain_pair;
}
