#ifndef BACKEND_H
#define BACKEND_H

#include "lib/support.h"
#include "lib/files.h"
#include <string>
#include <vector>

class Backend {
public:
    Backend();
    virtual ~Backend();
    std::vector<tPeriod> run(Tinydate::date in_date, Tinydate::date out_date, int price);
    std::vector<tPeriod> run_month(Tinydate::date in_date, int price);
    std::vector<std::string> GetTickers();

private:
    std::vector<tPeriod> investRun(Tinydate::date in_date, Tinydate::date out_date, int price, bool skipZeroVol);
    std::vector<tPeriod> investRunMonth(Tinydate::date in_date, int price, bool skipZeroVol);

    // other
    std::string dataDir = std::string("../data");
    static bool compare_by_gain(const tPeriod &a, const tPeriod &b);
};

#endif /* BACKEND_H */
