#ifndef DATE_CHOOSER_H
#define DATE_CHOOSER_H

#include "lib/tinydate.hpp"
#include <gtkmm/box.h>
#include <gtkmm/comboboxtext.h>
#include <string>

class DateChooser : public Gtk::Box
{
public:
    DateChooser(bool include_day=true);
    virtual ~DateChooser();
    void init();
    Tinydate::date get_date();

private:
    // Widgets
    Gtk::ComboBoxText m_Day;
    Gtk::ComboBoxText m_Month;
    Gtk::ComboBoxText m_Year;
    std::string pad_zeros(std::string a);
    int yrFirst, yrLast;
};

#endif /* DATE_CHOOSER_H */
