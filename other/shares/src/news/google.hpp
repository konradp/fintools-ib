#ifndef GOOGLE_HPP
#define GOOGLE_HPP

#include "../lib/net.hpp"
#include "../news.h"
#include "../lib/tinydate.hpp"
#include "../lib/support.h"
#include <algorithm>
#include <map>

class GoogleNews {
public:
    GoogleNews() {};
    virtual ~GoogleNews() {};

    // get/set
    void set_ticker(std::string name) { ticker_name_ = name; };
    void set_date_from(Tinydate::date d) { date_from_ = d; };
    void set_date_to(Tinydate::date d) {date_to_ = d; };

    // Get the news for previously specified ticker and time period
    // This should return vector of News data type
    std::vector<News::NewsItem> get() {
	// Convert yahoo->google naming: e.g. SDR.L to LON:SDR
	ticker_name_ = "LON:" + ticker_name_.substr(0, ticker_name_.find("."));
	
	// Compile url
	std::string url("https://www.google.co.uk/finance/company_news?q=");
	url += ticker_name_ + "&startdate=" + date_from_.get_date()
	    + "&enddate=" + date_to_.get_date()
	    + "&output=rss"
	    + "&num=100";
    
	// Download
	Net n;
	std::string page = n.get(url);
	return parse(page);
    };

    // for sorting NewsItems by date
    static bool compare_by_date(const News::NewsItem &a, News::NewsItem &b) {
	return Tinydate::date(a.pub_date) < Tinydate::date(b.pub_date);
    };

    std::vector<News::NewsItem> parse(std::string r) {
	std::vector<News::NewsItem> v_items; // note: no error checking
	// should throw exception if v_items empty!
	std::string tmp_value("");
	
	std::string item("");
	while(true) {
	    item = std::string("");

	    // item
	    if(Support::ExtractString(item, r, "<item>", "</item>")) {
		News::NewsItem i;

		// title
		if(Support::ExtractString(tmp_value, item, "<title>", "</title>"))
		    i.title = Support::ReplaceHtmlChars(tmp_value);

		// link
		if(Support::ExtractString(tmp_value, item, "<link>", "</link>"))
		    i.url = tmp_value;

		// date
		if(Support::ExtractString(tmp_value, item, "<pubDate>", "</pubDate>")) {
		    // Convert to YYY-MM-DD, from Fri, 15 Jan 2016 21:22:30 GMT
		    Tinydate::date d(NewsStrToDate(tmp_value));
		    if(d.is_valid()) i.pub_date = d;
		    else std::cout << "Invalid date" << std::endl;
		}

		// description
		if(Support::ExtractString(tmp_value, item, "<description>", "</description>"))
		    i.desc = tmp_value;

		v_items.push_back(i);
	    } else break;
	}
	sort(v_items.begin(), v_items.end(), compare_by_date);
	return v_items;
    };

private:
    // values
    std::string ticker_name_;
    Tinydate::date date_from_;
    Tinydate::date date_to_;

    std::string MonthToNumber(std::string m) {
	std::map<std::string, std::string> months {
	    {"Jan", "01"},
	    {"Feb", "02"},
	    {"Mar", "03"},
	    {"Apr", "04"},
	    {"May", "05"},
	    {"Jun", "06"},
	    {"Jul", "07"},
	    {"Aug", "08"},
	    {"Sep" ,"09"},
	    {"Oct", "10"},
	    {"Nov", "11"},
	    {"Dec", "12"}
	};
	auto it = months.find(m);

	if(it != months.end())
	    return it->second;
	// fail
	return m;
    };

    // input: Fri, 15 Jan 2016 23:22:30 GMT
    //       *    *++*+++*++++*        *
    // we iterate over whitespaces
    // output: 2016-01-15
    std::string NewsStrToDate(std::string s) {
	std::string y, m, d, a;
	std::string("") = y = m = d = a;
	for(int i=0; i < 4; i++) {
	    // iterate over empty spaces
	    if(Support::ExtractString(a, s, "", " ")) {
		if(i == 1) d = a;
		if(i == 2) m = MonthToNumber(a);
		if(i == 3) y = a;
	    }
	}
	return y+"-"+m+"-"+d;
    };

}; // class YahooNews
#endif /* GOOGLE_HPP */
