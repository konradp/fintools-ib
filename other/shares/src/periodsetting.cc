#include "periodsetting.h"
#include <string>
#include <iostream>

PeriodSetting::PeriodSetting()
: Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0),
  lbl_dates_("Choose dates and data to analyse"),
  lbl_from_("From:"),
  lbl_to_("To:"),
  lbl_price_("Price type:"),
  date_chooser1_(false),
  date_chooser2_(false),
  price_chooser_(),
  btn_run_("Run")
{
    lbl_dates_.set_halign(Gtk::ALIGN_START);
    lbl_from_.set_halign(Gtk::ALIGN_START);
    lbl_to_.set_halign(Gtk::ALIGN_START);
    lbl_price_.set_halign(Gtk::ALIGN_START);

    // Signals
    btn_run_.signal_clicked().connect(sigc::mem_fun(*this,
	&PeriodSetting::run_clicked));

    // Add widgets
    pack_start(lbl_dates_);
    pack_start(lbl_from_, Gtk::PACK_SHRINK);
    pack_start(date_chooser1_, Gtk::PACK_SHRINK);
    pack_start(lbl_to_, Gtk::PACK_SHRINK);
    pack_start(date_chooser2_, Gtk::PACK_SHRINK);
    pack_start(lbl_price_, Gtk::PACK_SHRINK);
    pack_start(price_chooser_, Gtk::PACK_SHRINK);
    pack_start(separator_);
    pack_start(btn_run_, Gtk::PACK_SHRINK);
}

PeriodSetting::~PeriodSetting()
{
}

void
PeriodSetting::init()
{
    // Reset/initialise date choosers
    date_chooser1_.init();
    date_chooser2_.init();
}

void
PeriodSetting::run_clicked()
{
    signal_run_clicked.emit(date_chooser1_.get_date(),
	date_chooser2_.get_date(),
	price_chooser_.get_price());
}
