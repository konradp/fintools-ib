#include <iostream>
#include "../src/ticker.cc"

int main()
{
    using std::cout;
    using std::endl;
    using namespace Tinydate;
    // Test 1
    cout << "Test1: TW.L" << endl;
    Ticker a("TW.L");
    gain_pair g;
    Tinydate::date d1("2016-02-01"), d2("2016-02-29");
    g = a.get_best_gain(d1, d2, 4);
    cout << a.get_name() << endl;
    cout << d1.get_date() << ": " << d2.get_date() << ": " << g.gain << endl;
    cout << "In: " << g.in.x.get_date() << endl;
    cout << "Out: " << g.out.x.get_date() << endl;

    cout << endl;

    // Test 2
    cout << "Test2: AV.L" << endl;
    a = Ticker("AV.L");
    d1 = Tinydate::date("2017-01-01");
    d2 = Tinydate::date("2017-01-31");
    g = a.get_best_gain(d1, d2, 4);
    cout << a.get_name() << endl;
    cout << d1.get_date() << ": " << d2.get_date() << ": " << g.gain << endl;
    cout << "In: " << g.in.x.get_date() << endl;
    cout << "Out: " << g.out.x.get_date() << endl;


}
