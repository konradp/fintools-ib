# Create DB, tables etc
import mysql.connector
from fintools_ib.lib.config import Config

cfg = Config()
db_name = cfg['db']['name']


## Create DB
try:
  # Connect to DB server and create database
  print('Create database')
  db_connection = mysql.connector.connect(
    host = cfg['db']['host'],
    user = cfg['db']['user'],
    passwd = cfg['db']['password'],
  )
  db_cursor = db_connection.cursor()
  db_cursor.execute('CREATE DATABASE IF NOT EXISTS ' + db_name)
except Exception as e:
  print('Unable to create database:', e)
  exit(1)


# Connect to DB
try:
  db_connection = mysql.connector.connect(
    host = cfg['db']['host'],
    user = cfg['db']['user'],
    passwd = cfg['db']['password'],
    database = db_name,
  )
  db_cursor = db_connection.cursor()
except Exception as e:
  print('Unable to reconnect to database:', e)
  exit(1)


## SYMBOLS / CONIDS
try:
  print('CREATE TABLE symbols')
  db_cursor.execute('''
    CREATE TABLE IF NOT EXISTS symbols (
      symbol VARCHAR(10),
      conid INT
    )
  ''')
except Exception as e:
  print(e)
  exit(1)


## CONTRACTS
try:
  print('CREATE TABLE contracts')
  db_cursor.execute('''
    CREATE TABLE IF NOT EXISTS contracts (
      symbol VARCHAR(10),
      category VARCHAR(100),
      industry VARCHAR(100),
      sector VARCHAR(100)
    )
  ''')
except Exception as e:
  print(e)
  exit(1)


## SNAPSHOTS
#insert into quotes (symbol, o, c, h, l, v, t) values ('XP', 23.43, 23.42, 23.5, 23.17, 817.26,
#  from_unixtime(1701095400000.0/1000));
try:
  print('CREATE TABLE snapshots')
  db_cursor.execute('''
    CREATE TABLE IF NOT EXISTS snapshots (
      symbol VARCHAR(10),
      v VARCHAR(100),
      t DATE,
      price_last DECIMAL(10,2),
      price_change DECIMAL(10,2),
      price_change_perc DECIMAL(5,2),
      market_cap VARCHAR(100),
      PRIMARY KEY(symbol)
    )
  ''')
except Exception as e:
  print('ERROR:', e)
  exit(1)

db_connection.close()
