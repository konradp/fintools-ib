# Get conid from company, and vice versa
# This either downloads the data from IB, or from disk (cache)
import csv
import json
import os
import time
#import socket
# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.config import Config
from lib.skipsymbols import Skipsymbols
from lib.util import error
from lib.util import log
from lib.util import mkdir

class BackendDisk:
  # Properties
  conid = None
  contract = None
  industry = None
  symbol = None
  cfg = {}
  # Constructor
  def __init__(self):
    # Constructor will download (or get from cache)
    # Config
    cfg = Config()
    self.cfg = {
      'dir_base': cfg['paths']['base'],
      'dir_conids': cfg['paths']['conids'],
      'dir_contracts': cfg['paths']['contracts'],
      'dir_day': cfg['paths']['day'],
      'dir_quotes': cfg['paths']['quotes'],
    }
    print(f"Backend disk: Using paths: {self.cfg}")
    for name, path in self.cfg.items():
      # Ensure cache paths exist
      mkdir(path)
    # CONID
    # TODO: backend.get_conid_from_symbol(self.symbol)
    # TODO: Why would this try to download anything in constructor?
    #try:
    #  # Get conid from cache
    #  self.conid = self.disk_find_by('symbol', self.symbol)
    #  if self.conid == 'None':
    #    raise Exception(f'Conid not in cache: {self.symbol}')
    #except Exception as e:
    #  # Not found in cache, download
    #  try:
    #    print('%s: Down conid' % self.symbol)
    #    self.conid = self.down_conid()
    #    if self.conid == 'None':
    #      raise Exception('Unable to down conid')
    #    print('conid', self.conid)
    #  except Exception as e:
    #    print('%s: Could not down conid: %s' % (self.symbol, e))
    #    self.add_to_skip_list()
    #    raise Exception(e)
    ## CONTRACT
    ## TODO: backend.get_industry_from_symbol(self.symbol)
    #try:
    #  # Get industry from cache
    #  contract = self.get_contract()
    #  self.industry = contract['industry']
    #except Exception as e:
    #  error('Could not get contract from cache or download')
    #  # Add to bad list
    #  # TODO: backend.add_to_skip_list()
    #  self.add_to_skip_list()
    #  raise Exception(e)


  ##### PUBLIC #####
  def get_conid(self, symbol):
    # Convert symbol to conid
    # Example: f('AAPL') -> 1234
    try:
      file_conid = self.cfg['dir_conids'] + '/' + symbol
      with open(file_conid, 'r') as f:
        return f.read()
    except Exception as e:
      raise Exception('Symbol %s not found' % symbol)


  def get_quote(self, period, bar):
    # Get quote
    # Note, we already have conid from constructor
    # TODO: Get from cache maybe if not yet have it
    try:
      return self.down_quote(self.conid, period, bar)
    except ApiException as e:
      raise Exception('Could not download quote: %s\n' % self.conid)


  def get_quote_single(self):
    # Get quote for one day
    # TODO: Get from cache maybe if not yet have it
    try:
      return self.disk_find('quote')
    except ApiException as e:
      raise Exception('Could not find quote in cache: %s\n' % self.conid)


  def get_day_cache(self):
    # Get detailed quote for one day
    try:
      return self.disk_find('day')
    except ApiException as e:
      raise Exception('Could not find quote in cache: %s\n' % self.conid)


  def get_contract(self, redownload=False):
    # Get contract
    if not redownload:
      try:
        # Get contract from cache
        self.contract = self.disk_find(kind='contract')
        self.industry = self.contract['industry']
        return self.contract
      except Exception as e:
        # Not in cache, redownload
        log(f'WARN: Contract not in cache. Download {self.symbol}')
    try:
      # Re-download: Not in cache, or redownload=True
      self.contract = self.down_contract()
    except ApiException as e:
      raise Exception('Could not download contract: %s (%s): %s\n'
        % (self.symbol, self.conid, e))
    # Populate data
    self.industry = self.contract['industry']
    return self.contract


  def get_sectors_for_symbols(self, symbols):
    # TODO: Where should they live?
    dir_data = f"{self.cfg['dir_base']}/data"
    file_sectors = f"{dir_data}/nasdaq_screener_1736274256746_nasdaq.csv"
    sectors = {}
    print(f"dir_base: {dir_data}")
    print(f"DIR: {os.listdir(dir_data)}")
    with open(file_sectors, mode='r') as f:
      reader = csv.DictReader(f)
      #data = [row for row in reader]
      sectors = {
        i['Symbol']: i['Sector']
        for i in reader
        if i['Symbol'] in symbols
      }
      if len(sectors) != len(symbols):
        raise Exception(f"Number of sectors retrieved is not the same as number of symbols requested", sectors, symbols)
    return sectors


  ##### PRIVATE #####
  def add_to_skip_list(self):
    print('%s: Add to skip list' % self.symbol)
    skip = Skipsymbols()
    skip.add(self.symbol)


  def down_conid(self):
    # API: Get conid for symbol
    if self.conid is None:
      # Download conid
      api = ContractApi(ib_web_api.ApiClient(Config().IbConfig()))
      # Main
      try:
        # Get conid
        conid = int
        response = api.iserver_secdef_search_post({ "symbol": self.symbol })
        for item in response:
          if item.description == 'NASDAQ':
            self.conid = item.conid
      except ApiException as e:
        if self.conid is not None:
          detail = self.conid
        else:
          detail = self.symbol
        raise Exception(f'Could not download conid {detail}')
    # Save conid to disk
    with open(self.cfg['dir_conids'] + '/' + self.symbol, 'w') as f:
      f.write(str(self.conid))
    return self.conid


  def down_quote(self, conid, period, bar):
    api = MarketDataApi(ib_web_api.ApiClient(Config().IbConfig()))
    res = None
    for i in range(1, 6):
      try:
        # Download conid
        res = api.iserver_marketdata_history_get(
          conid,
          period=period,
          bar=bar
        )
      except Exception as e:
        print(f'Retry {i}/6')
        time.sleep(5)
        if i == 6:
          raise Exception(f'Could not get quote: {e}')
        else:
          continue
    # If three days, pick the last day
    # TODO: Do we need it like this?
    if period == '3d' and bar == '1d':
      if hasattr(res, 'points'):
        point = int(res.points)
        res = res.data[point].to_dict()
      else:
        raise Exception(f'Missing "points" for {self.conid}')
    else:
      if res is not None:
        res = [ i.to_dict() for i in res.data ]
      else:
        print('Got no data')
        print(res)
        raise Exception('Could not get quote: {self.conid}')
    return res


  def down_contract(self):
    # Download contract from IB and save it to disk
    # Note: Already have conid from cache
    try:
      # Init API
      api = ContractApi(ib_web_api.ApiClient(Config().IbConfig()))
      # Get contract (name, industry, etc)
      contract = api.iserver_contract_conid_info_get(self.conid)
      # Remove unnecessary data
      contract = {
        'conid': contract.con_id,
        'category': contract.category,
        'industry': contract.industry,
      }
      self.contract = contract
    except ApiException as e:
      print('ERROR')
      raise e
    try:
      # Save contract to disk
      with open(self.cfg['dir_contracts'] + '/' + self.symbol + '.json', 'w') as f:
        print(f'SAVE: {self.cfg["dir_contracts"]}: {self.symbol}')
        f.write(json.dumps(contract))
    except Exception as e:
      log(f'WARN: Could not save contract: {e}')
      #raise Exception('Could not save contract')
    return self.contract


  # TODO: ditch this, in favour of backend
  def disk_find_by(self, kind, value):
    # Convert symbol to conid or conid to symbol
    # kind: conid or symbol
    # value: e.g. 1234 or AAPL
    # NOTE: Usually used to get conid from symbol
    # TODO: Scrap this, just search by symbol
    if kind == 'symbol':
      # SYMBOL
      symbol = value
      try:
        file_conid = self.cfg['dir_conids'] + '/' + symbol
        with open(file_conid, 'r') as f:
          return f.read()
      except Exception as e:
        raise Exception('Symbol %s not found' % symbol)
    elif kind == 'day':
      # DAY
      symbol = value
      try:
        f_path = self.cfg['dir_day'] + '/' + symbol
        with open(f_path, 'r') as f:
          return f.read()
      except Exception as e:
        raise Exception('Symbol %s not found' % symbol)
    else:
      raise Exception('Search by conids not yet implemented')


  # TODO: ditch this, in favour of backend
  def disk_find(self, kind='contract'):
    # Find contract or quote in cache, to decide if we need to download it
    # again
    # kind: contract, quote, day
    if kind == 'contract':
      path = self.cfg['dir_contracts']
    elif kind == 'quote':
      path = self.cfg['dir_quotes']
    elif kind == 'day':
      path = self.cfg['dir_day']
    else:
      raise Exception('Invalid kind specified')
    try:
      # Find in cache
      fpath = path + '/' + self.symbol + '.json'
      with open(fpath, 'r') as f:
        return json.load(f)
    except Exception as e:
      raise Exception(f'Symbol {self.symbol} not found in cache at {fpath}: {e}')
