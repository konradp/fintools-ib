# Utilities and helpers
# get_perc_best_pair <- the working algo
# get_perc_best_pair_simple <- the working algo

# OS
import datetime
import json
import os
import requests
import sys
import time
import urllib.request

# Local
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.config import Config
from lib.skipsymbols import Skipsymbols

def chunk(data, size):
  # https://stackoverflow.com/questions/312443/how-do-i-split-a-list-into-equally-sized-chunks
  # Yield successive n-sized chunks from data (list)
  # for i in range(0, len(data), size):
  #   yield data[i:i + size]
  size = max(1, size)
  return list(data[i:i+size] for i in range(0, len(data), size))


def ohlc_to_median(data):
  # Given data: [ { o, h, l, c, v, t }, ... ]
  # convert the o/h/l/c to a single median price, e.g.
  # output: [ { avg, v, t }, ... ]
  # Note: Median price calculated as L+H/2
  return [
    {
      'p': (p['l'] + p['h']) / 2,
      'v': p['v'],
      't': p['t'],
    }
    for p in data
  ]


def request_get(url, params=None):
  RETRIES = 5
  retry_idx = 0
  print('i', url, params)
  user_agent_header = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) \
      AppleWebKit/537.36 (KHTML, like Gecko) \
      Chrome/129.0.0.0 Safari/537.36",
    "accept-language": "en-US,en;q=0.9",
  }
  while True:
    try:
      res = requests.get(url=url, params=params, verify=False, headers=user_agent_header)
      res.raise_for_status()
      if res.json() == {}:
        raise Exception('Empty response')
      if res.json() == { 'mktDataDelay': 0 }:
        raise Exception('Empty response, mktDataDelay')
      return res
    except Exception as e:
      retry_idx += 1
      print(f'Retry {retry_idx}: {url} {params}: {e}')
      if retry_idx > RETRIES:
        # Exceeded retries
        print(f"Exceeded retries: {RETRIES}")
        raise e
    time.sleep(10)

def request_post(url, data=None):
  try:
    print('url', url)
    print('data', data)
    res = requests.post(url=url, json=data, verify=False)
    res.raise_for_status()
    #if res.json() == {}:
    #  raise Exception('Empty response')
    #if res.json() == { 'mktDataDelay': 0 }:
    #  raise Exception('Empty response, mktDataDelay')
    return res.json()
    #return res
  except Exception as e:
    print(f"Unable to POST: {e}, {res.json()}")
    raise Exception(str(res.json()))


def db_to_json(data):
  # Fields of type DECIMAL, DATE, DATETIME are not readily converted
  # to JSON. This helps with that
  # https://gist.github.com/iancoleman/4536693
  from decimal import Decimal
  from datetime import date, datetime
  class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
      if isinstance(obj, Decimal):
        return float(obj)
      if isinstance(obj, (datetime, date)):
        return obj.isoformat()
      return json.JSONEncoder.default(self, obj)
  return DecimalEncoder().encode(data)


def down_symbols(cfg, apply_filters=True):
  # TODO: Review this
  # Download list of symbols (filter out also)
  url_nasdaq_list = cfg['urls']['nasdaq_list']
  debug = cfg['main']['debug']
  # Limit to conid
  download_conids_quotes_limit = cfg['main']['download_conids_quotes_limit']
  download_conid_limit_enable = cfg.getboolean('main', 'download_conid_limit_enable')
  download_conid_limit_symbol = cfg['main']['download_conid_limit_symbol']

  with urllib.request.urlopen(url_nasdaq_list) as response:
    # Get NASDAQ symbols
    symb_nasdaq = [ line.split('|')[0]
      for line in response.read().decode('utf-8').splitlines()[1:-1]
    ]
    # Take only N symbols (test)
    if debug:
      symb_nasdaq = symb_nasdaq[0:int(download_conids_quotes_limit)]
    # Take only one symbol if specified
    if download_conid_limit_enable:
      symb_nasdaq = [ download_conid_limit_symbol ]
    # Skip bymbols in format AAAAW
    symb_nasdaq = [
      symbol
      for symbol in symb_nasdaq
      if not (len(symbol) == 5 and symbol[4] == 'W')
    ]
    # Skip bad symbols
    skip = Skipsymbols().get()
    symb_nasdaq = [
      symbol
      for symbol in symb_nasdaq
      if symbol not in skip
    ]
  return symb_nasdaq


def error(msg):
  print(msg, file=sys.stderr)


def get(point, kind):
  # point = { o, c, h, l, v, t }
  # kind = 'h' or 'l'
  return { 't': point['t'], 'value': point[kind], 'v': point['v'] }


def get_contracts():
  # List contracts from disk
  try:
    cfg = Config()
  except Exception as e:
    raise Exception('Could not read config')
  l = sorted(os.listdir(cfg['paths']['contracts']))
  l = [ el.replace('.json', '') for el in l ]
  return l


def get_symbols():
  # List conids from disk
  # TODO
  try:
    cfg = Config()
  except Exception as e:
    raise Exception('Could not read config')
  l = sorted(os.listdir(cfg['paths']['conids']))
  l.remove('README.md')
  return l


def get_quotes():
  # List quotes from disk
  try:
    cfg = Config()
  except Exception as e:
    raise Exception('Could not read config')
  l = sorted(os.listdir(cfg['paths']['quotes']))
  l = [ el.replace('.json', '') for el in l ]
  return l

def get_perc_from_history(data, perc):
  # TODO: This is not working, review
  # data = {
  #  { 'o': 1, 'h': 2, 'l': 3, 'c': 4 },
  # ...
  # }
  # perc: e.g. 4 for 4%
  # Sort by timestamp
  data = sorted(data, key=lambda k: k['t'])
  # Get points
  hi = get(data[0], 'h')
  lo = get(data[0], 'l')
  points = []
  for point in data:
    hi = get(point, 'h')
    if point['l'] < lo['value']:
      lo = get(point, 'l')
    if hi['value']/lo['value'] > (100+perc)/100:
      v0 = lo
      v1 = hi
      points.append({
        't0': v0['t'],
        't1': v1['t'],
        'y0': v0['value'],
        'y1': v1['value'],
        'v0': v0['v'],
        'v1': v1['v'],
        'perc': (hi['value']/lo['value'])*100 - 100
      })
      # For next iteration
      lo = get(point, 'h')
  return points


def log(msg):
  with open('/var/log/fib.log', 'a') as f:
    date = datetime.datetime.today().strftime('%Y%m%d %H:%M')
    print(msg)
    f.write(f"{date}: {msg}\n")


def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)


def pad_left(lst, size):
  if len(lst) >= size:
    return lst
  count = size - len(lst)
  new = lst
  for i in range(count):
    new.insert(0, 0)
  return new

def parse_post_data(data):
  return json.loads(data.data.decode('utf-8'))


def timestamp_to_date(timestamp):
  timestamp = int(timestamp)
  return datetime.datetime.fromtimestamp(timestamp/1000).strftime('%Y%m%d')


# DATA METHODS
def get_point(point, kind):
  # Shorthand. Useful for get_perc_stats
  # kind = l or h
  # TODO: Revise this, I don't like it
  return {
    'price': point[kind],
    't': point['t'],
    'd': str(datetime.datetime.fromtimestamp(int(point['t']/1000))),
  }

def get_perc_stats(data):
  # TODO: Revise this, it gives no results
  # Get percent stats winning pairs in a given day
  # data: [
  # { o:,h:,l:,c:,v:,t }
  best_pairs = []
  lo = get_point(data[0], 'l')
  hi = lo
  for point in data:
    if point['l'] < lo['price']:
      # Current low is lower than previous low
      #print('Found lower')
      if lo['t'] != hi['t']:
        #print('Add last pair to best list')
        best_pairs.append({
          'l': lo,
          'h': hi,
        })
      lo = get_point(point, 'l')
      hi = lo
    #print('hi', hi)
    if point['h'] > hi['price']:
      # Higher
      #print('Found higher')
      hi = get_point(point, 'h')
    #print(point['h'])
  print(json.dumps(best_pairs, indent=2))


def get_perc_best_pair(data):
  # TODO: This is the working algo
  # TODO: Revise it, it's not very clear
  # Get percent stats winning pairs in a given day
  # data: [
  # { o:,h:,l:,c:,v:,t }
  best_pair = None
  # best_pair = { 'l': { price, t, d }, 'h': ... , 'perc' }
  lo = get_point(data[0], 'l')
  hi = lo
  for point in data:
    #print('%.2f %.2f' % (point['l'], point['h']))
    #print(get_point(point, 'l')[')
    perc = 100*(hi['price']-lo['price'])/lo['price']
    if point['l'] < lo['price']:
      # Lower
      #print('Found lower as %.2f < %.2f' %(point['l'], lo['price']))
      if lo['t'] != hi['t']:
        # Are different, so is a good pair
        if best_pair == None or \
          ( (best_pair != None) and best_pair['perc'] < perc):
          #print('Make previous pair the best pair')
          best_pair = {
            'l': lo,
            'h': hi,
            'perc': perc,
          }
      lo = get_point(point, 'l')
      hi = lo
    if point['h'] > hi['price']:
      # Higher
      # Is this new pair better than best pair?
      hi = get_point(point, 'h')
      perc = 100*(hi['price']-lo['price'])/lo['price']
      if best_pair == None or \
        ( (best_pair != None) and best_pair['perc'] < perc):
        # Make current pair the best pair
        #print('Make current best')
        best_pair = {
          'l': lo,
          'h': hi,
          'perc': perc,
        }
  return best_pair


def get_perc_best_pair_simple(data):
  # TODO: This is the working algo
  # TODO: Revise it, it's not very clear
  # Get percent stats winning pairs in a given day
  # input: [ { price, t1 }, { price, t2 }, ... ]
  # output: {
  #   buy: { price, t, d },
  #   sell: { price, t, d },
  #   perc: 4.0,
  # }
  pair_best = None
  lo = data[0]
  lo['d'] = str(datetime.datetime.fromtimestamp(int(lo['t']/1000)))
  hi = data[0]
  hi['d'] = str(datetime.datetime.fromtimestamp(int(hi['t']/1000)))
  for i in data:
    i['d'] = str(datetime.datetime.fromtimestamp(int(i['t']/1000)))
    if i['p'] < lo['p']:
      lo = i
    pair_candidate = {
      'buy': lo,
      'sell': i,
      'perc': 100*(i['p']-lo['p'])/lo['p'],
    }
    if pair_best == None or pair_candidate['perc'] > pair_best['perc']:
      pair_best = pair_candidate
  return pair_best

def plot_HA(df):
  # Convert plot data to Heikin Ashi data
  df['HA_Close']=(df['Open']+ df['High']+ df['Low']+df['Close'])/4
  idx = df.index.name
  df.reset_index(inplace=True)
  for i in range(0, len(df)):
    if i == 0:
      df.at[i, 'HA_Open'] = (df.at[i, 'Open'] + df.at[i, 'Close']) / 2
    else:
      df.at[i, 'HA_Open'] = (df.at[i - 1, 'HA_Open'] + df.at[i - 1, 'HA_Close']) / 2
  if idx:
    df.set_index(idx, inplace=True)
  df['HA_High']=df[['HA_Open','HA_Close','High']].max(axis=1)
  df['HA_Low']=df[['HA_Open','HA_Close','Low']].min(axis=1)
  # Reassign
  df['Open'] = df['HA_Open']
  df['High'] = df['HA_High']
  df['Low'] = df['HA_Low']
  df['Close'] = df['HA_Close']
  return df


def load_json_file(path):
  with open(path, 'r') as f:
    data = json.loads(f.read())
    return data

def split_by_date(data):
  # Given data, split it by day, e.g.
  # AAPL: [ohlcvt, ohlcvt, ...]
  # into
  # AAPL: { day1: [ ohlcvt, ohlcvt ], day2: ... }
  ret = {}
  for bar in data:
    # Split data by date
    date = datetime.datetime.fromtimestamp(bar['t']/1000)
    day = str(date).split()[0]
    if day not in ret:
      ret[day] = []
    ret[day].append(bar)
  return ret


def number_to_million(n):
  # input e.g.:  79.8K,   2.072M, 2647.974B
  # output e.g.: 0.0798M, 2.072M, 2647974.0M
  n = n.replace(',', '')
  suffix = n[-1]
  if suffix in ['B', 'K', 'M']:
    n = n[0:-1]
    n = float(n)
    if suffix == 'B':
      n = n * 1000
    elif suffix == 'M':
      n = n
    elif suffix == 'K':
      n = n / 1000
    else:
      print('ERROR: Unknown suffix', suffix)
  return n
