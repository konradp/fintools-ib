##### GET methods (from the backend: cache/DB)
import json
import os
import time
import urllib3
import urllib.request
#import socket
# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.backend import Backend
from lib.config import Config
from lib.ib import Ib
from lib.skipsymbols import Skipsymbols
from lib.util import error
from lib.util import log
from lib.util import mkdir

# Config
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class ApiGet:
  # Properties
  backend = None
  conid = None
  contract = None
  ib = None
  industry = None
  symbol = None
  cfg = {}


  # Constructor
  def __init__(self):
    print("Initialising backend")
    self.backend = Backend()
    self.ib = Ib()


  def get_symbols(self):
    # Gets etoro symbols actually (the symbols in the DB)
    # return: [ 'AAPL', 'AMZN', ... ]
    try:
      return self.backend.get_symbols()
    except Exception as e:
      raise Exception(f'Could not get symbols: {e}')

  def get_symbols_price_less_than(self, price):
    # return: [ 'AAPL', 'AMZN', ... ]
    try:
      return self.backend.get_symbols_price_less_than(price)
    except Exception as e:
      raise Exception(f'Could not get symbols: {e}')

  def get_conid(self, symbol):
    try:
      return self.backend.get_conid(symbol)
    except Exception as e:
      raise Exception(f'Could not get conid: {symbol}: {e}')

  def get_quote(self, symbol):
    try:
      return self.backend.get_quote(symbol)
    except Exception as e:
      raise Exception(f'Could not get quote: {symbol}: {e}')

  def get_contract(self, symbol):
    try:
      return self.backend.get_contract(symbol)
    except Exception as e:
      raise Exception(f'Could not get contract: {symbol}: {e}')

  def get_sectors_for_symbols(self, symbols):
    try:
      return self.backend.get_sectors_for_symbols(symbols)
    except Exception as e:
      raise Exception(f'Could not get sectors for symbols: {e}')
