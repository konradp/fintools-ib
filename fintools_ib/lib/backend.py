# Backend can be disk storage (data dir) or DB
# These methods will return data from the backend
# The backend is defined in config.cfg, [general] > backend = db or disk
from lib.config import Config
from lib.util import load_json_file

class Backend:
  # Properties
  cfg = {}
  backend = None

  def __init__(self):
    cfg = Config()
    self.cfg = cfg
    backend_type = cfg['general']['backend']
    print(f"Using backend type: {backend_type}")
    if backend_type not in ['disk', 'db']:
      # Backend can be 'disk' or 'db'
      raise Exception(f'Unsupported backend: {self.backend}')
    if backend_type == 'db':
      from lib.backend_db import BackendDb
      self.backend = BackendDb()
    if backend_type == 'disk':
      from lib.backend_disk import BackendDisk
      self.backend = BackendDisk()


  #### PUB methods ####
  def get_conid(self, symbol):
    return self.backend.get_conid(symbol)
  def get_market_cap(self, symbol):
    return self.backend.get_market_cap(symbol)
  def get_symbols(self):
    return self.backend.get_symbols()
  def get_symbols_price_less_than(self, price):
    return self.backend.get_symbols_price_less_than(price)
  def get_quote(self, symbol):
    return self.backend.get_quote(symbol)
  def get_sectors_for_symbols(self, symbols):
    return self.backend.get_sectors_for_symbols(symbols)


  # ETORO GETs
  def get_etoro_symbols(self):
    return sorted(load_json_file('../data/etoro_symbols.json'))
  def get_etoro_symbols_skip(self):
    return sorted(load_json_file('../data/etoro_symbols_skip.json'))


  # SAVE methods
  def save_conid(self, symbol, conid):
    self.backend.save_conid(symbol, conid)
  def save_symbol(self, symbol):
    self.backend.save_symbol(symbol)
  def save_contract(self, symbol, category, industry):
    self.backend.save_contract(symbol, category, industry)
  def save_price_last(self, symbol, price_last):
    self.backend.save_price_last(symbol, price_last)
  def save_quote(self, symbol, price):
    self.backend.save_quote(symbol, price)
  def save_snapshot(self, snapshot):
    self.backend.save_snapshot(snapshot)
  def save_secdef(self, secdef):
    self.backend.save_secdef(secdef)


  # DELETE methods
  ## delete individual symbols
  def delete_symbol(self, symbol):
    self.backend.delete_symbol(symbol)
  def delete_contract(self, symbol):
    self.backend.delete_contract(symbol)
  def delete_snapshot(self, symbol):
    self.backend.delete_snapshot(symbol)
  ## delete whole tables
  def delete_symbols(self):
    self.backend.delete_symbols()
  def delete_contracts(self):
    self.backend.delete_contracts()
