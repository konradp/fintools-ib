# The main API, types of requests:
# Note: get_, fetch_ methods are in their own api_fetch.py and api_get.py files.
# - get_*: Gets data from cache/DB
# - fetch_*: Gets data directly from external APIs
# - save_*: Save to cache/DB
# Most methods in this API operate on symbols/tickers instead of conids
import json
import os
import time
import urllib3
import urllib.request
#import socket
# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api_fetch import ApiFetch
from lib.api_get import ApiGet
from lib.backend import Backend
from lib.config import Config
from lib.ib import Ib
from lib.skipsymbols import Skipsymbols
from lib.util import error
from lib.util import log
from lib.util import mkdir

# Config
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
#socket.setdefaulttimeout(1)

class Api:
  # Properties
  backend = None
  conid = None
  contract = None
  ib = None
  industry = None
  symbol = None
  cfg = {}


  # Constructor
  def __init__(self):
    self.backend = Backend()
    self.ib = Ib()


  ##### SAVE methods (to save in backend)
  def save_symbol(self, symbol):
    try:
      return self.backend.save_symbol(symbol)
    except Exception as e:
      raise Exception(f'Could not save symbol: {symbol}: {e}')

  def save_contract(self, symbol, category, industry):
    try:
      self.backend.save_contract(symbol, category, industry)
    except Exception as e:
      raise Exception(f'Could not save contract: {symbol}: {e}')

  def save_price_last(self, symbol, price_last):
    try:
      self.backend.save_price_last(symbol, price_last)
    except Exception as e:
      raise Exception(f'Could not save price_last: {symbol}: {price_last}: {e}')

  def save_quote(self, symbol, price):
    try:
      self.backend.save_quote(symbol, price)
    except Exception as e:
      raise Exception(f'Could not save quote: {symbol} {price}: {e}')

  def save_snapshot(self, snapshot):
    try:
      self.backend.save_snapshot(snapshot)
    except Exception as e:
      raise Exception(f'Could not save snapshot: {snapshot}: {e}')


  ##### DELETE #####
  def delete_symbol(self, symbol):
    try:
      self.backend.delete_symbol(symbol)
      self.backend.delete_contract(symbol)
      self.backend.delete_snapshot(symbol)
    except Exception as e:
      raise Exception(f'Could not delete symbol: {symbol}: {e}')
    

  ##### PRIV ####
  def _symbols_to_conids(self, symbols):
    conids = []
    for symbol in symbols:
      conids.append(self.backend.get_conid(symbol))
    return conids


  ##### RAW #####
  def iserver_marketdata_snapshot_get(self, symbols, since, fields):
    try:
      conids = []
      for symbol in symbols:
        print('s', symbol)
        conids.append(self.backend.get_conid(symbol))
      return self.ib.iserver_marketdata_snapshot_get(conids, since=since, fields=fields)
      print(res)
      return res
    except Exception as e:
      print(f"ERROR: {e}")
      raise e

