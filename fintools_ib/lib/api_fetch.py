# The main API, types of requests:
# - get_*: Gets data from cache/DB
# - fetch_*: Gets data directly from external APIs
# - save_*: Save to cache/DB
# Most methods in this API operate on symbols/tickers instead of conids
import json
import os
import requests
import time
import urllib3
import urllib.request
#import socket
# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
#from lib.backend import Backend
from lib.config import Config
from lib.ib import Ib
from lib.skipsymbols import Skipsymbols
from lib.util import error
from lib.util import log
from lib.util import mkdir
from lib.util import request_get

# Config
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
#socket.setdefaulttimeout(1)

class ApiFetch:
  # Properties
  backend = None
  conid = None
  contract = None
  ib = None
  industry = None
  symbol = None
  cfg = {}
  user_agent_header = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) \
      AppleWebKit/605.1.15 (KHTML, like Gecko) \
      Chrome/100.0.4896.127 Safari/605.1.15 Firefox/100.0" 
  }


  # Constructor
  def __init__(self):
    #try:
    #  self.backend = Backend()
    #except Exception as e:
    #  pass
    #  # TODO: Handle properly
    #  #print('exception', e)
    self.ib = Ib()


  ##### FETCH methods (combined etoro and IB)
  def fetch_all_symbols_combined(self):
    from fintools_ib.lib.etoro import fetch_symbols
    symbols = []
    # eToro
    print('Fetch eToro symbols')
    etoro_symbols = fetch_symbols()
    # IB
    print('Fetch IB symbols')
    ib_conids = self.fetch_conids()
    for e_sym in etoro_symbols:
      i_sym = [ i for i in ib_conids if i['ticker'] == e_sym['ticker'] ]
      if len(i_sym) == 1:
        # Matched eToro to IB
        i_sym = i_sym[0]
        symbols.append({
          'ticker': i_sym['ticker'],
          'conid': i_sym['conid'],
          'name': e_sym['name'],
          'stocksIndustry': e_sym['stocksIndustry'],
        })
    symbols = sorted(symbols, key=lambda x: x['ticker'])

    return symbols


  ##### FETCH methods (get from ib)
  def fetch_conid(self, symbol):
    try:
      conid = self.ib.fetch_conid(symbol)
      if conid is None:
        raise Exception('conid is None')
      return conid
    except Exception as e:
      raise Exception(f'Could not fetch conid: {symbol}: {e}')


  def fetch_conids(self):
    # NASDAQ conids
    try:
      conid = self.ib.fetch_conids()
      if conid is None:
        raise Exception('conid is None')
      return conid
    except Exception as e:
      raise Exception(f'Could not fetch conids: {e}')


  def fetch_contract(self, symbol):
    try:
      conid = self.get_conid(symbol)
      return self.ib.fetch_contract(conid)
    except Exception as e:
      raise Exception(f'Could not get contract: {symbol}: {e}')


  #def fetch_history_old(self, symbol, period, bar, start):
  #  # Do not use, unreliable
  #  try:
  #    conid = self.get_conid(symbol)
  #    print(symbol, period, bar, start)
  #    return self.ib.fetch_history_old(conid, period, bar, start)
  #  except Exception as e:
  #    raise Exception(f'Could not get history: {symbol}: {e}')


  def fetch_history(self, symbol, period, bar, start):
    try:
      conid = self.backend.get_conid(symbol)
      return self.ib.fetch_history(conid, period, bar, start)
    except Exception as e:
      raise Exception(f'Could not get history: {symbol}: {e}')


  def fetch_history_for_conid(self, conid, period, bar, start=None):
    try:
      return self.ib.fetch_history(conid, period, bar, start)
    except Exception as e:
      raise Exception(f'Could not get history: {conid}: {e}')

  def fetch_history_for_conid_old(self, conid, period, bar, start=None):
    try:
      return self.ib.fetch_history_old(conid, period, bar, start)
    except Exception as e:
      raise Exception(f'Could not get history: {conid}: {e}')


  def fetch_price_for_date(self, symbol, date=None):
    # date: 20240102
    # ret:
    # {'t': 1711632600000,
    #   'o': 0.5564,
    #   'c': 0.58,
    #   'h': 0.574,
    #   'l': 0.5505,
    #   'v': 4583018.0
    # }
    try:
      conid = self.get_conid(symbol)
      item = self.ib.fetch_history(conid, '1d', '1d', date+'-00:00:00')
      return item['data'][0]
    except Exception as e:
      raise Exception(f'Could not get price for date: {symbol}: {e}')

  def fetch_history_year_close(self, symbol):
    try:
      conid = self.get_conid(symbol)
      return self.ib.fetch_history_year_close(conid)
    except Exception as e:
      raise Exception(f'Could not get history: {symbol}: {e}')

  def fetch_quote(self, symbol):
    try:
      conid = self.backend.get_conid(symbol)
      return self.ib.fetch_quote(conid)
    except Exception as e:
      raise Exception(f'Could not fetch quote: {symbol}: {e}')

  def fetch_scanner(self, instrument, scanner_type, location, filters):
    try:
      return self.ib.fetch_scanner(instrument, scanner_type, location, filters)
    except Exception as e:
      raise Exception(f'Could not fetch scanner results: {e}')

  def fetch_scanner_params(self):
    try:
      return self.ib.fetch_scanner_params()
    except Exception as e:
      raise Exception(f'Could not fetch scanner params: {e}')

  def fetch_secdefs(self, symbols):
    #{
    #  "secdef": [
    #    {
    #      "ticker": "MCRB",
    #      "group": "Pharmaceuticals",
    #      "sector": "Consumer, Non-cyclical",
    #      "sectorGroup": "Medical-Drugs",
    try:
      conids = []
      for symbol in symbols:
        conids.append(self.backend.get_conid(symbol))
      return self.ib.fetch_secdefs(conids)
    except Exception as e:
      raise Exception(f'Could not fetch secdefs: {symbols}: {e}')

  def fetch_secdefs_by_id(self, conids):
    #{
    #  "secdef": [
    #    {
    #      "ticker": "MCRB",
    #      "group": "Pharmaceuticals",
    #      "sector": "Consumer, Non-cyclical",
    #      "sectorGroup": "Medical-Drugs",
    try:
      return self.ib.fetch_secdefs(conids)
    except Exception as e:
      raise Exception(f'Could not fetch secdefs: {symbols}: {e}')

  def fetch_snapshot_raw(self, symbol):
    try:
      conid = self.backend.get_conid(symbol)
      return self.ib.fetch_snapshot_raw(conid)
    except Exception as e:
      raise Exception(f'Could not fetch snapshot: {symbol}: {e}')


  def fetch_snapshots(self, symbols):
    # {
    #   price_last:
    #   symbol:
    #   price_change:
    #   price_change_percent:
    #   v: Always in millions # TODO
    #   industry:
    #   category:
    #   market_cap: Always in millions
    try:
      #conids = {}
      conids = []
      for symbol in symbols:
        conids.append(self.backend.get_conid(symbol))
        #conids[symbol] = self.backend.get_conid(symbol)
      return self.ib.fetch_snapshots(conids)
    except Exception as e:
      print(f"ERROR: {e}")
      raise e


  ##### FETCH: bulk calls that use the /snapshot API #####
  ## TODO: Add _conids_to_symbols(conids)
  def fetch_contracts(self, symbols):
    try:
      conids = []
      for symbol in symbols:
        conids.append(self.backend.get_conid(symbol))
      return self.ib.fetch_contracts(conids)
    except Exception as e:
      print(f"ERROR: {e}")
      raise e


  def fetch_quotes(self, symbols):
    try:
      conids = self._symbols_to_conids(symbols)
      return self.ib.fetch_quotes(conids)
    except Exception as e:
      print(f"ERROR: {e}")
      raise e

  ##### YAHOO METHODS #####
  def fetch_yh_chart(self, symbol, datefrom, dateto, interval):
    # datefrom, dateto <- timestamp
    url = f'https://query2.finance.yahoo.com/v8/finance/chart/{symbol}?period1={datefrom}&period2={dateto}&interval={interval}'
    res = requests.get(url=url, headers=self.user_agent_header)
    #print(res.json())
    return res.json()


  ##### PRIV ####
  def _symbols_to_conids(self, symbols):
    conids = []
    for symbol in symbols:
      conids.append(self.backend.get_conid(symbol))
    return conids


  def fetch_nasdaq100(self):
    try:
      url = "https://api.nasdaq.com/api/quote/list-type/nasdaq100"
      res = request_get(url=url)
      return res.json()
    except Exception as e:
      raise Exception(f'Could not fetch nasdaq100: {e}')
