# These methods will return data from the DB (cache)
# Hence, they're all named get_* (and not fetch_*)
from lib.config import Config
import mysql.connector

class BackendDb:
  # Properties
  #cfg = {}
  backend = None
  db = None
  c = None # cdb.cursor()

  # Constructor
  def __init__(self):
    # TODO: If db not exists, error here and point to a
    # script which will create it (db-client should do it)
    try:
      # Connect to database
      cfg = Config()
      self.db = mysql.connector.connect(
        host = cfg['db']['host'],
        database = cfg['db']['name'],
        user = cfg['db']['user'],
        passwd = cfg['db']['password'],
      )
      self.c = self.db.cursor(dictionary=True)
    except Exception as e:
      raise Exception(f'Unable to connect to database: {e}')


  #### PUB METHODS ####
  def get_symbols(self):
    # return [ 'AAAA', 'BBBB', .... ] or []
    try:
      self.c.execute('''
        SELECT symbol
        FROM symbols
      ''')
      return [ i['symbol'] for i in self.c.fetchall() ]
    except Exception as e:
      print(e)


  def get_symbols_price_less_than(self, price):
    # OLD: return [ 'AAAA', 'BBBB', .... ] or []
    # return { 'AAAA': 123.45, 'BBBB': 123.45, .... } or {}
    try:
      self.c.execute('''
        SELECT symbol, price_last
        FROM snapshots
        WHERE price_last <= %(price)s
      ''', {
        'price': price,
      })
      return {
        i['symbol']: i['price_last']
        for i in self.c.fetchall()
      }
    except Exception as e:
      print(e)


  def get_conid(self, symbol):
    try:
      self.c.execute('''
        SELECT conid
        FROM symbols
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
      })
      return self.c.fetchall()[0]['conid']
    except Exception as e:
      print(e)


  def get_market_cap(self, symbol):
    try:
      self.c.execute('''
        SELECT market_cap
        FROM snapshots
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
      })
      return self.c.fetchall()[0]['market_cap']
    except Exception as e:
      print(e)


  def get_quote(self, symbol):
    try:
      self.c.execute('''
        SELECT o, c, h, l, v, t
        FROM quotes
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
      })
      #print('AAA', symbol, self.c.fetchall())
      return self.c.fetchall()
    except Exception as e:
      print(e)


  ##### SAVE methods #####
  def save_conid(self, symbol, conid):
    print(f"SAVE {symbol} {conid}")
    try:
      self.c.execute('''
        UPDATE symbols SET
          symbol = %(symbol)s,
          conid = %(conid)s
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
        'conid': conid,
      })
      self.db.commit()
    except Exception as e:
      print(e)

  def save_contract(self, symbol, category, industry):
    try:
      self.c.execute('''
        INSERT INTO contracts (
          symbol,
          category,
          industry
        ) VALUES (
          %(symbol)s,
          %(category)s,
          %(industry)s
        )
      ''', {
        'symbol': symbol,
        'category': category,
        'industry': industry,
      })
      self.db.commit()
    except Exception as e:
      print(e)


  def save_price_last(self, symbol, price_last):
    #print('SNAPSHOT', snapshot)
    try:
      self.c.execute('''
        REPLACE INTO snapshots (
          symbol,
          price_last
        ) VALUES (
          %(symbol)s,
          %(price_last)s
        )
      ''', {
        'symbol': symbol,
        'price_last': price_last,
      })
      self.db.commit()
    except mysql.connector.Error as e:
      #print(f"Something went wrong: {e}")
      raise Exception(f'Failed to save price_last: {e}')
      #print(snapshot)
    #except Exception as e:
    #   print('error', e)

  def save_quote(self, symbol, price):
    print(f"SAVE {symbol} {price}")
    try:
      self.c.execute('''
        INSERT INTO prices (
          symbol,
          
        ) VALUES (
          symbol = %(symbol)s,
          conid = %(conid)s
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
        'conid': conid,
      })
      self.db.commit()
    except Exception as e:
      print(e)

  def save_secdef(self, secdef):
    # The only thing we need from here is the sector
    # contracts:
    # - symbol
    # - sector
    print(f"SAVE {secdef['symbol']}")
    try:
      self.c.execute('''
        UPDATE contracts SET
          sector = %(sector)s
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': secdef['symbol'],
        'sector': secdef['sector']
      })
      self.db.commit()
    except Exception as e:
      print(e)

  def save_snapshot(self, snapshot):
    #print('SNAPSHOT', snapshot)
    try:
      self.c.execute('''
        REPLACE INTO snapshots (
          symbol,
          price_last,
          price_change,
          price_change_perc,
          market_cap,
          v
        ) VALUES (
          %(symbol)s,
          %(price_last)s,
          %(price_change)s,
          %(price_change_perc)s,
          %(market_cap)s,
          %(v)s
        )
      ''', {
        'symbol': snapshot['symbol'],
        'price_last': snapshot['price_last'],
        'price_change': snapshot['price_change'],
        'price_change_perc': snapshot['price_change_perc'],
        'market_cap': snapshot['market_cap'],
        'v': snapshot['v'],
      })
      self.db.commit()
    except mysql.connector.Error as e:
      #print(f"Something went wrong: {e}")
      raise Exception(f'Failed to save snapshot: {e}')
      #print(snapshot)
    #except Exception as e:
    #   print('error', e)

  def save_symbol(self, symbol):
    try:
      self.c.execute('''
        INSERT INTO symbols (
          symbol
        ) VALUES (
          %(symbol)s
        )
      ''', {
        'symbol': symbol,
      })
      self.db.commit()
    except Exception as e:
      print(e)


  ##### DELETE methods: individual symbols ####
  def delete_symbol(self, symbol):
    try:
      self.c.execute('''
        DELETE FROM symbols
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
      })
      self.db.commit()
    except Exception as e:
      print(e)
  def delete_contract(self, symbol):
    try:
      self.c.execute('''
        DELETE FROM contracts
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
      })
      self.db.commit()
    except Exception as e:
      print(e)
  def delete_snapshot(self, symbol):
    try:
      self.c.execute('''
        DELETE FROM snapshots
        WHERE symbol = %(symbol)s
      ''', {
        'symbol': symbol,
      })
      self.db.commit()
    except Exception as e:
      print(e)

  def delete_contracts(self):
    try:
      self.c.execute('''
        DELETE FROM contracts
        WHERE 1=1
      ''')
      self.db.commit()
    except Exception as e:
      print(e)
  ##### DELETE methods: whole tables ####
  def delete_symbols(self):
    try:
      self.c.execute('''
        DELETE FROM symbols
        WHERE 1=1
      ''')
      self.db.commit()
    except Exception as e:
      print(e)

  def delete_contracts(self):
    try:
      self.c.execute('''
        DELETE FROM contracts
        WHERE 1=1
      ''')
      self.db.commit()
    except Exception as e:
      print(e)
