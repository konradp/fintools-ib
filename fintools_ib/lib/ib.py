# Get (fetch) data from IB
# Note: All the methods here pull from external API, not from disk nor cache
# Hence, they are named fetch_*
import json
import os
import pprint
import requests
import time
import urllib3
import urllib.request
from traceback import print_exc
#import socket
# Local
#from fintools_ib.lib.backend import Backend
from fintools_ib.lib.config import Config
from fintools_ib.lib.skipsymbols import Skipsymbols
from fintools_ib.lib.util import chunk
from fintools_ib.lib.util import error
from fintools_ib.lib.util import log
from fintools_ib.lib.util import mkdir
from fintools_ib.lib.util import number_to_million
from fintools_ib.lib.util import pad_left
from fintools_ib.lib.util import request_get, request_post
from fintools_ib.lib.util import timestamp_to_date

# Config
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Ib:
  cfg = {}
  debug = False
  url = None

  # Constructor
  def __init__(self):
    # TODO: Check connectivity here
    cfg = Config()
    self.cfg = cfg
    self.url = cfg['ibConfig']['host']
    self.debug = True if cfg['general']['debug'] == 'True' else False


  ##### PUB #####
  def fetch_conid(self, symbol):
    try:
      conid = None
      request_url = f"{self.url}/trsrv/all-conids?exchange=NASDAQ"
      res = requests.get(url=request_url, verify=False)
      res = res.json()
      #print(json.dumps(res, indent=2))
      for item in res:
        if item['ticker'] == symbol:
          conid = item['conid']
      return conid
    except Exception as e:
      raise Exception(f"Could not download conid for {symbol}: {e}: HTTP status code: {res.status_code}")


  def fetch_conids(self):
    ## All conids for NASDAQ
    try:
      request_url = f"{self.url}/trsrv/all-conids?exchange=NASDAQ"
      res = requests.get(url=request_url, verify=False)
      #print(res) # DEBUG
      conids = res.json()
      return conids
    except Exception as e:
      if res.status_code == 429:
        raise Exception(f"Could not download conids, HTTP status code: {res.status_code}, rate limit exceeded")
      else:
        raise Exception(f"Could not download conids: {e}: HTTP status code: {res.status_code}")


  def fetch_contract(self, conid):
    # TODO: still need this?
    # contains category, industry
    try:
      api = ContractApi(ib_web_api.ApiClient(Config().IbConfig()))
      contract = api.iserver_contract_conid_info_get(conid)
      return contract
    except Exception as e:
      print('ERROR')
      raise e


  def fetch_history(self, conid, period, bar, start=None):
    # start: 20240102-13:30:00
    try:
      url = f"{self.url}/hmds/history"
      params = {
        'conid': conid,
        'period': period,
        'bar': bar,
        'outsideRth': 'false',
        'direction': '1',
      }
      if start is not None:
        params['startTime'] = start
      #res = requests.get(url=url, params=params, verify=False)
      res = request_get(url=url, params=params)
      conids = res.json()
      return conids
    except Exception as e:
      raise Exception(f"Could not download conids: {e}")


  def fetch_history_old(self, conid, period, bar, start=None):
    # start: 20240102-13:30:00
    # Do not use, unreliable
    try:
      request_url = f"{self.url}/iserver/marketdata/history"
      params = {
        'conid': conid,
        'period': period,
        'bar': bar,
      }
      if start is not None:
        print('start', start)
        params['startTime'] = start
      res = request_get(url=request_url, params=params)
      conids = res.json()
      return conids
    except Exception as e:
      raise Exception(f"Could not download conids: {e}")


  def fetch_history_year_close(self, conid):
    # history for conid
    # a year of daily close prices (or ohlc?)
    try:
      api = MarketDataApi(ib_web_api.ApiClient(Config().IbConfig()))
      history = api.iserver_marketdata_history_get(
        conid=conid,
        period='1y',
        bar='1d',
      )
      ret = [
        [ timestamp_to_date(i.t), i.c ]
        for i in history.data
      ]
      return ret
    except Exception as e:
      print('ERROR')
      raise e


  def fetch_hmds_history(self, conid, period, bar):
    # period: min, h, d, w, m, y
    # bar: min, h, d, w, m
    try:
      url = f"{self.url}/hmds/history"
      params = {
        'conid': conid,
        'period': period,
        'bar': bar,
      }
      #res = requests.get(url=url, params=params, verify=False)
      res = request_get(url=url, params=params)
      conids = res.json()
      return conids
    except Exception as e:
      raise Exception(f"Could not download conids: {e}")


  def fetch_scanner_params(self):
    try:
      request_url = f"{self.url}/iserver/scanner/params"
      res = request_get(url=request_url)
      params = res.json()
      return params
    except Exception as e:
      raise Exception(f"Could not fetch scanner params: {e}")


  def fetch_scanner(self, instrument, scanner_type, location, filters):
    try:
      request_url = f"{self.url}/iserver/scanner/run"
      data = {
        'instrument': instrument,
        'type': scanner_type,
        'location': location,
        'filter': filters,
      }
      res = request_post(url=request_url, data=data)
      return res
    except Exception as e:
      raise Exception(f"Could not fetch scanner results: {e}")


  def fetch_secdefs(self, conids, trimmed=True):
    #{
    #  "secdef": [
    #    {
    #      "ticker": "MCRB",
    #      "conid": 12345,
    #      "group": "Pharmaceuticals",
    #      "name": "ABC company",
    #      "assetClass": "STK",
    #      "sector": "Consumer, Non-cyclical",
    #      "sectorGroup": "Medical-Drugs",
    #      "type": "COMMON",
    try:
      request_url = f"{self.url}/trsrv/secdef"
      conid_chunks = chunk([ str(conid) for conid in conids ], 300)
      ret = []
      for conids in conid_chunks:
        params = {
          'conids': ','.join(conids),
        }
        res = requests.get(url=request_url, params=params, verify=False)
        res = res.json()
        for i in res['secdef']:
          ret.append(i)

      if trimmed:
        ret_trimmed = [
          {
            'ticker': i['ticker'],
            'conid': i['conid'],
            'group': i['group'],
            'name': i['name'],
            'assetClass': i['assetClass'],
            'sector': i['sector'],
            'sectorGroup': i['sectorGroup'],
            'type': i['type'],
          }
          for i in ret
        ]
        return ret_trimmed
      return ret
    except Exception as e:
      print('ERROR')
      raise e


  ##### FETCH: bulk calls that use /snapshot API #####
  # fields: https://konradp.gitlab.io/blog-fin/posts/tech-ib-api-client-beta/#market-data-snapshot-fields
  def fetch_contracts(self, conids):
    # _55: symbol
    # 7281: Category
    # 7280: Industry
    try:
      request_url = f"{self.url}/iserver/marketdata/snapshot"
      conids = [ str(conid) for conid in conids ]
      conids = chunk(conids, 300)
      ret = []
      for conids_chunk in conids:
        conids_chunk = ','.join(conids_chunk)
        params = {
          'conids': conids_chunk,
          'fields': '55,7281,7280',
        }
        res = requests.get(url=request_url, params=params, verify=False)
        time.sleep(2)
        res = requests.get(url=request_url, params=params, verify=False)
        res = res.json()
        for i in res:
          ret.append({
            'symbol': i._55,
            'category': i._7281,
            'industry': i._7280,
          })
      return ret
    except Exception as e:
      print('ERROR')
      raise e


  def fetch_snapshot_raw(self, conid):
    # 31: last price
    # 55: symbol
    # 7296: close price
    request_url = f"{self.url}/iserver/marketdata/snapshot"
    params = { 'conids': conid, 'fields': '55,31,7296' }
    res = requests.get(url=request_url, params=params, verify=False).json()
    return res

  def fetch_snapshots(self, conids):
    # This takes as many conids as you want, and chunks them into 300-size batches
    # conids = { 'AAPL': 265598, 'AMZN': 265598, ... }
    # Returns price_last (field 31)
    # Note: field 55 is symbol
    ret = {}
    try:
      # Divide conids into chunks of 300
      #conids = symbols_conids.values()
      conids = chunk([ str(conid) for conid in conids ], 300)
      chunk_idx = 0
      for conids_chunk in conids:
        chunk_idx += 1
        for attempt in range(0,5):
          # Retry up to 5 times
          need_retry = False
          if attempt > 0:
            print(f"Chunk {chunk_idx}, retry attempt {attempt}")
          request_url = f"{self.url}/iserver/marketdata/snapshot"
          params = { 'conids': ','.join(conids_chunk), 'fields': '55,31' }
          res = requests.get(url=request_url, params=params, verify=False).json()
          for j in res:
            if j['conid'] == -1:
              print('Skipping ', j)
              continue
            conid = j['conid']
            if '55' in j:
              symbol = j['55']
            else:
              print(f"WARN: Symbol is empty, skipping: conid={conid}")
              print(j)
            if symbol == '-':
              continue
            # Init empty fields and update price_last if found non_empty
            if symbol not in ret:
              ret[symbol] = None
            if '31' in j and j['31'] is not None:
              price_last = j['31']
              if price_last.startswith('C'):
                price_last = price_last[1:]
              ret[symbol] = price_last
            else:
              print(f"WARN: price_last is None: symbol={symbol}, conid={conid}")
            # Check if need a retry
            if ret[symbol] is None:
              need_retry = True
          time.sleep(2)
          if not need_retry:
            break
          if attempt == 4:
            for symbol, price_last in ret.items():
              if price_last is None:
                print(f"ERROR: {symbol}: price_last is None")
            exit(1)
      #pprint.pprint(ret)
      return ret
    except Exception as e:
      print('ERROR')
      raise e


  #def fetch_snapshots(self, conids):
  #  # This takes as many conids as you want, and chunks them into 300-size batches
  #  # Fields:
  #  # 31: price_last
  #  # 55: symbol
  #  # 82: price_change: Change Price
  #  # 83: price_change_perc: Change Percent
  #  # 87: v: Volume
  #  # 7289: market_cap: Market Cap
  #  # 7296: close price
  #  # t: date
  #  fields = '31,55,82,83,87,7289,7296'
  #  try:
  #    api = MarketDataApi(ib_web_api.ApiClient(Config().IbConfig()))
  #    conids = [ str(conid) for conid in conids ]
  #    conids = chunk(conids, 300)
  #    ret = []
  #    for conids_chunk in conids:
  #      conids_chunk = ','.join(conids_chunk)
  #      res = api.iserver_marketdata_snapshot_get(conids=conids_chunk, fields=fields)
  #      #print(res)
  #      time.sleep(2)
  #      res = api.iserver_marketdata_snapshot_get(conids=conids_chunk, fields=fields)
  #      tmp_data = {}
  #      #print(res)
  #      for i in res:
  #        #print(i)
  #        conid = i.conid
  #        symbol = i._55
  #        if symbol == '-':
  #          print(f"WARN: Symbol is empty, skipping: symbol={symbol}, conid={conid}")
  #          continue
  #        for j in range(0,5):
  #          # Retry 6 times
  #          if i._31 is None:
  #            # TODO: Problem: price_last can be empty
  #            print(f"WARN: Retry. price_last is None. symbol={symbol}, conid={conid}")
  #            time.sleep(2)
  #          else:
  #            break
  #          i = api.iserver_marketdata_snapshot_get(conids=[conid], fields=fields)[0]
  #        tmp = {
  #          'price_last': i._31,
  #          'symbol': symbol,
  #          'price_change': i._82,
  #          'price_change_perc': i._83,
  #          'v': i._87,
  #          'market_cap': i._7289,
  #          'industry': i._7280,
  #          'category': i._7281,
  #          'conid': conid,
  #        }
  #        # TODO: Handle these problems, e.g. by retry
  #        has_empty = False
  #        for item in ['price_last', 'market_cap']:
  #          if tmp[item] is None:
  #            print(f"WARN: {item} is None: symbol={symbol}")
  #            has_empty = True
  #        if self.debug and has_empty:
  #          print(tmp)
  #        if tmp['price_last'] is not None:
  #          if tmp['price_last'].startswith('C'):
  #            tmp['price_last'] = tmp['price_last'][1:]
  #        if tmp['market_cap'] is not None:
  #          tmp['market_cap'] = number_to_million(tmp['market_cap'])
  #        if tmp['v'] is not None:
  #          tmp['v'] = number_to_million(tmp['v'])
  #        ret.append(tmp)
  #    return ret
  #  except Exception as e:
  #    print('ERROR')
  #    raise e


  def iserver_marketdata_snapshot_get(self, conids, since=None, fields=None):
    # fields: https://konradp.gitlab.io/blog-fin/posts/tech-ib-api-client-beta/#market-data-snapshot-fields
    # _31: last price
    # _55: symbol
    try:
      api = MarketDataApi(ib_web_api.ApiClient(Config().IbConfig()))
      res = api.iserver_marketdata_snapshot_get(conids='395194484', fields='7637')
      res = api.iserver_marketdata_snapshot_get(conids='395194484', fields='7637')
      return res
    except Exception as e:
      print('ERROR')
      raise e
