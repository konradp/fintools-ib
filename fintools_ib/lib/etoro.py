import urllib3
import requests
import json # DEBUG

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def fetch_instrument_groups():
  # {
  #   "InstrumentTypes": [
  #     { "InstrumentTypeID": 5, "InstrumentTypeDescription": "Stocks" },
  #     ...
  #   ],
  #   "ExchangeInfo": [
  #     { "ExchangeID": 4, "ExchangeDescription": "NASDAQ" },
  #   "StocksIndustries"
  #   "CryptoCategories"
  # }
  url = 'https://api.etorostatic.com/sapi/app-data/web-client/app-data/instruments-groups.json'
  res = requests.get(url=url, verify=False)
  res = res.json()
  return res


def fetch_symbols():
  instrument_groups = fetch_instrument_groups()
  # Map instrumentTypeId to instrumentTypeDescription
  instrumentTypes = {
    x['InstrumentTypeID']: x['InstrumentTypeDescription']
    for x in instrument_groups['InstrumentTypes']
  }
  exchangeInfo = {
    x['ExchangeID']: x['ExchangeDescription']
    for x in instrument_groups['ExchangeInfo']
  }
  stockIndustries = {
    x['IndustryID']: x['IndustryName']
    for x in instrument_groups['StocksIndustries']
  }

  # Get data
  url = 'https://api.etorostatic.com/sapi/instrumentsmetadata/V1.1/instruments/bulk?bulkNumber=1&totalBulks=1'
  res = requests.get(url=url, verify=False)
  res = res.json()
  res = res['InstrumentDisplayDatas']
  ret = []

  # Pick only the fields that we care about
  for x in res:
    if x['ExchangeID'] not in exchangeInfo:
      # Skip unknown exchanges (probably currencies)
      continue

    try:
      y = {
        #'instrumentId': x['InstrumentID'],
        'ticker': x['SymbolFull'],
        'name': x['InstrumentDisplayName'],
        'exchange': exchangeInfo[x['ExchangeID']],
        'instrumentType': instrumentTypes[x['InstrumentTypeID']],
      }
    except Exception as e:
      print('Error', e)
      print(json.dumps(x, indent=2))
      exit(1)
      continue

    if 'InstrumentTypeSubCategoryID' in x:
      y['instrumentTypeSubCategoryId'] = x['InstrumentTypeSubCategoryID']

    if 'StocksIndustryID' in x:
      y['stocksIndustry'] = stockIndustries[x['StocksIndustryID']]

    if x['IsInternalInstrument'] == True:
      # Skip internal instruments (not tradeable)
      continue

    if y['exchange'] != 'NASDAQ':
      # Only NASDAQ
      continue
    else:
      del y['exchange']

    if y['instrumentType'] != 'Stocks':
      # Only stocks
      continue
    else:
      del y['instrumentType']

    ret.append(y)
  return ret



  #ret = [
  #  {
  #    'instrumentId': x['InstrumentID'],
  #    'name': x['InstrumentDisplayName'],
  #    'exchangeId': x['ExchangeID'],
  #    'symbolFull': x['SymbolFull'],
  #    'instrumentType': x['InstrumentTypeID'],
  #    'instrumentTypeSubCategoryId': x['InstrumentTypeSubCategoryID'],
  #    'stocksIndustryId': x['StocksIndustryID'],
  #  }
  #  for x in res
  #  if x['IsInternalInstrument'] == False
  #]

  # Translate field

  # Set instrumentType
  return ret
