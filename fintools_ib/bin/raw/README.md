Exploration of the IB API

# ContractApi
- [ ] `iserver_contract_conid_algos_get`: IB Algo Params
- [ ] `iserver_contract_conid_info_and_rules_get`: Info and Rules
- [x] `iserver_contract_conid_info_get`: Contract Details  
  fetch_contract()
- [ ] `iserver_contract_rules_post`: Contract Rules
- [ ] `iserver_secdef_info_get`: Secdef Info
- [x] `iserver_secdef_search_post`: Search by Symbol or Name  
  fetch_conid()
- [ ] `iserver_secdef_strikes_get`: Search Strikes
- [ ] `trsrv_futures_get`: Security Futures by Symbol
- [ ] `trsrv_secdef_post`: Secdef by Conid
- [ ] `trsrv_secdef_schedule_get`: Get trading schedule for symbol
- [ ] `trsrv_stocks_get`: Security Stocks by Symbol

# MarketDataApi
- [ ] `hmds_history_get`: Market Data History (Beta)
- [ ] `iserver_marketdata_conid_unsubscribe_get`: Market Data Cancel (Single)
- [x] `iserver_marketdata_history_get`: Market Data History  
  fetch_quote()
- [ ] `iserver_marketdata_snapshot_get`: Market Data
- [ ] `iserver_marketdata_unsubscribeall_get`: Market Data Cancel (All)
- [ ] `md_snapshot_get`: Market Data Snapshot (Beta)

# ScannerApi
- [ ] `hmds_scanner_post`: Run Scanner (Beta)
- [ ] `iserver_scanner_params_get`: Scanner Parameters
- [ ] `iserver_scanner_run_post`: Scanner Run
