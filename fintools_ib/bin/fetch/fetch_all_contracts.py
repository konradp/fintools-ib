#!/usr/bin/env python3
# Download quotes (last prices) for symbols
# Note: Requires symbols to be pre-fetched
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

def main():
  api = Api()
  symbols = api.get_symbols()
  if not symbols:
    print('No symbols found')
    exit(1)
  contracts = api.fetch_contracts(symbols)
  #print(contracts)
  for c in contracts:
    print(c['symbol'], c['category'], c['industry'])


if __name__ == '__main__':
  main()
