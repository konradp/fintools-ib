#!/usr/bin/env python3
# Run as:  ./$0 SYMBOL SYMBOL
# Example: ./$0 AAPL AMZN
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

api = Api()
symbols = api.get_symbols()
secdefs = api.fetch_secdefs(symbols)
print(json.dumps(secdefs, indent=2))
