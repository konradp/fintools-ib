#!/usr/bin/env python3
# TODO: Ditch this
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../../'))
from fintools_ib.lib.api_fetch import ApiFetch

parser = argparse.ArgumentParser(
  description='Get conid for a given symbol from cache/DB'
)
parser.add_argument(
  'symbol',
  metavar='SYMBOL',
  type=str,
  help='Ticker, e.g. AAPL'
)
args = parser.parse_args()
symbol = args.symbol
try:
  api = ApiFetch()
  res = api.fetch_yh_chart(symbol)
except Exception as e:
  print(f"Could not get conid: {e}")
  exit(1)
