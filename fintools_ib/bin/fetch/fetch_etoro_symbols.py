#!/usr/bin/env python3
# Run as:  ./$0 SYMBOL SYMBOL
# Example: ./$0 AAPL AMZN
import json
import os
import sys
from fintools_ib.lib.etoro import fetch_symbols

res = fetch_symbols()
print(json.dumps(res, indent=2))
