#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

api = Api()
symbols = api.get_symbols()
snapshots = api.fetch_snapshots(symbols)
print(json.dumps(snapshots, indent=2))
