#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
import pprint
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

parser = argparse.ArgumentParser(
  description='Get conid for a given symbol from cache/DB'
)
parser.add_argument(
  'symbols',
  metavar='SYMBOLS',
  type=str,
  nargs='+',
  help='Symbols, e.g. AAPL AMZN'
)
args = parser.parse_args()
symbols = args.symbols
try:
  api = Api()
  res = api.fetch_quotes(symbols)
  for symbol in res:
    print(symbol, res[symbol])
except Exception as e:
  print(f"Could not get conid: {e}")
  exit(1)
