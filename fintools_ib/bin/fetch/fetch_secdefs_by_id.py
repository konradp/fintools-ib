#!/usr/bin/env python3
# Run as:  ./$0 SYMBOL SYMBOL
# Example: ./$0 AAPL AMZN
import argparse
import json
import os
import sys
from fintools_ib.lib.api_fetch import ApiFetch

parser = argparse.ArgumentParser(
  description='Fetch secdef for a given symbol'
)
parser.add_argument(
  'conids',
  metavar='CONIDS',
  type=str,
  nargs='+',
  help='Conids, e.g. 4157 4211',
)
args = parser.parse_args()
conids = args.conids
api = ApiFetch()
secdefs = api.fetch_secdefs_by_id(conids)
print(json.dumps(secdefs, indent=2))
