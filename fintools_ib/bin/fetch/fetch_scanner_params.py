#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
import argparse
import json
from fintools_ib.lib.api_fetch import ApiFetch

api = ApiFetch()
res = api.fetch_scanner_params()
print(json.dumps(res, indent=2))
