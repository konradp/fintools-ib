#!/usr/bin/env python3
# Fetch all instruments on NASDAQ
# [
#   {
#     "ticker": "ADI",
#     "conid": 4157,
#     "exchange": "NMS"
#   },
#   {
#     "ticker": "AEP",
#     "conid": 4211,
#     "exchange": "NMS"
# ...
# Note: exchange is the primary exchange. For example, conid 4157

import json
import os
import sys
from fintools_ib.lib.api_fetch import ApiFetch

def main():
  api = ApiFetch()
  conids = api.fetch_conids()
  print(json.dumps(conids, indent=2))

if __name__ == '__main__':
  main()
