#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import ApiFetch

parser = argparse.ArgumentParser(
  description='Get quote for a given symbol from cache/DB'
)
parser.add_argument('symbol', metavar='SYMBOL', type=str, help='Symbol, e.g. XP')
parser.add_argument('period', metavar='PERIOD',type=str, help='Period, e.g. 1y, 1min')
parser.add_argument('bar', metavar='BAR',type=str, help='Bar, e.g. 1d')
parser.add_argument('start', metavar='START',type=str, help='Start, e.g. 20240408-00:00:00')
args = parser.parse_args()
symbol = args.symbol
period = args.period
bar = args.bar
start = args.start

api = ApiFetch()
history = api.fetch_history(symbol, period=period, bar=bar, start=start)
print(json.dumps(history, indent=2))
