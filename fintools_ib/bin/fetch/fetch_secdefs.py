#!/usr/bin/env python3
# Run as:  ./$0 SYMBOL SYMBOL
# Example: ./$0 AAPL AMZN
import argparse
import json
import os
import sys
from fintools_ib.lib.api_fetch import ApiFetch

parser = argparse.ArgumentParser(
  description='Fetch secdef for a given symbol'
)
parser.add_argument(
  'symbols',
  metavar='SYMBOLS',
  type=str,
  nargs='+',
  help='Symbols, e.g. XP AAPL'
)
args = parser.parse_args()
symbols = args.symbols
api = ApiFetch()
secdefs = api.fetch_secdefs(symbols)
print(json.dumps(secdefs, indent=2))
