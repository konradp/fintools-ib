#!/usr/bin/env python3
# Fetch closing price for given date for all given symbols
# outputs a markdown table
import argparse
import json
import os
import sys
from datetime import datetime
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api
from lib.backend import Backend

parser = argparse.ArgumentParser(
  description='Fetch prices for a given day'
)
parser.add_argument('date', metavar='DATE',type=str, help='Date, e.g. 20240201')
parser.add_argument('symbols', metavar='SYMBOLS', type=str, nargs='+', help='Symbols, e.g. AGEN AIHS ALVR')
args = parser.parse_args()
date = args.date
symbols = args.symbols

api = Api()
backend = Backend()
print('symbol|date|price|volume')
print('---|---|---|--:')
for symbol in symbols:
  quote = api.fetch_price_for_date(symbol, date=date)
  #market_cap = backend.get_market_cap(symbol)
  if quote:
    try:
      quote_date = quote['t']/1000
      quote_date = datetime.fromtimestamp(quote_date).strftime('%Y%m%d-%H:%M:%S')
      quote_price = quote['c']
      quote_price = round(quote_price, 2)
      quote_price = f"{quote_price:.2f}"
      quote_volume = quote['v']
      print(f"{symbol}|{quote_date}|{quote_price}|{quote_volume}")
    except Exception as e:
      print(f"Error for symbol {symbol}: {e}")
      print(f"quote: {quote}")
