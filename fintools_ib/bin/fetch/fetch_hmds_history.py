#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
# /hmds/history
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.ib import Ib

parser = argparse.ArgumentParser(
  description='Get quote for a given symbol from cache/DB'
)
parser.add_argument('conid', metavar='CONID', type=str, help='Conid, e.g. 265598')
parser.add_argument('period', metavar='PERIOD',type=str, help='Period, e.g. 1y, 1min')
parser.add_argument('bar', metavar='BAR',type=str, help='Bar, e.g. 1d')
args = parser.parse_args()
conid = args.conid
period = args.period
bar = args.bar

api = Ib()
history = api.fetch_hmds_history(conid, period=period, bar=bar)
print(json.dumps(history, indent=2))
