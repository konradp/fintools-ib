#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))

from lib.api_fetch import ApiFetch

try:
  api = ApiFetch()
  res = api.fetch_scanner(
    instrument='STK',
    scanner_type="SCAN_lipperMktCapAvgLatest_DESC",
    location="STK.US.MAJOR",
    filters=[{
      'code': 'lipperMktCapAvgLatestAbove',
      'value': 2,
  }])
  print(f"res {res.json()}")
except Exception as e:
  print(f"Could not get conid: {e}")
  exit(1)
