#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
import argparse
import json
import matplotlib.ticker as ticker
import mplfinance as mpf
import numpy as np
import pandas as pd
from fintools_ib.lib.api_fetch import ApiFetch

parser = argparse.ArgumentParser(
  description='Get quote for a given conid from cache/DB'
)
parser.add_argument('symbol', metavar='SYMBOL', type=str, help='Symbol, e.g. AAPL')
parser.add_argument('date', metavar='DATE',type=str, help='Date, e.g. 20240408')
args = parser.parse_args()
date = args.date
symbol = args.symbol

api = ApiFetch()

conid = api.fetch_conid(symbol)
print(f'conid: {conid}')

h = api.fetch_history_for_conid(conid, period='1d', bar='5min', start=f'{date}-00:00:00')
#print(json.dumps(h, indent=2))

data = h
avgs = []
points = data['data']
opening_price = points[0]['h']
for point in points:
  avgs.append({
    'p': (point['o'] + point['h'] + point['l'] + point['c']) / 4,
    'h': point['h'],
    't': point['t'],
    'v': point['v']
  })
label = 0
h_avg = {
  'sell': None,
  'perc': 0,
  'price': 0,
}
h_price = {
  'sell': None,
  'perc': 0,
  'price': 0,
}
for i in avgs:
  perc = 100*(i['p']-opening_price)/opening_price
  if perc > h_avg['perc']:
    h_avg = {
      'sell': i['t'],
      'perc': round(perc, 2),
      'price': round(i['p'], 4),
    }
  if perc > h_price['perc']:
    h_price = {
      'sell': i['t'],
      'perc': round(perc, 2),
      'price': round(i['p'], 4),
    }
  if perc >= 4:
    label = 1
    break


print('buy')
print(json.dumps(points[0], indent=2))

print('sell highest average:')
print(json.dumps(h_avg, indent=2))

print('sell highest price:')
print(json.dumps(h_price, indent=2))

print('label:', label)


# Plot
# Convert the list of dictionaries to a DataFrame
df = pd.DataFrame(points)

# Convert the 'date' column to datetime format and set it as the index
df['t'] = pd.to_datetime(df['t']/1000, unit='s')
df.set_index('t', inplace=True)
df.rename(columns={'o': 'Open', 'h': 'High', 'l': 'Low', 'c': 'Close', 'v': 'Volume'}, inplace=True)
# Plot the OHLC chart
fig, axlist = mpf.plot(df, type='candle',
  style='charles', title=f'{date}_{symbol}',
  volume=True,
  returnfig=True,
  ylabel='Price')

axlist[0].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.4f'))
fig.savefig(f'{date}_{symbol}.png', bbox_inches='tight')
