#!/usr/bin/env python3
# Download quotes (last prices) for symbols
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

def main():
  api = Api()
  symbols = api.get_symbols()
  quotes = api.fetch_quotes(symbols)
  for c in quotes:
    print(c, quotes[c])


if __name__ == '__main__':
  main()
