#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api_fetch import ApiFetch


try:
  api = ApiFetch()
  ret = api.fetch_nasdaq100()
  print(ret)
except Exception as e:
  print(f"Could not fetch nasdaq100: {e}")
  exit(1)
