#!/usr/bin/env python3
# BG: Best Gain
# length: minutes difference between buy and sell
# data_size is 78 for 1d 5min
# cnt_no_price : no price movement
# cnt_empty_vol
# Note: set env var SKIP=False to avoid skipping stocks
import argparse
from datetime import datetime
import json
import os
import sys
import mplfinance as mpf
import pandas as pd
import pprint
# Local libs
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api
from lib.util import get_perc_best_pair
from lib.util import get_perc_best_pair_simple
from lib.util import ohlc_to_median

parser = argparse.ArgumentParser(
  description='Get quote for a given symbol from cache/DB'
)
parser.add_argument('date', metavar='DATE',type=str, help='Date, e.g. 20240408')
parser.add_argument('symbols', metavar='SYMBOLS', type=str, nargs='+', help='Symbols, e.g. AGEN AIHS ALVR')
args = parser.parse_args()
date = args.date
symbols = args.symbols

SKIP = os.getenv('SKIP', 'True')
if SKIP == 'True':
  print('Note: SKIP=True: Going to skip bad stocks')
else:
  print('Note: SKIP=False: Going to include bad stocks')

def fix_date(date):
  return datetime.fromtimestamp(date/1000).strftime('%H:%M:%S')

# Mkdir
dir_name = f"best_gains_{date}"
if not os.path.exists(dir_name):
  os.makedirs(dir_name)

# Fetch prices
errors = []
winners = []
api = Api()
print('symbol|date_buy|date_sell|price_buy|price_sell|perc|length|cnt_empty_vol|cnt_no_price')
print('---|---|---|--:|--:|--:|--:|--:|--:')
for symbol in symbols:
  prices = api.fetch_history(symbol, period='1d', bar='5min', start=date+'-00:00:00')
  if 'data' not in prices:
    errors.append(f"No data for {symbol}: {json.dumps(prices)}")
    continue
  prices = prices['data']

  # Best gain from median prices
  prices_avg = ohlc_to_median(prices)
  bg = get_perc_best_pair_simple(prices_avg)
  t_buy = fix_date(bg['buy']['t'])
  t_sell = fix_date(bg['sell']['t'])

  # Calculate time difference
  time_format = "%H:%M:%S"
  time1 = datetime.strptime(t_buy, time_format)
  time2 = datetime.strptime(t_sell, time_format)
  time_diff = abs(time2 - time1)
  minutes_diff = int(time_diff.total_seconds() / 60)

  # 39 is too much
  count_empty_vol = 0
  count_no_price = 0
  for i in prices:
    if i['v'] == 0:
      count_empty_vol += 1
    if i['o'] == i['h'] == i['l'] == i['c']:
      count_no_price += 1


  # Filter out bad stocks, skip
  if SKIP == 'True':
    # Skip if less than 30 min between buy and sell
    if minutes_diff < 30:
      errors.append(f"Less than 30 min for {symbol}: {minutes_diff}")
      continue
    if count_empty_vol > 40 or count_no_price > 40:
      errors.append(f"Empty vol or no price for {symbol}: {count_empty_vol} {count_no_price}")
      continue

  if bg['perc'] >= 4:
    winners.append(symbol)

  print('|'.join([
    f"[{symbol}](/media/study-penny-stocks/{dir_name}/{date}-{symbol}.png)",
    t_buy,
    t_sell,
    f"{bg['buy']['p']:.4f}",
    f"{bg['sell']['p']:.4f}",
    f"{bg['perc']:.2f}",
    str(minutes_diff),
    str(count_empty_vol),
    str(count_no_price),
  ]))
  two_points = [ (bg['buy']['d'],  bg['buy']['p']),
                 (bg['sell']['d'], bg['sell']['p']) ]

  # Plot
  df = pd.DataFrame(prices)
  df.rename(columns={'o': 'Open', 'h': 'High', 'l': 'Low', 'c': 'Close', 'v': 'Volume'}, inplace=True)
  df['t'] = pd.to_datetime(df['t'], unit='ms')
  df.set_index('t', inplace=True)
  mpf.plot(df, type='candle', style='charles', volume=True,
    title=symbol, ylabel='', ylabel_lower='',
    addplot=[mpf.make_addplot(
        [i['p'] for i in prices_avg],
        color='black', width=0.5
    )],
    tight_layout=True,
    alines=dict(alines=two_points, colors=['blue']),
    savefig=f"{dir_name}/{date}-{symbol}.png"
  )

print('ERRORS:')
print('- ' + '\n- '.join(errors))
print('WINNERS: (perc_gain >= 4)')
print(' '.join(winners))
