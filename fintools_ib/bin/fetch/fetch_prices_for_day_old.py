#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
# Outputs table of stock prices for given symbols, e.g.
# ./fetch_prices_to_table.py 20240201 AGEN AIHS ALVR
# symbol|date|price|volume
# ---|---|---
# AGEN|20240201-00:00:00|1.23|1000
# AIHS|20240201-00:00:00|4.56|2000
# ALVR|20240201-00:00:00|7.89|3000
import argparse
import json
import os
import sys
from datetime import datetime
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

parser = argparse.ArgumentParser(
  description='Get quote for a given symbol from cache/DB'
)
parser.add_argument('date', metavar='DATE',type=str, help='Date, e.g. 20240201')
parser.add_argument('symbols', metavar='SYMBOLS', type=str, nargs='+', help='Symbols, e.g. AGEN AIHS ALVR')
args = parser.parse_args()
date = args.date
symbols = args.symbols

api = Api()
print('symbol|date|price|volume')
print('---|---|---|--:')
for symbol in symbols:
    quote = api.fetch_history(symbol, period='1d', bar='1d', start=date+'-00:00:00')
    if quote:
      try:
        quote_date = quote['data'][0]['t']/1000
        quote_date = datetime.fromtimestamp(quote_date).strftime('%Y%m%d-%H:%M:%S')
        quote_price = quote['data'][0]['c']
        quote_price = round(quote_price, 2)
        quote_price = f"{quote_price:.2f}"
        quote_volume = quote['data'][0]['v']
        print(f"{symbol}|{quote_date}|{quote_price}|{quote_volume}")
      except Exception as e:
        print(f"Error for symbol {symbol}: {e}")
        print(f"quote: {quote}")
