#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
# Example: ./fetch_history.py CONID PERIOD BAR START
# Note: This calls /hmds/history
import argparse
import json
import sys
from fintools_ib.lib.api_fetch import ApiFetch

parser = argparse.ArgumentParser(
  description='Get quote for a given conid from cache/DB',
  epilog=f"Example: {sys.argv[0]} 265598 1m 5min 20240408-00:00:00"
)
parser.add_argument('conid', metavar='CONID', type=str, help='Conid, e.g. 1234')
parser.add_argument('period', metavar='PERIOD',type=str, help='Period, e.g. 1y, 1m, 1d, 1min, supported: min, h, d, w, m, y')
parser.add_argument('bar', metavar='BAR',type=str, help='Bar, e.g. 1d, supported: min, h, d, w, m')
parser.add_argument('start', metavar='START',type=str, help='Start, e.g. 20240408-00:00:00')
args = parser.parse_args()
conid = args.conid
period = args.period
bar = args.bar
start = args.start

api = ApiFetch()
history = api.fetch_history_for_conid(conid, period=period, bar=bar, start=start)
#history = api.fetch_history_for_conid(conid, period=period, bar=bar)
print(json.dumps(history, indent=2))
