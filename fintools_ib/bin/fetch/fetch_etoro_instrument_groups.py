#!/usr/bin/env python3
# Run as:  ./$0 SYMBOL SYMBOL
# Example: ./$0 AAPL AMZN
import json
import os
import sys
from fintools_ib.lib.etoro import fetch_instrument_groups

res = fetch_instrument_groups()
print(json.dumps(res, indent=2))
