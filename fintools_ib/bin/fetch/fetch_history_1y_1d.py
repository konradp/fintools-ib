#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

parser = argparse.ArgumentParser(
  description='Get quote for a given symbol from cache/DB'
)
parser.add_argument(
  'symbol',
  metavar='SYMBOL',
  type=str,
  help='Symbol, e.g. XP'
)
args = parser.parse_args()
symbol = args.symbol
api = Api()
history = api.fetch_history(symbol, period='1y', bar='1d')
print(json.dumps(history, indent=2))
