#!/usr/bin/env python3
# Get IB conid for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
# Example: ./fetch_history.py AGEN 1d 1d 20240201-00:00:00
import argparse
import json
import os
import sys
import mplfinance as mpf
import pandas as pd
import pprint

# Local libs
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api
from lib.util import get_perc_best_pair
from lib.util import get_perc_best_pair_simple
from lib.util import ohlc_to_median

parser = argparse.ArgumentParser(
  description='Get quote for a given symbol from cache/DB'
)
parser.add_argument('date', metavar='DATE',type=str, help='Date, e.g. 20240408')
parser.add_argument('symbol', metavar='SYMBOL', type=str, help='Symbol, e.g. XP')
args = parser.parse_args()
symbol = args.symbol
date = args.date

api = Api()

# Fetch prices
prices = api.fetch_history(symbol, period='1d', bar='5min', start=date+'-00:00:00')
prices = prices['data']

prices_avg = ohlc_to_median(prices)
best_gain_avg = get_perc_best_pair_simple(prices_avg)
pprint.pprint(best_gain_avg)
two_points = [ (best_gain_avg['buy']['d'],  best_gain_avg['buy']['p']),
               (best_gain_avg['sell']['d'], best_gain_avg['sell']['p']) ]

## Best gain
#best_gain = get_perc_best_pair(prices)

# Plot
df = pd.DataFrame(prices)
df.rename(columns={'o': 'Open', 'h': 'High', 'l': 'Low', 'c': 'Close', 'v': 'Volume'}, inplace=True)
df['t'] = pd.to_datetime(df['t'], unit='ms')
df.set_index('t', inplace=True)
# TODO: Plot best gain as points and a line

prices_avg_plot = [i['p'] for i in prices_avg]
add_plot1 = mpf.make_addplot(prices_avg_plot, color='black', width=0.5)

mpf.plot(df, type='candle', style='charles', volume=True,
  title=symbol, ylabel='', ylabel_lower='',
  addplot=[add_plot1],
  tight_layout=True,
  alines=dict(alines=two_points, colors=['blue']),
  savefig=f"{date}-{symbol}.png"
)
#mpf.plot(df, type='line', style='charles', volume=True, title=symbol, ylabel='price', ylabel_lower='vol', savefig=f"{date}-{symbol}.png")
