#!/usr/bin/env python3
# Used for getting test data for training, see study-machine-learning.md blog
import json
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
import time
from datetime import datetime
from fintools_ib.lib.api_fetch import ApiFetch
from glob import glob
api = None

def fetch_history(symbol):
  global api
  return h
  # h = [
  #  {'symbol': 'AAOI', 'ticker': 'AAOI', 'data': [
  #    {'t': 1561383000000, 'o': 9.25, 'c': 9.11, 'h': 9.42, 'l': 8.99, 'v': 408124.0}, ...
  #  ]}, ... ]


def main():
  ret = {}

  history_files = glob('/opt/fintools-ib/data/0year_histories/*.json')
  ret = {}
  for history_file in history_files:
    with open(history_file) as f:
      h = json.load(f)
      for i in h['data']:
        # Read each date
        if i['l'] <= 1:
          #print(f'Low price {i["l"]} for {h["symbol"]} on {i["t"]}')
          if i['t'] not in ret:
            ret[i['t']] = []
          ret[i['t']].append({
            'symbol': h['symbol'],
            'conid': h['conid'],
            'l': i['l'],
          })
  ret = {k: ret[k] for k in sorted(ret)}
  # Write file
  with open('/opt/fintools-ib/data/low_penny_stocks.json', 'w') as f:
    json.dump(ret, f, indent=2)
  x = []
  y = []
  for date, symbols in ret.items():
    x.append(date)
    y.append(len(symbols))
    print(f'{date}: {len(symbols)}')
  x = [datetime.fromtimestamp(epoch / 1000) for epoch in x]
  plt.figure(figsize=(10, 6))
  plt.plot(x, y, linestyle='-', color='black')
  plt.title('Count of penny stocks per date')
  plt.xlabel('date')
  plt.ylabel('count')
  # Formatting the x-axis to show dates
  plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
  locator = mdates.AutoDateLocator()
  plt.gca().xaxis.set_major_locator(locator)

  #plt.gca().xaxis.set_major_locator(mdates.DayLocator())
  plt.gcf().autofmt_xdate()
  plt.grid(True)
  #plt.legend()
  plt.savefig('/opt/fintools-ib/data/penny_stock_count.png')


if __name__ == '__main__':
  main()
