#!/usr/bin/env python3
# Used for getting test data for training, see study-machine-learning.md blog
import json
import os
import sys
import time
from fintools_ib.lib.api_fetch import ApiFetch
api = None


def main():
  global api
  api = ApiFetch()
  # [ {
  #   "ticker": "ZUMZ",
  #   "conid": 34466024,
  #   "name": "Zumiez Inc",
  #   "stocksIndustry": "ConsumerGoods"
  # }, ... ]
  print('Fetch symbols')
  symbols = api.fetch_all_symbols_combined()
  print(f'Number of symbols: {len(symbols)}')
  histories = []
  idx = 0
  for symbol in symbols:
    idx += 1
    if not os.path.exists(f'/opt/fintools-ib/data/histories/{symbol["ticker"]}.json'):
      print(f'Fetching history for {idx} : {symbol["ticker"]} {symbol["conid"]}')
      h = api.fetch_history_for_conid(conid=symbol['conid'], period='5y', bar='1d')
      if 'data' not in h:
        print(f'ERROR: Failed to fetch history for {symbol["ticker"]} {symbol["conid"]}')
        exit(1)
      # h = [
      #  {'symbol': 'AAOI', 'ticker': 'AAOI', 'data': [
      #    {'t': 1561383000000, 'o': 9.25, 'c': 9.11, 'h': 9.42, 'l': 8.99, 'v': 408124.0}, ...
      #  ]}, ... ]
      h = {
        'symbol': symbol['ticker'],
        'conid': symbol['conid'],
        'data': h['data'],
      }
      
      with open(f'/opt/fintools-ib/data/histories/{symbol["ticker"]}.json', 'w') as f:
        f.write(json.dumps(h, indent=2))


if __name__ == '__main__':
  main()
