#!/usr/bin/env python3
# Used for getting test data for training, see study-machine-learning.md blog
import glob
import json
import os
import pytz
import sys
import time
from datetime import datetime, timezone

DATADIR = '/opt/fintools-ib/data/training_raw'


def to_yyyymmdd(timestamp):
  timestamp = timestamp/1000
  # TODO

def to_hhmm(timestamp):
  # In ET
  timestamp = int(timestamp)/1000
  utc_dt = datetime.utcfromtimestamp(timestamp)
  eastern = pytz.timezone('US/Eastern')
  eastern_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(eastern)
  formatted = eastern_dt.strftime('%H:%M')
  return formatted
  


def main():
  global DATADIR
  if len(sys.argv) == 2:
    DATADIR = sys.argv[1]
  dates = os.listdir(DATADIR)

  # Check for missing files
  for date in dates:
    day1_path = f'{DATADIR}/{date}/day1'
    day2_path = f'{DATADIR}/{date}/day2'

    # Check that day2 files exist for each day1 file
    day1_files = os.listdir(day1_path)
    for day1_file in day1_files:
      if not os.path.exists(f'{day2_path}/{day1_file}'):
        print(f'Missing {date}/day2/{day1_file}, but exists: {date}/day1/{day1_file}')

    # Check that day1 files exist for each day2 file
    day2_files = os.listdir(day2_path)
    for day2_file in day2_files:
      if not os.path.exists(f'{day1_path}/{day2_file}'):
        print(f'Missing {date}/day1/{day2_file}, but exists: {date}/day2/{day2_file}')

  for date in dates:
    for file in glob.glob(f'{DATADIR}/{date}/**/*.json'):
      with open(file, 'r') as f:
        data = json.load(f)
        if data['points'] <= 50:
          # Check enough datapoints in each
          # Should be 78, but we alert on 50
          #print(f'{file} has {data["points"]} points')
          pass

  for date in dates:
    for file in glob.glob(f'{DATADIR}/{date}/**/*.json'):
      time_rng = []
      with open(file, 'r') as f:
        data = json.load(f)
        data = data['data']
        for i in data:
          fmted = to_hhmm(i['t'])
          if fmted not in time_rng:
            time_rng.append(fmted)
    time_rng = sorted(time_rng)
    print(date, time_rng[0], time_rng[-1], file)
    

if __name__ == '__main__':
  main()
