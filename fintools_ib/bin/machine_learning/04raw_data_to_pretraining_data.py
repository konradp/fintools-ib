#!/usr/bin/env python3
# Used for getting test data for training, see study-machine-learning.md blog
# - Create dir 2pretraining_data/
# - LABEL DATA
# - include day of week
# - include penny_stock_count
# - timezones
import glob
import json
import os
import pytz
import sys
import time
from datetime import datetime, timezone

DATADIR = '/opt/fintools-ib/data/1data_raw'


def to_yyyymmdd(timestamp):
  timestamp = int(timestamp)
  timestamp = timestamp/1000
  date_fmt = datetime.fromtimestamp(timestamp)
  date_fmt = date_fmt.strftime('%Y%m%d')
  return date_fmt


def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)


def main():
  global DATADIR
  if len(sys.argv) == 2:
    DATADIR = sys.argv[1]
  OUTDIR = f'{DATADIR}/../2pretraining_data'
  mkdir(OUTDIR)
  dates = os.listdir(DATADIR)
  time_range = []
  #print(dates)

  # Check for missing files
  for date in dates:
    day1_path = f'{DATADIR}/{date}/day1'
    day2_path = f'{DATADIR}/{date}/day2'
    date_fmt = to_yyyymmdd(date)
    day1_files = os.listdir(day1_path)
    penny_stock_count = len(day1_files)
    for day1_file in day1_files:
      day1_file_path = f'{day1_path}/{day1_file}'
      day2_file_path = f'{day2_path}/{day1_file}'
      symbol = day1_file.replace('.json', '')

      # Open day2_file_path, calculate averages, calculate the label
      with open(day2_file_path, 'r') as f:
        data = json.load(f)
        avgs = []
        points = data['data']
        opening_price = points[0]['h']
        for point in points:
          avgs.append({
            'p': (point['o'] + point['h'] + point['l'] + point['c']) / 4,
            't': point['t'],
            'v': point['v']
          })
        label = 0
        for i in avgs:
          perc = 100*(i['p']-opening_price)/opening_price
          if perc >= 4:
            label = 1
            break

        # Open day1_file_path
        # save to OUTDIR/data_fmt/symbol.json
        # symbol, label, day_of_week, penny_stock_count
        with open(day1_file_path, 'r') as f:
          day1_data = json.load(f)
          # Convert prices to o+h+l+c/4
          prices = []
          for p in day1_data['data']:
            avg = (p['o'] + p['h'] + p['l'] + p['c']) / 4
            prices.append({
              't': p['t'],
              'v': p['v'],
              'p': round(avg, 4),
            })
            # Get time of day
            utc_time = int(p['t'])/1000
            #t = datetime.fromtimestamp(t)
            utc_time = datetime.utcfromtimestamp(utc_time)
            et_timezone = pytz.timezone('US/Eastern')
            et_time = utc_time.astimezone(et_timezone)
            time_of_day = et_time.strftime('%H:%M')
            if time_of_day not in time_range:
              time_range.append(time_of_day)

          # Get day of week
          t = int(date)/1000
          t = datetime.fromtimestamp(t)
          day_of_week = t.weekday() + 1


          # Save day1 data with label
          data = {
            'symbol': symbol,
            'label': label,
            'day_of_week': day_of_week,
            'penny_stock_count': penny_stock_count,
            'data': prices,
          }
          out_path = f'{OUTDIR}/{date_fmt}_{symbol}.json'
          with open(out_path, 'w') as f:
            json.dump(data, f, indent=2)
  time_range = sorted(time_range)
  print(json.dumps(time_range, indent=2))
  print(f'Time range length: {len(time_range)}')

if __name__ == '__main__':
  main()
