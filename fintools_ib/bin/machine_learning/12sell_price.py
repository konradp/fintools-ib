#!/usr/bin/env python3
# Calculates sell price e.g. 4% increase from given price
# Suppress TensorFlow logging messages
import os
import sys

def usage():
  print(f'Usage: {sys.argv[0]} PRICE')
  print(f'Example: {sys.argv[0]} 1')


def main():
  if len(sys.argv) != 2:
    usage()
    exit(1)
  PRICE = sys.argv[1]
  print(f'Given price: {PRICE}')
  print(f'Sell price 3% gain: {float(PRICE) * 1.03}')
  print(f'Sell price 4% gain: {float(PRICE) * 1.04}')
  print(f'Sell price 5% gain: {float(PRICE) * 1.05}')
  print(f'Sell price 6% gain: {float(PRICE) * 1.06}')


if __name__ == '__main__':
  main()
