#!/usr/bin/env python3

import json
import os
import sys
import numpy as np

from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Concatenate, Conv1D, Dense, Flatten, Input



DATADIR = None

def usage():
  print(f'Usage: {sys.argv[0]} DATADIR')
  print(f'Example: {sys.argv[0]} ~/konradp/fintools-ib/data')


def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)


def main():
  global DATADIR
  if len(sys.argv) != 2:
    usage()
    exit(1)
  DATADIR = sys.argv[1]

  INPUTDIR = f'{DATADIR}/3training_data'
  #OUTPUTDIR = f'{DATADIR}/3training_data'
  #mkdir(OUTPUTDIR)
  #print(f'files: {files}')


  # Read data
  # Extracting features and labels
  p_values = []
  v_values = []
  penny_stock_counts = []
  day_of_weeks = []
  labels = []

  files = os.listdir(INPUTDIR)
  for file in files:
    with open(f'{INPUTDIR}/{file}', 'r') as f:
      sample = json.load(f)
      p = [point['p'] for point in sample['data']]
      v = [point['v'] for point in sample['data']]
      
      p_values.append(p)
      v_values.append(v)
      penny_stock_counts.append(sample['penny_stock_count'])
      day_of_weeks.append(sample['day_of_week'])
      labels.append(sample['label'])

  # Converting lists to numpy arrays
  p_values = np.array(p_values).reshape(-1, 78, 1)
  v_values = np.array(v_values).reshape(-1, 78, 1)
  penny_stock_counts = np.array(penny_stock_counts).reshape(-1, 1)
  day_of_weeks = np.array(day_of_weeks)
  labels = np.array(labels)

  # Ensuring the shapes are correct
  print("p_values shape:", p_values.shape)
  print("v_values shape:", v_values.shape)
  print("penny_stock_counts shape:", penny_stock_counts.shape)
  print("day_of_weeks shape:", day_of_weeks.shape)
  print("labels shape:", labels.shape)




  # Build model
  # Inputs
  seq_length = 78
  num_features = 2 # 'p' and 'v'
  p_input = Input(shape=(seq_length, 1), name='p_input')
  v_input = Input(shape=(seq_length, 1), name='v_input')
  penny_stock_count_input = Input(shape=(1,), name='penny_stock_count_input')
  day_of_week_input = Input(shape=(5,), name='day_of_week_input')
  # CNN layers for sequential data
  p_conv = Conv1D(filters=32, kernel_size=3, activation='relu')(p_input)
  p_conv = Conv1D(filters=16, kernel_size=3, activation='relu')(p_conv)
  p_flatten = Flatten()(p_conv)
  v_conv = Conv1D(filters=32, kernel_size=3, activation='relu')(v_input)
  v_conv = Conv1D(filters=16, kernel_size=3, activation='relu')(v_conv)
  v_flatten = Flatten()(v_conv)
  # Dense layers for other inputs
  penny_stock_count_dense = Dense(16, activation='relu')(penny_stock_count_input)
  day_of_week_dense = Dense(16, activation='relu')(day_of_week_input)
  # Concatenate all features
  concatenated = Concatenate()([p_flatten, v_flatten, penny_stock_count_dense, day_of_week_dense])
  # Fully connected layers
  dense1 = Dense(64, activation='relu')(concatenated)
  dense2 = Dense(32, activation='relu')(dense1)
  output = Dense(1, activation='sigmoid')(dense2)

  # Build and compile model
  model = Model(inputs=[p_input, v_input, penny_stock_count_input, day_of_week_input], outputs=output)
  model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
  model.summary()

  # Train model
  history = model.fit(
      [p_values, v_values, penny_stock_counts, day_of_weeks],
      labels,  # Target labels
      epochs=50,  # Number of epochs
      batch_size=32,  # Batch size
      validation_split=0.2  # Fraction of data to use for validation
  )

  # Evaluate the model
  loss, accuracy = model.evaluate([p_values, v_values, penny_stock_counts, day_of_weeks], labels)
  print(f"Loss: {loss}, Accuracy: {accuracy}")


  # Save model
  model.save('model.keras')
  #model.predict(new_data)





  # old
  #model = Sequential()
  #model.add(Dense(64, input_dim=input_dim, activation='relu'))
  #model.add(Dense(32, activation='relu'))
  # Single neuron with sigmoid activation for binary classification
  #model.add(Dense(1, activation='sigmoid'))


if __name__ == '__main__':
  main()
