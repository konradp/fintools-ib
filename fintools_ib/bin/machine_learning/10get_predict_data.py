#!/usr/bin/env python3
import concurrent.futures
import json
import os
import pytz
import sys
from datetime import datetime, timezone
from fintools_ib.lib.api_fetch import ApiFetch
api = None
penny_stock_count = None
DATA = None
DAY_OF_WEEK = None


def usage():
  print(f'Usage: {sys.argv[0]} OUTDIR DATE')
  print(f'Example: {sys.argv[0]} ~/konradp/fintools-ib/data 20240627')


def to_hhmm(timestamp):
  # In ET
  timestamp = int(timestamp)/1000
  #utc_dt = datetime.utcfromtimestamp(timestamp)
  utc_dt = datetime.fromtimestamp(timestamp, tz=timezone.utc)
  eastern = pytz.timezone('US/Eastern')
  #eastern_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(eastern)
  eastern_dt = utc_dt.astimezone(eastern)
  formatted = eastern_dt.strftime('%H:%M')
  return formatted


def date_to_day_of_week(date):
  # Input: 20240627
  # Output: [ 0, 0, 0, 0, 1 ]
  arr = [ 0, 0, 0, 0, 0 ]
  date_obj = datetime.strptime(date, "%Y%m%d")
  day_of_week = date_obj.weekday()
  arr[day_of_week] = 1
  return arr


def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)


def fetch_price(conid):
  global DATE
  ret = api.fetch_history_for_conid(conid, period='1d', bar='1d', start=f'{DATE}-00:00:00')
  return ret


def fetch_day_prices(conid):
  global DATE
  ret = api.fetch_history_for_conid(conid, period='1d', bar='5min', start=f'{DATE}-00:00:00')
  return ret


def normalise_data(symbol, data):
  # Rework the history data for a stock to match the format required for the
  # model
  # input:
  # {
  #   "startTime": "20240627-15:30:00",
  #   "startTimeVal": 1719495000000,
  #   "endTime": "20240627-22:00:00",
  #   "endTimeVal": 1719518400000,
  #   "data": [
  #     { "t": 1719495000000, "o": 0.93, "c": 0.92, "h": 0.93, "l": 0.91, "v": 54019.0 },
  #     ...
  # OUTPUT:
  # {
  #   "symbol": "LILM",
  #   "day_of_week": [ 0, 0, 0, 0, 1 ],
  #   "penny_stock_count": 0.9545454545454546,
  #   "data": [
  #     { "t": "09:30", "p": 0.0895, "v": 0.4411 },
  #     { "t": "09:35", "p": 0.2445, "v": 0.7469 },
  global DAY_OF_WEEK
  TIMES = [
    '09:30', '09:35', '09:40', '09:45', '09:50', '09:55',
    '10:00', '10:05', '10:10', '10:15', '10:20', '10:25', '10:30', '10:35', '10:40', '10:45', '10:50', '10:55',
    '11:00', '11:05', '11:10', '11:15', '11:20', '11:25', '11:30', '11:35', '11:40', '11:45', '11:50', '11:55',
    '12:00', '12:05', '12:10', '12:15', '12:20', '12:25', '12:30', '12:35', '12:40', '12:45', '12:50', '12:55',
    '13:00', '13:05', '13:10', '13:15', '13:20', '13:25', '13:30', '13:35', '13:40', '13:45', '13:50', '13:55',
    '14:00', '14:05', '14:10', '14:15', '14:20', '14:25', '14:30', '14:35', '14:40', '14:45', '14:50', '14:55',
    '15:00', '15:05', '15:10', '15:15', '15:20', '15:25', '15:30', '15:35', '15:40', '15:45', '15:50', '15:55',
  ]
  # Normalise price and volume and fill missing data with -1
  # find max and min price
  # Normalise volume
  # find max and min volume
  prices = [ (i['o']+i['c']+i['h']+i['l'])/4 for i in data['data'] ]
  min_price = min(prices)
  max_price = max(prices)
  if max_price == 0:
    print('max price is 0')
    exit(1)
  if min_price == max_price:
    print(f'max price is same as min price for {file}')
  vols = [ i['v'] for i in data['data'] ]
  max_vol = max(vols)
  # scale prices: (x-xmin)/(xmax-xmin)
  # scale volumes: x/xmax
  ret_data = []
  # Fill gaps with -1
  # Dict for quicker lookup
  y_dict = {
    to_hhmm(i['t']): {
      'p': (i['o']+i['c']+i['h']+i['l'])/4,
      'v': i['v']
    }
    for i in data['data']
  }
  for t in TIMES:
    if t in y_dict:
      if min_price == max_price:
        price = y_dict[t]['p']
      else:
        price = (y_dict[t]['p']-min_price)/(max_price-min_price)
      point = {
        't': t,
        'p': price,
        'v': y_dict[t]['v']/max_vol,
      }
    else:
      point = {
        't': t,
        'p': -1,
        'v': -1,
      }
    ret_data.append(point)
  # Assemble return data
  ret = {
    'symbol': symbol,
    'day_of_week': DAY_OF_WEEK,
    'penny_stock_count': penny_stock_count/66,
    'data': ret_data
  }
  return ret


def main():
  global api
  global penny_stock_count
  global DATE
  global DAY_OF_WEEK
  if len(sys.argv) != 3:
    usage()
    exit(1)
  OUTDIR = sys.argv[1]
  DATE = sys.argv[2]
  mkdir(f'{OUTDIR}/predict_data/{DATE}')
  DAY_OF_WEEK = date_to_day_of_week(DATE)

  api = ApiFetch()
  # [ {
  #   "ticker": "ZUMZ",
  #   "conid": 34466024,
  #   "name": "Zumiez Inc",
  #   "stocksIndustry": "ConsumerGoods"
  # }, ... ]
  print('Fetch symbols')
  try:
    symbols = api.fetch_all_symbols_combined()
  except Exception as e:
    print(f'Failed to fetch symbols: {e}')
    exit(1)
  print(f'Number of symbols: {len(symbols)}')

  # Get list of penny stocks
  penny_stocks = []
  #symbols = symbols[:100] # DEBUG
  symbols = {
    s['ticker']: s['conid']
    for s in symbols
  }
  with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    future_to_data = {
      executor.submit(fetch_price, conid) : symbol
      for symbol, conid in symbols.items()
    }
    for future in concurrent.futures.as_completed(future_to_data):
      try:
        res = future.result()
        symbol = future_to_data[future]
        if res['data'][0]['l'] <= 1:
          print(f'{symbol} price: {res["data"][0]["l"]}')
          penny_stocks.append({
            'ticker': symbol,
            'conid': symbols[symbol],
            'price': res['data'][0]['l']
          })
      except Exception as e:
        print(f'Fail fetching data for {symbol}: {e}: {res}')
    penny_stock_count = len(penny_stocks)
    print(f'Got {penny_stock_count} penny stocks')


  # Get day data for penny stocks
  with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    future_to_data = {
      executor.submit(fetch_day_prices, s['conid']) : s['ticker']
      for s in penny_stocks
    }
    for future in concurrent.futures.as_completed(future_to_data):
      try:
        res = future.result()
        symbol = future_to_data[future]
        print(symbol)
        data = normalise_data(symbol, res)
        data = json.dumps(data, indent=2)
        with open(f'{OUTDIR}/predict_data/{DATE}/{symbol}.json', 'w') as f:
          f.write(data)
      except Exception as e:
        print(f'Fail fetching day data for {symbol}: {e}: {res}')
        print(f'Exception: {e}')


if __name__ == '__main__':
  main()
