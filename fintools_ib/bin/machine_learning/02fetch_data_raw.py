#!/usr/bin/env python3
# Used for getting test data for training, see study-machine-learning.md blog
import json
import os
import sys
import time
from datetime import datetime, timezone
from fintools_ib.lib.api_fetch import ApiFetch

DATADIR = '/opt/fintools-ib/data'
OUTDIR = 

def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)


def main():
  # TODO: This should take an argument if I want it to be run from outside a container
  global DATADIR
  OUTDIR = f'{DATADIR}/1data_raw'
  api = ApiFetch()
  # open /opt/fintools-ib/data/low_penny_stocks.json
  with open(f'{DATADIR}/low_penny_stocks.json') as f:
    data = json.loads(f.read())
    #"1561383000000": [
    #  {
    #    "symbol": "CODX",
    #    "conid": 280904395,
    #    "l": 0.7113
    #  },
    #  {
    #    "symbol": "MVIS",
    #    "conid": 102558681,
    #    "l": 0.7715
    #  },
    # We want dir structure:
    # data/
    #   1561383000000/
    #     day1/
    #       CODX.json
    #       MVIS.json
    #     day2/
    #       CODX.json
    #       MVIS.json
    #   1561469400000/
    #     day1/
    #       AAPL.json
    #       MSFT.json
    #     day2/
    #     ...
    #   ...
    for day1 in data:
      #print('== day1', day1)
      mkdir(f'{OUTDIR}/{day1}')
      mkdir(f'{OUTDIR}/{day1}/day1')
      mkdir(f'{OUTDIR}/{day1}/day2')
      for s in data[day1]:
        #if s['symbol'] == 'AADI' or s['symbol'] == 'NKLA':
        if s['symbol'] == 'AADI':
        #  # DEBUG, skip for now
          continue
        #print('-', s['symbol'], s['conid'])
        fname_day1 = f'{OUTDIR}/{day1}/day1/{s["symbol"]}.json'
        fname_day2 = f'{OUTDIR}/{day1}/day2/{s["symbol"]}.json'
        if not os.path.exists(fname_day1):
          start = datetime.fromtimestamp(int(day1)/1000.0, tz=timezone.utc)
          start = start.strftime('%Y%m%d-%H:%M:%S')
          print(f'Fetching history for {s["symbol"]} {s["conid"]}, {day1} {start}')
          try:
            h = api.fetch_history_for_conid(conid=s['conid'], period='1d', bar='5min', start=start)
          except Exception as e:
            if s['symbol'] == 'NKLA':
              continue
          if 'data' not in h:
            print(f'ERROR: Failed to fetch history for {s["symbol"]} {s["conid"]} {start}')
            #exit(1)
          else:
            with open(fname_day1, 'w') as f:
              print(f'Write to {fname_day1}')
              f.write(json.dumps(h, indent=2))
        if not os.path.exists(fname_day2):
          start = datetime.fromtimestamp(int(day1)/1000.0+86400, tz=timezone.utc)
          start = start.strftime('%Y%m%d-%H:%M:%S')
          h = api.fetch_history_for_conid(conid=s['conid'], period='1d', bar='5min', start=start)
          if 'data' not in h:
            print(f'ERROR: Failed to fetch history for {s["symbol"]} {s["conid"]} {start}')
            if s['symbol'] == 'NKLA':
              continue
            #exit(1)
          else:
            with open(fname_day2, 'w') as f:
              print(f'Write to {fname_day2}')
              f.write(json.dumps(h, indent=2))


if __name__ == '__main__':
  main()
