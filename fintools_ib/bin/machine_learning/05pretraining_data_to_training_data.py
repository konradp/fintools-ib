#!/usr/bin/env python3
# Create dir 3training_data
# Final cleaning and normalising of the data:
# - normalise day of week: [ 1, 0, 0, 0, 0 ] for Monday
# - normalise prices: 0 to 1
# - missing data points become -1
# - normalise penny_stock_count
import glob
import json
import os
import pytz
import sys
import time
from datetime import datetime, timezone

DATADIR = None
TIMES = [
  '09:30', '09:35', '09:40', '09:45', '09:50', '09:55',
  '10:00', '10:05', '10:10', '10:15', '10:20', '10:25', '10:30', '10:35', '10:40', '10:45', '10:50', '10:55',
  '11:00', '11:05', '11:10', '11:15', '11:20', '11:25', '11:30', '11:35', '11:40', '11:45', '11:50', '11:55',
  '12:00', '12:05', '12:10', '12:15', '12:20', '12:25', '12:30', '12:35', '12:40', '12:45', '12:50', '12:55',
  '13:00', '13:05', '13:10', '13:15', '13:20', '13:25', '13:30', '13:35', '13:40', '13:45', '13:50', '13:55',
  '14:00', '14:05', '14:10', '14:15', '14:20', '14:25', '14:30', '14:35', '14:40', '14:45', '14:50', '14:55',
  '15:00', '15:05', '15:10', '15:15', '15:20', '15:25', '15:30', '15:35', '15:40', '15:45', '15:50', '15:55',
]


def usage():
  print(f'Usage: {sys.argv[0]} DATADIR')
  print(f'Example: {sys.argv[0]} ~/konradp/fintools-ib/data')


def to_hhmm(timestamp):
  # In ET
  timestamp = int(timestamp)/1000
  utc_dt = datetime.utcfromtimestamp(timestamp)
  eastern = pytz.timezone('US/Eastern')
  eastern_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(eastern)
  formatted = eastern_dt.strftime('%H:%M')
  return formatted


def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)


def day_of_week_to_arr(day_of_week):
  arr = [ 0, 0, 0, 0, 0 ]
  arr[day_of_week-1] = 1
  return arr


def main():
  global DATADIR
  if len(sys.argv) != 2:
    usage()
    exit(1)
  DATADIR = sys.argv[1]

  INPUTDIR = f'{DATADIR}/2pretraining_data'
  OUTPUTDIR = f'{DATADIR}/3training_data'
  mkdir(OUTPUTDIR)
  files = os.listdir(INPUTDIR)
  #print(f'files: {files}')

  # Read each file to get the max penny_stock_count
  max_penny_stock_count = 0
  for file in files:
    with open(f'{INPUTDIR}/{file}', 'r') as f:
      f_data = json.load(f)
      max_penny_stock_count = max(max_penny_stock_count, f_data['penny_stock_count'])

  # Process each file
  for file in files:
    #print(file)
    with open(f'{INPUTDIR}/{file}', 'r') as f:
      f_data = json.load(f)
      day_of_week = day_of_week_to_arr(f_data['day_of_week'])

      # Get min price
      prices = [ i['p'] for i in f_data['data'] ]
      min_price = min(prices)
      max_price = max(prices)
      if max_price == 0:
        print('max price is 0')
        exit(1)
      if min_price == max_price:
        print(f'max price is same as min price for {file}')

      vols = [ i['v'] for i in f_data['data'] ]
      max_vol = max(vols)

      # scale prices: (x-xmin)/(xmax-xmin)
      # scale volumes: x/xmax
      ret_data = []
      # Fill gaps with -1
      # Dict for quicker lookup
      y_dict = {
        to_hhmm(i['t']): { 'p': i['p'], 'v': i['v'] }
        for i in f_data['data']
      }
      for t in TIMES:
        if t in y_dict:
          if min_price == max_price:
            price = y_dict[t]['p']
          else:
            price = (y_dict[t]['p']-min_price)/(max_price-min_price)
          point = {
            't': t,
            'p': price,
            'v': y_dict[t]['v']/max_vol,
          }
        else:
          point = {
            't': t,
            'p': -1,
            'v': -1,
          }
        ret_data.append(point)
    ret_main = {
      'symbol': f_data['symbol'],
      'label': f_data['label'],
      'day_of_week': day_of_week,
      'penny_stock_count': f_data['penny_stock_count']/max_penny_stock_count,
      'data': ret_data,
    }

    # Save file
    with open(f'{OUTPUTDIR}/{file}', 'w') as f:
      json.dump(ret_main, f, indent=2)
        

if __name__ == '__main__':
  main()
