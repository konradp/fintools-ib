#!/usr/bin/env python3
# Suppress TensorFlow logging messages
import json
import os
import warnings
# Suppress INFO and WARNING messages
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# Suppress warnings
warnings.filterwarnings('ignore')
import sys
import numpy as np
from tensorflow.keras.models import load_model


def usage():
  print(f'Usage: {sys.argv[0]} DIR')
  print(f'Example: {sys.argv[0]} ~/konradp/fintools-ib/data/predict_data/20240627')


def main():
  if len(sys.argv) != 2:
    usage()
    exit(1)
  DIR = sys.argv[1]
  files = os.listdir(DIR)
  print(files)
  model = load_model('model.keras')

  predictions_arr = {}
  for file in files:
    print(file)
    with open(f'{DIR}/{file}', 'r') as f:
      data = json.load(f)

      p_values = np.array([ i['p'] for i in data['data'] ]).reshape(1, 78, 1)
      v_values = np.array([ i['v'] for i in data['data'] ]).reshape(1, 78, 1)
      penny_stock_count = np.array([ data['penny_stock_count'] ]).reshape(1, 1)
      day_of_week = np.array(data['day_of_week']).reshape(1, 5, 1)

      # Ensure the shapes are correct
      #print("p_values shape:", p_values.shape)
      #print("v_values shape:", v_values.shape)
      #print("penny_stock_count shape:", penny_stock_count.shape)
      #print("day_of_week shape:", day_of_week.shape)

      # Make predictions
      predictions = model.predict([p_values, v_values, penny_stock_count, day_of_week])
      predictions_arr[data['symbol']] = round(float(predictions[0][0]), 2)
      # Output the predictions
      #print("Predictions:", predictions)
  predictions_arr = {key: predictions_arr[key] for key in sorted(predictions_arr.keys())}

  print(json.dumps(predictions_arr, indent=2))
  #print(predictions_arr)
  for stock, value in predictions_arr.items():
    if value > 0.5:
      print(f'{stock}: {value}')


if __name__ == '__main__':
  main()
