#!/usr/bin/env python3
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api
from lib.ib import Ib
from lib.backend import Backend

print('Save secdefs')
api = Api()
backend = Backend()
symbols = api.get_symbols()
secdefs = api.fetch_secdefs(symbols)
for s in secdefs:
  backend.save_secdef(s)
