#!/usr/bin/env python3
# Save the given symbol to DB/cache
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.backend import Backend
from lib.api import Api

parser = argparse.ArgumentParser(
  description='Save symbol to cache/DB'
)
parser.add_argument(
  'symbol',
  metavar='SYMBOL',
  type=str,
  help='Symbol, e.g. AAPL'
)
args = parser.parse_args()
symbol = args.symbol

api = Api()
secdef = api.fetch_secdefs([symbol])[0]
print(secdef)
backend = Backend()
Backend().save_secdef(secdef)
print(f"Saved symbol {secdef['symbol']}")
