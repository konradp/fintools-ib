#!/usr/bin/env python3
# Save the given symbol to DB/cache
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
import argparse
import json
import os
from lib.backend import Backend

parser = argparse.ArgumentParser(
  description='Save symbol to cache/DB'
)
parser.add_argument(
  'symbol',
  metavar='SYMBOL',
  type=str,
  help='Symbol, e.g. AAPL'
)
args = parser.parse_args()
symbol = args.symbol
try:
  backend = Backend()
  Backend().save_symbol(symbol)
  print(f"Saved symbol {symbol}")
except Exception as e:
  print(f"Could not get conid: {e}")
  exit(1)
