#!/usr/bin/env python3
# Save (update) the given symbol+conid to DB/cache
# Note: Requires the symbol to exist already, and conid is NULL
# Run as:  ./$0 SYMBOL CONID
# Example: ./$0 XP 395194484
import argparse
import json
import os
from lib.backend import Backend

parser = argparse.ArgumentParser(
  description='Save symbol+conid to cache/DB'
)
parser.add_argument(
  'symbol',
  metavar='SYMBOL',
  type=str,
  help='Symbol, e.g. XP'
)
parser.add_argument(
  'conid',
  metavar='CONID',
  type=str,
  help='Conid, e.g. 395194484'
)
args = parser.parse_args()
symbol = args.symbol
conid = args.conid
Backend().save_conid(symbol, conid)
print(f"Saved conid: {symbol} {conid}")
