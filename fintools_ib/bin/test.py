#!/usr/bin/env python3
import ib_web_api
from ib_web_api.rest import ApiException

# create an instance of the API class
configuration = ib_web_api.Configuration()
configuration.verify_ssl = False


api_instance = ib_web_api.AccountApi(ib_web_api.ApiClient(configuration))


try:
  # Brokerage Accounts
  api_response = api_instance.iserver_accounts_get()
  pprint(api_response)
except ApiException as e:
  print("Exception when calling AccountApi->iserver_accounts_get: %s\n" % e)
