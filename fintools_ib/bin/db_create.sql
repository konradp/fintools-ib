CREATE TABLE FiveDay (
  symbol   VARCHAR(5)   NOT NULL,
  industry VARCHAR(50)  NOT NULL,
  t        TIMESTAMP    NOT NULL,
  o        NUMERIC(5,2),
  h        NUMERIC(5,2),
  l        NUMERIC(5,2),
  c        NUMERIC(5,2),
  v        NUMERIC(12,2),
  CONSTRAINT id PRIMARY KEY (industry, t)
);
