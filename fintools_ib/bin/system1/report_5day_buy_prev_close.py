#!/usr/bin/env python3
# Get cheap symbols, and their categories

import datetime
import json
import os
from statistics import mean, median
from tabulate import tabulate
from traceback import print_exc

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair

# Main
# Check params
if len(sys.argv) < 2:
  print('Provide dir')
  exit(2)
dir_path = sys.argv[1]

try:
  # Repad symbols data into per-date data:
  # raw_data = {
  #   'ANY': {
  #     '2020-12-24': [
  #         { t,o,h,l,c,v }, ...
  #     ],
  #     '2020-12-28': [ ... ]
  #   },
  print('Prepare data')
  raw_data = {}
  # Get symbols from dir
  symbols = [ s.strip('.json') for s in os.listdir(dir_path) ]
  print('Got %i symbols' % len(symbols))

  for symbol in symbols:
    # Split symbols by date
    raw_data[symbol] = {}
    f_path = dir_path + '/' + symbol + '.json'
    with open(f_path, 'r') as f:
      # Read file
      data = json.loads(f.read())
    print('%s %i' % (symbol, len(data)), end=' ')
    for bar in data:
      # Split data by date
      date = datetime.datetime.fromtimestamp(bar['t']/1000)
      day = str(date).split()[0]
      if day not in raw_data[symbol]:
        raw_data[symbol][day] = []
      raw_data[symbol][day].append(bar)
  print()
except Exception as e:
  print('ERROR: Could not split data:', e)
  print_exc()
  exit(1)

try:
  # Calculate stats
  print('Calculate stats')
  stats = {}
  for symbol, symbol_days in raw_data.items():
    # Symbol
    prev_close = 0
    stats[symbol] = {
      'per_day': {},
    }
    for day, day_points in symbol_days.items():
      # Day
      print(day)
      find_buy = True
      finished = False
      if prev_close == 0:
        # First day: skip, but get close price
        print('----------')
        prev_close = day_points[len(day_points)-1]['c']
        continue
      p_buy = prev_close
      # Work day data
      for p in day_points:
        if finished:
          continue
        if find_buy:
          if p['l'] <= p_buy:
            find_buy = False
            continue
        if not find_buy:
          perc = 100*(p['h']-p_buy)
          if perc > 4:
            p_sell = p['h']
            print('Found', p_buy, p_sell, perc)
            finished = True
            continue
      # Iterated day
      stats[symbol]['per_day'][day] = {
        'prev_close': prev_close,
        'found':finished,
      }
      prev_close = day_points[len(day_points)-1]['c']
except Exception as e:
  print('ERROR: Could not split data:', e)
  print_exc()
  exit(1)

print(json.dumps(stats, indent=2))

# Print report
print('Print report')
#print(json.dumps(report_data, indent=2))
headers = [ 'SYMB' ]
is_header = True
for symbol, data in stats.items():
  if is_header:
    # THEADER
    headers = list(stats[symbol]['per_day'].keys())
    headers.insert(0, 'SYMB')
    print(' | '.join(headers))
    is_header = False
    # DIVIDER
    dividers = [ '---' for i in headers ]
    print(' | '.join(dividers))
  #data['per_day'])
  d = [ str(data['per_day'][day]['found']) for day in data['per_day'] ]
  d.insert(0, symbol)
  print(' | '.join(d))
print()


# Per day chance of success
print('Per day chance of success')
headers = [ 'SYMB' ]
is_header = True
raw = {}
# raw = { 'd1': 30%, 'd2': 40%, ... }
len_symbols = len(stats)
print(len_symbols)
for symbol, data in stats.items():
  for day, info in data['per_day'].items():
    if day not in raw:
      raw[day] = 0
    raw[day] += info['found']
res = {}
for day, len_success in raw.items():
  res[day] = 100*(len_success/len_symbols)

# Print
days = [ d for d in res ]
perc = [ '%.2f' % perc for _, perc in res.items() ]
print(' | '.join(days))
print('        | '.join([ ':--' for i in days ]))
print('      | '.join(perc))


## Exclusions
## 
#reject = []
#for symbol, data in stats.items():
#  if data['vol_zero_perc'] >= 50:
#    reject.append(symbol)
#
## Remove duplicates
#reject = sorted(list(dict.fromkeys(reject)))
#
## Print
#print()
#print('Rejected %i symbols' % len(reject))
#print('Rejections')
#print(reject)
#print('Good symbols')
#symbols_good = sorted([ s for s in symbols if s not in reject ])
#print(symbols_good)
