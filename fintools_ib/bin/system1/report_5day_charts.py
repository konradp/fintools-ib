#!/usr/bin/env python3
# Generate charts for each day

import argparse
import datetime
import json
#import mplfinance as mpf
import os
from datetime import date
from traceback import print_exc

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair
from lib.util import data_to_panda


# Check params
if len(sys.argv) < 2:
  print('Provide dir, without the "data"')
  exit(2)
dir_root = sys.argv[1]


### MAIN ###
dir_data = '%s/data' % dir_root
dir_img = '%s/img' % dir_root

print('Create data dir')
if not os.path.exists(dir_data):
  os.makedirs(dir_data)
if not os.path.exists(dir_img):
  os.makedirs(dir_img)

try:
  # Repad symbols data into per-date data:
  # raw_data = {
  #   'ANY': {
  #     '2020-12-24': [
  #         { t,o,h,l,c,v }, ...
  #     ],
  #     '2020-12-28': [ ... ]
  #   },
  print('Prepare data')
  raw_data = {}
  # Get symbols from dir
  symbols = [ s.strip('.json') for s in os.listdir(dir_data) ]
  print('Got %i symbols' % len(symbols))

  for symbol in symbols:
    # Split symbols by date
    raw_data[symbol] = {}
    f_path = '%s/%s.json' % (dir_data, symbol)
    with open(f_path, 'r') as f:
      # Read file
      data = json.loads(f.read())
    print('%s %i' % (symbol, len(data)), end=' ')
    for bar in data:
      # Split data by date
      date = datetime.datetime.fromtimestamp(bar['t']/1000)
      day = str(date).split()[0]
      if day not in raw_data[symbol]:
        raw_data[symbol][day] = []
      raw_data[symbol][day].append(bar)
  print()
except Exception as e:
  print('ERROR: Could not split data:', e)
  print_exc()
  exit(1)

#try:
#  # Plot
#  print('Generate charts')
#  for symbol in raw_data:
#    print(symbol)
#    total_points = []
#    for date, points in raw_data[symbol].items():
#      total_points += points
#      points = data_to_panda(points)
#      mpf.plot(
#        points,
#        type='candle',
#        volume=True,
#        style='yahoo',
#        savefig='%s/%s_%s.png' % (dir_img, symbol, date)
#      )
#    # Total
#    total_points = data_to_panda(total_points)
#    mpf.plot(
#      total_points,
#      type='candle',
#      volume=True,
#      style='yahoo',
#      savefig='%s/%s_total.png' % (dir_img, symbol)
#    )
#except Exception as e:
#  print('ERROR: Could not plot data:', e)
#  print_exc()
#  exit(1)
