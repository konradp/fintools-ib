# System 1
This is the setup which relies on storing data on the filesystem/DB.  
The scripts have a prefix which hints at how they work:

- **down_**: Scripts which fetch data and store it in fs
- **report_**: Scripts which pull data from the fs and manipulate it to generate further insights
