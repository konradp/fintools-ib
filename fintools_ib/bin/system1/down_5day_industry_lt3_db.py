#!/usr/bin/env python3
# For industry (Software), get last 5 days data for each symbol (lt3)
# save to
# /opt/fintools-ib/data/
# /5days/Software/2021-01-05
# Return exitcode 1 if symbol is not NASDAQ or not in IB
# Run as:  ./0 INDUSTRY
# Example: ./0 Software

import argparse
import json
import mysql.connector
#import numpy as np
import os
import pprint
from datetime import date
from statistics import mean, median
from traceback import print_exc

# Local
from lib.company import Company
from lib.filters import get_contracts_cheaper_than

# Parse args
parser = argparse.ArgumentParser(description='Display last day data for symbol')
parser.add_argument('industry',
                    metavar='INDUSTRY', type=str,
                    help='Industry, e.g. Software')
parser.add_argument('-s',
                    dest='small',
                    action='store_true',
                    help='Print only result (category), without the symbol'
)
args = parser.parse_args()
industry = args.industry

### MAIN ###
# Connect to DB
db_connection = mysql.connector.connect(
  host='localhost',
  user='trade',
  passwd='Jik3iecaeca8',
  database='trade',
)
db_cursor = db_connection.cursor()

date = date.today()

print('Get symbols')
try:
  symbols = get_contracts_cheaper_than(3)
  symbols = sorted({
    symbol: info
    for symbol, info
    in symbols.items()
    if info['industry'] == industry
  })
  print('Got %i symbols' % len(symbols))
  print(symbols)
except Exception as e:
  print('ERROR: Could not get contracts:', e)
  print_exc()
  exit(1)

#print(json.dumps(symbols, indent=2))

print('Download data for each symbol')
for symbol in symbols:
  print('Get %s' % symbol)
  c = Company(symbol)
  quote = Company(symbol).get_quote(period='5d', bar='2min')
  print('INSERT')
  for i in quote:
    q = """
      REPLACE INTO FiveDay (symbol, t, industry, o, h, l, c, v)
      VALUES ('%s', FROM_UNIXTIME(%s), '%s', '%s', '%s', '%s', '%s', '%s')
    """ % (symbol, int(i['t']/1000), industry, i['o'], i['h'], i['l'], i['c'], i['v'])
    #Execute cursor and pass query as well as student data
    try:
      db_cursor.execute(q)
    except Exception as e:
      print_exc()
      print({
        't': int(i['t']),
        'industry': industry,
        'o': i['o'], 'h': i['h'], 'l': i['l'], 'c': i['c'], 'v': i['v']
      })
      exit(1)
  db_connection.commit()
  #print(json.dumps(day))
