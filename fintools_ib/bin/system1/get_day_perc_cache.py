#!/usr/bin/env python3
# For a given symbol, get the day percentage increase
# (highest achievable low to high)
# Note: Gets from cache

import argparse
import json
#import mplfinance as mpf
#import pandas as pd
import os
import datetime

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_winners_lt_perc
from lib.util import error, get_perc_stats, get_perc_best_pair


debug = False

# Parse args
parser = argparse.ArgumentParser(
    description='Display last day data for ticker')
parser.add_argument(
    'symbol',
    metavar='SYMBOL',
    type=str,
    help='Symbol, e.g. AACG')
args = parser.parse_args()
symbol = args.symbol

# Main
try:
  # Get symbol day data
  # ret: ?
  company = Company(symbol)
  data = company.disk_find('day')
except Exception as e:
  print('ERROR: Could not get day data:', e)
  exit(1)

#try:
#  # Reformat data
#  reformatted_data = dict()
#  reformatted_data['Date'] = []
#  reformatted_data['Open'] = []
#  reformatted_data['High'] = []
#  reformatted_data['Low'] = []
#  reformatted_data['Close'] = []
#  reformatted_data['Volume'] = []
#  for d in data:
#      reformatted_data['Date'].append(datetime.datetime.fromtimestamp(d['t']/1000))
#      reformatted_data['Open'].append(d['o'])
#      reformatted_data['High'].append(d['h'])
#      reformatted_data['Low'].append(d['l'])
#      reformatted_data['Close'].append(d['c'])
#      reformatted_data['Volume'].append(d['v'])
#  pdata = pd.DataFrame.from_dict(reformatted_data) 
#  pdata.set_index('Date', inplace=True)
#except Exception as e:
#  print('ERROR: Could not reformat day data:', e)
#  exit(1)
#
#def HA(df):
#    df['HA_Close']=(df['Open']+ df['High']+ df['Low']+df['Close'])/4
#
#    idx = df.index.name
#    df.reset_index(inplace=True)
#
#    for i in range(0, len(df)):
#        if i == 0:
#            df.at[i, 'HA_Open'] = (df.at[i, 'Open'] + df.at[i, 'Close']) / 2
#        else:
#            df.at[i, 'HA_Open'] = (df.at[i - 1, 'HA_Open'] + df.at[i - 1, 'HA_Close']) / 2
#
#    if idx:
#        df.set_index(idx, inplace=True)
#
#    df['HA_High']=df[['HA_Open','HA_Close','High']].max(axis=1)
#    df['HA_Low']=df[['HA_Open','HA_Close','Low']].min(axis=1)
#    # Reassign
#    df['Open'] = df['HA_Open']
#    df['High'] = df['HA_High']
#    df['Low'] = df['HA_Low']
#    df['Close'] = df['HA_Close']
#    return df
#
#
#pdata = HA(pdata)

#try:
#  # Plot data
#  mpf.plot(pdata, type='candle', volume=True, savefig=symbol+'.png', style='yahoo')
#except Exception as e:
#  print('ERROR: Could not plot day data:', e)
#  exit(1)


#try:
#  # Get perc increase
#get_perc_stats(data)
data = get_perc_best_pair(data)
print(json.dumps(data, indent=2))
#except Exception as e:
#  print('ERROR: Could not get day perc increase:', e)
#  exit(1)
