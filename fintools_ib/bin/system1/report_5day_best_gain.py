#!/usr/bin/env python3
# Get cheap symbols, and their categories

import datetime
import json
import os
from statistics import mean, median
from tabulate import tabulate

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair
from lib.util import load_json_file

# Main
debug = False
debug = True
# Check params
if len(sys.argv) < 2:
  print('Provide dir')
  exit(2)
dir_path = sys.argv[1]
sorting = False
if len(sys.argv) == 3:
  sorting = True
  sort_by = int(sys.argv[2])

try:
  # Get symbols
  if debug:
    print('Prepare data')
  report_data = {}
  symbols = [ s.strip('.json') for s in os.listdir(dir_path) ]
  print('Got %i symbols' % len(symbols))
  for symbol in symbols:
    report_data[symbol] = {}
    c = Company(symbol)
    f_path = '/opt/fintools-ib/data/5days/Software/2021-04-19/data/' + symbol + '.json'
    data = load_json_file(f_path)
    for bar in data:
      date = datetime.datetime.fromtimestamp(bar['t']/1000)
      #bar['t'] = str(date)
      day = str(date).split()[0]
      if day not in report_data[symbol]:
        report_data[symbol][day] = []
      report_data[symbol][day].append(bar)
      
    #best_pair = get_perc_best_pair(c.disk_find('day'))
    #print(json.dumps(best_pair, indent=2))
    #report_data[symbol] = best_pair
except Exception as e:
  print('ERROR: Could not get contracts:', e)
  exit(1)


#try:
#  # Generate JSON data
#  data = {}
#  print('Generate report data')
#  for symbol in symbols:
#    c = Company(symbol)
#    day = c.get_day_cache()
#    vol_non_zero = [ e['v'] for e in day if e['v'] != 0.0 ]
#    vol = [ e['v']  for e in day ]
#    days_info[symbol] = {
#      'price': symbols_contracts[symbol]['price'],
#      'vol_len': len(day),
#      'vol_non_zero_len': len(vol_non_zero),
#      'vol_avg': round(mean(vol), 2),
#      'vol_median': round(median(vol), 2),
#      'vol_median_non_zero': round(median(vol_non_zero), 2),
#      'vol_min': min(vol_non_zero),
#      'vol_max': max(vol),
#    }
#except Exception as e:
#  print('ERROR: Could not build report: %s' % e)
#  exit(1)

# Print report
#print(json.dumps(report_data, indent=2))
report = []
headers = [
  'SYMB',
  'd1',
  'd2',
  'd3',
  'd4',
  'd5',
]
for symbol in report_data:
  headers = [
    'SYMB',
  ]
  l = [
    symbol
  ]
  for day, d in report_data[symbol].items():
    headers.append(day)
    l.append(get_perc_best_pair(d)['perc'])
  report.append([
      l[0],
      "%0.2f" % l[1],
      "%0.2f" % l[2],
      "%0.2f" % l[3],
      "%0.2f" % l[4],
      "%0.2f" % l[5],
  ])
if sorting:
  sort_idx = sort_by
else:
  sort_idx = 0

if sort_idx == 0:
  report = sorted(report, key=lambda x: x[sort_idx], reverse=False)
else:
  report = sorted(report, key=lambda x: float(x[sort_idx]), reverse=False)

print(tabulate(report, headers=headers, numalign='right', floatfmt=".2f"))
print("""
Legend:
- SYMB: symbol
- d1: best perc increase on day 1
- d2, d3, d4, d5: like above
""")
