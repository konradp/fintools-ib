#!/usr/bin/env python3
# Get cheap symbols, and their categories

import datetime
import json
import os
import pandas as pd
from statistics import mean, median
from tabulate import tabulate

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair
from lib.util import plot_HA

# Main
# Check params
if len(sys.argv) < 2:
  print('Provide symbols')
  exit(2)
symbols = sys.argv[1].split()
print(symbols)

try:
  # Get symbols
  report_data = {}
  for symbol in symbols:
    report_data[symbol] = {}
    c = Company(symbol)
    f_path = '/opt/fintools-ib/data/day_5days/' + symbol + '.json'
    with open(f_path, 'r') as f:
      print('Read', symbol)
      data = json.loads(f.read())
    print('Got', len(data))
    for bar in data:
      date = datetime.datetime.fromtimestamp(bar['t']/1000)
      #bar['t'] = str(date)
      day = str(date).split()[0]
      if day not in report_data[symbol]:
        report_data[symbol][day] = []
      report_data[symbol][day].append(bar)
      
    #best_pair = get_perc_best_pair(c.disk_find('day'))
    #print(json.dumps(best_pair, indent=2))
    #report_data[symbol] = best_pair
except Exception as e:
  print('ERROR: Could not get contracts:', e)
  exit(1)



# Print report
#print(json.dumps(report_data, indent=2))
report = []
headers = [
  'SYMB',
  'd1',
  'd2',
  'd3',
  'd4',
  'd5',
]
for symbol in report_data:
  print(symbol)
  headers = [
    'SYMB',
  ]
  l = [
    symbol
  ]
  for day, info in report_data[symbol].items():
    try:
      # Reformat data
      reformatted_data = dict()
      reformatted_data['Date'] = []
      reformatted_data['Open'] = []
      reformatted_data['High'] = []
      reformatted_data['Low'] = []
      reformatted_data['Close'] = []
      reformatted_data['Volume'] = []
      for d in info:
        reformatted_data['Date'].append(datetime.datetime.fromtimestamp(d['t']/1000))
        reformatted_data['Open'].append(d['o'])
        reformatted_data['High'].append(d['h'])
        reformatted_data['Low'].append(d['l'])
        reformatted_data['Close'].append(d['c'])
        reformatted_data['Volume'].append(d['v'])
      pdata = pd.DataFrame.from_dict(reformatted_data)
      pdata.set_index('Date', inplace=True)
    except Exception as e:
      print('ERROR: Could not reformat day data:', e)
      exit(1)

    pdata = plot_HA(pdata)
    first = 16
    positive = []
    for i in range(first):
      o = pdata['Open'][i]
      c = pdata['Close'][i]
      positive.append(o<c)
    # Perc count
    perc_count = 100.0*sum(positive)/len(positive)
    pdata = pdata.head(first)

    headers.append(day)
    l.append(perc_count)
  headers.append('avg')
  headers.append('avg-1')
  report.append([
      l[0],
      "%0.2f" % l[1],
      "%0.2f" % l[2],
      "%0.2f" % l[3],
      "%0.2f" % l[4],
      "%0.2f" % l[5],
      "%0.2f" % mean([l[1],l[2],l[3],l[4],l[5]]),
      "%0.2f" % mean([l[1],l[2],l[3],l[4]]),
  ])
report = sorted(report, key=lambda x: x[0], reverse=False)
print(tabulate(report, headers=headers, numalign='right', floatfmt=".2f"))
print("""
Legend:
- SYMB: symbol
- d1: best perc increase on day 1
- d2, d3, d4, d5: like above
- avg: average percent of green candles at beginning of day
- avg-1: same as above, but not counting most recent day
""")
