#!/usr/bin/env python3
# Get cheap symbols, and their categories

import datetime
import json
import numpy
import os
from statistics import mean, median
from tabulate import tabulate
from traceback import print_exc

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair
from lib.util import split_by_date
from lib.util import load_json_file

# Config
debug = False

# Main
# Check params
if len(sys.argv) < 2:
  print('Provide dir')
  exit(2)
dir_path = sys.argv[1]

try:
  # Repad symbols data into per-date data:
  # raw_data = {
  #   'ANY': {
  #     '2020-12-24': [
  #         { t,o,h,l,c,v }, ...
  #     ],
  #     '2020-12-28': [ ... ]
  #   },
  print('Prepare data')
  raw_data = {}
  # Get symbols from dir
  symbols = [ s.strip('.json') for s in os.listdir(dir_path) ]
  print('Got %i symbols' % len(symbols))

  for symbol in symbols:
    # Split symbols by date
    data = load_json_file(dir_path + '/' + symbol + '.json')
    raw_data[symbol] = split_by_date(data)
  print()
except Exception as e:
  print('ERROR: Could not split data:', e)
  print_exc()
  exit(1)

try:
  # Get total data per symbol
  total_data = {}
  for symbol, symbol_days in raw_data.items():
    total_data[symbol] = []
    for day, day_points in symbol_days.items():
      total_data[symbol] += day_points
except Exception as e:
  print('ERROR: Could not get total data:', e)
  print_exc()
  exit(1)

stats = {}

try:
  # Get total stats
  for symbol, data in total_data.items():
    prices = [ (d['l']+d['h'])/2 for d in data ]
    m = mean(prices)
    std = numpy.std(prices)
    stats[symbol] = {
      'mean': m,
      'std_dev': std,
      'std_dev_perc': 100*(std/m)
    }
except Exception as e:
  print('ERROR: Could not get total stats:', e)
  print_exc()
  exit(1)

if debug:
  print(json.dumps(stats, indent=2))


# Print report
print('Print report')
#print(json.dumps(report_data, indent=2))
headers = [ 'SYMB' ]
is_header = True
for symbol in stats:
  fields = [ i for i in stats[symbol].keys() if i != 'per_day' ]
  #fields.remove('vol_min')
  if is_header:
    # THEADER
    headers = fields.copy()
    headers.insert(0, 'SYMB')
    print(' | '.join(headers))
    # DIVIDER
    dividers = [ '--:' for i in headers ]
    dividers[0] = '---'
    is_header = False
    print(' | '.join(dividers))
  # TBODY
  vals = [ '%.2f' % stats[symbol][field] for field in fields]
  vals.insert(0, symbol)
  print(' | '.join(vals))

print('Print winners report')
