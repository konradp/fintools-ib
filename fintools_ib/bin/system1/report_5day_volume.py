#!/usr/bin/env python3
# Get cheap symbols, and their categories

import datetime
import json
import os
from statistics import mean, median
from tabulate import tabulate
from traceback import print_exc

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair

# Main
# Check params
if len(sys.argv) < 2:
  print('Provide dir')
  exit(2)
dir_path = sys.argv[1]

try:
  # Repad symbols data into per-date data:
  # raw_data = {
  #   'ANY': {
  #     '2020-12-24': [
  #         { t,o,h,l,c,v }, ...
  #     ],
  #     '2020-12-28': [ ... ]
  #   },
  print('Prepare data')
  raw_data = {}
  # Get symbols from dir
  symbols = [ s.strip('.json') for s in os.listdir(dir_path) ]
  print('Got %i symbols' % len(symbols))

  for symbol in symbols:
    # Split symbols by date
    raw_data[symbol] = {}
    f_path = dir_path + '/' + symbol + '.json'
    with open(f_path, 'r') as f:
      # Read file
      data = json.loads(f.read())
    print('%s %i' % (symbol, len(data)), end=' ')
    for bar in data:
      # Split data by date
      date = datetime.datetime.fromtimestamp(bar['t']/1000)
      day = str(date).split()[0]
      if day not in raw_data[symbol]:
        raw_data[symbol][day] = []
      raw_data[symbol][day].append(bar)
  print()
except Exception as e:
  print('ERROR: Could not split data:', e)
  print_exc()
  exit(1)

try:
  # Calculate stats
  # COMMON_PROPERTIES: {
  #   'count': 0,
  #   'vol_avg': 0,
  #   'vol_min'
  #   'vol_max'
  #   'vol_median': 0,
  #   'vol_nonz_count': 0,
  #   'vol_nonz_avg': 0,
  #   'vol_nonz_median': 0,
  #   'vol_zero_count': 0,
  # }
  #
  # stats = {
  #   'ANY': {
  #     'per_day': {
  #       '2020-12-24': {
  #         COMMON_PROPERTIES,
  #       },
  #       '2020-12-28': {...}
  #     },
  #     COMMON_PROPERTIES,
  #     'vol_nonz_median': 0,
  #   }
  # }
  print('Calculate stats')
  stats = {}
  for symbol, symbol_days in raw_data.items():
    # Symbol
    stats[symbol] = {
      'per_day': {},
      'count': 0,        # Temp
      'vol': [],         # Temporary
      'vol_zero': [],    # Temporary
      'vol_nonz': [],    # Temporary
    }
    for day, day_points in symbol_days.items():
      # Day
      vol = [ p['v'] for p in day_points ]
      vol_zero = [ p for p in vol if p == 0 ]
      vol_nonz = [ p for p in vol if p != 0 ]
      stats[symbol]['per_day'][day] = {
        'count': len(vol),
        'vol_avg': mean(vol),
        'vol_min': min(vol),
        'vol_max': max(vol),
        'vol_median': median(vol),
        'vol_nonz_min': min(vol_nonz),
        'vol_nonz_count': len(vol_nonz),
        'vol_nonz_avg': mean(vol_nonz),
        'vol_nonz_median': median(vol_nonz),
        'vol_zero_count': len(vol_zero),
      }
      # Cumulative properties
      stats[symbol]['vol'] += vol
      stats[symbol]['vol_nonz'] += vol_nonz
      stats[symbol]['vol_zero'] += vol_zero
    # Symbol
    d = stats[symbol]
    stats[symbol]['count']           = len(d['vol'])
    stats[symbol]['vol_avg']         = mean(d['vol'])
    stats[symbol]['vol_min']         = min(d['vol'])
    stats[symbol]['vol_max']         = max(d['vol'])
    stats[symbol]['vol_median']      = median(d['vol'])
    stats[symbol]['vol_nonz_min']    = min(d['vol_nonz']),
    stats[symbol]['vol_nonz_count']  = len(d['vol_nonz'])
    stats[symbol]['vol_nonz_avg']    = mean(stats[symbol]['vol_nonz'])
    stats[symbol]['vol_nonz_median'] = median(stats[symbol]['vol_nonz'])
    stats[symbol]['vol_zero_count']  = len(d['vol_zero'])
    # Derivatives
    stats[symbol]['vol_zero_perc']  = 100*(len(d['vol_zero'])/len(d['vol']))
    # Clean tmp data
    del stats[symbol]['vol']
    del stats[symbol]['vol_nonz']
    del stats[symbol]['vol_zero']
except Exception as e:
  print('ERROR: Could not split data:', e)
  print_exc()
  exit(1)

print(json.dumps(stats, indent=2))

# Print report
print('Print report')
#print(json.dumps(report_data, indent=2))
headers = [ 'SYMB' ]
is_header = True
for symbol in stats:
  fields = [ i for i in stats[symbol].keys() if i != 'per_day' ]
  fields.remove('vol_min')
  if is_header:
    # THEADER
    headers = fields.copy()
    headers.insert(0, 'SYMB')
    print(' | '.join(headers))
    # DIVIDER
    dividers = [ '--:' for i in headers ]
    dividers[0] = '---'
    is_header = False
    print(' | '.join(dividers))
  # TBODY
  vals = [ '%.2f' % stats[symbol][field] for field in fields]
  vals.insert(0, symbol)
  print(' | '.join(vals))

# Exclusions
reject = []
for symbol, data in stats.items():
  if data['vol_zero_perc'] >= 50:
    reject.append(symbol)

# Remove duplicates
reject = sorted(list(dict.fromkeys(reject)))

# Print
print()
print('Rejected %i symbols' % len(reject))
print('Rejections')
print(reject)
print('Good symbols')
symbols_good = sorted([ s for s in symbols if s not in reject ])
print(symbols_good)
