#!/usr/bin/env python3
# Get cheap symbols, and their categories

import json
import os
from statistics import mean, median
from tabulate import tabulate

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than

# Main
# Check params
if len(sys.argv) < 2:
  print('Provide industry')
  exit(2)
industry = sys.argv[1]

try:
  # Get symbols
  print('Get cheaper than')
  contracts = get_contracts_cheaper_than(3)
  # price, category, industry
  symbols_contracts = {
    symbol: info
    for symbol, info
    in contracts.items()
    if info['industry'] == industry
  }
  symbols = symbols_contracts.keys()
  print('Got %i symbols' % len(symbols))
except Exception as e:
  print('ERROR: Could not get contracts:', e)
  exit(1)

try:
  # Generate JSON data
  days_info = {}
  print('Generate report data')
  for symbol in symbols:
    c = Company(symbol)
    day = c.get_day_cache()
    p_low = min([ e['l'] for e in day ])
    p_hi = max([ e['h'] for e in day ])
    o = day[0]['o']
    c = day[len(day)-1]['c']
    
    days_info[symbol] = {
      'price': symbols_contracts[symbol]['price'],
      'vol_len': len(day),
      'l': p_low,
      'h': p_hi,
      'lh_perc': 100*(p_hi-p_low)/p_low,
      'o': o,
      'c': c,
      'oc_perc': 100*(c-o)/o,
    }
except Exception as e:
  print('ERROR: Could not build report: %s' % e)
  exit(1)

#try:
# Print report
#print(json.dumps(days_info, indent=2))
report = []
headers = [ 'SYMB', 'P', 'l', 'h', 'lh_perc', 'o', 'c', 'oc_perc', ]
for symbol in symbols:
  d = days_info[symbol]
  report.append([
      symbol,
      d['price'],
      d['l'],
      d['h'],
      d['lh_perc'],
      d['o'],
      d['c'],
      d['oc_perc'],
  ])
report = sorted(report, key=lambda x: x[4], reverse=True)
print(tabulate(report, headers=headers, numalign='right', floatfmt=".2f"))
print("""
Legend:
- SYMB: symbol
- P: price
- l: low
- h: high
- lh_perc: lo to hi percentage difference
- o: open
- c: close
- oc_perc: open to close percentage change
""")
print('')
print('Symbols')
print(" ".join(symbols))
#except Exception as e:
#  print('ERROR: Could not print report: %s' % e)
#  exit(1)

