#!/usr/bin/env python3
# For industry (Software), get last 5 days data for each symbol (lt3)
# save to
# /opt/fintools-ib/data/
# /5days/Software/2021-01-05
# Return exitcode 1 if symbol is not NASDAQ or not in IB
# Run as:  ./0 INDUSTRY
# Example: ./0 Software

import argparse
import json
#import numpy as np
import os
from datetime import date
from statistics import mean, median
from traceback import print_exc

# Local
from lib.company import Company
from lib.filters import get_contracts_cheaper_than

# Config
dir_data = '/opt/fintools-ib/data/5days'

# Parse args
parser = argparse.ArgumentParser(
  description='Display last day data for symbol')
parser.add_argument('industry', metavar='INDUSTRY', type=str,
  help='Industry, e.g. Software')
parser.add_argument(
  '-s',
  dest='small',
  action='store_true',
  help='Print only result (category), without the symbol'
)
args = parser.parse_args()
industry = args.industry

### MAIN ###
print('Create data dir')
date = date.today()
dir_data = '%s/%s/%s/data' % (dir_data, industry, date)
if not os.path.exists(dir_data):
  os.makedirs(dir_data)
else:
  print('Data dir already exists:', dir_data)

print('Get symbols')
try:
  symbols = get_contracts_cheaper_than(3)
  symbols = sorted({
    symbol: info
    for symbol, info
    in symbols.items()
    if info['industry'] == industry
  })
  print('Got %i symbols' % len(symbols))
except Exception as e:
  print('ERROR: Could not get contracts:', e)
  print_exc()
  exit(1)

print('Download data for each symbol')
for symbol in symbols:
  print('Get %s' % symbol)
  day = Company(symbol).get_quote(period='5d', bar='2min')
  with open('%s/%s.json' % (dir_data, symbol), 'w') as f:
    f.write(json.dumps(day))
