#!/usr/bin/env python3
# For a given symbol, get the day percentage increase
# (highest achievable low to high)
# Note: Gets from cache

import argparse
import json
#import mplfinance as mpf
import os
import pandas as pd
import datetime

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_winners_lt_perc
from lib.util import error, get_perc_stats, get_perc_best_pair
from lib.util import plot_HA


debug = False

# Parse args
parser = argparse.ArgumentParser(
    description='Display last day data for ticker')
parser.add_argument(
    'symbol',
    metavar='SYMBOL',
    type=str,
    help='Symbol, e.g. AACG')
args = parser.parse_args()
symbol = args.symbol

# Main
try:
  # Get symbol day data
  # ret: ?
  company = Company(symbol)
  data = company.disk_find('day')
except Exception as e:
  print('ERROR: Could not get day data:', e)
  exit(1)

try:
  # Reformat data
  reformatted_data = dict()
  reformatted_data['Date'] = []
  reformatted_data['Open'] = []
  reformatted_data['High'] = []
  reformatted_data['Low'] = []
  reformatted_data['Close'] = []
  reformatted_data['Volume'] = []
  for d in data:
      reformatted_data['Date'].append(datetime.datetime.fromtimestamp(d['t']/1000))
      reformatted_data['Open'].append(d['o'])
      reformatted_data['High'].append(d['h'])
      reformatted_data['Low'].append(d['l'])
      reformatted_data['Close'].append(d['c'])
      reformatted_data['Volume'].append(d['v'])
  pdata = pd.DataFrame.from_dict(reformatted_data) 
  pdata.set_index('Date', inplace=True)
except Exception as e:
  print('ERROR: Could not reformat day data:', e)
  exit(1)

# Convert to Heikin Ashi
pdata = plot_HA(pdata)
#plot = False
plot = True

try:
  if plot:
    # Plot data
    print('Plot')
    #mpf.plot(pdata, type='candle', volume=True, savefig=symbol+'.png', style='yahoo')
    #mpf.plot(pdata, type='candle', volume=True, style='yahoo')
except Exception as e:
  print('ERROR: Could not plot day data:', e)
  exit(1)


#try:
# Get perc from count of green candles
first = 31
positive = []
for i in range(first):
  o = pdata['Open'][i]
  c = pdata['Close'][i]
  positive.append(o<c)
# Perc count
print('%.2f' % (100.0*sum(positive)/len(positive)))
#except Exception as e:
#  print('ERROR: Could not get day perc increase:', e)
#  exit(1)
