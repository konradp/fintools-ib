#!/usr/bin/env python3
# Get cheap symbols, and their categories

import json
import os
from statistics import mean, median
from tabulate import tabulate

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_contracts_cheaper_than
from lib.util import get_perc_best_pair

# Main
# Check params
if len(sys.argv) < 2:
  print('Provide symbols')
  exit(2)
symbols = sys.argv[1].split()
print(symbols)

try:
  # Get symbols
  report_data = {}
  for symbol in symbols:
    c = Company(symbol)
    best_pair = get_perc_best_pair(c.disk_find('day'))
    print(json.dumps(best_pair, indent=2))
    report_data[symbol] = best_pair
except Exception as e:
  print('ERROR: Could not get contracts:', e)
  exit(1)


#try:
#  # Generate JSON data
#  data = {}
#  print('Generate report data')
#  for symbol in symbols:
#    c = Company(symbol)
#    day = c.get_day_cache()
#    vol_non_zero = [ e['v'] for e in day if e['v'] != 0.0 ]
#    vol = [ e['v']  for e in day ]
#    days_info[symbol] = {
#      'price': symbols_contracts[symbol]['price'],
#      'vol_len': len(day),
#      'vol_non_zero_len': len(vol_non_zero),
#      'vol_avg': round(mean(vol), 2),
#      'vol_median': round(median(vol), 2),
#      'vol_median_non_zero': round(median(vol_non_zero), 2),
#      'vol_min': min(vol_non_zero),
#      'vol_max': max(vol),
#    }
#except Exception as e:
#  print('ERROR: Could not build report: %s' % e)
#  exit(1)

try:
  # Print report
  print(json.dumps(report_data, indent=2))
  report = []
  headers = [
    'SYMB',
    'tbuy',
    'tsell',
    'pbuy',
    'psell',
    'perc',
  ]
  for symbol, d in report_data.items():
    report.append([
        symbol,
        d['l']['d'],
        d['h']['d'],
        d['l']['price'],
        d['h']['price'],
        "%0.2f" % d['perc'],
    ])
  report = sorted(report, key=lambda x: float(x[5]), reverse=True)
  print(tabulate(report, headers=headers, numalign='right', floatfmt=".2f"))
  print("""
Legend:
- SYMB: symbol
- tbuy: buy time
- tsell: sell time
- pbuy: buy price
- psell: sell price
""")
except Exception as e:
  print('ERROR: Could not print report: %s' % e)
  exit(1)

