#!/usr/bin/env python3
# datefrom = '20240408'
# For a given date above, get 5 days of 5min price data
# for NASDAQ100 tickers. Write these to data/system2/DATE/0_5min_data
# It does:
# - / get nasdaq 100 symbols
# - fetch conids for IB
# - get 5 min chart for an entire week (5 days)
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))

from lib.api_fetch import ApiFetch
from lib.util import mkdir

api = None




def get_conids():
  # Return: [
  # {'ticker': 'ORIS', 'conid': 735671031, 'exchange': 'SCM'},
  # ... ]
  global api
  conids = api.fetch_conids()
  return conids


def get_nasdaq_100():
  # Return: [
  # {'symbol': 'AAPL', 'companyName': 'Apple PLC'},
  # ... ]
  global api
  symbols = api.fetch_nasdaq100()
  symbols = symbols['data']['data']['rows']
  symbols = [ {
    'symbol': i['symbol'],
    'companyName': i['companyName'],
  }
    for i in symbols
  ]
  #print(json.dumps(symbols, indent=2))
  return symbols


def main():
  global api
  api = ApiFetch()
  # Get nasdaq100 symbols
  print('Get NASDAQ 100 symbols')
  symbols = get_nasdaq_100()

  # Get conids
  conids = get_conids()
  tmp = []
  for s in symbols:
    symbol = s['symbol']
    conid = None
    for i in conids:
      if symbol == i['ticker']:
        conid = i['conid']
        company_name = s['companyName']
        break
    tmp.append({
      'symbol': symbol,
      'companyName': company_name,
      'conid': conid,
    })
  symbols = tmp
  print(json.dumps(symbols, indent=2))

  # Get 5 day 5min data for each
  datefrom = '20240408'
  print('Fetching history')
  dirdata = f"../../../data/system2/{datefrom}/0_5min_data"
  mkdir(dirdata)
  for s in symbols:
    symbol = s['symbol']
    conid = s['conid']
    print(symbol)
    history = api.fetch_history_for_conid(conid, period='5d', bar='5min', start=f'{datefrom}-00:00:00')
    # TODO: untested
    if 'data' not in history:
      print('WARN: No data for this symbol')
      continue
    with open(f"{dirdata}/{symbol}.json", "w") as f:
      f.write(json.dumps(history, indent=2))


if __name__ == '__main__':
  main()
