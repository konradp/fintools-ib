#!/usr/bin/env python3
# datefrom = '20240408'
# From a given date above, read the 5day 5min data
# for each day, find:
# - search for:
#   - buy at the high of the first 30mins
#   - sell anytime after gain >= 4%
import json
import os
import sys
from datetime import datetime, timezone
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))

from lib.api_fetch import ApiFetch
from lib.util import mkdir

api = None
datefrom = '20240408'
human_dates = True
PERC_GAIN = 3

def analyse_day(data):
  # - buy at the high of the first 30mins
  # - sell anytime after gain >= 4%
  # TODO: get the index of the first 30 mins: from starting point
  # or until certain time?
  # what if the data doesn't start from where it's supposed to
  # 13:30 - 14:00: first 30 minutes
  # TODO: We only care about the first match, the first 4% gain
  ret = {
    'gain': None,
    'max_gain': None,
    'buy': None,
    'sell': None,
  }
  times = ['13:30', '13:35', '13:40', '13:45', '13:50', '13:55', '14:00']
  # Buy
  first_30mins = [
    p for p in data
    if datetime.fromtimestamp(p['t'] / 1000, tz=timezone.utc).strftime('%H:%M')
    in times
  ]
  highs = [ p['h'] for p in first_30mins ]
  high = max(highs)
  for p in data:
    if ret['buy'] is not None:
      continue
    if p['h'] == high:
      ret['buy'] = p
  # Sell
  for p in data:
    time = datetime.fromtimestamp(p['t'] / 1000, tz=timezone.utc).strftime('%H:%M')
    if time in times:
      # Skip first 30 mins
      continue
    perc = 100*(p['c'] - high)/high
    #print(high, p['c'], perc)
    close = p['c']
    if perc >= PERC_GAIN:
      #print('MATCH:', p)
      if ret['sell'] is None:
        ret['sell'] = p
        ret['gain'] = perc
      if ret['max_gain'] is None or perc > ret['max_gain']:
        ret['max_gain'] = perc
  if ret['sell'] is None:
    return None
  # Convert to human dates
  # TODO
  if human_dates:
    ret['buy']['t'] = datetime.fromtimestamp(ret['buy']['t'] / 1000, tz=timezone.utc).strftime('%Y%m%d %H:%M')
    ret['sell']['t'] = datetime.fromtimestamp(ret['sell']['t'] / 1000, tz=timezone.utc).strftime('%Y%m%d %H:%M')
  return ret


def main():
  global datefrom
  dirdata = f"../../../data/system2/{datefrom}/0_5min_data"
  files = os.listdir(dirdata)
  ret = {}
  # ret = {
  #   '20240201': [
  #     { symbol: AAPL, gain: 4, max_gain: 5, buy: {...}, sell: {...} },
  #     ...
  #   ],
  #   ...
  #}
  for fname in files:
    symbol = fname.replace('.json', '')
    with open(f"{dirdata}/{fname}", 'r') as f:
      data_per_day = {}
      data = json.loads(f.read())['data']
      # divide into days
      for p in data:
        date = datetime.fromtimestamp(p['t'] / 1000, tz=timezone.utc).strftime('%Y%m%d')
        if date not in data_per_day:
          data_per_day[date] = []
        data_per_day[date].append(p)
      # For each date, analyse
      for date in data_per_day:
        if date not in ret:
          ret[date] = []
        gains = analyse_day(data_per_day[date])
        if gains is not None:
          gains['symbol'] = symbol
          ret[date].append(gains)
  print(json.dumps(ret, indent=2))


if __name__ == '__main__':
  main()
