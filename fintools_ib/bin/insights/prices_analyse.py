#!/usr/bin/env python3
import datetime
import json
import os
import matplotlib.pyplot as plt
import sys

DIR_DATA = '../../../data/0histories'
DATE = None
RES = {
  'count': 0,
  'date': None,
  'data': {},
}
STOCKS_IN_RANGE = {}


def usage():
  print(f'Usage: {os.path.basename(__file__)} MIN MAX')
  print(f'Example: {os.path.basename(__file__)} 100 1000')
  print(f'Example: {os.path.basename(__file__)} -1 -1')
  exit(1)


def to_datestr(timestamp):
  return datetime.datetime.fromtimestamp(timestamp/1000).strftime('%Y%m%d')


def main():
  MIN = float(sys.argv[1])
  MAX = float(sys.argv[2])
  with open('prices.json', 'r') as f:
    file = json.load(f)
    data = file['data']
    #data = data.values()
    tmp_data = []
    prices = data.values()
    avg = sum(prices)/len(prices)

    for symbol, price in data.items():
      if price >= MIN and price <= MAX:
        STOCKS_IN_RANGE[symbol] = price

    print(json.dumps(STOCKS_IN_RANGE, indent=2))
    print(f'average price: {avg}')
    print(f'min price: {min(prices)}')
    print(f'max price: {max(prices)}')
    print(f'count above average: {len([p for p in prices if p >= avg])}')
    print(f'count below average: {len([p for p in prices if p < avg])}')
    print(f'count between {MIN} and {MAX}: {len(STOCKS_IN_RANGE)}')


    exit(0)
    for symbol, price in data.items():
      if price > 100:
        print('symbol:', symbol, 'price:', price)
        continue
      tmp_data.append(price)
    #exit(0)
    values = tmp_data
    

    # Extracting keys and values
    #labels = list(data.keys())
    #values = list(data.values())

    # Creating the histogram
    #plt.figure(figsize=(8, 5))
    plt.hist(values, bins=10)

    # Adding titles and labels
    plt.title('Histogram of Price Distribution')
    plt.xlabel('value')
    plt.ylabel('frequency')

    # Displaying the histogram
    plt.show()


if __name__ == '__main__':
  main()
