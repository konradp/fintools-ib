#!/usr/bin/env python3
# Get year data for symbols whose price is less than $1
# Run as:  ./$0 SYMBOL
# Example: ./$0 AAPL
from datetime import datetime
import json
import os
import pprint
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

api = Api()
symbols = api.get_symbols_price_less_than(1)
ret = {}
for symbol in symbols:
  print(symbol)
  h = api.fetch_history_year_close(symbol)
  ret[symbol] = h
print(json.dumps(ret, indent=2))
now = datetime.now().strftime("%Y%m%d")
with open(f"data_365_lt1_{now}.json", 'w+') as f:
  f.write(json.dumps(ret, indent=2))
#pprint.pprint(ret)
