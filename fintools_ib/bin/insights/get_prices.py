#!/usr/bin/env python3
import datetime
import json
import os

DIR_DATA = '../../../data/0histories'
DATE = None
RES = {
  'count': 0,
  'date': None,
  'data': {},
}


def to_datestr(timestamp):
  return datetime.datetime.fromtimestamp(timestamp/1000).strftime('%Y%m%d')


def main():
  files = os.listdir(DIR_DATA)
  is_first = True
  for file in files:
    f_path = os.path.join(DIR_DATA, file)
    with open(f_path, 'r') as f:
      data = json.load(f)
      symbol = data['symbol']
      last_point = data['data'][-1]
      timestamp = last_point['t']
      price = last_point['c']
      date = to_datestr(timestamp)
      if is_first:
        # First iteration: the date is the main date
        # and all other stocks must be the same date
        print(date)
        RES['date'] = date
        is_first = False
      if date != RES['date']:
        print(f"WARN: date for {symbol} should be {RES['date']}, but is {date}")
        continue
      # Add to final result
      RES['data'][symbol] = price

  RES['count'] = len(RES['data'])
  RES['data'] = dict(sorted(RES['data'].items(), key=lambda item: item[1]))
  print(json.dumps(RES, indent=2))


if __name__ == '__main__':
  main()
