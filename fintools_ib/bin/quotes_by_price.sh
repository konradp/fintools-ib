#!/bin/bash
for FNAME in ../data/quotes/*; do
  SYMBOL=$(basename $FNAME)
  SYMBOL=${SYMBOL%%.*}
  PRICE=$(jq '.c' $FNAME)
  echo $SYMBOL $PRICE
done | sort > ../data/etoro_by_price.tsv
