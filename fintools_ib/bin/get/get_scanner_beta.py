#!/usr/bin/env python3
# Get NASDAQ conid for given ticker
# Return exitcode 1 if ticker is not NASDAQ or not in IB
# Run as:  ./get_conid_nasdaq.py TICKERS
# Example: ./get_conid_nasdaq.py AAPL

import ib_web_api
import json
import os
import pprint
import urllib3
from datetime import datetime
from ib_web_api import ScannerApi
from ib_web_api.rest import ApiException
from lib.config import Config
from lib.util import parse_post_data

api = ScannerApi(Config().Client())


res = api.hmds_scanner_post(
{
  "instrument": "STK",
  "scanCode": "TOP_PERC_GAIN",
  "secType": "STK",
  "filters": [
    {
      "code": "priceBelow",
      "value": 1,
    },
  ],
  "locations": "STK.US.MAJOR",
  "size": "10",
},
_preload_content=False,
)
res = parse_post_data(res)
print(json.dumps(res, indent=2))
