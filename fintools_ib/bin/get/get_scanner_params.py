#!/usr/bin/env python3
# Get scanner parameters

import ib_web_api
import json
from ib_web_api import ScannerApi
from lib.config import Config


api = ScannerApi(Config().Client())
res = api.iserver_scanner_params_get()
print(json.dumps(res.to_dict(), indent=2))
