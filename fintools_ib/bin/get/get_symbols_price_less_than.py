#!/usr/bin/env python3
# Get symbols in cache/DB
# Run as:  ./$0 PRICE
# Example: ./$0 1
import argparse
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api

parser = argparse.ArgumentParser(
  description='Get symbols for which price is less than PRICE'
)
parser.add_argument(
  'price',
  metavar='PRICE',
  type=int,
  help='Price, e.g. 1',
)
args = parser.parse_args()
price = args.price

api = Api()
symbols = api.get_symbols_price_less_than(price)
for symbol, price in symbols.items():
  print(symbol, price)
