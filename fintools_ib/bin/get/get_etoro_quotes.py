#!/usr/bin/env python3
import json
import os
# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.util import get_etoro_symbols


## MAIN
symbols = get_etoro_symbols()

for symbol in symbols:
  print(symbol)
  if symbol.startswith('A') \
    or symbol.startswith('B') \
    or symbol.startswith('C') \
    or symbol.startswith('D') \
    or symbol.startswith('E') \
    or symbol.startswith('F') \
    or symbol.startswith('G') \
    or symbol.startswith('H') \
    or symbol.startswith('I') \
    or symbol.startswith('J') \
    or symbol.startswith('K') \
    or symbol.startswith('L') \
    or symbol.startswith('M') \
    or symbol.startswith('N') \
    or symbol.startswith('O') \
    or symbol.startswith('P') \
    or symbol.startswith('Q') \
    or symbol.startswith('R') \
    or symbol.startswith('S') \
    or symbol.startswith('T') \
    or symbol.startswith('U') \
  :
    continue
  c = Company(symbol)
  quote = c.get_quote(period='3d', bar='1d')
  print(quote)
