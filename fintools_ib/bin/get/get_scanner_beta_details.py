#!/usr/bin/env python3
# Get NASDAQ conid for given ticker
# Return exitcode 1 if ticker is not NASDAQ or not in IB
# Run as:  ./get_conid_nasdaq.py TICKERS
# Example: ./get_conid_nasdaq.py AAPL

import ib_web_api
import json
import os
import pprint
import urllib3
from datetime import datetime
from ib_web_api import AccountApi, ContractApi, MarketDataApi, ScannerApi
from ib_web_api.rest import ApiException
from lib.config import Config
from lib.util import parse_post_data

api = ScannerApi(Config().Client())

res = api.hmds_scanner_post(
{
  "instrument": "STK",
  "scanCode": "TOP_PERC_GAIN",
  "secType": "STK",
  "filters": [
    {
      "code": "priceBelow",
      "value": 1,
    },
  ],
  "locations": "STK.US.MAJOR",
  "size": "10",
},
_preload_content=False,
)
res = parse_post_data(res)
#print(json.dumps(res, indent=2))
conids = [
  int(contract['contractID'])
  for contract in res['Contracts']['Contract']
]

api = ContractApi(Config().Client())
res = api.trsrv_secdef_post({
  'conids': conids,
})
#print(json.dumps(res, indent=2))
res = {
  i['conid']: {
    'ticker': i['ticker'],
    'name': i['name'],
    'group': i['group'],
    'sector': i['sector'],
    'sectorGroup': i['sectorGroup']
  }
  for i in res['secdef']
  if i['listingExchange'] == 'NASDAQ'
}
#print(json.dumps(res, indent=2))


#api = AccountApi(Config().Client())
#r = api.iserver_accounts_get()
#print(r)

api = MarketDataApi(Config().Client())
conids = [ str(conid) for conid in conids ]
print(len(conids))
conids = ','.join(conids)
detail = parse_post_data(api.md_snapshot_get(conids=conids, _preload_content=False))
print(len(detail))
print(json.dumps(detail, indent=2))


##conids = [ str(i) for i in conids ]
#for conid in res:
#  print('CCCC', conid)
#  detail = parse_post_data(api.hmds_history_get(conid=int(conid), period='1d', bar='1d', _preload_content=False))
#  perc = 100*detail['data'][0]['c']-detail['data'][0]['o']/detail['data'][0]['o']
#  res[conid]['perc'] = perc
#print(res)
#detail = api.md_snapshot_get(conids=','.join(conids), fields='fields=_82')
#print(detail)
#for conid in res:
#  print(conid)
