#!/usr/bin/env python3
# Get NASDAQ conid for given ticker
# Return exitcode 1 if ticker is not NASDAQ or not in IB
# Run as:  ./get_conid_nasdaq.py TICKERS
# Example: ./get_conid_nasdaq.py AAPL

import ib_web_api
import json
import os
import pprint
import urllib3
from datetime import datetime
from ib_web_api import AccountApi, ContractApi, MarketDataApi, ScannerApi
from ib_web_api.rest import ApiException
from lib.config import Config
from lib.util import parse_post_data


api = MarketDataApi(Config().Client())
conid = "560345283,460612924,497901061,470458970"
print('CCCC', conid)
detail = parse_post_data(api.md_snapshot_get(conids=conid, _preload_content=False))
print(json.dumps(detail, indent=2))
#detail = api.md_snapshot_get(conids=','.join(conids), fields='fields=_82')
#print(detail)
#for conid in res:
#  print(conid)
