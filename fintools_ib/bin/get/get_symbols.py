#!/usr/bin/env python3
# Get symbols in cache/DB
# Run as:  ./$0
import json
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.api import Api
api = Api()
symbols = api.get_symbols()
for symbol in symbols:
  print(symbol)
