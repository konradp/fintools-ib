#!/usr/bin/env python3
# Get quote for given symbol (from cache)
# Run as:  ./$0 SYMBOL
# Example: ./$0.py AAPL
import argparse
import json
import os
from lib.api import Api
from lib.util import db_to_json

parser = argparse.ArgumentParser(
  description='Get quote for symbol'
)
parser.add_argument(
  'symbol',
  metavar='SYMBOL',
  type=str,
  help='Symbol, e.g. AAPL'
)
args = parser.parse_args()
symbol = args.symbol
try:
  quote = Api().get_quote(symbol)
  print(db_to_json(quote))
except Exception as e:
  print(f"Could not get quote: {e}")
  exit(1)
