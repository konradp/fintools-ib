#!/usr/bin/env python3
# Get scanner parameters
# ./0 | jq -r '.contracts[].symbol'

import ib_web_api
import json
from ib_web_api import ScannerApi
from ib_web_api.rest import ApiException

from lib.config import Config
from lib.util import parse_post_data


api = ScannerApi(Config().Client())
res = api.iserver_scanner_run_post(
{
  "instrument": "STK",
  "type": "TOP_PERC_GAIN",
  "filter": [
    {
      "code": "priceBelow",
      "value": 1,
    },
  ],
  "location": "STK.US.MAJOR",
  "size": "10",
},
_preload_content=False,
)
res = parse_post_data(res)
print(json.dumps(res, indent=2))
