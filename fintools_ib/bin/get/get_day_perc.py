#!/usr/bin/env python3
# For a given symbol, get the day percentage increase
# (highest achievable low to high)
# Note: Gets from cache

import argparse
import json
#import mplfinance as mpf
#import pandas as pd
import os
import datetime

# Local
import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.company import Company
from lib.filters import get_winners_lt_perc
from lib.util import error, get_perc_stats, get_perc_best_pair


debug = False

# Parse args
parser = argparse.ArgumentParser(
    description='Display last day data for ticker')
parser.add_argument(
    'symbol',
    metavar='SYMBOL',
    type=str,
    help='Symbol, e.g. AACG')
args = parser.parse_args()
symbol = args.symbol

# Main
try:
  # Get symbol day data
  data = Company(symbol).get_quote('1d', '5min')
except Exception as e:
  print('ERROR: Could not get day data:', e)
  exit(1)

data = get_perc_best_pair(data)
print(json.dumps(data, indent=2))
