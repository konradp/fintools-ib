#!/usr/bin/env python3
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
from lib.util import pad_left

# Test padding
print('Test padding')
new_size = 8
lst = [ 1, 2, 3, 4, 5 ]
new = pad_left(lst, new_size)
print(f"List size before: {len(lst)}, size after: {len(new)}")
print('New list:', new)
if len(new) != new_size:
  print('ERROR')
