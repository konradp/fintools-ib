# db

## Database structure
The database is created by the db-client container, using the init_db.py script.

db name: fib

Tables and fields:

symbols:
  - symbol, e.g. AAPL
  - conid, e.g. 1234

contracts:
  - symbol, e.g. AAPL
  - sector, 10 of them:
    - Basic Materials
    - Communications
    - Consumer, Cyclical
    - Consumer, Non-cyclical
    - Diversified
    - Energy
    - Financial
    - Industrial
    - Technology
    - Utilities
  - industry: 58 of them, e.g. Advertising, Aerospace/Defense, Agriculture, Airlines
  - category: 206 of them, e.g. Advertising Services, Aerospace/Defense, Agricultural Operations, Airlines

snapshots:
  - symbol, e.g. AAPL: 55
  - v: Volume: 87: TODO, values can be e.g. 15K
  - price_last: 31
  - price_change: Change Price: 82
  - price_change_perc: Change Percent: 83
  - market_cap: Market Cap: 7289, e.g. 29.372B, TODO
  - t: date

## TODO
- snapshots: volume can be a string e.g. 15.1K
- snapshots: market_cap can be a string, e.g. 29.372B
