# fintools-ib
Table of contents:
- [fintools-ib](#fintools-ib)
   - [Run](#run)
   - [Test](#test)
   - [Usage](#usage)
      - [Get etoro symbols](#get-etoro-symbols)
   - [Other](#other)
      - [docker: Tidy](#docker-tidy)
      - [bin](#bin)
      - [Removing symbol](#removing-symbol)

<div align="center">
![fintools-ib](other/20240401-JAGX.png "fintools-ib")
</div>

## Run
The entire app with all its components can be run as Docker containers via the docker-compose.yml.
```
dc build
dc up
```

Go to https://localhost:5000/ to sign into IB gateway.  
Grafana: http://localhost:8001/

## Test
Test by connecting to the container called 'bin' and running the test script.
```
dsh bin
./test_auth_docker.sh
```
**Note:** The `dsh` is my alias for `docker exec -it CONTAINER_NAME sh`.

## Usage
### Get etoro symbols
- Use instead `./fetch_etoro_symbols.py`
- TODO: Combine this with download-snapshots.sh, maybe
- Right-click, copy object, paste into data/etoro_symbols.json. There should be 875 symbols.
- Import into the DB/cache:
  ```
  dsh bin
  cd ../cron
  ./import-etoro-symbols.py
  ./download-snapshots.sh
  ```
Note: 1583 symbols, skips 84, reduces to 1503.  
Symbols for which can't fetch snapshot: CLNE, HOLX (temp), NBIX

## Other
### docker: Tidy
Some useful commands for when docker eats too much disk space.
```
docker system prune --force --volumes
dc rm -v data
```

### bin
```
pip3 install -r bin/requirements.py
```

### Removing symbol
e.g. if symbol was delisted
```
vi data/etoro_symbols_skip.json
```

clean DB
```
./cron/delete_symbol.py AAPL
```
