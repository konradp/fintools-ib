#!/usr/bin/env python3
import json

with open('nasdaq100.json', 'r') as file:
  data = json.load(file)
symbols = data['data']['data']['rows']
symbols = [
  s['symbol']
  for s in symbols
]
with open('edgar_company_tickers_exchange.json', 'r') as file:
  data = json.load(file)
edgar = data['data']
ret = {}
for s in edgar:
  cik = s[0]
  symbol = s[2]
  exchange = s[3]
  if exchange == 'Nasdaq' \
    and symbol in symbols:
      ret[symbol] = cik
print(json.dumps(ret, indent=2))
