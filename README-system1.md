# Quickstart
```
# Daily
cron/data-clean.py

# MANUAL: Weekly: 1min, 42s
# Note: Need to run this manually due to connection drops
./download-conids.py

# Weekly: 45min
# quotes: latest price
# runs long time, for everything!
./download-quotes.py

# Daily: 1m10s
bin/down_5day_industry_lt3.py Software
```

# Quick urls
These are for my own use.

- dashboard: http://192.168.0.100:8080: `systemctl start fintools-ib-dashboard`
- API: http://192.168.0.100/, service fintools-ib, logs in /var/log/uwsgi. flask + uwsgi + nginx
- IB-gw service: `cd /opt/ib-gw; ./bin/run.sh root/conf.yaml >> /var/log/ib-gw.log &` <- port 5000

Useful urls
- http://192.168.0.100/health
- http://192.168.0.100/lt/3


# Ansible deployment

Run the playbook
```
cd ansible
ansible-playbook install-ib.yml
```

# Operation
Consists of:

- **download-conids-quotes.py**: download symbols, conids, quotes (3679 of them)
    - conids: fast to download
    - quotes: for today's price: quotes should be previous day price, generate them after market close
- **download-day.py**: for cheap tickers (defined in config, e.g. less than $3/share, download today's data/prices)


## dashboard
http://fintools-ib:8080/
```
service fintools-ib-dashboard status
```

## download-conids-quotes.py
Run this once a day or less.  

It does:
- get NASDAQ tickers
- download conids: convert tickers to conids
- download quotes

Conids are saved to:

- /opt/fintools-ib/data/conids

The conids are what IB uses instead of tickers, such as AAPL.

## download-day.py
?

# IB
Check service running, or run it
TODO: Make this a service.
```
cd /opt/ib-gw
./bin/run.sh root/conf.yaml >> /var/log/ib-gw.log &
```

# fintools-ib: App (API)
```
curl http://app-addr:80/lt/10 | jq '.'
```
